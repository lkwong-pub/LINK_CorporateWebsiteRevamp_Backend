/**
 *
 * @api {get} http://www-dev.linkhk.com/linkweb/api/leasingInfo/      Leasing Info Landing
 *
 * @apiName Leasing Info Landing
 * @apiGroup Batch3_2-LeasingInfo
 * @apiVersion 1.0.0
 *
 @apiDescription
 Retrieve Leasing Info Landing Page content<br/>
 Data included 7 components content:<br/>
 1. SEO Details<br/>
 2. Banner Details<br/>
 3. Tenants Hero Highlight<br/>
 4. Leasing Info Description<br/>
 5. Area Details<br/>
 6. District Details<br/>
 7. Leasing Info Details<br/>

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.banner		Leasing Info - Banner Object
 @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
 @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
 @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
 @apiSuccess (Success 200) {Object[]}	    data.tenantsHero	Leasing Info - Tenant Hero Object Array
 @apiSuccess (Success 200) {Integer}	    data.tenantsHero.id					Tenants Hero ID
 @apiSuccess (Success 200) {String}	    data.tenantsHero.titleEn	Tenants Hero Title (EN)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.titleTc	Tenants Hero Title (TC)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.titleSc	Tenants Hero Title (SC)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.dateEn		Tenants Hero Date (EN)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.dateTc		Tenants Hero Date (TC)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.dateSc		Tenants Hero Date (SC)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.imageUrlEn		Tenants Hero Image URL (EN)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.imageUrlTc		Tenants Hero Image URL (TC)
 @apiSuccess (Success 200) {String}	    data.tenantsHero.imageUrlSc		Tenants Hero Image URL (SC)
 @apiSuccess (Success 200) {Object}	    data.description 	Leasing Info - Description
 @apiSuccess (Success 200) {String}	    data.description.description1En		Description 1 HTML (EN)
 @apiSuccess (Success 200) {String}	    data.description.description1Tc		Description 1 HTML (TC)
 @apiSuccess (Success 200) {String}	    data.description.description1Sc		Description 1 HTML (SC)
 @apiSuccess (Success 200) {String}	    data.description.description2En		Description 2 HTML (EN)
 @apiSuccess (Success 200) {String}	    data.description.description2Tc		Description 2 HTML (TC)
 @apiSuccess (Success 200) {String}	    data.description.description2Sc		Description 2 HTML (SC)
 @apiSuccess (Success 200) {String}	    data.description.description3En		Description 3 HTML (EN)
 @apiSuccess (Success 200) {String}	    data.description.description3Tc		Description 3 HTML (TC)
 @apiSuccess (Success 200) {String}	    data.description.description3Sc		Description 3 HTML (SC)
 @apiSuccess (Success 200) {String}	    data.description.description4En		Description 4 HTML (EN)
 @apiSuccess (Success 200) {String}	    data.description.description4Tc		Description 4 HTML (TC)
 @apiSuccess (Success 200) {String}	    data.description.description4Sc		Description 4 HTML (SC)
 @apiSuccess (Success 200) {String}	    data.description.description5En		Description 5 HTML (EN)
 @apiSuccess (Success 200) {String}	    data.description.description5Tc		Description 5 HTML (TC)
 @apiSuccess (Success 200) {String}	    data.description.description5Sc		Description 5 HTML (SC)
 @apiSuccess (Success 200) {String}	    data.description.medicalTable1IntroEn      Medical Table Introduction 1 (EN)
 @apiSuccess (Success 200) {String}	    data.description.medicalTable1IntroTc      Medical Table Introduction 1 (TC)
 @apiSuccess (Success 200) {String}	    data.description.medicalTable1IntroSc      Medical Table Introduction 1 (SC)
 @apiSuccess (Success 200) {String}	    data.description.medicalTable2IntroEn      Medical Table Introduction 2 (EN)
 @apiSuccess (Success 200) {String}	    data.description.medicalTable2IntroTc      Medical Table Introduction 2 (TC)
 @apiSuccess (Success 200) {String}	    data.description.medicalTable2IntroSc      Medical Table Introduction 2 (SC)
 @apiSuccess (Success 200) {String}	    data.description.bankTable1IntroEn      Bank & ATM Table Introduction 1 (EN)
 @apiSuccess (Success 200) {String}	    data.description.bankTable1IntroTc      Bank & ATM Table Introduction 1 (TC)
 @apiSuccess (Success 200) {String}	    data.description.bankTable1IntroSc      Bank & ATM Table Introduction 1 (SC)
 @apiSuccess (Success 200) {String}	    data.description.bankTable2IntroEn      Bank & ATM Table Introduction 2 (EN)
 @apiSuccess (Success 200) {String}	    data.description.bankTable2IntroTc      Bank & ATM Table Introduction 2 (TC)
 @apiSuccess (Success 200) {String}	    data.description.bankTable2IntroSc      Bank & ATM Table Introduction 2 (SC)
 @apiSuccess (Success 200) {String}	    data.numberOfMedicalTable      Leasing Info - Number Of Table For Medical
 @apiSuccess (Success 200) {Object[]}	    data.areaInfo		Leasing Info - Area Info
 @apiSuccess (Success 200) {String}	    data.areaInfo.areaCode      Area Code
 @apiSuccess (Success 200) {String}	    data.areaInfo.areaNameEn    Area Name (EN)
 @apiSuccess (Success 200) {String}	    data.areaInfo.areaNameTc    Area Name (TC)
 @apiSuccess (Success 200) {String}	    data.areaInfo.areaNameSc    Area Name (SC)
 @apiSuccess (Success 200) {Object[]}	    data.districtInfo		Leasing Info - District Info
 @apiSuccess (Success 200) {Integer} 	data.districtInfo.districtId		District ID
 @apiSuccess (Success 200) {String} 	data.districtInfo.areaCode		District Area Code
 @apiSuccess (Success 200) {String} 	data.districtInfo.districtNameEn		District Name (EN)
 @apiSuccess (Success 200) {String} 	data.districtInfo.districtNameTc		District Name (TC)
 @apiSuccess (Success 200) {String} 	data.districtInfo.districtNameSc		District Name (SC)
 @apiSuccess (Success 200) {Object[]}	    data.medicalList1    Leasing Info - Leasing Info 1 Object Array
 @apiSuccess (Success 200) {Integer}	    data.medicalList1.id    Leasing Info ID
 @apiSuccess (Success 200) {String}	    data.medicalList1.areaCode   Leasing Info AreaCode
 @apiSuccess (Success 200) {Integer}	    data.medicalList1.districtId Leasing Info District ID
 @apiSuccess (Success 200) {Integer}	    data.medicalList1.shopCentreId Leasing Info Shop Centre ID
 @apiSuccess (Success 200) {String}	    data.medicalList1.propCode Leasing Info Shop Centre Prop Code
 @apiSuccess (Success 200) {String}	    data.medicalList1.letter    Letter of the alphabet
 @apiSuccess (Success 200) {String}	    data.medicalList1.tradeNatureEn    Leasing Info Title (EN)
 @apiSuccess (Success 200) {String}	    data.medicalList1.tradeNatureTc    Leasing Info Title (TC)
 @apiSuccess (Success 200) {String}	    data.medicalList1.tradeNatureSc    Leasing Info Title (SC)
 @apiSuccess (Success 200) {String}	    data.medicalList1.shopCentreNameEn    Shop Centre Name (EN)
 @apiSuccess (Success 200) {String}	    data.medicalList1.shopCentreNameTc    Shop Centre Name (TC)
 @apiSuccess (Success 200) {String}	    data.medicalList1.shopCentreNameSc    Shop Centre Name (SC)
 @apiSuccess (Success 200) {Object[]}	    data.medicalList2    Leasing Info - Leasing Info 2 Object Array
 @apiSuccess (Success 200) {Integer}	    data.medicalList2.id    Leasing Info ID
 @apiSuccess (Success 200) {String}	    data.medicalList2.areaCode   Leasing Info AreaCode
 @apiSuccess (Success 200) {Integer}	    data.medicalList2.districtId Leasing Info District ID
 @apiSuccess (Success 200) {Integer}	    data.medicalList2.shopCentreId Leasing Info Shop Centre ID
 @apiSuccess (Success 200) {String}	    data.medicalList2.propCode Leasing Info Shop Centre Prop Code
 @apiSuccess (Success 200) {String}	    data.medicalList2.letter    Letter of the alphabet
 @apiSuccess (Success 200) {String}	    data.medicalList2.tradeNatureEn    Leasing Info Title (EN)
 @apiSuccess (Success 200) {String}	    data.medicalList2.tradeNatureTc    Leasing Info Title (TC)
 @apiSuccess (Success 200) {String}	    data.medicalList2.tradeNatureSc    Leasing Info Title (SC)
 @apiSuccess (Success 200) {String}	    data.medicalList2.shopCentreNameEn    Shop Centre Name (EN)
 @apiSuccess (Success 200) {String}	    data.medicalList2.shopCentreNameTc    Shop Centre Name (TC)
 @apiSuccess (Success 200) {String}	    data.medicalList2.shopCentreNameSc    Shop Centre Name (SC)
 @apiSuccess (Success 200) {Object[]}	    data.bankingList    Leasing Info - Banking Object Array
 @apiSuccess (Success 200) {Integer}	    data.bankingList.id    Leasing Info ID
 @apiSuccess (Success 200) {String}	    data.bankingList.areaCode   Leasing Info AreaCode
 @apiSuccess (Success 200) {Integer}	    data.bankingList.districtId Leasing Info District ID
 @apiSuccess (Success 200) {Integer}	    data.bankingList.shopCentreId Leasing Info Shop Centre ID
 @apiSuccess (Success 200) {String}	    data.bankingList.propCode Leasing Info Shop Centre Prop Code
 @apiSuccess (Success 200) {String}	    data.bankingList.letter    Letter of the alphabet
 @apiSuccess (Success 200) {String}	    data.bankingList.tradeNatureEn    Leasing Info Title (EN)
 @apiSuccess (Success 200) {String}	    data.bankingList.tradeNatureTc    Leasing Info Title (TC)
 @apiSuccess (Success 200) {String}	    data.bankingList.tradeNatureSc    Leasing Info Title (SC)
 @apiSuccess (Success 200) {String}	    data.bankingList.shopCentreNameEn    Shop Centre Name (EN)
 @apiSuccess (Success 200) {String}	    data.bankingList.shopCentreNameTc    Shop Centre Name (TC)
 @apiSuccess (Success 200) {String}	    data.bankingList.shopCentreNameSc    Shop Centre Name (SC)

 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::LEASING_INFO_LIST_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Leasing Info Listing",
      "seoTitleTc":"Leasing Info Listing",
      "seoTitleSc":"Leasing Info Listing",
      "seoKeywordEn":"Leasing Info Listing",
      "seoKeywordTc":"Leasing Info Listing",
      "seoKeywordSc":"Leasing Info Listing",
      "seoDescriptionEn":"Leasing Info Listing",
      "seoDescriptionTc":"Leasing Info Listing",
      "seoDescriptionSc":"Leasing Info Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "tenantsHero":{
         "titleEn":"Tenants Hero",
         "titleTc":"Tenants Hero (TC)",
         "titleSc":"Tenants Hero (SC)",
         "dateEn":"08 Feb 2012 (Thu)",
         "dateTc":"08 Feb 2012 (Thu)",
         "dateSc":"08 Feb 2012 (Thu)",
         "imageUrlEn":"http://yahoo.com/abc.png",
         "imageUrlTc":"http://yahoo.com/abc.png",
         "imageUrlSc":"http://yahoo.com/abc.png"
      },
      "description":{
         "description1En":"description1En",
         "description1Tc":"description1Tc",
         "description1Sc":"description1Sc",
         "description2En":"description2En",
         "description2Tc":"description2Tc",
         "description2Sc":"description2Sc",
         "description3En":"description3En",
         "description3Tc":"description3Tc",
         "description3Sc":"description3Sc",
         "description4En":"description4En",
         "description4Tc":"description4Tc",
         "description4Sc":"description4Sc",
         "description5En":"description5En",
         "description5Tc":"description5Tc",
         "description5Sc":"description5Sc",
         "medicalTable1IntroEn":"",
         "medicalTable1IntroTc":"",
         "medicalTable1IntroSc":"",
         "medicalTable2IntroEn":"",
         "medicalTable2IntroTc":"",
         "medicalTable2IntroSc":"",
         "bankTable1IntroEn":"",
         "bankTable1IntroTc":"",
         "bankTable1IntroSc":"",
         "bankTable2IntroEn":"",
         "bankTable2IntroTc":"",
         "bankTable2IntroSc":""
      },
      "noOfMedicalTable":2,
      "areaInfo":[
         {
            "areaCode":"",
            "areaNameEn":"",
            "areaNameTc":"",
            "areaNameSc":""
         }
      ],
      "districtInfo":[
         {
            "districtId":1,
            "areaCode":"NT",
            "districtNameEn":"Tai Po",
            "districtNameTc":"大埔",
            "districtNameSc":"大埔"
         }
      ],
      "medicalList1":[
         {
            "id":1,
            "areaCode":"NT",
            "districtId":1,
            "shopCentreId" : 1,
            "propCode" : "wttxc2",
            "letter":"a",
            "tradeNatureEn":"",
            "tradeNatureTc":"",
            "tradeNatureSc":"",
            "shopCentreNameEn":"",
            "shopCentreNameTc":"",
            "shopCentreNameSc":""
         }
      ],
      "medicalList2":[
         {
            "id":1,
            "areaCode":"NT",
            "districtId":1,
            "shopCentreId" : 1,
            "propCode" : "wttxc2",
            "letter":"a",
            "tradeNatureEn":"",
            "tradeNatureTc":"",
            "tradeNatureSc":"",
            "shopCentreNameEn":"",
            "shopCentreNameTc":"",
            "shopCentreNameSc":""
         }
      ],
      "bankingList":[
         {
            "id":1,
            "areaCode":"NT",
            "districtId":1,
            "shopCentreId" : 1,
            "propCode" : "wttxc2",
            "letter":"a",
            "tradeNatureEn":"",
            "tradeNatureTc":"",
            "tradeNatureSc":"",
            "shopCentreNameEn":"",
            "shopCentreNameTc":"",
            "shopCentreNameSc":""
         }
      ]
   }
}
::LEASING_INFO_LIST_API_END::
*/

/**

 @api {get} http://www-dev.linkhk.com/linkweb/api/leasingInfo/{propCode}      Shop Centre Details

 @apiName Shop Centre Details
 @apiGroup Batch3_2-LeasingInfo
 @apiVersion 1.0.0

 @apiDescription
 Retrieve Promotion Details Page content<br/>
 Data included 5 components content:<br/>
 1. SEO Details<br/>
 2. Shop Centre Info<br/>
 3. Leasing Info Label<br/>
 4. Leasing Info Details<br/>

 @apiParam {String}	propCode		Shop Centre Prop Code

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.shopCentreInfo		Leasing Info - Shop Centre Info
 @apiSuccess (Success 200) {Integer} 	data.shopCentreInfo.shopCentreId    Shop Centre ID
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.headlineEn	Section Headline (EN)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.headlineTc	Section Headline (TC)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.headlineSc	Section Headline (SC)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.shopCentreNameEn	Shop Centre Namme (EN)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.shopCentreNameTc	Shop Centre Namme (TC)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.shopCentreNameSc	Shop Centre Namme (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressLabelEn	Address Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressLabelTc	Address Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressLabelSc	Address Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressEn		Address (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressTc		Address (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressSc		Address (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLineLabelEn	Customer Hotline Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLineLabelTc	Customer Hotline Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLineLabelSc	Customer Hotline Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLine		Customer Hotline
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiryLabelEn		Carpark Enquiry Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiryLabelTc		Carpark Enquiry Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiryLabelSc		Carpark Enquiry Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiry		Carpark Enquiry
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiryLabelEn		Leasing Enquiry Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiryLabelTc		Leasing Enquiry Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiryLabelSc		Leasing Enquiry Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiry	Leasing Enquiry
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.viewLocationThumbnailUrlEn View Location Icon (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.viewLocationThumbnailUrlTc View Location Icon (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.viewLocationThumbnailUrlSc View Location Icon (SC)
 @apiSuccess (Success 200) {Object[]}	data.shopCentreInfo.mallThumbnails	Shop Centre Thumbnail Object Array
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnail	Shop Centre Thumbnail URL
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnailAltTextEn		Shop Centre Thumbnail Alt Text (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnailAltTextTc		Shop Centre Thumbnail Alt Text (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnailAltTextSc		Shop Centre Thumbnail Alt Text (SC)
 @apiSuccess (Success 200) {Object}	    data.label       Leasing Info - Details Label
 @apiSuccess (Success 200) {String}	    data.label.locationLabelEn     Location Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.locationLabelTc     Location Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.locationLabelSc     Location Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.sizeLabelEn     Size Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.sizeLabelTc     Size Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.sizeLabelSc     Size Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.tradeNatureLabelEn     Trade Nature Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.tradeNatureLabelTc     Trade Nature Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.tradeNatureLabelSc     Trade Nature Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.managementFeeLabelEn     Management Fee Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.managementFeeLabelTc     Management Fee Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.managementFeeLabelSc     Management Fee Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.acFeeLabelEn     A/C Fee Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.acFeeLabelTc     A/C Fee Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.acFeeLabelSc     A/C Fee Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.availableDateLabelEn     Available Date Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.availableDateLabelTc     Available Date Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.availableDateLabelSc     Available Date Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.yearOfIntakeLabelEn     Year Of Intake Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.yearOfIntakeLabelTc     Year Of Intake Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.yearOfIntakeLabelSc     Year Of Intake Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.noOfClinicLabelEn     Number Of Existing Clinics Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.noOfClinicLabelTc     Number Of Existing Clinics Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.noOfClinicLabelSc     Number Of Existing Clinics Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.contactPersonLabelEn     Contact Person Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.contactPersonLabelTc     Contact Person Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.contactPersonLabelSc     Contact Person Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.floorPlanIconEn     Floor Plan Icon URL (EN)
 @apiSuccess (Success 200) {String}	    data.label.floorPlanIconTc     Floor Plan Icon URL (TC)
 @apiSuccess (Success 200) {String}	    data.label.floorPlanIconSc     Floor Plan Icon URL (SC)
 @apiSuccess (Success 200) {Object[]}	data.list       Leasing Info - Details List
 @apiSuccess (Success 200) {String}	    data.list.introductionEn     Leasing Info Introduction (EN)
 @apiSuccess (Success 200) {String}	    data.list.introductionTc     Leasing Info Introduction (TC)
 @apiSuccess (Success 200) {String}	    data.list.introductionSc     Leasing Info Introduction (SC)
 @apiSuccess (Success 200) {String}	    data.list.locationEn     Location (EN)
 @apiSuccess (Success 200) {String}	    data.list.locationTc     Location (TC)
 @apiSuccess (Success 200) {String}	    data.list.locationSc     Location (SC)
 @apiSuccess (Success 200) {String}	    data.list.sizeEn     Size (EN)
 @apiSuccess (Success 200) {String}	    data.list.sizeTc     Size (TC)
 @apiSuccess (Success 200) {String}	    data.list.sizeSc     Size (SC)
 @apiSuccess (Success 200) {String}	    data.list.tradeNatureEn     Trade Nature (EN)
 @apiSuccess (Success 200) {String}	    data.list.tradeNatureTc     Trade Nature (TC)
 @apiSuccess (Success 200) {String}	    data.list.tradeNatureSc     Trade Nature (SC)
 @apiSuccess (Success 200) {String}	    data.list.managementFeeEn     Management Fee (EN)
 @apiSuccess (Success 200) {String}	    data.list.managementFeeTc     Management Fee (TC)
 @apiSuccess (Success 200) {String}	    data.list.managementFeeSc     Management Fee (SC)
 @apiSuccess (Success 200) {String}	    data.list.acFeeEn     A/C Fee (EN)
 @apiSuccess (Success 200) {String}	    data.list.acFeeTc     A/C Fee (TC)
 @apiSuccess (Success 200) {String}	    data.list.acFeeSc     A/C Fee (SC)
 @apiSuccess (Success 200) {String}	    data.list.availableDateEn     Available Date (EN)
 @apiSuccess (Success 200) {String}	    data.list.availableDateTc     Available Date (TC)
 @apiSuccess (Success 200) {String}	    data.list.availableDateSc     Available Date (SC)
 @apiSuccess (Success 200) {String}	    data.list.yearOfIntake     Year Of Intake (EN)
 @apiSuccess (Success 200) {String}	    data.list.noOfClinicEn     Number Of Existing Clinics (EN)
 @apiSuccess (Success 200) {String}	    data.list.noOfClinicTc     Number Of Existing Clinics (TC)
 @apiSuccess (Success 200) {String}	    data.list.noOfClinicSc     Number Of Existing Clinics (SC)
 @apiSuccess (Success 200) {String}	    data.list.contactPersonEn     Contact Person (EN)
 @apiSuccess (Success 200) {String}	    data.list.contactPersonTc     Contact Person (TC)
 @apiSuccess (Success 200) {String}	    data.list.contactPersonSc     Contact Person (SC)
 @apiSuccess (Success 200) {String}	    data.list.floorPlanPath     Floor Plan Image URL
 @apiSuccess (Success 200) {String}	    data.remarks1En     Remarks 1 (EN)
 @apiSuccess (Success 200) {String}	    data.remarks1Tc     Remarks 1 (TC)
 @apiSuccess (Success 200) {String}	    data.remarks1Sc     Remarks 1 (SC)
 @apiSuccess (Success 200) {String}	    data.remarks2En     Remarks 2 (EN)
 @apiSuccess (Success 200) {String}	    data.remarks2Tc     Remarks 2 (TC)
 @apiSuccess (Success 200) {String}	    data.remarks2Sc     Remarks 2 (SC)


 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::LEASING_INFO_DETAILS_API_START::
 {
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{
      "id":1,
      "seoTitleEn":"Leasing Info Details",
      "seoTitleTc":"Leasing Info Details",
      "seoTitleSc":"Leasing Info Details",
      "seoKeywordEn":"Leasing Info Details",
      "seoKeywordTc":"Leasing Info Details",
      "seoKeywordSc":"Leasing Info Details",
      "seoDescriptionEn":"Leasing Info Details",
      "seoDescriptionTc":"Leasing Info Details",
      "seoDescriptionSc":"Leasing Info Details",
      "shopCentreInfo":{
         "shopCentreId":7,
         "headlineEn":"Leasing",
         "headlineTc":"Leasing (TC)",
         "headlineSc":"Leasing (SC)",
         "addressLabelEn":"Address",
         "addressLabelTc":"地址",
         "addressLabelSc":"地址",
         "addressEn":"Lok Fu Place 198 Junction Road Wang Tau Hom,  Kowloon ",
         "addressTc":"九龍橫頭磡聯合道198號樂富廣場",
         "addressSc":"九龙横头磡联合道198号乐富广场",
         "customerHotLineLabelEn":"Customer Hotline",
         "customerHotLineLabelTc":"客戶服務熱線",
         "customerHotLineLabelSc":"客户服务热线",
         "customerHotLine":"2338 7781",
         "carParkEnquiryLabelEn":"Car Park Enquiry",
         "carParkEnquiryLabelTc":"停車場查詢",
         "carParkEnquiryLabelSc":"停车场查询",
         "carParkEnquiry":"2626 7777",
         "leasingEnquiryLabelEn":"Leasing Enquiry",
         "leasingEnquiryLabelTc":"租務查詢",
         "leasingEnquiryLabelSc":"租务查询",
         "leasingEnquiry":"2175 1747 ",
         "viewLocationThumbnailUrlEn":"/shopDetailViewLocationEn.png",
         "viewLocationThumbnailUrlTc":"/shopDetailViewLocationTc.png",
         "viewLocationThumbnailUrlSc":"/shopDetailViewLocationSc.png",
         "mallThumbnails":[
            {
               "thumbnail":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
               "thumbnailAltTextEn":"Outlook",
               "thumbnailAltTextTc":"外觀",
               "thumbnailAltTextSc":"外观"
            },
            {
               "thumbnail":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/LF_after_5night.jpg",
               "thumbnailAltTextEn":"Podium Food Square",
               "thumbnailAltTextTc":"美食坊",
               "thumbnailAltTextSc":"美食坊"
            },
            {
               "thumbnail":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/LF_after_6.jpg",
               "thumbnailAltTextEn":"Atrium",
               "thumbnailAltTextTc":"中庭",
               "thumbnailAltTextSc":"中庭"
            }
         ]
      },
      "label":{
         "locationLabelEn":"Location",
         "locationLabelTc":"Location",
         "locationLabelSc":"Location",
         "sizeLabelEn":"Size",
         "sizeLabelTc":"Size",
         "sizeLabelSc":"Size",
         "tradeNatureLabelEn":"Trade Nature",
         "tradeNatureLabelTc":"Trade Nature",
         "tradeNatureLabelSc":"Trade Nature",
         "managementFeeLabelEn":"Management Fee",
         "managementFeeLabelTc":"Management Fee",
         "managementFeeLabelSc":"Management Fee",
         "acFeeLabelTc":"A/C Fee",
         "acFeeLabelEn":"A/C Fee",
         "acFeeLabelSc":"A/C Fee",
         "availableDateLabelEn":"Available Date",
         "availableDateLabelTc":"Available Date",
         "availableDateLabelSc":"Available Date",
         "yearOfIntakeLabelEn":"Year Of Intake",
         "yearOfIntakeLabelTc":"Year Of Intake",
         "yearOfIntakeLabelSc":"Year Of Intake",
         "noOfClinicLabelEn":"Number Of Existing Clinic",
         "noOfClinicLabelTc":"Number Of Existing Clinic",
         "noOfClinicLabelSc":"Number Of Existing Clinic",
         "contactPersonLabelEn":"Contact Person",
         "contactPersonLabelTc":"Contact Person",
         "contactPersonLabelSc":"Contact Person",
         "floorPlanIconEn":"http://domain.com/floorPlanIcon.png",
         "floorPlanIconTc":"http://domain.com/floorPlanIcon.png",
         "floorPlanIconSc":"http://domain.com/floorPlanIcon.png"
      },
      "list":[
         {
            "introductionEn":"",
            "introductionTc":"",
            "introductionSc":"",
            "locationEn":"",
            "locationTc":"",
            "locationSc":"",
            "sizeEn":"",
            "sizeTc":"",
            "sizeSc":"",
            "tradeNatureEn":"",
            "tradeNatureTc":"",
            "tradeNatureSc":"",
            "managementFeeEn":"",
            "managementFeeTc":"",
            "managementFeeSc":"",
            "acFeeEn":"",
            "acFeeTc":"",
            "acFeeSc":"",
            "availableDateEn":"",
            "availableDateTc":"",
            "availableDateSc":"",
            "yearOfIntake":"",
            "noOfClinicEn":"",
            "noOfClinicTc":"",
            "noOfClinicSc":"",
            "contactPersonEn":"",
            "contactPersonTc":"",
            "contactPersonSc":"",
            "floorPlanPath":""
         }
      ],
        "remarks1En":"",
        "remarks1Tc":"",
        "remarks1Sc":"",
        "remarks2En":"",
        "remarks2Tc":"",
        "remarks2Sc":""
   }
}
 ::LEASING_INFO_DETAILS_API_END::
 */

