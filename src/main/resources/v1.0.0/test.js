/**
 *
 * @api {get} /api/test/deployTest	Deploy Test
 * @apiName deployTest
 * @apiGroup Test
 * @apiVersion 1.0.0
 *
 * @apiParam {-}		-					-
 *
 * @apiSuccess (Success 200) {String}			status																							API Call Status. "0000" for success.
 * @apiSuccess (Success 200) {String}			errorMsg																						Error Message When API Call Fails ( status is not "0000" ) .
 * @apiSuccess (Success 200) {Object}			data						Data Object.
 * @apiSuccess (Success 200) {String}			data.releaseDate        Release Date
 * @apiSuccess (Success 200) {String}			data.version            Release Version
 * @apiSuccessExample {json} Example
 * HTTP/1.1 200 OK
 * {
		  "data": {
		    "releaseDate" : "20161220",
		    "version" : "1"
		  },
		  "status": "0000",
		  "errorMsg": ""
	   }
 *
 */

/**
 *
 * @api {get} /api/test/healthCheck	Health Check
 * @apiName healthCheck
 * @apiGroup Test
 * @apiVersion 1.0.0
 *
 * @apiParam {-}		-					-
 *
 * @apiSuccess (Success 200) {String}			resourceDbStatus										Resource Related Database Status. 0 for Inactive. 1 for Active.
 * @apiSuccess (Success 200) {String}			applicationDbStatus									Application Related Database Status. 0 for Inactive. 1 for Active.
 * @apiSuccess (Success 200) {String}			mongoDbStatus												MongoDb Status. 0 for Inactive. 1 for Active.
 * @apiSuccess (Success 200) {String}			serverStatus												Server Status and Controlled by Database System Setting Table. 0 for Inactive. 1 for Active.
 * @apiSuccess (Success 200) {String}			active															And Logic of Above Status. 0 for Inactive. 1 for Active.
 * @apiSuccessExample {json} Example
 * HTTP/1.1 200 OK
 * {
	 * 	"resourceDbStatus":1,
	 * 	"applicationDbStatus":1,
	 * 	"mongoDbStatus":1,
	 * 	"active":1
	 * }
 *
 */