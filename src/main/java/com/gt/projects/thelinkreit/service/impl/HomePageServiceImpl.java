package com.gt.projects.thelinkreit.service.impl;

import com.gt.projects.thelinkreit.entity.mongo.HomePage;
import com.gt.projects.thelinkreit.repository.mongo.HomePageRepository;
import com.gt.projects.thelinkreit.service.HomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomePageServiceImpl implements HomePageService {

    @Autowired
    private HomePageRepository homePageRepository;

    public List<HomePage> findAll() {
        return homePageRepository.findAll();
    }

}
