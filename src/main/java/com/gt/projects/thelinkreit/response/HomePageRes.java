package com.gt.projects.thelinkreit.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class HomePageRes {

	private Integer id;
	private String seoTitleEn;
	private String seoTitleTc;
	private String seoTitleSc;
	private String seoKeywordEn;
	private String seoKeywordTc;
	private String seoKeywordSc;
	private String seoDescriptionEn;
	private String seoDescriptionTc;
	private String seoDescriptionSc;
    private HomePageBannerRes banner;
    private HomePageTheNetworkRes theNetwork;
    private HomePageHappeningHighlightRes happeningHighlight;
    private HomePageExperienceHighlightRes experienceHighlight;
    private List<HomePageParallaxRes> parallax;
    private List<HomePageImportantNoticeRes> importantNotices;
    
    public HomePageRes(){
    	banner = new HomePageBannerRes();
    	theNetwork = new HomePageTheNetworkRes();
    	happeningHighlight = new HomePageHappeningHighlightRes();
    	experienceHighlight = new HomePageExperienceHighlightRes();
    	parallax = new ArrayList<HomePageParallaxRes>();
    	importantNotices = new ArrayList<HomePageImportantNoticeRes>();
    }
    
    
    @Data
    public class HomePageBannerRes{
		private String videoUrlTc;
		private String videoUrlSc;
		private String videoUrlEn;
		private String videoThumbnailEn;
		private String videoThumbnailTc;
		private String videoThumbnailSc;
		private String videoAltTextEn;
		private String videoAltTextTc;
		private String videoAltTextSc;
		private List<HomePageBannerImageRes> images;
		
		public HomePageBannerRes(){
			this.images = new ArrayList<HomePageBannerImageRes>();
		}
	};
	
	@Data
	public class HomePageBannerImageRes{
		private String imgStandardEn;
        private String imgStandardTc;
        private String imgStandardSc;
        private String imgNarrowEn;
        private String imgNarrowTc;
        private String imgNarrowSc;
        private String altTextEn;
        private String altTextTc;
        private String altTextSc;
	}
	
	@Data
	public class HomePageTheNetworkRes{
		List<HomePageTheNetworkParkingRes> parking;
		List<HomePageTheNetworkShoppingRes> shopping;
		List<HomePageTheNetworkMarketRes> market;
		public HomePageTheNetworkRes(){
			this.parking = new ArrayList<HomePageTheNetworkParkingRes>();
			this.shopping = new ArrayList<HomePageTheNetworkShoppingRes>();
			this.market =  new ArrayList<HomePageTheNetworkMarketRes>();
		}
	}
	
	@Data
	public class HomePageTheNetworkParkingRes{
		private Integer facilityKey;
		private String thumnailPath;
		private String carParkFacilityNameEn;
		private String carParkFacilityNameTc;
		private String carParkFacilityNameSc;
	}
	
	@Data
	public class HomePageTheNetworkShoppingRes{
		private Integer shopCentreId;
		private String thumnailPath;
		private String shopCentreNameEn;
		private String shopCentreNameTc;
		private String shopCentreNameSc;
	}
	
	@Data
	public class HomePageTheNetworkMarketRes{
		private String marketCode;
		private String thumnailPath;
		private String marketNameEn;
		private String marketNameTc;
		private String marketNameSc;
	}
	
	@Data 
	public class HomePageHappeningHighlightRes{
		private String titleEn;
		private String titleTc;
		private String titleSc;
		private List<HomePageHappeningHighlightEventRes> eventList;
		public HomePageHappeningHighlightRes(){
			this.eventList = new ArrayList<HomePageHappeningHighlightEventRes>();
		}
	}
	
	@Data
	public class HomePageHappeningHighlightEventRes{
		private Integer id;
		private String happeningHighlightTitleEn;
		private String happeningHighlightTitleTc;
        private String happeningHighlightTitleSc;
		private String happeningHighlightDescriptionEn;
        private String happeningHighlightDescriptionTc;
        private String happeningHighlightDescriptionSc;
		private String happeningHighlightImageUrlEn;
		private String happeningHighlightImageUrlTc;
        private String happeningHighlightImageUrlSc;
		private String happeningHighlightLinkEn;
        private String happeningHighlightLinkTc;
        private String happeningHighlightLinkSc;
        private Date happeningHighlightStartDatetime;
        private Date happeningHighlightEndDatetime;
	}
	
	@Data
	public class HomePageExperienceHighlightRes{
		private String backgroundStandard;
		private String backgroundNarrow;
	    private String videoThumbnailStandardEn;
	    private String videoThumbnailStandardTc;
	    private String videoThumbnailStandardSc;
	    private String videoThumbnailNarrowEn;
	    private String videoThumbnailNarrowTc;
	    private String videoThumbnailNarrowSc;
	    private String videoUrlEn;
	    private String videoUrlTc;
	    private String videoUrlSc;
	    private String videoAltTextEn;
	    private String videoAltTextTc;
	    private String videoAltTextSc;
	    private String headlineEn;
	    private String headlineTc;
	    private String headlineSc;
	    private String subHeadlineEn;
	    private String subHeadlineTc;
	    private String subHeadlineSc;
	    private List<HomePageExperienceHighlightFeatureBlocksRes> featureBlocks;
	    
	    public HomePageExperienceHighlightRes(){
	    	this.featureBlocks = new ArrayList<HomePageExperienceHighlightFeatureBlocksRes>();
	    }
	}
	
	@Data
	public class HomePageExperienceHighlightFeatureBlocksRes{
		private String imgStandard;
        private String imgNarrow;
        private String iconEn;
        private String iconTc;
        private String iconSc;
        private String altTextEn;
        private String altTextTc;
        private String altTextSc;
        private String urlEn;
        private String urlTc;
        private String urlSc;
	}
	
	@Data
	public class HomePageParallaxRes{
		private String value;
	}
	
	@Data
	public class HomePageImportantNoticeRes{
		private String importantNoticeImageUrlEn;
        private String importantNoticeImageUrlTc;
        private String importantNoticeImageUrlSc;
        private String importantNoticeLinkEn;
        private String importantNoticeLinkTc;
        private String importantNoticeLinkSc;
        private String importantNoticeTitleEn;
        private String importantNoticeTitleTc;
        private String importantNoticeTitleSc;
        private String importantNoticeDescriptionEn;
        private String importantNoticeDescriptionTc;
        private String importantNoticeDescriptionSc;
        private String importantNoticeStartDatetime;
        private String importantNoticeEndDatetime;
	}
}

