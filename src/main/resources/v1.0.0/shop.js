/**
	 
 @api {get} http://www-dev.linkhk.com/linkweb/api/shop/      Shop Listing
 
 @apiName Shop Listing
 @apiGroup Batch2-Shop
 @apiVersion 1.0.0
 
 @apiDescription 
 Retrieve Shop Listing Page content<br/> 
 Data included 6 components content:<br/> 
 1. SEO Details<br/> 
 2. Banner<br/> 
 3. Category Filter<br/>
 4. Theme Banner<br/> 
 5. Shop List<br/> 
 6. Paging Info<br/> 
 
 @apiParam {Integer}	pageType		Section Page Type (1= Shop, 2=Dine)
 @apiParam {Integer}	categoryId		Category ID
 @apiParam {Integer}	districtId		District ID
 @apiParam {Integer}	shopCentreId		Shop Centre ID
 @apiParam {Integer}	pageNo		Page No ( start = 0)
 @apiParam {Integer}	pageSize	Number of list record per request
 
	@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
	@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
	@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
	@apiSuccess (Success 200) {String} 	data		Data Object.
	@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
	@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
	@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
	@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
	@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
	@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
	@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
	@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
	@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
	@apiSuccess (Success 200) {Object} 	data.banner		Shop List - Banner Object
	@apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
	@apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
	@apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
	@apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
	@apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
	@apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
	@apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
	@apiSuccess (Success 200) {Object} 	data.category	Shop List - Category Object
	@apiSuccess (Success 200) {Integer} 	data.category.shopCategoryTypeId	Category ID
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryNameEn	Category Name (EN)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryNameTc	Category Name (TC)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryNameSc	Category Name (SC)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryDescriptionEn		Category Description (EN)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryDescriptionTc		Category Description (TC)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryDescriptionSc		Category Description (SC)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryBannerImgStandard		Category Banner (Desktop)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryBannerImgNarrow		Category Banner (Mobile)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryPickerImg		Category Image
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryThumbnailAltTextEn	Category Image Alt Text (EN)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryThumbnailAltTextTc	Category Image Alt Text (TC)
    @apiSuccess (Success 200) {String} 	data.category.shopCategoryThumbnailAltTextSc	Category Image Alt Text (SC)
    @apiSuccess (Success 200) {String} 	data.category.isExist							Category Icon Checking (True = Exist Data, False = No Data)
    @apiSuccess (Success 200) {Object} 	data.themeBanner	Shop List - ThemeBanner
    @apiSuccess (Success 200) {String} 	data.themeBanner.imgStandardEn		Banner URL (Desktop)(EN)
    @apiSuccess (Success 200) {String} 	data.themeBanner.imgStandardTc		Banner URL (Desktop)(TC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.imgStandardSc		Banner URL (Desktop)(SC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.imgNarrowEn		Banner URL (Mobile)(EN)
    @apiSuccess (Success 200) {String} 	data.themeBanner.imgNarrowTc		Banner URL (Mobile)(TC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.imgNarrowSc		Banner URL (Mobile)(SC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.bannerAltTextEn		Banner Alt Text (EN)
    @apiSuccess (Success 200) {String} 	data.themeBanner.bannerAltTextTc		Banner Alt Text (TC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.bannerAltTextSc		Banner Alt Text (SC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.linkEn				Banner Redirect URL (EN)
    @apiSuccess (Success 200) {String} 	data.themeBanner.linkTc				Banner Redirect URL (TC)
    @apiSuccess (Success 200) {String} 	data.themeBanner.linkSc				Banner Redirect URL (SC)
    @apiSuccess (Success 200) {Object[]} 	data.shopList		Shop List - Shop List Object Array
    @apiSuccess (Success 200) {Integer} 	data.shopList.shopId	Shop ID
    @apiSuccess (Success 200) {String} 	data.shopList.shopCentreNameEn		Shop Centre Name (EN)
    @apiSuccess (Success 200) {String} 	data.shopList.shopCentreNameTc		Shop Centre Name (TC)
    @apiSuccess (Success 200) {String} 	data.shopList.shopCentreNameSc		Shop Centre Name (SC)
    @apiSuccess (Success 200) {Object[]} 	data.shopList.shopCategoryIconPath		Shop Category Icon URL Object Array
    @apiSuccess (Success 200) {String} 	data.shopList.shopCategoryIconPath.url		Shop Category Icon URL
    @apiSuccess (Success 200) {String} 	data.shopList.shopNo			Shop No
    @apiSuccess (Success 200) {String} 	data.shopList.shopNameEn		Shop Name (EN)
    @apiSuccess (Success 200) {String} 	data.shopList.shopNameTc		Shop Name (TC)
    @apiSuccess (Success 200) {String} 	data.shopList.shopNameSc		Shop Name (SC)
    @apiSuccess (Success 200) {String} 	data.shopList.telephone		Telephone no.
    @apiSuccess (Success 200) {String} 	data.shopList.shopPhotoPath		Shop Image URL  (*Remove Later)
    @apiSuccess (Success 200) {String} 	data.shopList.shopPhotoPathEn		Shop Image URL (EN)
    @apiSuccess (Success 200) {String} 	data.shopList.shopPhotoPathTc		Shop Image URL (TC)
    @apiSuccess (Success 200) {String} 	data.shopList.shopPhotoPathSc		Shop Image URL (SC)
    @apiSuccess (Success 200) {String} 	data.shopList.altTextEn	Shop Image Alt Text (EN)
    @apiSuccess (Success 200) {String} 	data.shopList.altTextTc	Shop Image Alt Text (TC)
    @apiSuccess (Success 200) {String} 	data.shopList.altTextSc	Shop Image Alt Text (SC)
    @apiSuccess (Success 200) {Boolean} 	data.shopList.happyVisaCard    Happy Visa Card Indicator Checking
    @apiSuccess (Success 200) {String} 	data.shopList.happyVisaCardIcon    Happy Visa Card Indicator
    @apiSuccess (Success 200) {String} 	data.shopList.happyVisaCardIconAltTextEn    Happy Visa Card Indicator Alt Text (EN)
    @apiSuccess (Success 200) {String} 	data.shopList.happyVisaCardIconAltTextTc    Happy Visa Card Indicator Alt Text (TC)
    @apiSuccess (Success 200) {String} 	data.shopList.happyVisaCardIconAltTextSc    Happy Visa Card Indicator Alt Text (SC)
	@apiSuccess (Success 200) {Object} 	data.pageInfo	Shop List - Page Info Object
	@apiSuccess (Success 200) {Integer} 	data.pageInfo.total		No. of Total Record
    @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageCount		No. of Page Count
    @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageSize		No. per page
    @apiSuccess (Success 200) {Integer} 	data.pageInfo.curPage		Current Page No
@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::SHOP_LIST_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "seoTitleEn":"Shopping Varieties",
      "seoTitleTc":"購物品種",
      "seoTitleSc":"购物品种",
      "seoKeywordEn":"",
      "seoKeywordTc":"",
      "seoKeywordSc":"",
      "seoDescriptionEn":"",
      "seoDescriptionTc":"",
      "seoDescriptionSc":"",
      "banner":{  
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "category":[  
         {  
            "shopCategoryTypeId":1,
            "shopCategoryNameEn":"FASHION & ACCESSORIES",
            "shopCategoryNameTc":"FASHION & ACCESSORIES TC",
            "shopCategoryNameSc":"FASHION & ACCESSORIES SC",
            "shopCategoryDescriptionEn":"FASHION & ACCESSORIES",
            "shopCategoryDescriptionTc":"FASHION & ACCESSORIES TC",
            "shopCategoryDescriptionSc":"FASHION & ACCESSORIES SC",
            "shopCategoryThumbnailAltTextEn":"FASHION & ACCESSORIES",
            "shopCategoryThumbnailAltTextTc":"FASHION & ACCESSORIES TC",
            "shopCategoryThumbnailAltTextSc":"FASHION & ACCESSORIES SC",
            "shopCategoryBannerImgStandard" : "/BannerStandard.png",
            "shopCategoryBannerImgNarrow" : "/BannerNarrow.png",
            "shopCategoryPickerImg":"/pickerImg.png",
            "isExist" : true
         }
      ],
      "themeBanner":[  
         {  
            "imgStandardEn" : "/SiteAssets/ShopThemeStandard.PNG",
		    "imgStandardTc" : "/SiteAssets/ShopThemeStandard.PNG",
		    "imgStandardSc" : "/SiteAssets/ShopThemeStandard.PNG",
		    "imgNarrowEn" : "/SiteAssets/ShopThemeNarrow.PNG",
		    "imgNarrowTc" : "/SiteAssets/ShopThemeNarrow.PNG",
		    "imgNarrowSc" : "/SiteAssets/ShopThemeNarrow.PNG",
		    "altTextEn" : "approximately 150 shopping centres!",
		    "altTextTc" : "大約150個商場！",
		    "altTextSc" : "大约150个商场！",
		    "linkEn" : null,
		    "linkTc" : null,
		    "linkSc" : null
         }
      ],
      "shopList":[
         {  
            "shopId":17402,
            "shopCentreNameEn":"",
            "shopCentreNameTc":"",
            "shopCentreNameSc":"",
            "shopCategoryIconPath":[
            	{
            		"url": ""
            	}
            ],
            "shopNo":"123",
            "shopNameEn":"Colourmix",
            "shopNameTc":"Colourmix TC",
            "shopNameSc":"Colourmix SC",
            "telephone":"2870 0139",
            "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
            "shopPhotoPathEn","images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
            "shopPhotoPathTc","images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
            "shopPhotoPathSc","images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
            "shopPhotoAltTextEn":"Dr. Soong Hung Hung Henry Dental Clinic",
            "shopPhotoAltTextTc":"宋鴻鴻牙科醫務所",
            "shopPhotoAltTextSc":"宋鸿鸿牙科医务所",
            "happyVisaCard" : true,
            "happyVisaCardIcon" : "",
            "happyVisaCardIconAltTextEn" : "",
            "happyVisaCardIconAltTextTc" : "",
            "happyVisaCardIconAltTextSc" : ""
         }
      ]
   },
   "pageInfo":{  
      "total":49,
      "pageCount":1,
      "pageSize":49,
      "curPage":1
   }
}
::SHOP_LIST_API_END::
*/
/**
	 
 @api {get} http://www-dev.linkhk.com/linkweb/api/shop/{id}      Shop Details
 
 @apiName Shop Details
 @apiGroup Batch2-Shop
 @apiVersion 1.0.0
 
 @apiDescription 
 Retrieve Shop Details Page content<br/> 
 Data included 6 components content:<br/> 
 1. SEO Details<br/> 
 2. Shop Info<br/> 
 3. Apps Feature<br/> 
 4. Partner Details<br/> 
 5. Extra Field Content<br/> 
 6. You may also like<br/>
 <br/>
 <br/>
 Remark <br/>
 1. Category Filter Handling<br/>
 	- "All Category" title & description: Table -> PageResource<br/>
 	- "All Category" thumbnail : Table -> FileResource<br/>
 2. Extra Field Handling<br/>
	- Long Extra Field: Table-> shop - field 07-13<br/>
	- Short Extra Field: Table-> shop - field 02-06<br/>
	- special icon : Table -> shop - field 01<br/>

 @apiParam {Integer}	id		Shop ID
	 
@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
@apiSuccess (Success 200) {String} 	data		Data Object.
@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
@apiSuccess (Success 200) {Object} 	data.shopInfo		Shop - Shop Info Object
@apiSuccess (Success 200) {Integer} 	data.shopInfo.shopId		Shop ID
@apiSuccess (Success 200) {String} 	data.shopInfo.propCode		Shop Centre Property Code
@apiSuccess (Success 200) {Integer} 	data.shopInfo.shopCentreId		Shop Centre ID
@apiSuccess (Success 200) {Integer} 	data.shopInfo.shopType		Shop Type (1 = Shop , 2= Dine)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopTypeTextEn	Shop Category (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopTypeTextTc	Shop Category (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopTypeTextSc	Shop Category (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopNameEn		Shop (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopNameTc		Shop (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopNameSc		Shop (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.locationLabelEn	Address Label (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.locationLabelTc	Address Label (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.locationLabelSc	Address Label (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.locationEn		Address (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.locationTc		Address (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.locationSc		Address (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.telephoneLabelEn	Telephone Label (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.telephoneLabelTc	Telephone Label (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.telephoneLabelSc	Telephone Label (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.telephone			Telephone No.
@apiSuccess (Success 200) {String} 	data.shopInfo.openingHoursLabelEn	Opening Hours Label (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.openingHoursLabelTc	Opening Hours Label (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.openingHoursLabelSc	Opening Hours Label (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.openingHoursEn		Opening Hours Details (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.openingHoursTc		Opening Hours Details (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.openingHoursSc		Opening Hours Details (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.websiteUrlLabelEn		Website URL Label (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.websiteUrlLabelTc		Website URL	Label (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.websiteUrlLabelSc		Website URL Label (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.websiteUrl		Website URL
@apiSuccess (Success 200) {String} 	data.shopInfo.specialIconUrlEn		Shop Special Activities Icon URL (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.specialIconUrlTc		Shop Special Activities Icon URL (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.specialIconUrlSc		Shop Special Activities Icon URL (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.goParkingThumbnailUrlEn	Go Parking Thumbnail URL (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.goParkingThumbnailUrlTc	Go Parking Thumbnail URL (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.goParkingThumbnailUrlSc	Go Parking Thumbnail URL (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.goShoppingThumbnailUrlEn	Go Shopping Thumbnail URL (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.goShoppingThumbnailUrlTc	Go Shopping Thumbnail URL (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.goShoppingThumbnailUrlSc	Go Shopping Thumbnail URL (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.goDiningThumbnailUrlEn	Go Dining Thumbnail URL (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.goDiningThumbnailUrlTc	Go Dining Thumbnail URL (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.goDiningThumbnailUrlSc	Go Dining Thumbnail URL (SC)
@apiSuccess (Success 200) {String} 	data.shopInfo.viewLocationThumbnailUrlEn	View Location Thumbnail URL (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.viewLocationThumbnailUrlTc	View Location Thumbnail URL (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.viewLocationThumbnailUrlSc	View Location Thumbnail URL (SC)
@apiSuccess (Success 200) {Object[]} 	data.shopInfo.shopPhotos		Shop Image Object Array
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoPath	Shop Image URL
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoPathEn	Shop Image En URL
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoPathTc	Shop Image Tc URL
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoPathSc	Shop Image Sc URL 
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoAltTextEn		Shop Image Alt Text (EN)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoAltTextTc		Shop Image Alt Text (TC)
@apiSuccess (Success 200) {String} 	data.shopInfo.shopPhotos.shopPhotoAltTextSc		Shop Image Alt Text (SC)
@apiSuccess (Success 200) {Object} 	data.appFeature		Shop - App Feature Object
@apiSuccess (Success 200) {String} 	data.appFeature.appFeatureThumbnailUrlEn	App Feature Icon URL (EN)
@apiSuccess (Success 200) {String} 	data.appFeature.appFeatureThumbnailUrlTc	App Feature Icon URL (TC)
@apiSuccess (Success 200) {String} 	data.appFeature.appFeatureThumbnailUrlSc	App Feature Icon URL (SC)
@apiSuccess (Success 200) {String} 	data.appFeature.eQueuingThumbnailUrlEn	E-Queuing Icon URL (EN)
@apiSuccess (Success 200) {String} 	data.appFeature.eQueuingThumbnailUrlTc	E-Queuing Icon URL (TC)
@apiSuccess (Success 200) {String} 	data.appFeature.eQueuingThumbnailUrlSc	E-Queuing Icon URL (SC)
@apiSuccess (Success 200) {String} 	data.appFeature.eCouponThumbnailUrlEn	E-Coupon Icon URL (EN)
@apiSuccess (Success 200) {String} 	data.appFeature.eCouponThumbnailUrlTc	E-Coupon Icon URL (TC)
@apiSuccess (Success 200) {String} 	data.appFeature.eCouponThumbnailUrlSc	E-Coupon Icon URL (SC)
@apiSuccess (Success 200) {String} 	data.appFeature.eDirectoryThumbnailUrlEn	E-Diretory Icon URL (EN)
@apiSuccess (Success 200) {String} 	data.appFeature.eDirectoryThumbnailUrlTc	E-Diretory Icon URL (TC)
@apiSuccess (Success 200) {String} 	data.appFeature.eDirectoryThumbnailUrlSc	E-Diretory Icon URL (SC)
@apiSuccess (Success 200) {String} 	data.appFeature.findMyCarThumbnailUrlEn	Find My Car Icon URL (EN)
@apiSuccess (Success 200) {String} 	data.appFeature.findMyCarThumbnailUrlTc	Find My Car Icon URL (TC)
@apiSuccess (Success 200) {String} 	data.appFeature.findMyCarThumbnailUrlSc	Find My Car Icon URL (SC)
@apiSuccess (Success 200) {Boolean} 	data.appFeature.isEQueuing		Is E-Queuing
@apiSuccess (Success 200) {Boolean} 	data.appFeature.isECoupon		Is E-Coupon
@apiSuccess (Success 200) {Boolean} 	data.appFeature.isEDirectory	Is E-Directory
@apiSuccess (Success 200) {Boolean} 	data.appFeature.isFindMyCar		Is Find My Car
@apiSuccess (Success 200) {Object[]}	data.partner 				Shop - Shop Partner Object Array
@apiSuccess (Success 200) {String}		data.partner.provider		Shop Partner Content
@apiSuccess (Success 200) {String}		data.partner.providerIcon	Shop Partner Icon
@apiSuccess (Success 200) {Object[]}	data.partner.indexBox		Shop Partner index box Object Array
@apiSuccess (Success 200) {String}		data.partner.indexBox.titleEn	Shop Partner index box title (EN)
@apiSuccess (Success 200) {String}		data.partner.indexBox.titleTc	Shop Partner index box title (TC)
@apiSuccess (Success 200) {String}		data.partner.indexBox.titleSc	Shop Partner index box title (SC)
@apiSuccess (Success 200) {String}		data.partner.indexBox.icon		Shop Partner index box icon
@apiSuccess (Success 200) {Integer}		data.partner.indexBox.digits	Shop Partner index box digits
@apiSuccess (Success 200) {String}		data.partner.indexBox.url		Shop Partner index box url
@apiSuccess (Success 200) {Object[]}	data.partner.thumbnail			Shop Partner thumbnail Object Array
@apiSuccess (Success 200) {String}		data.partner.thumbnail.image	Shop Partner thumbnail path
@apiSuccess (Success 200) {String}		data.partner.thumbnail.url		Shop Partner thumbnail url
@apiSuccess (Success 200) {Object}		data.extraField		Shop - Extra Field
@apiSuccess (Success 200) {Object[]}	data.extraField.longExtraField		Long Extra Field Object Array
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.fieldTitleEn		Long Extra Field Title (EN)
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.fieldTitleTc		Long Extra Field Title (TC)
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.fieldTitleSc		Long Extra Field Title (SC)
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.fieldContentEn	Long Extra Field Content (EN)
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.fieldContentTc	Long Extra Field Content (TC)
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.fieldContentSc	Long Extra Field Content (SC)
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.noticeImageURL	Long Extra Field Image URL
@apiSuccess (Success 200) {String}		data.extraField.longExtraField.noticeImageAlign	Long Extra Field Image Alignment
@apiSuccess (Success 200) {Object[]}	data.extraField.shortExtraField		Short Extra Field Object Array
@apiSuccess (Success 200) {String}		data.extraField.shortExtraField.fieldTitleEn	Short Extra Field Title (EN)
@apiSuccess (Success 200) {String}		data.extraField.shortExtraField.fieldTitleTc	Short Extra Field Title (TC)
@apiSuccess (Success 200) {String}		data.extraField.shortExtraField.fieldTitleSc	Short Extra Field Title (SC)
@apiSuccess (Success 200) {String}		data.extraField.shortExtraField.fieldContentEn	Short Extra Field Content (EN)
@apiSuccess (Success 200) {String}		data.extraField.shortExtraField.fieldContentTc	Short Extra Field Content (TC)
@apiSuccess (Success 200) {String}		data.extraField.shortExtraField.fieldContentSc	Short Extra Field Content (SC)
@apiSuccess (Success 200) {Object}		data.youWillAlsoLike	Shop - You May Also Like Object 
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleEn	Title (EN)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleTc	Title (TC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleSc	Title (SC) 
@apiSuccess (Success 200) {Object[]}		data.youWillAlsoLike.youWillAlsoLikeList	You May Also Like List Object Array
@apiSuccess (Success 200) {Integer}		data.youWillAlsoLike.youWillAlsoLikeList.shopId		Shop ID
@apiSuccess (Success 200) {Integer}		data.youWillAlsoLike.youWillAlsoLikeList.shopCategoryTypeId		Shop Category Type ID
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopCentreNameEn		Shop Centre Name (EN)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopCentreNameTc		Shop Centre Name (TC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopCentreNameSc		Shop Centre Name (SC)
@apiSuccess (Success 200) {Object[]}		data.youWillAlsoLike.youWillAlsoLikeList.shopCategoryIconPath	Shop Category Icon URL Object Array
@apiSuccess (Success 200) {Integer}		data.youWillAlsoLike.youWillAlsoLikeList.shopCategoryIconPath.shopCategoryTypeId	Shop Category ID
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopCategoryIconPath.url	Shop Category Icon URL
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopNameEn		Shop Name (EN)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopNameTc		Shop Name (TC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopNameSc		Shop Name (SC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopPhotoPath	Shop Image URL 
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopPhotoAltTextEn		Shop Image Alt Text (EN)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopPhotoAltTextTc		Shop Image Alt Text (TC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.shopPhotoAltTextSc		Shop Image Alt Text (SC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllEn		View All Content (EN)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllTc		View All Content (TC)
@apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllSc		View All Content (SC)
@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::SHOP_DETAILS_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "id":1,
      "seoTitleEn": "LINK Customer Site Shop Details EN",
	  "seoTitleTc": "LINK Customer Site Shop Details TC",
	  "seoTitleSc": "LINK Customer Site Shop Details SC",
	  "seoKeywordEn": "LINK Customer Site Shop Details EN",
	  "seoKeywordTc": "LINK Customer Site Shop Details TC",
	  "seoKeywordSc": "LINK Customer Site Shop Details SC",
	  "seoDescriptionEn": "This is LINK Customer Site Shop Details. EN",
	  "seoDescriptionTc": "This is LINK Customer Site Shop Details. TC",
	  "seoDescriptionSc": "This is LINK Customer Site Shop Details. SC",
      "shopInfo":{  
         "shopId":17402,
         "propCode" : "csmos2"
         "shopCentreId" : 13,
         "shopType" : 1,
         "shopTypeTextTc":"BEAUTY, HEALTH & PERSONAL CARE",
         "shopTypeTextSc":"BEAUTY, HEALTH & PERSONAL CARE",
         "shopTypeTextEn":"BEAUTY, HEALTH & PERSONAL CARE",
         "shopNameEn":"Colourmix",
         "shopNameTc":"Colourmix TC",
         "shopNameSc":"Colourmix SC",
         "locationLabelEn":"Address",
         "locationLabelTc":"Address TC",
         "locationLabelSc":"Address SC",
         "locationEn":"No 20, Lei Moon House (High Block), Ap Lei Chau Estate, Southern",
         "locationTc":"港島南鴨脷洲邨利滿樓高座地下20號舖",
         "locationSc":"港岛南鸭脷洲邨利满楼高座地下20号舖",
         "telephoneLabelEn":"Telephone",
         "telephoneLabelTc":"Telephone TC",
         "telephoneLabelSc":"Telephone SC",
         "telephone":"2870 0139",
         "openingHoursLabelEn":"Opening Hours",
         "openingHoursLabelTc":"Opening Hours TC",
         "openingHoursLabelSc":"Opening Hours SC",
         "openingHoursEn":"Monday to Saturday: 2:00 pm - 8:30 pm<br />Sunday & Public Holiday: Closed",
         "openingHoursTc":"星期一 至 星期六: 2:00 pm - 8:30 pm<br />星期日 及 公眾假期: 休息<br />",
         "openingHoursSc":"星期一 至 星期六: 2:00 pm - 8:30 pm<br />星期日 及 公众假期: 休息",
         "websiteUrlLabelEn" : "Website",
         "websiteUrlLabelTc" : "Website TC",
         "websiteUrlLabelSc" : "Website SC",
         "websiteUrl":"",
         "specialIconUrlEn" : "",
         "specialIconUrlTc" : "",
         "specialIconUrlSc" : "",
         "goParkingThumbnailUrlEn":"/shopDetailGoParkingEn.png",
         "goParkingThumbnailUrlTc":"/shopDetailGoParkingTc.png",
         "goParkingThumbnailUrlSc":"/shopDetailGoParkingSc.png",
         "goShoppingThumbnailUrlEn":"/shopDetailGoShoppingEn.png",
         "goShoppingThumbnailUrlTc":"/shopDetailGoShoppingTc.png",
         "goShoppingThumbnailUrlSc":"/shopDetailGoShoppingSc.png",
         "goDiningThumbnailUrlEn":"/shopDetailGoDiningEn.png",
         "goDiningThumbnailUrlTc":"/shopDetailGoDiningTc.png",
         "goDiningThumbnailUrlSc":"/shopDetailGoDiningSc.png",
         "viewLocationThumbnailUrlEn":"/shopDetailViewLocationEn.png",
         "viewLocationThumbnailUrlTc":"/shopDetailViewLocationTc.png",
         "viewLocationThumbnailUrlSc":"/shopDetailViewLocationSc.png",
         "shopPhotos":[  
            {  
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopPhotoAltTextEn":"Colourmix",
               "shopPhotoAltTextTc":"Colourmix TC",
               "shopPhotoAltTextSc":"Colourmix SC"
            }
         ]
      },
      "appFeature":{  
         "appFeatureThumbnailUrlEn":"appFeature.png",
         "appFeatureThumbnailUrlTc":"appFeature.png",
         "appFeatureThumbnailUrlSc":"appFeature.png",
         "eQueuingThumbnailUrlEn":"eQueuing.png",
         "eQueuingThumbnailUrlTc":"eQueuing.png",
         "eQueuingThumbnailUrlSc":"eQueuing.png",
         "eCouponThumbnailUrlEn":"eCoupon.png",
         "eCouponThumbnailUrlTc":"eCoupon.png",
         "eCouponThumbnailUrlSc":"eCoupon.png",
         "eDirectoryThumbnailUrlEn":"eDirectory.png",
         "eDirectoryThumbnailUrlTc":"eDirectory.png",
         "eDirectoryThumbnailUrlSc":"eDirectory.png",
         "findMyCarThumbnailUrlEn":"findMyCar.png",
         "findMyCarThumbnailUrlTc":"findMyCar.png",
         "findMyCarThumbnailUrlSc":"findMyCar.png",
         "isEqueuing" : true,
         "isCoupon" : true,
         "isEDirectory" : true,
         "isFindMyCar" : true
      },
      "partner":[  
         {  
            "provider":"資料由XXX提供",
            "providerIcon":"http://xxx.com/images/providerIcon.png",
            "indexBox":[  
               {  
                  "titleEn":"食評",
                  "titleTc":"食評",
                  "titleSc":"食評",
                  "icon":"http://xxx.com/images/icon.png",
                  "digits":"20",
                  "url":"http://www.ipick.com"
               },
               {  
                  "titleEn":"餐牌",
                  "titleTc":"餐牌",
                  "titleSc":"餐牌",
                  "icon":"http://xxx.com/images/icon.png",
                  "digits":"20",
                  "url":"http://www.ipick.com"
               },
               {  
                  "titleEn":"相片",
                  "titleTc":"相片",
                  "titleSc":"相片",
                  "icon":"http://xxx.com/images/icon.png",
                  "digits":"20",
                  "url":"http://www.ipick.com"
               }
            ],
            "thumbnail":[  
               {  
                  "image":"http://xxx.com/images/thumbnail.png",
                  "url":"http://www.ipick.com"
               },
               {  
                  "image":"http://xxx.com/images/thumbnail.png",
                  "url":"http://www.ipick.com"
               },
               {  
                  "image":"http://xxx.com/images/thumbnail.png",
                  "url":"http://www.ipick.com"
               }
            ]
         }
      ],
      "extraField":{  
         "longExtraField":[  
            {  
               "fieldTitleTc":"Important notice 1",
               "fieldTitleSc":"Important notice 1",
               "fieldTitleEn":"Important notice 1",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":"",
               "noticeImageUrl":"",
               "noticeImageAlign":""
            },
            {  
               "fieldTitleTc":"Important notice 2",
               "fieldTitleSc":"Important notice 2",
               "fieldTitleEn":"Important notice 2",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":"",
               "noticeImageUrl":"",
               "noticeImageAlign":""
            },
            {  
               "fieldTitleTc":"Important notice 3",
               "fieldTitleSc":"Important notice 3",
               "fieldTitleEn":"Important notice 3",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":"",
               "noticeImageUrl":"Notice.png",
               "noticeImageAlign":"R"
            }
         ],
         "shortExtraField":[  
            {  
               "fieldTitleTc":"Terms & Conditions 1",
               "fieldTitleSc":"Terms & Conditions 1",
               "fieldTitleEn":"Terms & Conditions 1",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":""
            },
            {  
               "fieldTitleTc":"Terms & Conditions 2",
               "fieldTitleSc":"Terms & Conditions 2",
               "fieldTitleEn":"Terms & Conditions 2",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":""
            },
            {  
               "fieldTitleTc":"Terms & Conditions 3",
               "fieldTitleSc":"Terms & Conditions 3",
               "fieldTitleEn":"Terms & Conditions 3",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":""
            },
            {  
               "fieldTitleTc":"Appendix 1",
               "fieldTitleSc":"Appendix 1",
               "fieldTitleEn":"Appendix 1",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":""
            },
            {  
               "fieldTitleTc":"Appendix 2",
               "fieldTitleSc":"Appendix 2",
               "fieldTitleEn":"Appendix 2",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":""
            },
            {  
               "fieldTitleTc":"Appendix 3",
               "fieldTitleSc":"Appendix 3",
               "fieldTitleEn":"Appendix 3",
               "fieldContentTc":"",
               "fieldContentSc":"",
               "fieldContentEn":""
            }
         ]
      },
      "youWillAlsoLike":{  
         "titleEn":"You may also like En",
         "titleTc":"You may also like Tc",
         "titleSc":"You may also like Sc",
         "youWillAlsoLikeList":[  
            {  
               "shopId":17402,
               "shopCategoryTypeId" : 11,
               "shopCentreNameEn":"",
               "shopCentreNameTc":"",
               "shopCentreNameSc":"",
               "shopCategoryIconPath":[
	            	{
	            		"shopCategoryTypeId" : 1,
	            		"url": ""
	            	}
	            ],
               "shopNameEn":"Colourmix",
               "shopNameTc":"Colourmix TC",
               "shopNameSc":"Colourmix SC",
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopPhotoAltTextEn":"Dr. Soong Hung Hung Henry Dental Clinic",
               "shopPhotoAltTextTc":"宋鴻鴻牙科醫務所",
               "shopPhotoAltTextSc":"宋鸿鸿牙科医务所"
            },
            {  
               "shopId":17402,
               "shopCategoryTypeId" : 11,
               "shopCentreNameEn":"",
               "shopCentreNameTc":"",
               "shopCentreNameSc":"",
               "shopCategoryIconPath":[
	            	{
	            		"shopCategoryTypeId" : 1,
	            		"url": ""
	            	}
	            ],
               "shopNameEn":"Colourmix",
               "shopNameTc":"Colourmix TC",
               "shopNameSc":"Colourmix SC",
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopPhotoAltTextEn":"Dr. Soong Hung Hung Henry Dental Clinic",
               "shopPhotoAltTextTc":"宋鴻鴻牙科醫務所",
               "shopPhotoAltTextSc":"宋鸿鸿牙科医务所"
            }
         ],
         "viewAllEn":"View All En",
         "viewAllTc":"View All Tc",
         "viewAllSc":"View All Sc"
      }
   }
}
::SHOP_DETAILS_API_END::*/