package com.gt.projects.thelinkreit.config.database;

import java.beans.PropertyVetoException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySources(value = { @PropertySource({ "classpath:config/mssql.datasource.application.properties" }) })
@EnableTransactionManagement
@EnableJpaRepositories( //
basePackages = { "com.gt.projects.thelinkreit.repository.application" }, //
entityManagerFactoryRef = "applicationEntityManagerFactory", //
transactionManagerRef = "applicationTransactionManager" //
)
public class ApplicationDbConfig {

	@Autowired
	private Environment environment;

	@Bean(name = "applicationJpaVendorAdapter")
	public JpaVendorAdapter getJpaVendorAdapter() {
		AbstractJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setDatabasePlatform(this.environment.getProperty("hibernate.application.dialect"));
		jpaVendorAdapter.setShowSql(this.environment.getProperty("hibernate.application.show.sql", Boolean.class));
		return jpaVendorAdapter;
	}

	@Bean(name = "applicationEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean getEntityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setPackagesToScan("com.gt.projects.thelinkreit.entity.application");
		entityManagerFactory.setJpaVendorAdapter(this.getJpaVendorAdapter());
		entityManagerFactory.setDataSource(this.getDataSource());
		entityManagerFactory.setJpaPropertyMap(this.getJpaProperties());
		return entityManagerFactory;
	}

	@Bean(name = "applicationTransactionManager")
	public PlatformTransactionManager getTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(this.getEntityManagerFactory().getObject());
		return transactionManager;
	}

	// @Bean(name = "exceptionTranslation")
	// public PersistenceExceptionTranslationPostProcessor getExceptionTranslation() {
	// return new PersistenceExceptionTranslationPostProcessor();
	// }

	private Map<String, String> getJpaProperties() {
		Map<String, String> jpaProperties = new HashMap<String, String>();
		jpaProperties.put(org.hibernate.cfg.Environment.DIALECT, this.environment.getRequiredProperty("hibernate.application.dialect"));
		jpaProperties.put(org.hibernate.cfg.Environment.FORMAT_SQL, this.environment.getRequiredProperty("hibernate.application.format.sql"));
		jpaProperties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, this.environment.getRequiredProperty("hibernate.application.hbm2ddl.auto"));
		jpaProperties.put(org.hibernate.cfg.Environment.SHOW_SQL, this.environment.getRequiredProperty("hibernate.application.show.sql"));
		return jpaProperties;
	}

	@Bean(name = "applicationDataSource", destroyMethod = "close")
	public DataSource getDataSource() {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		try {
			dataSource.setDriverClass(this.environment.getProperty("dataSource.application.driverClass"));
		} catch (PropertyVetoException propertyVetoException) {
			propertyVetoException.printStackTrace();
		}

		dataSource.setJdbcUrl(this.environment.getProperty("dataSource.application.jdbcUrl"));
		dataSource.setUser(this.environment.getProperty("dataSource.application.user"));
		dataSource.setPassword(this.environment.getProperty("dataSource.application.password"));
		dataSource.setAutoCommitOnClose(this.environment.getProperty("dataSource.application.autoCommitOnClose", Boolean.class));
		dataSource.setCheckoutTimeout(this.environment.getProperty("dataSource.application.checkoutTimeout", Integer.class));
		dataSource.setMinPoolSize(this.environment.getProperty("dataSource.application.minPoolSize", Integer.class));
		dataSource.setMaxPoolSize(this.environment.getProperty("dataSource.application.maxPoolSize", Integer.class));
		dataSource.setInitialPoolSize(this.environment.getProperty("dataSource.application.initialPoolSize", Integer.class));
		dataSource.setMaxIdleTime(this.environment.getProperty("dataSource.application.maxIdleTime", Integer.class));
		dataSource.setAcquireIncrement(this.environment.getProperty("dataSource.application.acquireIncrement", Integer.class));
		dataSource.setIdleConnectionTestPeriod(this.environment.getProperty("dataSource.application.idleConnectionTestPeriod", Integer.class));
		dataSource.setPreferredTestQuery(this.environment.getProperty("dataSource.application.preferredTestQuery"));

		return dataSource;
	}
}
