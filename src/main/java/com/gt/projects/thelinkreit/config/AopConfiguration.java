//package com.gt.projects.thelinkreit.config;
//
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.aop.Advisor;
//import org.springframework.aop.aspectj.AspectJExpressionPointcut;
//import org.springframework.aop.support.DefaultPointcutAdvisor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.EnableAspectJAutoProxy;
//
//import com.gt.projects.thelinkreit.config.interceptor.PerfInterceptor;
//
//@Configuration
//@EnableAspectJAutoProxy
//@Aspect
//public class AopConfiguration {
//
//	@Bean(name = "perfInterceptor")
//	public PerfInterceptor perfInterceptor() throws Exception {
//	    return new PerfInterceptor();
//	}
//
//	@Bean
//    public Advisor jpaRepositoryAdvisor() throws Exception {
//        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
//        pointcut.setExpression("execution(* com.gt.projects.thelinkreit.controller.api.*.*(..))");
//        return new DefaultPointcutAdvisor(pointcut, perfInterceptor());
//    }
//
//}
