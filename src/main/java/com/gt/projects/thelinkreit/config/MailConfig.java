//package com.gt.projects.thelinkreit.config;
//
//import java.util.Properties;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;
//
//import com.gt.projects.thelinkreit.service.SystemService;
//
//@Configuration
//public class MailConfig {
//
//	@Autowired
//	private SystemService systemService;
//
//	@Bean
//	public JavaMailSender getJavaMailSender() {
//		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
//		javaMailSenderImpl.setDefaultEncoding("UTF-8");
//		javaMailSenderImpl.setHost(this.systemService.getProperty("mailHost"));
//		javaMailSenderImpl.setPort(this.systemService.getIntegerProperty("mailPort", 25));
//		javaMailSenderImpl.setUsername(this.systemService.getProperty("mailUsername"));
//		javaMailSenderImpl.setPassword(this.systemService.getProperty("mailPassword"));
//		Properties properties = new Properties();
//		properties.put("mail.smtp.auth", this.systemService.getBooleanProperty("mailSmtpAuth", false));
//		properties.put("mail.smtp.starttls.enable", this.systemService.getBooleanProperty("mailSmtpStarttlsEnable", false));
//		javaMailSenderImpl.setJavaMailProperties(properties);
//		return javaMailSenderImpl;
//	}
//
//}
