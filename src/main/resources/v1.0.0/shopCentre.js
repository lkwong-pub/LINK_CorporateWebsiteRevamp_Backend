/**
	 * 
	 * @api {get} http://www-dev.linkhk.com/linkweb/api/shopCentre/{id}      Shop Centre Details
	 * 
	 * @apiName Shop Centre Details
	 * @apiGroup Batch2-ShopCentre
	 * @apiVersion 1.0.0
	 * 
	  @apiDescription
		Retrieve Shop Centre Details Page content<br/> 
		Data included 12 components content:<br/>
		1. SEO Details<br/> 
		2. Banner Details<br/> 
		3. Your Journey Details<br/> 
		4. Promotions Details<br/> 
		5. Shop Centre Description<br/> 
		6. Shop Centre Info<br/> 
		7. Parking Vacancy<br/> 
		8. Dine Details<br/> 
		9. Shop Details<br/> 
		10. Market Details<br/> 
		11. The Nearest Mall<br/> 
		12. Sightseeing Nearby<br/> 
	  
	 * 
	 * @apiParam {Integer}	id		Shop Centre ID
	 * 
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
		@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
		@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
		@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
		@apiSuccess (Success 200) {Object} 	data.mallBanner		Shop Centre Details - Banner Object
		@apiSuccess (Success 200) {String}  data.mallBanner.bannerImgStandard 	Banner URL (Desktop)
       	@apiSuccess (Success 200) {String}  data.mallBanner.bannerImgNarrow		Banner URL (Mobile)
      	@apiSuccess (Success 200) {String}  data.mallBanner.propertyTypeEn		Property kind Type (EN)
		@apiSuccess (Success 200) {String}  data.mallBanner.propertyTypeTc		Property kind Type (TC)
      	@apiSuccess (Success 200) {String}	data.mallBanner.propertyTypeSc 		Property kind Type (SC)
      	@apiSuccess (Success 200) {String}	data.mallBanner.shopCentreNameEn	Shop Centre Name (EN)
      	@apiSuccess (Success 200) {String}	data.mallBanner.shopCentreNameTc	Shop Centre Name (TC)
      	@apiSuccess (Success 200) {String}	data.mallBanner.shopCentreNameSc	Shop Centre Name (SC)
		@apiSuccess (Success 200) {Object}	data.yourJourney	Shop Centre Details - Your Journey Object
		@apiSuccess (Success 200) {String}	data.yourJourney.happeningLabelEn	Label - Happening (EN)
		@apiSuccess (Success 200) {String}	data.yourJourney.happeningLabelTc	Label - Happening (TC)
		@apiSuccess (Success 200) {String}	data.yourJourney.happeningLabelSc	Label - Happening (SC)
		@apiSuccess (Success 200) {String}	data.yourJourney.parkLabelEn	Label - Parking (EN)
		@apiSuccess (Success 200) {String}	data.yourJourney.parkLabelTc	Label - Parking (TC)
		@apiSuccess (Success 200) {String}	data.yourJourney.parkLabelSc	Label - Parking (SC)
		@apiSuccess (Success 200) {String}	data.yourJourney.dineLabelEn	Label - Dining (EN)
		@apiSuccess (Success 200) {String}	data.yourJourney.dineLabelTc	Label - Dining (TC)
		@apiSuccess (Success 200) {String}	data.yourJourney.dineLabelSc	Label - Dining (SC)
		@apiSuccess (Success 200) {String}	data.yourJourney.shopLabelEn	Label - Shopping (EN)
		@apiSuccess (Success 200) {String}	data.yourJourney.shopLabelTc	Label - Shopping (TC)
		@apiSuccess (Success 200) {String}	data.yourJourney.shopLabelSC	Label - Shopping (SC)
		@apiSuccess (Success 200) {String}	data.yourJourney.marketLabelEn	Label - Market (EN)
		@apiSuccess (Success 200) {String}	data.yourJourney.marketLabelTc	Label - Market (TC)
		@apiSuccess (Success 200) {String}	data.yourJourney.marketLabelSc	Label - Market (SC)
		@apiSuccess (Success 200) {String}	data.yourJourney.happeningThumbnailUrl	Happening Icon URL	
		@apiSuccess (Success 200) {String}	data.yourJourney.parkThumbnailUrl	Parking Icon URL
		@apiSuccess (Success 200) {String}	data.yourJourney.dineThumbnailUrl	Dining Icon URL
		@apiSuccess (Success 200) {String}	data.yourJourney.shopThumbnailUrl	Shopping Icon URL
		@apiSuccess (Success 200) {String}	data.yourJourney.marketThumbnailUrl	Market Icon URL
		@apiSuccess (Success 200) {Boolean}	data.yourJourney.isHappening	Is Happening
        @apiSuccess (Success 200) {Boolean}	data.yourJourney.isPark		Is Parking
        @apiSuccess (Success 200) {Boolean}	data.yourJourney.isDine		Is Dining
        @apiSuccess (Success 200) {Boolean}	data.yourJourney.isShop		Is Shopping
        @apiSuccess (Success 200) {Boolean}	data.yourJourney.isMarket	Is Market
		@apiSuccess (Success 200) {Object[]}	data.promotions		Shop Centre Details - Promotion Object Array
		@apiSuccess (Success 200) {Integer}	data.promotions.id					Promotion ID
		@apiSuccess (Success 200) {String}	data.promotions.promotionTitleEn	Promotion Title (EN)
		@apiSuccess (Success 200) {String}	data.promotions.promotionTitleTc	Promotion Title (TC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionTitleSc	Promotion Title (SC)
		@apiSuccess (Success 200) {String}	data.promotions.shopCentreNameEn	Shop Centre Name (EN)
		@apiSuccess (Success 200) {String}	data.promotions.shopCentreNameTc	Shop Centre Name (TC)
		@apiSuccess (Success 200) {String}	data.promotions.shopCentreNameSc	Shop Centre Name (SC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionDateEn		Promotion Date (EN)
		@apiSuccess (Success 200) {String}	data.promotions.promotionDateTc		Promotion Date (TC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionDateSc		Promotion Date (SC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionTimeEn		Promotion Time (EN)
		@apiSuccess (Success 200) {String}	data.promotions.promotionTimeTc		Promotion Time (TC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionTimeSc		Promotion Time (SC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionImageUrlEn		Promotion Image URL (EN)
		@apiSuccess (Success 200) {String}	data.promotions.promotionImageUrlTc		Promotion Image URL (TC)
		@apiSuccess (Success 200) {String}	data.promotions.promotionImageUrlSc		Promotion Image URL (SC)
		@apiSuccess (Success 200) {Object}	data.mallDescription 	Shop Centre Details - Description
		@apiSuccess (Success 200) {String}	data.mallDescription.descriptionTitleEn		Description Title (EN)
		@apiSuccess (Success 200) {String}	data.mallDescription.descriptionTitleTc		Description Title (TC)
		@apiSuccess (Success 200) {String}	data.mallDescription.descriptionTitleSc		Description Title (SC)
		@apiSuccess (Success 200) {String}	data.mallDescription.descriptionContentEn		Description (EN)
		@apiSuccess (Success 200) {String}	data.mallDescription.descriptionContentTc		Description (TC)
		@apiSuccess (Success 200) {String}	data.mallDescription.descriptionContentSc		Description (SC)
		@apiSuccess (Success 200) {Object}	data.mallInfo		Shop Centre Details - Shop Centre Info
 		@apiSuccess (Success 200) {Integer}	data.mallInfo.shopCentreId		Shop Centre ID
		@apiSuccess (Success 200) {Integer}	data.mallInfo.propertyTypeId	Property Type ID
		@apiSuccess (Success 200) {String}	data.mallInfo.addressLabelEn	Address Label (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.addressLabelTc	Address Label (TC)
		@apiSuccess (Success 200) {String}	data.mallInfo.addressLabelSc	Address Label (SC)
		@apiSuccess (Success 200) {String}	data.mallInfo.addressEn		Address (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.addressTc		Address (TC)
		@apiSuccess (Success 200) {String}	data.mallInfo.addressSc		Address (SC)
		@apiSuccess (Success 200) {String}	data.mallInfo.customerHotLineLabelEn	Customer Hotline Label (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.customerHotLineLabelTc	Customer Hotline Label (TC)
		@apiSuccess (Success 200) {String}	data.mallInfo.customerHotLineLabelSc	Customer Hotline Label (SC)
		@apiSuccess (Success 200) {String}	data.mallInfo.customerHotLine		Customer Hotline
		@apiSuccess (Success 200) {String}	data.mallInfo.carParkEnquiryLabelEn		Carpark Enquiry Label (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.carParkEnquiryLabelTc		Carpark Enquiry Label (TC)
		@apiSuccess (Success 200) {String}	data.mallInfo.carParkEnquiryLabelSc		Carpark Enquiry Label (SC)
		@apiSuccess (Success 200) {String}	data.mallInfo.carParkEnquiry		Carpark Enquiry
		@apiSuccess (Success 200) {String}	data.mallInfo.leasingEnquiryLabelEn		Leasing Enquiry Label (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.leasingEnquiryLabelTc		Leasing Enquiry Label (TC)
		@apiSuccess (Success 200) {String}	data.mallInfo.leasingEnquiryLabelSc		Leasing Enquiry Label (SC)
		@apiSuccess (Success 200) {String}	data.mallInfo.leasingEnquiry	Leasing Enquiry
 		@apiSuccess (Success 200) {String}	data.mallInfo.websiteUrlLabelEn		Website URL Label (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.websiteUrlLabelTc		Website URL Label (TC)
		@apiSuccess (Success 200) {String}	data.mallInfo.websiteUrlLabelSc		Website URL Label (SC)
		@apiSuccess (Success 200) {String}	data.mallInfo.websiteUrl	Website URL
		@apiSuccess (Success 200) {Object}	data.mallInfo.specialService	Special Service Object
		@apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceNoticeEn	Service Notice (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceNoticeTc	Service Notice (TC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceNoticeSc	Service Notice (SC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceNoticeDescriptionEn	Service Notice Description (EN)
		@apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceNoticeDescriptionTc	Service Notice Description (TC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceNoticeDescriptionSc	Service Notice Description (SC)
        @apiSuccess (Success 200) {Object[]}	data.mallInfo.specialService.serviceList	Special Service Object Array
        @apiSuccess (Success 200) {Integer}	data.mallInfo.specialService.serviceList.shopCentreServiceTypeId	Special Service Type ID
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceTypeNameEn	Special Service Type Name (EN)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceTypeNameTc	Special Service Type Name (TC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceTypeNameSc	Special Service Type Name (SC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceThumbnailPathEn	Special Service Type Thumbnail URL (EN)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceThumbnailPathTc	Special Service Type Thumbnail URL (TC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceThumbnailPathSc	Special Service Type Thumbnail URL (SC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceThumbmailAltTextEn	Special Service Type Thumbnail Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceThumbnailAltTextTc	Special Service Type Thumbnail Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.mallInfo.specialService.serviceList.shopCentreServiceThumbnailAltTextSc	Special Service Type Thumbnail Alt Text (SC)
		@apiSuccess (Success 200) {Object[]}	data.mallInfo.mallThumbnails	Shop Centre Thumbnail Object Array
		@apiSuccess (Success 200) {String}	data.mallInfo.mallThumbnails.thumbnail	Shop Centre Thumbnail URL
        @apiSuccess (Success 200) {String}	data.mallInfo.mallThumbnails.thumbnailAltTextEn		Shop Centre Thumbnail Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.mallInfo.mallThumbnails.thumbnailAltTextTc		Shop Centre Thumbnail Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.mallInfo.mallThumbnails.thumbnailAltTextSc		Shop Centre Thumbnail Alt Text (SC)
        @apiSuccess (Success 200) {Object}	data.parkingVacancy		Shop Centre Details - Parking Vacancy Object
        @apiSuccess (Success 200) {String}	data.parkingVacancy.facilityKey		Parking Facility Key
        @apiSuccess (Success 200) {String}	data.parkingVacancy.carparkFacilityNameEn	Parking Faility Name (EN)
        @apiSuccess (Success 200) {String}	data.parkingVacancy.carparkFacilityNameTc	Parking Faility Name (TC)
        @apiSuccess (Success 200) {String}	data.parkingVacancy.carparkFacilityNameSc	Parking Faility Name (SC)
        @apiSuccess (Success 200) {String}	data.parkingVacancy.thumbnailPath		Parking Faility Thumbnail URL
        @apiSuccess (Success 200) {Integer}	data.parkingVacancy.availableCarParkSpace	Available Carpark Space
        @apiSuccess (Success 200) {Object}	data.dine		Shop Centre Details - Dining
        @apiSuccess (Success 200) {String}	data.dine.titleEn	Dining Title (EN)
        @apiSuccess (Success 200) {String}	data.dine.titleTc	Dining Title (TC)
        @apiSuccess (Success 200) {String}	data.dine.titleSc	Dining Title (SC)
        @apiSuccess (Success 200) {String}	data.dine.thumbnailUrl		Dining Thumbnail
        @apiSuccess (Success 200) {String}	data.dine.descriptionEn		Dining Description (EN)
        @apiSuccess (Success 200) {String}	data.dine.descriptionTc		Dining Description (TC)
        @apiSuccess (Success 200) {String}	data.dine.descriptionSc		Dining Description (SC)
        @apiSuccess (Success 200) {Object[]}	data.dine.dineList		Dining List Object Array
        @apiSuccess (Success 200) {Integer}	data.dine.dineList.shopId	Shop ID
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopNameEn	Shop Name (EN)
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopNameTc	Shop Name (TC)
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopNameSc	Shop Name (SC)
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopPhotoPath		Shop Image URL
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopPhotoAltTextEn	Shop Image Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopPhotoAltTextTc	Shop Image Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.dine.dineList.shopPhotoAltTextSc	Shop Image Alt Text (SC)
        @apiSuccess (Success 200) {Object}	data.shop		Shop Centre Details - Shopping
        @apiSuccess (Success 200) {String}	data.shop.titleEn	Shopping Title (EN)
        @apiSuccess (Success 200) {String}	data.shop.titleTc	Shopping Title (TC)
        @apiSuccess (Success 200) {String}	data.shop.titleSc	Shopping Title (SC)
        @apiSuccess (Success 200) {String}	data.shop.thumbnailUrl		Shopping Thumbnail
        @apiSuccess (Success 200) {String}	data.shop.descriptionEn		Shopping Description (EN)
        @apiSuccess (Success 200) {String}	data.shop.descriptionTc		Shopping Description (TC)
        @apiSuccess (Success 200) {String}	data.shop.descriptionSc		Shopping Description (SC)
        @apiSuccess (Success 200) {Object[]}	data.shop.shopList		Shopping List Object Array
        @apiSuccess (Success 200) {Integer}	data.shop.shopList.shopId	Shop ID
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopNameEn	Shop Name (EN)
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopNameTc	Shop Name (TC)
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopNameSc	Shop Name (SC)
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopPhotoPath		Shop Image URL
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopPhotoAltTextEn	Shop Image Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopPhotoAltTextTc	Shop Image Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.shop.shopList.shopPhotoAltTextSc	Shop Image Alt Text (SC)
        @apiSuccess (Success 200) {Object}	data.market		Shop Centre Details - Market
        @apiSuccess (Success 200) {String}	data.market.titleEn	Market Title (EN)
        @apiSuccess (Success 200) {String}	data.market.titleTc	Market Title (TC)
        @apiSuccess (Success 200) {String}	data.market.titleSc	Market Title (SC)
        @apiSuccess (Success 200) {String}	data.market.thumbnailUrl		Market Thumbnail
        @apiSuccess (Success 200) {String}	data.market.descriptionEn		Market Description (EN)
        @apiSuccess (Success 200) {String}	data.market.descriptionTc		Market Description (TC)
        @apiSuccess (Success 200) {String}	data.market.descriptionSc		Market Description (SC)
        @apiSuccess (Success 200) {Object}	data.market.market		Market - Market Shop Object
 		@apiSuccess (Success 200) {Integer}	data.market.market.shopId	Shop ID
        @apiSuccess (Success 200) {String}	data.market.market.marketCode	Market Code
        @apiSuccess (Success 200) {String}	data.market.market.marketNameEn	Market Name (EN)
        @apiSuccess (Success 200) {String}	data.market.market.marketNameTc	Market Name (TC)
        @apiSuccess (Success 200) {String}	data.market.market.marketNameSc	Market Name (SC)
        @apiSuccess (Success 200) {String}	data.market.market.marketPhotoPath		Market Image URL
        @apiSuccess (Success 200) {String}	data.market.market.marketPhotoAltTextEn	Market Image Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.market.market.marketPhotoAltTextTc	Market Image Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.market.market.marketPhotoAltTextSc	Market Image Alt Text (SC)
        @apiSuccess (Success 200) {Object}	data.market.recipe		Market - Recipe Object 
        @apiSuccess (Success 200) {Integer}	data.market.recipe.recipeId	Market ID
        @apiSuccess (Success 200) {String}	data.market.recipe.recipeNameEn	Market Name (EN)
        @apiSuccess (Success 200) {String}	data.market.recipe.recipeNameTc	Market Name (TC)
        @apiSuccess (Success 200) {String}	data.market.recipe.recipeNameSc	Market Name (SC)
        @apiSuccess (Success 200) {String}	data.market.recipe.recipePhotoPath		Market Image URL
        @apiSuccess (Success 200) {String}	data.market.recipe.recipePhotoAltTextEn	Market Image Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.market.recipe.recipePhotoAltTextTc	Market Image Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.market.recipe.recipePhotoAltTextSc	Market Image Alt Text (SC)
        @apiSuccess (Success 200) {String}	data.market.marketNews		Market News
        @apiSuccess (Success 200) {Object}	data.theNearestMall		Shop Centre Details - The Nearest Mall
		@apiSuccess (Success 200) {String}	data.theNearestMall.titleEn		The Nearest Mall Title (EN)
		@apiSuccess (Success 200) {String}	data.theNearestMall.titleTc		The Nearest Mall Title (TC)
		@apiSuccess (Success 200) {String}	data.theNearestMall.titleSc		The Nearest Mall Title (SC)
		@apiSuccess (Success 200) {Object[]}	data.theNearestMall.theNearestMallList	The Nearest Mall List Object Array
		@apiSuccess (Success 200) {Integer}	data.theNearestMall.theNearestMallList.shopCentreId		Shop Centre ID
 		@apiSuccess (Success 200) {Integer}	data.theNearestMall.theNearestMallList.propCode		Shop Centre Property Code
		@apiSuccess (Success 200) {String}	data.theNearestMall.theNearestMallList.shopCentreNameEn		Shop Centre Name (EN)
		@apiSuccess (Success 200) {String}	data.theNearestMall.theNearestMallList.shopCentreNameTc		Shop Centre Name (TC)
		@apiSuccess (Success 200) {String}	data.theNearestMall.theNearestMallList.shopCentreNameSc		Shop Centre Name (SC)
		@apiSuccess (Success 200) {String}	data.theNearestMall.theNearestMallList.thumbnailPath		Shop Centre Thumbnail URL
        @apiSuccess (Success 200) {Object}	data.sightseeingNearby		Shop Centre Details - Nearby Sightseeing  Object
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.titleEn		Nearby Sightseeing Title (EN)
      	@apiSuccess (Success 200) {String}	data.sightseeingNearby.titleTc		Nearby Sightseeing Title (TC)
      	@apiSuccess (Success 200) {String}	data.sightseeingNearby.titleSc		Nearby Sightseeing Title (SC)
      	@apiSuccess (Success 200) {Object[]}	data.sightseeingNearby.sightseeings		Nearby Sightseeing List Object Array
      	@apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteNameEn	Site Name (EN)
      	@apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteNameTc	Site Name (TC)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteNameSc	Site Name (SC)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteDescriptionEn	Site Description (EN)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteDescriptionTc	Site Description (TC)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteDescriptionSc	Site Description (SC)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteImagePath	Site Image URL 
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteImageAltTextEn	Site Image Alt Text (EN)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteImageAltTextTc	Site Image Alt Text (TC)
        @apiSuccess (Success 200) {String}	data.sightseeingNearby.sightseeings.siteImageAltTextSc	Site Image Alt Text (SC)
      	@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::SHOP_CENTRE_DETAILS_API_START::
 {
   "error": "0000",
   "errorMessage": "",
   "errorCode": "",
   "data": {
     "seoTitleEn": "Lok Fu Place",
     "seoTitleTc": "樂富廣場",
     "seoTitleSc": "乐富广场",
     "seoKeywordEn": "Lok Fu Place",
     "seoKeywordTc": "樂富廣場",
     "seoKeywordSc": "乐富广场",
     "seoDescriptionEn": "Lok Fu Place",
     "seoDescriptionTc": "樂富廣場",
     "seoDescriptionSc": "乐富广场",
     "mallBanner": {
       "bannerImgStandard": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/lok_fu_plaza_banner.png",
       "bannerImgNarrow": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/lok_fu_plaza_narrow.png",
       "propertyTypeEn": "Destination",
       "propertyTypeTc": "購物地標",
       "propertyTypeSc": "购物地标",
       "shopCentreNameEn": "Lok Fu Place",
       "shopCentreNameTc": "樂富廣場",
       "shopCentreNameSc": "乐富广场"
     },
     "yourJourney": {
       "happeningLabelEn": "HAPPENINGS",
       "happeningLabelTc": "活動及推廣",
       "happeningLabelSc": "活动及推广",
       "parkLabelEn": "PARK",
       "parkLabelTc": "泊車",
       "parkLabelSc": "停车",
       "dineLabelEn": "DINE",
       "dineLabelTc": "美食",
       "dineLabelSc": "美食",
       "shopLabelEn": "SHOP",
       "shopLabelTc": "購物",
       "shopLabelSc": "购物",
       "marketLabelEn": "FRESH MARKET",
       "marketLabelTc": "街巿",
       "marketLabelSc": "街巿",
       "happeningThumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_happenings.svg",
       "parkThumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_park.svg",
       "dineThumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_dine.svg",
       "shopThumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_shop.svg",
       "marketThumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_fresh_market.svg",
       "isHappening": false,
       "isPark": true,
       "isDine": true,
       "isShop": true,
       "isMarket": true
     },
     "promotions": null,
     "mallDescription": {
       "descriptionTitleEn": "An Easily-Accessible Lifestyle Destination in Central Kowloon",
       "descriptionTitleTc": "在九龍中部一個易於訪問的生活方式目的地",
       "descriptionTitleSc": "在九龙中部一个易於访问的生活方式目的地",
       "descriptionContentEn": "Lok Fu Place is a leisure destination in bustling Central Kowloon that has preserved unique aspects of the community’s culture. With an anchor department store, a medical centre, and a variety of shops and restaurants, Lok Fu Place has become the place to go for everything from shopping for a fashionable, healthy and leisurely lifestyle, dining, and picking up daily necessities and fresh groceries. Shopping highlights include well-known sportswear and shoe shops, jewelry, wine and cigars. Dining highlights include a popular noodle restaurant, a beloved bakery, and Japanese restaurants. Disabled provisions and a baby-care room cater to the needs of all shoppers. ",
       "descriptionContentTc": "樂富廣場位於繁華的中九龍，商場在提升服務質素之餘，亦致力保存社區獨特的文化。 引入大型日式百貨公司及各類商店食肆，使樂富廣場成為市民選購日用品和新鮮食品、享受美食和悠閒購物的必到消費熱點。 \n\n樂富廣場內的購物好去處包括知名運動專門店和鞋店、珠寶、紅酒及雪茄專賣店。\n\n美食選擇亦多不勝數，如熱門麵店、馳名餅家和日本餐廳。商場內設有傷健配套和育嬰室，滿足顧客的不同需要。",
       "descriptionContentSc": "乐富广场位于繁华的中九龙，在提升服务质素之余，亦致力报讯社区的独特文化。于2011年完成达4.23亿的优化工程后，引入大型日式百货公司，使乐富广场成为市民选购日用品和新鲜食品、享受美食和休闲购物的必到消费热点。\n\n乐富广场内的购物好去处包括知名运动员专门店和谐店、珠宝、洋酒及雪茄专卖店。\n美食选择多不胜数，如热门面馆、驰名中式饼店和日本餐厅。商场设有伤健配套和育婴室，满足顾客的不同需要。"
     },
     "mallInfo": {
       "shopCentreId": 7,
       "propertyTypeId": 1,
       "addressLabelEn": "Address",
       "addressLabelTc": "地址",
       "addressLabelSc": "地址",
       "addressEn": "Lok Fu Place 198 Junction Road Wang Tau Hom,  Kowloon ",
       "addressTc": "九龍橫頭磡聯合道198號樂富廣場",
       "addressSc": "九龙横头磡联合道198号乐富广场",
       "customerHotLineLabelEn": "Customer Hotline",
       "customerHotLineLabelTc": "客戶服務熱線",
       "customerHotLineLabelSc": "客户服务热线",
       "customerHotLine": "2338 7781",
       "carParkEnquiryLabelEn": "Car Park Enquiry",
       "carParkEnquiryLabelTc": "停車場查詢",
       "carParkEnquiryLabelSc": "停车场查询",
       "carParkEnquiry": "2626 7777",
       "leasingEnquiryLabelEn": "Leasing Enquiry",
       "leasingEnquiryLabelTc": "租務查詢",
       "leasingEnquiryLabelSc": "租务查询",
       "leasingEnquiry": "2175 1747 ",
       "websiteUrlLabelEn" : "Website",
       "websiteUrlLabelTc" : "Website TC",
       "websiteUrlLabelSc" : "Website SC",
       "websiteUrl":"",
       websiteUrl
       "specialServiceLabelEn": "Special Services",
       "specialServiceLabelTc": "特別服務",
       "specialServiceLabelSc": "特别服务",
       "specialService": {
         "serviceNoticeTc": "特別服務",
         "serviceNoticeSc": "特别服务",
         "serviceNoticeEn": "Special Services",
         "serviceList": [
           {
             "shopCentreServiceTypeId": 26,
             "shopCentreServiceTypeNameEn": "Bridal Dressing Suite",
             "shopCentreServiceTypeNameTc": "新娘化妝間",
             "shopCentreServiceTypeNameSc": "新娘化妆间",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/bridal_dressing_suite.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/bridal_dressing_suite.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/bridal_dressing_suite.svg",
             "shopCentreServiceThumbnailAltTextEn": "Bridal Dressing Suite",
             "shopCentreServiceThumbnailAltTextTc": "新娘化妝間",
             "shopCentreServiceThumbnailAltTextSc": "新娘化妆间",
             "isCurrentShopCentreService": false
           },
           {
             "shopCentreServiceTypeId": 1,
             "shopCentreServiceTypeNameEn": "LinkWiFi",
             "shopCentreServiceTypeNameTc": "LinkWiFi",
             "shopCentreServiceTypeNameSc": "LinkWiFi",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/link_wifi.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/link_wifi.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/link_wifi.svg",
             "shopCentreServiceThumbnailAltTextEn": "LinkWiFi",
             "shopCentreServiceThumbnailAltTextTc": "LinkWiFi",
             "shopCentreServiceThumbnailAltTextSc": "LinkWiFi",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 2,
             "shopCentreServiceTypeNameEn": "Pet Facilities",
             "shopCentreServiceTypeNameTc": "寵物設施",
             "shopCentreServiceTypeNameSc": "宠物设施",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/pet_facilities.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/pet_facilities.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/pet_facilities.svg",
             "shopCentreServiceThumbnailAltTextEn": "Pet Facilities",
             "shopCentreServiceThumbnailAltTextTc": "寵物設施",
             "shopCentreServiceThumbnailAltTextSc": "宠物设施",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 4,
             "shopCentreServiceTypeNameEn": "Bike / Bike Tools Lending",
             "shopCentreServiceTypeNameTc": "單車/單車工具借用",
             "shopCentreServiceTypeNameSc": "自行车/自行车工具借用",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/bike_tool_lending.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/bike_tool_lending.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/bike_tool_lending.svg",
             "shopCentreServiceThumbnailAltTextEn": "Bike / Bike Tools Lending",
             "shopCentreServiceThumbnailAltTextTc": "單車/單車工具借用",
             "shopCentreServiceThumbnailAltTextSc": "自行车/自行车工具借用",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 5,
             "shopCentreServiceTypeNameEn": "Parking Redemption",
             "shopCentreServiceTypeNameTc": "泊車優惠",
             "shopCentreServiceTypeNameSc": "泊车优惠",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/parking_redemption.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/parking_redemption.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/parking_redemption.svg",
             "shopCentreServiceThumbnailAltTextEn": "Parking Redemption",
             "shopCentreServiceThumbnailAltTextTc": "泊車優惠",
             "shopCentreServiceThumbnailAltTextSc": "泊车优惠",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 6,
             "shopCentreServiceTypeNameEn": "Children's Playground",
             "shopCentreServiceTypeNameTc": "兒童遊樂場",
             "shopCentreServiceTypeNameSc": "儿童游乐场",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/childrens_playground.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/childrens_playground.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/childrens_playground.svg",
             "shopCentreServiceThumbnailAltTextEn": "Children's Playground",
             "shopCentreServiceThumbnailAltTextTc": "兒童遊樂場",
             "shopCentreServiceThumbnailAltTextSc": "儿童游乐场",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 7,
             "shopCentreServiceTypeNameEn": "Lockers",
             "shopCentreServiceTypeNameTc": "儲物櫃",
             "shopCentreServiceTypeNameSc": "存物柜",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/lockers.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/lockers.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/lockers.svg",
             "shopCentreServiceThumbnailAltTextEn": "Lockers",
             "shopCentreServiceThumbnailAltTextTc": "儲物櫃",
             "shopCentreServiceThumbnailAltTextSc": "存物柜",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 8,
             "shopCentreServiceTypeNameEn": "Electric Vehicle Charging System",
             "shopCentreServiceTypeNameTc": "電動車充電站",
             "shopCentreServiceTypeNameSc": "电动汽车充电站",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/electric_vehicle_charging_system.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/electric_vehicle_charging_system.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/electric_vehicle_charging_system.svg",
             "shopCentreServiceThumbnailAltTextEn": "Electric Vehicle Charging System",
             "shopCentreServiceThumbnailAltTextTc": "電動車充電站",
             "shopCentreServiceThumbnailAltTextSc": "电动汽车充电站",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 9,
             "shopCentreServiceTypeNameEn": "Ambassador",
             "shopCentreServiceTypeNameTc": "親善大使",
             "shopCentreServiceTypeNameSc": "亲善大使",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/ambassador.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/ambassador.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/ambassador.svg",
             "shopCentreServiceThumbnailAltTextEn": "Ambassador",
             "shopCentreServiceThumbnailAltTextTc": "親善大使",
             "shopCentreServiceThumbnailAltTextSc": "亲善大使",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 10,
             "shopCentreServiceTypeNameEn": "Lost and Found",
             "shopCentreServiceTypeNameTc": "失物認領",
             "shopCentreServiceTypeNameSc": "失物招领",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/lost_and_found.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/lost_and_found.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/lost_and_found.svg",
             "shopCentreServiceThumbnailAltTextEn": "Lost and Found",
             "shopCentreServiceThumbnailAltTextTc": "失物認領",
             "shopCentreServiceThumbnailAltTextSc": "失物招领",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 11,
             "shopCentreServiceTypeNameEn": "Umbrella Lending",
             "shopCentreServiceTypeNameTc": "雨傘借用",
             "shopCentreServiceTypeNameSc": "雨伞借用",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/umbrella_lending.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/umbrella_lending.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/umbrella_lending.svg",
             "shopCentreServiceThumbnailAltTextEn": "Umbrella Lending",
             "shopCentreServiceThumbnailAltTextTc": "雨傘借用",
             "shopCentreServiceThumbnailAltTextSc": "雨伞借用",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 12,
             "shopCentreServiceTypeNameEn": "Wheelchair Lending",
             "shopCentreServiceTypeNameTc": "輪椅借用",
             "shopCentreServiceTypeNameSc": "轮椅借用",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/wheelchair_lending.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/wheelchair_lending.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/wheelchair_lending.svg",
             "shopCentreServiceThumbnailAltTextEn": "Wheelchair Lending",
             "shopCentreServiceThumbnailAltTextTc": "輪椅借用",
             "shopCentreServiceThumbnailAltTextSc": "轮椅借用",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 13,
             "shopCentreServiceTypeNameEn": "Baby Care Room",
             "shopCentreServiceTypeNameTc": "嬰兒護理室",
             "shopCentreServiceTypeNameSc": "婴儿护理室",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/baby_care_room.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/baby_care_room.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/baby_care_room.svg",
             "shopCentreServiceThumbnailAltTextEn": "Baby Care Room",
             "shopCentreServiceThumbnailAltTextTc": "嬰兒護理室",
             "shopCentreServiceThumbnailAltTextSc": "婴儿护理室",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 14,
             "shopCentreServiceTypeNameEn": "Baby Stroller Lending",
             "shopCentreServiceTypeNameTc": "嬰兒手推車借用",
             "shopCentreServiceTypeNameSc": "婴儿手推车借用",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/baby_stroller_lending.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/baby_stroller_lending.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/baby_stroller_lending.svg",
             "shopCentreServiceThumbnailAltTextEn": "Baby Stroller Lending",
             "shopCentreServiceThumbnailAltTextTc": "嬰兒手推車借用",
             "shopCentreServiceThumbnailAltTextSc": "婴儿手推车借用",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 15,
             "shopCentreServiceTypeNameEn": "Health Care",
             "shopCentreServiceTypeNameTc": "救傷設備",
             "shopCentreServiceTypeNameSc": "救伤设备",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/health_care.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/health_care.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/health_care.svg",
             "shopCentreServiceThumbnailAltTextEn": "Health Care",
             "shopCentreServiceThumbnailAltTextTc": "救傷設備",
             "shopCentreServiceThumbnailAltTextSc": "救伤设备",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 17,
             "shopCentreServiceTypeNameEn": "Jumper Cable",
             "shopCentreServiceTypeNameTc": "汽車充電線",
             "shopCentreServiceTypeNameSc": "汽车充电线",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/jumper_cable.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/jumper_cable.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/jumper_cable.svg",
             "shopCentreServiceThumbnailAltTextEn": "Jumper Cable",
             "shopCentreServiceThumbnailAltTextTc": "汽車充電線",
             "shopCentreServiceThumbnailAltTextSc": "汽车充电线",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 18,
             "shopCentreServiceTypeNameEn": "Complimentary Raincoat",
             "shopCentreServiceTypeNameTc": "雨衣供應",
             "shopCentreServiceTypeNameSc": "雨衣供应",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/complimentary_raincoat.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/complimentary_raincoat.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/complimentary_raincoat.svg",
             "shopCentreServiceThumbnailAltTextEn": "Complimentary Raincoat",
             "shopCentreServiceThumbnailAltTextTc": "雨衣供應",
             "shopCentreServiceThumbnailAltTextSc": "雨衣供应",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 19,
             "shopCentreServiceTypeNameEn": "Complimentary Hygiene Mask",
             "shopCentreServiceTypeNameTc": "衛生口罩供應",
             "shopCentreServiceTypeNameSc": "卫生口罩供应",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/complimentary_hygiene_mask.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/complimentary_hygiene_mask.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/complimentary_hygiene_mask.svg",
             "shopCentreServiceThumbnailAltTextEn": "Complimentary Hygiene Mask",
             "shopCentreServiceThumbnailAltTextTc": "衛生口罩供應",
             "shopCentreServiceThumbnailAltTextSc": "卫生口罩供应",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 20,
             "shopCentreServiceTypeNameEn": "Weather Information",
             "shopCentreServiceTypeNameTc": "天氣資訊",
             "shopCentreServiceTypeNameSc": "天气资讯",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/weather_information.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/weather_information.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/weather_information.svg",
             "shopCentreServiceThumbnailAltTextEn": "Weather Information",
             "shopCentreServiceThumbnailAltTextTc": "天氣資訊",
             "shopCentreServiceThumbnailAltTextSc": "天气资讯",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 21,
             "shopCentreServiceTypeNameEn": "Leasing Enquiry",
             "shopCentreServiceTypeNameTc": "租務查詢",
             "shopCentreServiceTypeNameSc": "租务查询",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/leasing_enquiry.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/leasing_enquiry.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/leasing_enquiry.svg",
             "shopCentreServiceThumbnailAltTextEn": "Leasing Enquiry",
             "shopCentreServiceThumbnailAltTextTc": "租務查詢",
             "shopCentreServiceThumbnailAltTextSc": "租务查询",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 22,
             "shopCentreServiceTypeNameEn": "Mobile Battery Recharger",
             "shopCentreServiceTypeNameTc": "手提電話電池充電服務",
             "shopCentreServiceTypeNameSc": "手机电池充电服务",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/mobile_battery_recharger.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/mobile_battery_recharger.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/mobile_battery_recharger.svg",
             "shopCentreServiceThumbnailAltTextEn": "Mobile Battery Recharger",
             "shopCentreServiceThumbnailAltTextTc": "手提電話電池充電服務",
             "shopCentreServiceThumbnailAltTextSc": "手机电池充电服务",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 23,
             "shopCentreServiceTypeNameEn": "Photo Copying Service",
             "shopCentreServiceTypeNameTc": "影印服務",
             "shopCentreServiceTypeNameSc": "复印服务",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/photo_copying_service.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/photo_copying_service.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/photo_copying_service.svg",
             "shopCentreServiceThumbnailAltTextEn": "Photo Copying Service",
             "shopCentreServiceThumbnailAltTextTc": "影印服務",
             "shopCentreServiceThumbnailAltTextSc": "复印服务",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 24,
             "shopCentreServiceTypeNameEn": "Local Outgoing Facsimile",
             "shopCentreServiceTypeNameTc": "本地傳真服務",
             "shopCentreServiceTypeNameSc": "本地传真服务",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/local_outgoing_facsimile.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/local_outgoing_facsimile.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/local_outgoing_facsimile.svg",
             "shopCentreServiceThumbnailAltTextEn": "Local Outgoing Facsimile",
             "shopCentreServiceThumbnailAltTextTc": "本地傳真服務",
             "shopCentreServiceThumbnailAltTextSc": "本地传真服务",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 25,
             "shopCentreServiceTypeNameEn": "Free Internet",
             "shopCentreServiceTypeNameTc": "免費上網服務",
             "shopCentreServiceTypeNameSc": "免费上网服务",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/free_internet.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/free_internet.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/free_internet.svg",
             "shopCentreServiceThumbnailAltTextEn": "Free Internet",
             "shopCentreServiceThumbnailAltTextTc": "免費上網服務",
             "shopCentreServiceThumbnailAltTextSc": "免费上网服务",
             "isCurrentShopCentreService": true
           },
           {
             "shopCentreServiceTypeId": 16,
             "shopCentreServiceTypeNameEn": "Sports Facilities",
             "shopCentreServiceTypeNameTc": "運動設施",
             "shopCentreServiceTypeNameSc": "运动设施",
             "shopCentreServiceThumbnailPathEn": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/sports_facilities.svg",
             "shopCentreServiceThumbnailPathTc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/sports_facilities.svg",
             "shopCentreServiceThumbnailPathSc": "http://www-dev.linkhk.com/linkapi/file/SiteAssets/SectionPages/Shop/sports_facilities.svg",
             "shopCentreServiceThumbnailAltTextEn": "Sports Facilities",
             "shopCentreServiceThumbnailAltTextTc": "運動設施",
             "shopCentreServiceThumbnailAltTextSc": "运动设施",
             "isCurrentShopCentreService": true
           }
         ],
         "serviceNoticeDescriptionTc": "Special Services Description tc",
         "serviceNoticeDescriptionSc": "Special Services Description sc",
         "serviceNoticeDescriptionEn": "Special Services Description"
       },
       "mallThumbnails": [
         {
           "thumbnail": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
           "thumbnailAltTextEn": "Outlook",
           "thumbnailAltTextTc": "外觀",
           "thumbnailAltTextSc": "外观"
         },
         {
           "thumbnail": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/LF_after_5night.jpg",
           "thumbnailAltTextEn": "Podium Food Square",
           "thumbnailAltTextTc": "美食坊",
           "thumbnailAltTextSc": "美食坊"
         },
         {
           "thumbnail": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/LF_after_6.jpg",
           "thumbnailAltTextEn": "Atrium",
           "thumbnailAltTextTc": "中庭",
           "thumbnailAltTextSc": "中庭"
         }
       ]
     },
     "parkingVacancy": [
       {
         "facilityKey": 4334,
         "carparkFacilityNameEn": "Lok Fu Zone A Car Park ",
         "carparkFacilityNameTc": "樂富A停車場 (ABC場)",
         "carparkFacilityNameSc": "乐富A停车场 (ABC场)",
         "thumbnailPath": "http://www-dev.linkhk.com/linkapi/file/CarParkFacilityPhoto/4334.jpg",
         "availableCarParkSpace": 14
       },
       {
         "facilityKey": 4332,
         "carparkFacilityNameEn": "Lok Fu UNY Car Park ",
         "carparkFacilityNameTc": "樂富UNY",
         "carparkFacilityNameSc": "乐富UNY",
         "thumbnailPath": "http://www-dev.linkhk.com/linkapi/file/CarParkFacilityPhoto/4332.jpg",
         "availableCarParkSpace": 159
       },
       {
         "facilityKey": 4335,
         "carparkFacilityNameEn": "Lok Fu Zone B Car Park ",
         "carparkFacilityNameTc": "樂富B停車場",
         "carparkFacilityNameSc": "乐富B停车场",
         "thumbnailPath": "http://www-dev.linkhk.com/linkapi/file/CarParkFacilityPhoto/4335.jpg",
         "availableCarParkSpace": 62
       },
       {
         "facilityKey": 4352,
         "carparkFacilityNameEn": "Lok Fu Estate Car Park ",
         "carparkFacilityNameTc": "樂富邨停車場",
         "carparkFacilityNameSc": "乐富邨停车场",
         "thumbnailPath": "http://www-dev.linkhk.com/linkapi/file/CarParkFacilityPhoto/4352.jpg",
         "availableCarParkSpace": 4
       }
     ],
     "dine": {
       "titleEn": "DINE",
       "titleTc": "美食",
       "titleSc": "美食",
       "thumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_dine.svg",
       "descriptionEn": "Dine Description",
       "descriptionTc": "Dine Description tc",
       "descriptionSc": "Dine Description sc",
       "dineList": [
         {
           "shopId": 23300,
           "shopNameEn": "Super Super Congee & Noodles",
           "shopNameTc": "一粥麵",
           "shopNameSc": "一粥面",
           "shopPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_1113-1114.jpg",
           "shopPhotoAltTextEn": "Super Super Congee & Noodles",
           "shopPhotoAltTextTc": "一粥麵",
           "shopPhotoAltTextSc": "一粥面"
         },
         {
           "shopId": 23412,
           "shopNameEn": "Foo Lum Palace",
           "shopNameTc": "富臨皇宮",
           "shopNameSc": "富临皇宫",
           "shopPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_U206-U211.jpg",
           "shopPhotoAltTextEn": "Foo Lum Palace",
           "shopPhotoAltTextTc": "富臨皇宮",
           "shopPhotoAltTextSc": "富临皇宫"
         },
         {
           "shopId": 23306,
           "shopNameEn": "Genki Sushi",
           "shopNameTc": "元氣壽司",
           "shopNameSc": "元气寿司",
           "shopPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_1157.jpg",
           "shopPhotoAltTextEn": "Genki Sushi",
           "shopPhotoAltTextTc": "元氣壽司",
           "shopPhotoAltTextSc": "元气寿司"
         }
       ]
     },
     "shop": {
       "titleEn": "SHOP",
       "titleTc": "購物",
       "titleSc": "购物",
       "thumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_shop.svg",
       "descriptionEn": "Shop Description",
       "descriptionTc": "Shop Description tc",
       "descriptionSc": "Shop Description sc",
       "shopList": [
         {
           "shopId": 23610,
           "shopNameEn": "嘉升雜誌屋",
           "shopNameTc": "嘉升雜誌屋",
           "shopNameSc": "嘉升杂志屋",
           "shopPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_D041.jpg",
           "shopPhotoAltTextEn": "嘉升雜誌屋",
           "shopPhotoAltTextTc": "嘉升雜誌屋",
           "shopPhotoAltTextSc": "嘉升杂志屋"
         },
         {
           "shopId": 23589,
           "shopNameEn": "美麗時裝",
           "shopNameTc": "美麗時裝",
           "shopNameSc": "美丽时装",
           "shopPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_D017.jpg",
           "shopPhotoAltTextEn": "美麗時裝",
           "shopPhotoAltTextTc": "美麗時裝",
           "shopPhotoAltTextSc": "美丽时装"
         },
         {
           "shopId": 23435,
           "shopNameEn": "Jeanswest",
           "shopNameTc": "真維斯",
           "shopNameSc": "真维斯",
           "shopPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_2210-2215.jpg",
           "shopPhotoAltTextEn": "Jeanswest",
           "shopPhotoAltTextTc": "真維斯",
           "shopPhotoAltTextSc": "真维斯"
         }
       ]
     },
     "market": {
       "titleEn": "FRESH MARKET",
       "titleTc": "街巿",
       "titleSc": "街巿",
       "thumbnailUrl": "http://www-dev.linkhk.com/linkapi/file/File%20Resources/Image/mall_detail_yourjourney_fresh_market.svg",
       "descriptionEn": "Market Description",
       "descriptionTc": "Market Description tc",
       "descriptionSc": "Market Description sc",
       "market": [
         {
			"shopId": 24496,
			"marketCode": "lfxxc2_mkt2",
			"marketNameEn": "01 Lok Fu Market",
			"marketNameTc": "樂富街市",
			"marketNameSc": "乐富街市",
			"marketPhotoPath": "http://www-dev.linkhk.com/linkapi/file/ShopPhoto/lfxxc2/lfxxc2_lokfumarket.jpg",
			"marketPhotoAltTextEn": "01 Lok Fu Market",
			"marketPhotoAltTextTc": "樂富街市",
			"marketPhotoAltTextSc": "乐富街市"
         }
       ],
       "recipe": null,
       "marketNews": ""
     },
     "theNearestMall": {
       "titleEn": "The Nearest Malls",
       "titleTc": "鄰近商場",
       "titleSc": "邻近商场",
       "theNearestMallList": [
         {
           "shopCentreId": 5,
           "propCode" : "ftxxc2",
           "shopCentreNameEn": "Fung Tak Shopping Centre",
           "shopCentreNameTc": "鳳德商場",
           "shopCentreNameSc": "凤德商场",
           "thumbnailPath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/ftxxc2/03_TPF33700029.JPG"
         },
         {
           "shopCentreId": 13,
           "propCode" : "wthxd2",
           "shopCentreNameEn": "Wang Tau Hom (Wang Fai Centre)",
           "shopCentreNameTc": "橫頭磡 (宏輝中心)",
           "shopCentreNameSc": "横头磡 (宏辉中心)",
           "thumbnailPath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/wthxd2/05_WTS29890005.jpg"
         }
       ]
     },
     "sightseeingNearby": {
       "titleEn": "Sightseeing Nearby",
       "titleTc": "鄰近觀光景點",
       "titleSc": "邻近观光景点",
       "sightseeings": [
         {
           "siteNameTc": "",
           "siteNameSc": "",
           "siteNameEn": "",
           "siteDescriptionTc": "樂富遊樂場的22人足球場採用人造草地，看台可容納500名觀眾，並設有蓋更衣室72個，更備有洗手間及殘疾人士洗手間，深受足球員及其他團體運動員歡迎，每日開放時間為上午7：30至下午11時。",
           "siteDescriptionSc": "乐富游乐场的22人足球场采用人造草地，看台可容纳500名观众，并设有盖更衣室72个，更备有洗手间及残疾人士洗手间，深受足球员及其他团体运动员欢迎，每日开放时间为上午7时半至下午11时。",
           "siteDescriptionEn": "Lok Fu Recreation Ground is a popular venue for soccer matches and other team sports activities. The 11-a-side, artificial turf soccer pitch on Heng Lam Street is open daily from 7:30am to 11pm. The 500-seat spectator stand also offers cover for 72 seats. There are changing rooms, toilets, and a disabled toilet.",
           "siteImagePath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/01.jpg",
           "siteImageAltTextTc": "樂富遊樂場",
           "siteImageAltTextSc": "乐富游乐场",
           "siteImageAltTextEn": "Lok Fu Recreation Ground"
         },
         {
           "siteNameTc": "",
           "siteNameSc": "",
           "siteNameEn": "",
           "siteDescriptionTc": "坐落山坡上的富安街，樂富公園鋪設整潔通道供遊人閒逛，沿途可飽覽鄰近景色，距離樂富地鐵站、聯合道公園及樂富遊樂場只是數步之遙。",
           "siteDescriptionSc": "坐落山坡上的富安街，乐富公园铺设整洁通道供游人闲逛，沿途可饱览临近景色，居里乐富地铁站、联合道公园及乐富游乐场只是数步之遥。",
           "siteDescriptionEn": "Situated up a hill on Fu On Street, Lok Fu Park offers paved paths for leisurely walks with scenic views of the neighboring areas. It is within walking distance of Lok Fu MTR station, Junction Road Park as well as Lok Fu Recreation Ground.",
           "siteImagePath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/02.jpg",
           "siteImageAltTextTc": "樂富公園",
           "siteImageAltTextSc": "乐富公园",
           "siteImageAltTextEn": "Lok Fu Park"
         },
         {
           "siteNameTc": "",
           "siteNameSc": "",
           "siteNameEn": "",
           "siteDescriptionTc": "聯合道公園設有六個網球場，並備有其他運動及娱乐設施，包括一塊每邊7人的硬地足球場、兩個籃球場、一個兒童天地及一系列長者健身設施。園內的跑步徑有265米長，配有健身設施及卵石路，供市民強身健體。開放時間從上午7時至下午11時。",
           "siteDescriptionSc": "联合道公园设有六个网球场，并具有其他运动及娱乐设施，包括一块每边7人的硬地足球场、两个篮球场、一个儿童天地及一系列长者健身设施。园内的跑步径有265米长，配有健身设施及卵石路，供市民强身健体。开放时间从上午7时至下午11时。",
           "siteDescriptionEn": "There are six, tennis courts at Junction Road Park. Other sports and recreational facilities include: a hard-surface, 7-a-side soccer pitch, two basketball courts, children’s play area, and an elderly fitness area, a 265-meter jogging track with fitness stations as well as a pebble walking trail. The park is open daily from 7am to 11pm.",
           "siteImagePath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/03.jpg",
           "siteImageAltTextTc": "聯合道公園",
           "siteImageAltTextSc": "联合道公园",
           "siteImageAltTextEn": "Junction Road Park"
         },
         {
           "siteNameTc": "",
           "siteNameSc": "",
           "siteNameEn": "",
           "siteDescriptionTc": "獅子山公園為香港歷史最早劃定的三大公園之一，佔地557公頃，495米長的山頂外形貌似獅子盤山而坐，因而得其名。遊人更可前往沙田紅梅谷頂親手感受望夫石，細聽望夫謠。除了有著名的岩石層極具地質學價值外，獅子山公園還連接衛奕信徑及麥理浩徑第5段。行山人士也可選擇比較輕鬆易走的鷹巢山及紅梅谷自然教育徑，老少咸宜。",
           "siteDescriptionSc": "狮子山公园为香港历史最早划定的三大公园之一，占地557公顷，495米长的山顶外形貌似狮子盘山而坐，因而得其名。游人更可前往沙田红梅谷顶亲手感受望夫石，细听望夫谣。除了有著名的岩石层极具地质学价值外，狮子山公园还连接卫奕信径及麦理浩径第5段。远足人士也可选择比较轻松易走的鹰巢山及红梅谷自然教育径，老少咸宜。",
           "siteDescriptionEn": "One of Hong Kong’s three earliest country parks, Lion Rock Country Park is known for its 495m-meter peak that looks like a lion is perched on top. Also famous is the touching Amah Rock, which sits atop Hung Mui Kuk in Shatin. Aside from the famous rock formations, stage 5 of Wilson and MacLehose Trails and the easier Eagle’s Nest and Hung Mui Kuk Nature Trails draw many hikers to this 557-hectare park.",
           "siteImagePath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/04.jpg",
           "siteImageAltTextTc": "獅子山公園",
           "siteImageAltTextSc": "狮子山公园",
           "siteImageAltTextEn": "Lion Rock Country Park"
         },
         {
           "siteNameTc": "",
           "siteNameSc": "",
           "siteNameEn": "",
           "siteDescriptionTc": "摩士公園以二戰時期逝世的匯豐銀行主席兼總司理亞瑟‧摩士爵士命名，佔地16公頃，屬城市公園。這個熱帶棕櫚樹林於植物區內種有23種棕櫚樹及30餘種珍稀樹種，令園地綠樹成蔭。此外，公園更設有的露天圓形劇場（部分座位設有天幕）、游泳池、適合多項運動的體育中心及一個蝴蝶閣，一應俱全，適合需要脫離網絡的都市人士。",
           "siteDescriptionSc": "摩士公园以二战时期逝世的汇丰银行主席兼总司理亚瑟‧摩士爵士命名，占地16公顷，属城市花园。这个热带棕榈树林于植物区内种有23种棕榈树及30余种珍稀树种，令园地绿树成荫。此外，公园更设有的露天圆形剧场（部分座位设有天幕）、游泳池，适合多项运动的体育中心及一个蝴蝶阁，一应俱全，适合需要脱离网络的都市人士。",
           "siteDescriptionEn": "Named after Sir Arthur Morse, the head of The Hong Kong and Shanghai Banking Corporation during and after World War II, Morse Park is a nearly 16-hectare urban park. A tropical palm garden with 23 species and another 30 rare species of trees in the park’s arboretum provide plenty of shaded lawns in Morse Park. The park also contains an amphitheater with partially covered seating, swimming pool, sports center for a variety of sports, and a butterfly corner.",
           "siteImagePath": "http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/05.jpg",
           "siteImageAltTextTc": "摩士公園",
           "siteImageAltTextSc": "摩士公园",
           "siteImageAltTextEn": "Morse Park"
         }
       ]
     }
   }
 }
::SHOP_CENTRE_DETAILS_API_END::
	*/