package com.gt.projects.thelinkreit.utils;

import java.util.List;

public class LocationUtil {

	public static Double[] calculateRange(Double longitude, Double latitude, Double distance) {
		Double longitude1 = longitude - distance / (double) Math.abs(Math.cos(Math.toRadians(latitude)) * 69f);
		Double longitude2 = longitude + distance / (double) Math.abs(Math.cos(Math.toRadians(latitude)) * 69f);
		Double latitude1 = latitude - (distance / 69f);
		Double latitude2 = latitude + (distance / 69f);
		return new Double[] { longitude1, latitude1, longitude2, latitude2 };
	}

	public static Double calculateDistance(Double longitude1, Double latitude1, Double longitude2, Double latitude2) {
		Double R = 6371d;
		Double dLongitude = (double) Math.toRadians(longitude2 - longitude1);
		Double dLatitude = (double) Math.toRadians(latitude2 - latitude1);
		Double newLatitude1 = (double) Math.toRadians(latitude1);
		Double newLatitude2 = (double) Math.toRadians(latitude2);
		Double a = (double) (Math.sin(dLatitude / 2) * Math.sin(dLatitude / 2) + Math.sin(dLongitude / 2) * Math.sin(dLongitude / 2) * Math.cos(newLatitude1) * Math.cos(newLatitude2));
		Double b = (double) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
		return R * b;
	}

	public static Double[] findCentralGeo(List<Double[]> geoLocations) {
		if (geoLocations == null || geoLocations.size() == 0)
			return null;

		if (geoLocations.size() == 1)
			return geoLocations.get(0);

		double x = 0;
		double y = 0;
		double z = 0;

		for (Double[] geoLocation : geoLocations) {
			double longitude = geoLocation[0] * Math.PI / 180;
			double latitude = geoLocation[1] * Math.PI / 180;

			x += Math.cos(latitude) * Math.cos(longitude);
			y += Math.cos(latitude) * Math.sin(longitude);
			z += Math.sin(latitude);
		}

		x = x / geoLocations.size();
		y = y / geoLocations.size();
		z = z / geoLocations.size();

		double centralLongitude = Math.atan2(y, x);
		double centralSquareRoot = Math.sqrt(x * x + y * y);
		double centralLatitude = Math.atan2(z, centralSquareRoot);

		return new Double[] { centralLongitude * 180 / Math.PI, centralLatitude * 180 / Math.PI };
	}

}
