package com.gt.projects.thelinkreit.service;

import com.gt.projects.thelinkreit.entity.mongo.HomePage;

import java.util.List;

public interface HomePageService {

    List<HomePage> findAll();
}
