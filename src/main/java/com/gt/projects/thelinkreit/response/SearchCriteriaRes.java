package com.gt.projects.thelinkreit.response;

import java.util.List;

import com.gt.projects.thelinkreit.response.SearchCriteriaRes.SearchCriteriaAresRes;

import lombok.Data;

@Data
public class SearchCriteriaRes{
	private List<SearchCriteriaAresRes> districtsList;
	private List<SearchCriteriaShopCentreRes> shopCentreList;
	private List<SearchCriteriaCategoryRes> categoryList;
    private List<SearchCriteriaCarParkRes> carParkList;
    private List<SearchCriteriaParkingPrivilegeTypeRes> carParkPrivilegeTypeList;
	
	@Data
	public class SearchCriteriaAresRes {
		private String areaCode;
		private String areaNameEn;
		private String areaNameTc;
		private String areaNameSc;
		private List<SearchCriteriaDistrictRes> districts;
	}

	@Data
	public class SearchCriteriaDistrictRes {
		private Integer districtId;
		private String districtNameEn;
		private String districtNameTc;
		private String districtNameSc;
	}

	@Data
	public class SearchCriteriaShopCentreRes {
		private Integer shopCentreId;
		private Integer districtId;
		private String shopCategoryIds;
		private String shopCentreNameEn;
		private String shopCentreNameTc;
		private String shopCentreNameSc;
	}

	@Data
	public class SearchCriteriaCategoryRes {
		private Integer shopCategoryId;
		private String shopCategoryNameEn;
		private String shopCategoryNameTc;
		private String shopCategoryNameSc;
	}

    @Data
    public class SearchCriteriaCarParkRes {
        private Integer facilityKey;
        private String propCode;
        private Integer districtId;
        private String type;	// Shop Center or Car Park
		private String propNameEn;
		private String propNameTc;
		private String propNameSc;

    }

    @Data
    public class SearchCriteriaParkingPrivilegeTypeRes {
        private Integer carParkPrivilegeId;
        private String carParkPrivilegeNameEn;
        private String carParkPrivilegeNameTc;
        private String carParkPrivilegeNameSc;
    }
    
}

