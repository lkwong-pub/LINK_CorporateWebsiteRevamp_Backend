//package com.gt.projects.thelinkreit.controller.api;
//
//import com.gt.projects.thelinkreit.config.Url;
//import com.gt.projects.thelinkreit.service.MongoService;
//
//import com.mongodb.gridfs.GridFSDBFile;
//import org.apache.commons.io.IOUtils;
//import org.apache.http.HttpStatus;
//import org.apache.http.entity.ContentType;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.MimeType;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.HandlerMapping;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import java.io.BufferedInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.nio.ByteBuffer;
//import java.nio.channels.Channels;
//import java.nio.channels.ReadableByteChannel;
//import java.nio.channels.WritableByteChannel;
//
//
//@Controller
//@RequestMapping(Url.API_PREFIX + Url.API_FILE_PREFIX)
//public class FileController extends _AbstractApiController {
//
//	@Autowired
//	private MongoService mongoService;
//
//	Logger log = Logger.getLogger(this.getClass());
//
//	@RequestMapping(value = "/**")
//	public void media( HttpServletRequest request, HttpServletResponse response) throws Exception {
//		// log.info("Get media request");
//		String filename = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
//        filename = filename.replaceAll(Url.API_PREFIX + Url.API_FILE_PREFIX, "");
//
//        try {
//                printResponse(response, filename);
//        } catch (Exception e) {
//                log.error(e.getMessage());
//                response.setStatus(HttpStatus.SC_NOT_FOUND);
//        }
//
//	}
//
//	private void printResponse(HttpServletResponse response, String filename) throws IOException {
//		// log.info("Get mongodb file");
//		GridFSDBFile file = this.mongoService.getFileByFilename(filename);
//		// log.info("Finish get mongodb file");
//
//        filename = filename.toLowerCase();	// Added on 20170314 for check if file extension is upper case
//
//        String type = "";
//        if( filename.contains(".jpg") ){
//            type = MediaType.IMAGE_JPEG_VALUE;
//        } else if( filename.contains(".png") ){
//            type = MediaType.IMAGE_PNG_VALUE;
//        } else if (filename.contains(".svg")){
//            type = "image/svg+xml";
//        } else if (filename.contains(".pdf")){
//            type = "application/pdf";
//        } else if (filename.contains(".mp4")) {
//            type = "video/mp4";
//        }
//
//        response.setContentType(type);
//        response.setHeader("Cache-Control", "public, max-age=2592000");
//        response.setHeader("Pragma", "cache");
//        Integer length = Integer.parseInt(String.valueOf(file.getLength()));
//        response.setContentLength(length);
//        response.setHeader("Content-Length", length.toString());
//        response.setHeader("Content-Disposition", "filename=\"" + filename + "\"");
////        response.getOutputStream().write(IOUtils.toByteArray(file.getInputStream()));
////        response.getOutputStream().flush();
////        response.getOutputStream().close();
//        // log.info("Finish setting header");
//        stream(file.getInputStream(), response.getOutputStream());
//    }
//
//	public long stream(InputStream input, OutputStream output) throws IOException {
//		// log.info("Start output file");
//		try (
//	        ReadableByteChannel inputChannel = Channels.newChannel(input);
//	        WritableByteChannel outputChannel = Channels.newChannel(output);
//	    ) {
//	        ByteBuffer buffer = ByteBuffer.allocateDirect(10240);
//	        long size = 0;
//
//	        while (inputChannel.read(buffer) != -1) {
//	        	// log.info("Outputing file");
//	            buffer.flip();
//	            size += outputChannel.write(buffer);
//	            buffer.clear();
//	        }
//
//	        // log.info("Finish outputing file");
//
//	        return size;
//	    }
//	}
//
//}
