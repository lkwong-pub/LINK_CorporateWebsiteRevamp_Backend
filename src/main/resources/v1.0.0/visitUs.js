/**
	  
	  @api {get} http://www-dev.linkhk.com/linkweb/api/visitUs/      Visit Us
	  
	  @apiName Retrieve Website VisitUs Page Initial Content
	  @apiGroup Batch2-VisitUs
	  @apiVersion 1.0.0
	  
	  @apiDescription
		Retrieve Website VisitUs Page Initial Content<br/>
		Data included 6 components content:<br/>
		1. SEO Details<br/>
		2. Shop Centre Location List<br/>
		3. Car Park Facility Location List<br/>
		4. Market Location List<br/>
		5. Icon URL<br/> 
		6. Shop Centre List<br/> 
	  
	  	@apiParam {String}	longitude		Current Longitude
	  	@apiParam {String}	latitude		Current Latitude
	  	
	  
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {String} 	data.id			ID
		@apiSuccess (Success 200) {String} 	data.titleEn	SEO Title (EN)
		@apiSuccess (Success 200) {String} 	data.titleTc	SEO Title (TC)
		@apiSuccess (Success 200) {String} 	data.titleSc	SEO Title (SC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
		@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
		
		@apiSuccess (Success 200) {Integer} 	data.defaultShopCentreId		Default Shop Centre Id
		
		@apiSuccess (Success 200) {Object[]} 	data.shopCentreLocationList		VisitUs - Shop Centre Location List Object Array
		@apiSuccess (Success 200) {Integer} 	data.shopCentreLocationList.shopCentreId	Shop Centre ID
		@apiSuccess (Success 200) {Integer} 	data.shopCentreLocationList.districtId	Shop Centre District ID
		@apiSuccess (Success 200) {String} 	data.shopCentreLocationList.longitude		Shop Centre Longitude	
		@apiSuccess (Success 200) {String} 	data.shopCentreLocationList.latitude		Shop Centre Latitude
		@apiSuccess (Success 200) {Object[]} 	data.carParkFacilityLocationList		VisitUs - Car Park Facility Location List Object Array
		@apiSuccess (Success 200) {Integer} 	data.carParkFacilityLocationList.facilityKey	Car Park Facility ID
		@apiSuccess (Success 200) {Integer} 	data.carParkFacilityLocationList.districtId		Car Park Facility District ID
		@apiSuccess (Success 200) {String} 	data.carParkFacilityLocationList.longitude		Car Park Facility Longitude	
		@apiSuccess (Success 200) {String} 	data.carParkFacilityLocationList.latitude		Car Park Facility Latitude
		@apiSuccess (Success 200) {Object[]} 	data.marketLocationList		VisitUs - Market Location List Object Array
		@apiSuccess (Success 200) {Integer} 	data.marketLocationList.marketCode	Market Code
		@apiSuccess (Success 200) {Integer} 	data.marketLocationList.districtId	Market District ID
		@apiSuccess (Success 200) {String} 	data.marketLocationList.longitude		Market Longitude	
		@apiSuccess (Success 200) {String} 	data.marketLocationList.latitude		Market Latitude
		@apiSuccess (Success 200) {String}	data.serviceIconDineEn	Dine Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconDineTc	Dine Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconDineSc	Dine Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconDineSelectedEn	Dine Selected Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconDineSelectedTc	Dine Selected Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconDineSelectedSc	Dine Selected Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconShopEn	Shop Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconShopTc	Shop Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconShopSc	Shop Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconShopSelectedEn	Shop Selected Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconShopSelectedTc	Shop Selected Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconShopSelectedSc	Shop Selected Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconHappeningEn	Happening Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconHappeningTc	Happening Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconHappeningSc	Happening Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconHappeningSelectedEn	Happening Selected Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconHappeningSelectedTc	Happening Selected Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconHappeningSelectedSc	Happening Selected Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconMarketEn	Market Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconMarketTc	Market Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconMarketSc	Market Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconMarketSelectedEn	Market Selected Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconMarketSelectedTc	Market Selected Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconMarketSelectedSc	Market Selected Icon URL (SC)
		@apiSuccess (Success 200) {String}	data.serviceIconAllMallEn	All Mall Icon URL (EN)
		@apiSuccess (Success 200) {String}	data.serviceIconAllMallTc	All Mall Icon URL (TC)
		@apiSuccess (Success 200) {String}	data.serviceIconAllMallSc	All Mall Icon URL (SC)
		@apiSuccess (Success 200) {Object[]}	data.shopCentreList		Shop Centre List (for all mall)	Object Array
		@apiSuccess (Success 200) {String} 		data.shopCentreList.areaCode 	Area Code
		@apiSuccess (Success 200) {String} 	data.shopCentreList.areaNameEn 	Area Name (EN) 
		@apiSuccess (Success 200) {String} 	data.shopCentreList.areaNameTc		Area Name (TC)
		@apiSuccess (Success 200) {String} 	data.shopCentreList.areaNameSc		Area Name (SC)
		@apiSuccess (Success 200) {Object[]} 	data.shopCentreList.shopCentres		Shop Centre Object Array
		@apiSuccess (Success 200) {Integer} 	data.shopCentreList.shopCentres.shopCentreId		Shop Centre ID
 		@apiSuccess (Success 200) {String} 		data.shopCentreList.shopCentres.propCode		Shop Centre Property Code
		@apiSuccess (Success 200) {String} 		data.shopCentreList.shopCentres.shopCentreNameEn		Shop Centre Name (EN)
		@apiSuccess (Success 200) {String} 		data.shopCentreList.shopCentres.shopCentreNameTc		Shop Centre Name (TC)
		@apiSuccess (Success 200) {String} 		data.shopCentreList.shopCentres.shopCentreNameSc		Shop Centre Name (SC)
		@apiSuccess (Success 200) {String} 		data.shopCentreList.shopCentres.websiteUrl		Shop Centre Website URL
		@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::VISIT_US_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "seoTitleEn": "LINK Customer Site Visit Us EN",
	  "seoTitleTc": "LINK Customer Site Visit Us TC",
	  "seoTitleSc": "LINK Customer Site Visit Us SC",
	  "seoKeywordEn": "LINK Customer Site Visit Us EN",
	  "seoKeywordTc": "LINK Customer Site Visit Us TC",
	  "seoKeywordSc": "LINK Customer Site Visit Us SC",
	  "seoDescriptionEn": "This is LINK Customer Site Visit Us. EN",
	  "seoDescriptionTc": "This is LINK Customer Site Visit Us. TC",
	  "seoDescriptionSc": "This is LINK Customer Site Visit Us. SC",
      "shopCentreLocationList":[  
         {  
            "shopCentreId":1,
            "districtId":7,
            "longitude":"114.217987",
            "latitude":"22.335945"
         }
      ],
      "carParkFacilityLocationList":[  
         {  
            "facilityKey":1037,
            "districtId":7,
            "longitude":"114.217987",
            "latitude":"22.335945"
         }
      ],
      "marketLocationList":[  
         {  
            "marketCode":"cwn1c2_mkt1",
            "districtId":7,
            "longitude":"114.217987",
            "latitude":"22.335945"
         }
      ],
      "serviceIconDineEn":"/dine.png",
      "serviceIconDineTc":"/dine.png",
      "serviceIconDineSc":"/dine.png",
      "serviceIconDineSelectedEn":"/dineSelected.png",
      "serviceIconDineSelectedTc":"/dineSelected.png",
      "serviceIconDineSelectedSc":"/dineSelected.png",
      "serviceIconShopEn":"/shop.png",
      "serviceIconShopTc":"/shop.png",
      "serviceIconShopSc":"/shop.png",
      "serviceIconShopSelectedEn":"/shopSelected.png",
      "serviceIconShopSelectedTc":"/shopSelected.png",
      "serviceIconShopSelectedSc":"/shopSelected.png",
      "serviceIconHappeningEn":"/happening.png",
      "serviceIconHappeningTc":"/happening.png",
      "serviceIconHappeningSc":"/happening.png",
      "serviceIconHappeningSelectedEn":"/happeningSelected.png",
      "serviceIconHappeningSelectedTc":"/happeningSelected.png",
      "serviceIconHappeningSelectedSc":"/happeningSelected.png",
      "serviceIconMarketEn":"/market.png",
      "serviceIconMarketTc":"/market.png",
      "serviceIconMarketSc":"/market.png",
      "serviceIconMarketSelectedEn":"/marketSelected.png",
      "serviceIconMarketSelectedTc":"/marketSelected.png",
      "serviceIconMarketSelectedSc":"/marketSelected.png",
      "serviceIconAllMallEn":"allMall.png",
      "serviceIconAllMallTc":"allMall.png",
      "serviceIconAllMallSc":"allMall.png",
      "shopCentreList":[  
         {  
            "areaCode":"NT",
            "areaNameEn":"HKISLAND",
            "areaNameTc":"HKISLAND TC",
            "areaNameSc":"HKISLAND SC",
            "shopCentres":[  
               {  
                  "shopCentreId":1,
                  "propCode" : "chfxc2",
                  "shopCentreNameEn":"",
                  "shopCentreNameTc":"",
                  "shopCentreNameSc":"",
                  "websiteUrl":"http://abc.com"
               }
            ]
         }
      ]
   }
}
::VISIT_US_API_END::
		*/

/**
	  
	  @api {get} http://www-dev.linkhk.com/linkweb/api/visitUs/result      Visit Us	Result
	  
	  @apiName Retrieve Website VisitUs Page Search Result
	  @apiGroup Batch2-VisitUs
	  @apiVersion 1.0.0
	  
	  @apiDescription
		Retrieve Website VisitUs Page Initial Content<br/>
		Data included 3 components content:<br/>
		1. Selected Shop Centre<br/>
			1.1	Shop Centre Details<br/>
			1.2 Dine List<br/> 
			1.3 Shop List<br/>
			1.4 Happening Highlight List<br/>
			1.5 Market List<br/>
		2. Selected Car Park Facility<br/>
		3. Selected Market<br/> 
	  
	  	@apiParam {Integer}	districtId		District ID
	  	@apiParam {Integer}	shopCentreId	Shop Centre ID
	  	@apiParam {Integer}	facilityKey		Car Park Facility ID
	  	@apiParam {String}	marketCode		Market Code
	  	
	  
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {Object} 	data.selectedShopCentre		Selected Shop Centre Object
		@apiSuccess (Success 200) {Integer} 	data.selectedShopCentre.shopCentreId	Shop Centre ID
 		@apiSuccess (Success 200) {String} 	data.selectedShopCentre.propCode	Shop Centre Property Code
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopCentreNameEn	Shop Centre Name (EN)
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopCentreNameTc	Shop Centre Name (TC)
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopCentreNameSc	Shop Centre Name (SC)
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.longitude		Shop Centre Longitude
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.latitude		Shop Centre Latitude
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.thumbnailPath	Shop Centre Thumbnail URL
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.addressEn		Shop Centre Address (EN)
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.addressTc		Shop Centre Address (TC)
	    @apiSuccess (Success 200) {String} 	data.selectedShopCentre.addressSc		Shop Centre Address (SC)
	    @apiSuccess (Success 200) {Object[]} 	data.selectedShopCentre.carParkFacilityLocationList		Shop Centre - Car Park Facility Location Object Array
	    @apiSuccess (Success 200) {Integer} 	data.selectedShopCentre.carParkFacilityLocationList.facilityKey		Facility Key
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.longitude		Facility Longitude
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.latitude		Facility Latitude
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.carParkFacilityNameEn	Car Park Facility Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.carParkFacilityNameTc	Car Park Facility Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.carParkFacilityNameSc	Car Park Facility Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.availableCarParkSpace	Available Car Park Space
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.carParkFacilityLocationList.totalCarParkSpace		Total Car Park Space
        @apiSuccess (Success 200) {Object[]} 	data.selectedShopCentre.dineList	Shop Centre - Dine Object Array
		@apiSuccess (Success 200) {Integer} 	data.selectedShopCentre.dineList.shopId		Shop ID
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.dineList.shopNameEn		Shop Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.dineList.shopNameTc		Shop Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.dineList.shopNameSc		Shop Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.dineList.shopPhotoPath	Shop Image URL
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.dineList.shopNo			Shop NO
		@apiSuccess (Success 200) {Object[]} 	data.selectedShopCentre.shopList	Shop Centre - Shop Object Array
		@apiSuccess (Success 200) {Integer} 	data.selectedShopCentre.shopList.shopId		Shop ID
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopList.shopNameEn		Shop Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopList.shopNameTc		Shop Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopList.shopNameSc		Shop Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopList.shopPhotoPath	Shop Image URL
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.shopList.shopNo			Shop NO
        @apiSuccess (Success 200) {Object[]} 	data.selectedShopCentre.happeningHighlight		Shop Centre - Happening Highlight Object Array
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.id		Happening Highlight ID
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightTitleEn	Happening Highlight Title (EN)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightTitleTc	Happening Highlight Title (TC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightTitleSc	Happening Highlight Title (SC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightImageUrlEn	Happening Highlight Image URL (EN)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightImageUrlTc	Happening Highlight Image URL (TC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightImageUrlSc	Happening Highlight Image URL (SC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightStartDatetime		Happening Highlight Start Time
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.happeningHighlight.happeningHighlightEndDatetime		Happening Highlight End Time
        @apiSuccess (Success 200) {Object[]} 	data.selectedShopCentre.marketList	Shop Centre - Market Object Array
		@apiSuccess (Success 200) {Integer} data.selectedShopCentre.marketList.shopId			Shop Id
		@apiSuccess (Success 200) {String} 	data.selectedShopCentre.marketList.marketCode		Market Code
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.marketList.marketNameEn		Market Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.marketList.marketNameTc		Market Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.marketList.marketNameSc		Market Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedShopCentre.marketList.thumbnailPath	Market Image URL
        @apiSuccess (Success 200) {Object} 	data.selectedCarParkFacility		Select Car Park Facility Object
        @apiSuccess (Success 200) {Integer} 	data.selectedCarParkFacility.facilityKey		Car Park Facility Key
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityNameEn		Car Park Facility Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityNameTc		Car Park Facility Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityNameSc		Car Park Facility Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.availableCarParkSpace		Available Car Park Space
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.totalCarParkSpace			Total Car Park Space
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.thumbnailPath				Car Park Facility Thumbnail URL
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.longitude					Car Park Facility Longitude
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.latitude					Car Park Facility Latitude
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.addressEn					Car Park Facility Address (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.addressTc					Car Park Facility Address (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.addressSc					Car Park Facility Address (SC)
        @apiSuccess (Success 200) {Object[]} 	data.selectedCarParkFacility.carParkFacilityLocationList		Shop Centre - Car Park Facility Location Object Array
	    @apiSuccess (Success 200) {Integer} 	data.selectedCarParkFacility.carParkFacilityLocationList.facilityKey		Facility Key
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.longitude		Facility Longitude
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.latitude		Facility Latitude
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.carParkFacilityNameEn	Car Park Facility Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.carParkFacilityNameTc	Car Park Facility Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.carParkFacilityNameSc	Car Park Facility Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.availableCarParkSpace	Available Car Park Space
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.carParkFacilityLocationList.totalCarParkSpace		Total Car Park Space
        @apiSuccess (Success 200) {Object[]} 	data.selectedCarParkFacility.dineList	Shop Centre - Dine Object Array
		@apiSuccess (Success 200) {Integer} 	data.selectedCarParkFacility.dineList.shopId		Shop ID
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.dineList.shopNameEn		Shop Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.dineList.shopNameTc		Shop Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.dineList.shopNameSc		Shop Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.dineList.shopPhotoPath	Shop Image URL
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.dineList.shopNo			Shop NO
		@apiSuccess (Success 200) {Object[]} 	data.selectedCarParkFacility.shopList	Shop Centre - Shop Object Array
		@apiSuccess (Success 200) {Integer} 	data.selectedCarParkFacility.shopList.shopId		Shop ID
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.shopList.shopNameEn		Shop Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.shopList.shopNameTc		Shop Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.shopList.shopNameSc		Shop Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.shopList.shopPhotoPath	Shop Image URL
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.shopList.shopNo			Shop NO
        @apiSuccess (Success 200) {Object[]} 	data.selectedCarParkFacility.happeningHighlight		Shop Centre - Happening Highlight Object Array
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.id		Happening Highlight ID
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightTitleEn	Happening Highlight Title (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightTitleTc	Happening Highlight Title (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightTitleSc	Happening Highlight Title (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightImageUrlEn	Happening Highlight Image URL (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightImageUrlTc	Happening Highlight Image URL (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightImageUrlSc	Happening Highlight Image URL (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightStartDatetime		Happening Highlight Start Time
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.happeningHighlight.happeningHighlightEndDatetime		Happening Highlight End Time
        @apiSuccess (Success 200) {Object[]} 	data.selectedCarParkFacility.marketList	Shop Centre - Market Object Array
		@apiSuccess (Success 200) {Integer} data.selectedCarParkFacility.marketList.shopId			Shop Id
		@apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.marketList.marketCode		Market Code
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.marketList.marketNameEn		Market Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.marketList.marketNameTc		Market Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.marketList.marketNameSc		Market Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedCarParkFacility.marketList.thumbnailPath	Market Image URL
        @apiSuccess (Success 200) {Object} 	data.selectedMarket						Select Market Object
		@apiSuccess (Success 200) {Integer} data.selectedMarket.shopId				Shop Id
		@apiSuccess (Success 200) {String} 	data.selectedMarket.marketCode			Market Code
        @apiSuccess (Success 200) {String} 	data.selectedMarket.marketNameEn		Market Name (EN)
        @apiSuccess (Success 200) {String} 	data.selectedMarket.marketNameTc		Market Name (TC)
        @apiSuccess (Success 200) {String} 	data.selectedMarket.marketNameSc		Market Name (SC)
        @apiSuccess (Success 200) {String} 	data.selectedMarket.longitude			Market Longitude
        @apiSuccess (Success 200) {String} 	data.selectedMarket.latitude			Market Latitude
        @apiSuccess (Success 200) {String} 	data.selectedMarket.thumbnailPath		Market  Thumbnail URL
        @apiSuccess (Success 200) {String} 	data.selectedMarket.addressEn			Market Address (EN)
        @apiSuccess (Success 200) {String} 	data.selectedMarket.addressTc			Market Address (TC)
        @apiSuccess (Success 200) {String} 	data.selectedMarket.addressSc			Market Address (SC)
 @apiSuccessExample {json} Example
HTTP/1.1 200 OK
::VISIT_US_RESULT_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "selectedShopCentre":{  
         "shopCentreId":1,
         "propCode" : "chfxc2",
         "shopCentreNameEn":"Choi Fai Estate Retail and Car Park",
         "shopCentreNameTc":"彩輝邨商舖及停車場",
         "shopCentreNameSc":"彩辉邨商舖及停车场",
         "longitude":"114.217987",
         "latitude":"22.335945",
         "thumbnailPath":"images/desktop/visit-us/img_carpark_mall_thumbnail.png",
         "addressEn":"Choi Fai Estate Retail and Car Park, 2 Ngau Chi Wan Street, Ngau Chi Wan, Kowloon",
         "addressTc":"九龍牛池灣牛池灣街2號彩輝邨商舖及停車場1224",
         "addressSc":"九龙牛池湾牛池湾街2号彩辉邨商舖及停车场2324",
         "carParkFacilityLocationList":[  
            {  
               "facilityKey":1037,
               "longitude":"114.217987",
               "latitude":"22.335945",
               "carParkFacilityNameEn":"Yiu Tung Car Park 5 ",
	           "carParkFacilityNameTc":"耀東5停車場",
	           "carParkFacilityNameSc":"耀东5停车场",
	           "availableCarParkSpace":69,
	           "totalCarParkSpace":330
            }
         ],
         "dineList":[  
            {  
               "shopId":17402,
               "shopNameEn":"Colourmix",
               "shopNameTc":"Colourmix TC",
               "shopNameSc":"Colourmix SC",
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopNo":"20"
            }
         ],
         "shopList":[  
            {  
               "shopId":17402,
               "shopNameEn":"Colourmix",
               "shopNameTc":"Colourmix TC",
               "shopNameSc":"Colourmix SC",
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopNo":"20"
            }
         ],
         "happeningHighlight":[  
            {  
               "id":1,
               "happeningHighlightTitleEn":"happening highlight 1",
               "happeningHighlightTitleTc":"精彩亮點1",
               "happeningHighlightTitleSc":"精彩亮点1",
               "happeningHighlightImageUrlEn":"images/desktop/homepage/img_event_party.jpg",
               "happeningHighlightImageUrlTc":"images/desktop/homepage/img_event_party.jpg",
               "happeningHighlightImageUrlSc":"images/desktop/homepage/img_event_party.jpg",
               "happeningHighlightStartDatetime":"2016-04-15T00:00:00+08:00",
               "happeningHighlightEndDatetime":"2016-05-31T00:00:00+08:00"
            }
         ],
         "marketList":[  
            {
            	 "shopId":24955,
                 "marketCode":"cwn1c2_mkt1",
		         "marketNameEn":"Choi Fai Estate Retail and Car Park",
		         "marketNameTc":"彩輝邨商舖及停車場",
		         "marketNameSc":"彩辉邨商舖及停车场",
		         "thumbnailPath":"images/desktop/visit-us/img_carpark_mall_thumbnail.png"
            }
         ]
      },
      "selectedCarParkFacility":{  
         "facilityKey":1037,
         "carParkFacilityNameEn":"Yiu Tung Car Park 5 ",
         "carParkFacilityNameTc":"耀東5停車場",
         "carParkFacilityNameSc":"耀东5停车场",
         "availableCarParkSpace":69,
         "totalCarParkSpace":330,
         "thumbnailPath":"images/desktop/visit-us/img_carpark_mall_thumbnail.png",
         "longitude":"114.2567987",
         "latitude":"22.3365945",
         "addressEn":"Yiu Tung Shopping Centre, 12 Yiu Hing Road, Shau Kei Wan,  Hong Kong",
         "addressTc":"香港筲箕灣耀興道12號耀東商場",
         "addressSc":"香港筲箕湾耀兴道12号耀东商场",
         "carParkFacilityLocationList":[  
            {  
               "facilityKey":1037,
               "longitude":"114.217987",
               "latitude":"22.335945",
               "carParkFacilityNameEn":"Yiu Tung Car Park 5 ",
	           "carParkFacilityNameTc":"耀東5停車場",
	           "carParkFacilityNameSc":"耀东5停车场",
	           "availableCarParkSpace":69,
	           "totalCarParkSpace":330
            }
         ],
         "dineList":[  
            {  
               "shopId":17402,
               "shopNameEn":"Colourmix",
               "shopNameTc":"Colourmix TC",
               "shopNameSc":"Colourmix SC",
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopNo":"20"
            }
         ],
         "shopList":[  
            {  
               "shopId":17402,
               "shopNameEn":"Colourmix",
               "shopNameTc":"Colourmix TC",
               "shopNameSc":"Colourmix SC",
               "shopPhotoPath":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "shopNo":"20"
            }
         ],
         "happeningHighlight":[  
            {  
               "id":1,
               "happeningHighlightTitleEn":"happening highlight 1",
               "happeningHighlightTitleTc":"精彩亮點1",
               "happeningHighlightTitleSc":"精彩亮点1",
               "happeningHighlightImageUrlEn":"images/desktop/homepage/img_event_party.jpg",
               "happeningHighlightImageUrlTc":"images/desktop/homepage/img_event_party.jpg",
               "happeningHighlightImageUrlSc":"images/desktop/homepage/img_event_party.jpg",
               "happeningHighlightStartDatetime":"2016-04-15T00:00:00+08:00",
               "happeningHighlightEndDatetime":"2016-05-31T00:00:00+08:00"
            }
         ],
         "marketList":[  
            {
            	 "shopId":24955,
                 "marketCode":"cwn1c2_mkt1",
		         "marketNameEn":"Choi Fai Estate Retail and Car Park",
		         "marketNameTc":"彩輝邨商舖及停車場",
		         "marketNameSc":"彩辉邨商舖及停车场",
		         "thumbnailPath":"images/desktop/visit-us/img_carpark_mall_thumbnail.png"
            }
         ]
      },
      "selectedMarket":{
		 "shopId":24955,
         "marketCode":"cwn1c2_mkt1",
         "marketNameEn":"Choi Fai Estate Retail and Car Park",
         "marketNameTc":"彩輝邨商舖及停車場",
         "marketNameSc":"彩辉邨商舖及停车场",
         "longitude":"114.217987",
         "latitude":"22.335945",
         "thumbnailPath":"images/desktop/visit-us/img_carpark_mall_thumbnail.png",
         "addressEn":"Choi Fai Estate Retail and Car Park, 2 Ngau Chi Wan Street, Ngau Chi Wan, Kowloon",
         "addressTc":"九龍牛池灣牛池灣街2號彩輝邨商舖及停車場1224",
         "addressSc":"九龙牛池湾牛池湾街2号彩辉邨商舖及停车场2324"
      }
   }
}       	
::VISIT_US_RESULT_API_END::
*/