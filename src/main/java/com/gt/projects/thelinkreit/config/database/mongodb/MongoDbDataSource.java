//package com.gt.projects.thelinkreit.config.database.mongodb;
//
//import java.net.UnknownHostException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import com.mongodb.*;
//import org.bson.types.ObjectId;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//public class MongoDbDataSource {
//
//    private Mongo mongo;
//
//    private String address;
//    private Integer port;
//    private String username;
//    private String password;
//    private Boolean isHavePassword;
//    private String db;
//
//    List<ServerAddress> serverAddressList = new ArrayList<ServerAddress>();
//    private List<MongoCredential> mongoCredentialList = new ArrayList<MongoCredential>();
//
//    public void init() throws UnknownHostException {
//		/*
//		List<ServerAddress> seeds = new ArrayList<ServerAddress>();
//		if (!this.address.contains(";"))
//			seeds.add(new ServerAddress(this.address, this.port));
//		else {
//			String[] addresses = this.address.split(";");
//			for (int i = 0; i < addresses.length; i++)
//				seeds.add(new ServerAddress(addresses[i]));
//		}
//
//		if (this.username != null && !this.username.equals("")) {
//			MongoCredential credential = MongoCredential.createCredential(this.username, this.db, this.password.toCharArray());
//			this.mongo = new MongoClient(seeds, Arrays.asList(credential));
//		} else {
//			this.mongo = new MongoClient(seeds);
//		}
//		*/
//
////        try {
////            System.out.println("serverAddressList:" + new ObjectMapper().writeValueAsString(serverAddressList));
////            System.out.println("mongoCredentialList:" + new ObjectMapper().writeValueAsString(mongoCredentialList));
////        } catch (JsonProcessingException e) {
////            // TODO Auto-generated catch block
////            e.printStackTrace();
////        }
//
//        if (this.isHavePassword) {
//            this.mongo = new MongoClient(serverAddressList, mongoCredentialList);
//        } else {
//            this.mongo = new MongoClient(serverAddressList);
//        }
//    }
//
//    public void close() {
//        if (this.mongo != null)
//            this.mongo.close();
//    }
//
//    public void createIndex(String collection, DBObject keys, DBObject options) {
//        if (options == null)
//            this.mongo.getDB(this.db).getCollection(collection).createIndex(keys);
//        else
//            this.mongo.getDB(this.db).getCollection(collection).createIndex(keys, options);
//    }
//
//    public void batchInsert(String collection, List<DBObject> list) {
//        this.mongo.getDB(this.db).getCollection(collection).insert(list);
//    }
//
//    public String insert(String collection, DBObject jo) {
//        this.mongo.getDB(this.db).getCollection(collection).insert(jo);
//        return ((ObjectId) jo.get("_id")).toString();
//    }
//
//    public void save(String collection, DBObject jo) {
//        this.mongo.getDB(this.db).getCollection(collection).save(jo);
//    }
//
//    public Long count(String collection, DBObject query) {
//        return this.mongo.getDB(this.db).getCollection(collection).count(query);
//    }
//
//    public List<DBObject> find(String collection, DBObject ref, DBObject keys, DBObject sortBy) {
//        DBCursor dbCursor = null;
//        if(ref == null)
//            dbCursor = this.mongo.getDB(this.db).getCollection(collection).find();
//        else if (keys == null)
//            if (sortBy == null)
//                dbCursor = this.mongo.getDB(this.db).getCollection(collection).find(ref);
//            else
//                dbCursor = this.mongo.getDB(this.db).getCollection(collection).find(ref).sort(sortBy);
//        else if (sortBy == null)
//            dbCursor = this.mongo.getDB(this.db).getCollection(collection).find(ref, keys);
//        else
//            dbCursor = this.mongo.getDB(this.db).getCollection(collection).find(ref, keys).sort(sortBy);
//        return dbCursor.toArray();
//    }
//
//    public Integer update(String collection, DBObject query, DBObject operation, boolean upsert, boolean multi) {
//        WriteResult result = this.mongo.getDB(this.db).getCollection(collection).update(query, operation, upsert, multi);
//        return result.getN();
//    }
//
//    public void setAddress(String address, Integer port) {
//        this.address = address;
//        this.port = port;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public void setIsHavePassword(Boolean isHavePassword) {
//        this.isHavePassword = isHavePassword;
//    }
//
//    public void setDb(String db) {
//        this.db = db;
//    }
//
//    public DB getMongoDb(){
//        return this.mongo.getDB(this.db);
//    }
//
//    public void addServerAddress(String address, Integer port) throws UnknownHostException {
//        this.serverAddressList.add(new ServerAddress(address, port));
//    }
//
//    public void addMongoCredential(String username, String db, String password) {
//        this.mongoCredentialList.add(MongoCredential.createCredential(username, db, password.toCharArray()));
//    }
//
//}
