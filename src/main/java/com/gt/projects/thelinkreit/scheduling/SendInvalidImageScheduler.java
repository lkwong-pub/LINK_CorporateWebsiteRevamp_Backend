//package com.gt.projects.thelinkreit.scheduling;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//
//import com.gt.projects.thelinkreit.service.ApplicationService;
//import com.gt.projects.thelinkreit.service.FileService;
//
//@Service("sendInvalidImageScheduler")
//public class SendInvalidImageScheduler {
//
//	@Autowired
//	private ApplicationService applicationService;
//
//	@Autowired
//	private FileService fileService;
//
//	@Scheduled(cron = "0 0 0 * * *")
//	public void execute() {
//		this.sendInvalidImageEmail();
//	}
//
//	private void sendInvalidImageEmail() {
//		List<String[]> invalidImages = this.fileService.getInvalidImages();
//		this.applicationService.sendInvalidImageEmail(invalidImages);
//	}
//
//}
