package com.gt.projects.thelinkreit.entity.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class BaseResponse<T> {

	private static final long serialVersionUID = 4887660149735554786L;

	private Integer status = 0;
	private String developerMessage;
	private String errorCode;
	private T data;
	
	public BaseResponse() {
		super();
		this.developerMessage = "";
		this.errorCode = "";
	}
	public BaseResponse(T data ) {
		super();
		this.developerMessage = "";
		this.errorCode = "";
		this.data = data;
	}
	
}
