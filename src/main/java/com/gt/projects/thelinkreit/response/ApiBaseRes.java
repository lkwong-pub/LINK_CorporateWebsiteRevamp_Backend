package com.gt.projects.thelinkreit.response;

import lombok.Data;
/**
 * Created by gt on 16/11/2016.
 */

@Data
public abstract class ApiBaseRes {
    private String seoTitleEn;
    private String seoTitleTc;
    private String seoTitleSc;
    private String seoKeywordEn;
    private String seoKeywordTc;
    private String seoKeywordSc;
    private String seoDescriptionEn;
    private String seoDescriptionTc;
    private String seoDescriptionSc;
}
