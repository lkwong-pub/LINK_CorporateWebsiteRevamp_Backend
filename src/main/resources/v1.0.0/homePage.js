/**
	  
	  @api {get} http://www-dev.linkhk.com/linkweb/api/homepage/      Homepage
	  
	  @apiName Retrieve Website homepage content
	  @apiGroup Batch1-Homepage
	  @apiVersion 1.0.0
	  
	  @apiDescription
		Retrieve Website homepage content<br/>
		Data included 7 components content:<br/>
		1. SEO Details<br/>
		2. Banner Details<br/>
		3. The Network Details<br/>
		4. Happening Highlight Details<br/>
		5. Experience Highlight Details<br/> 
		6. Parallax Details<br/> 
		7. Important Notice Details<br/> 
	  
	  	@apiParam {Double} latitude 	[Optional] Device Current Latitude
	  	@apiParam {Double} longitude 	[Optional] Device Current Longitude
	  	
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {String} 	data.id			ID
		@apiSuccess (Success 200) {String} 	data.titleEn	SEO Title (EN)
		@apiSuccess (Success 200) {String} 	data.titleTc	SEO Title (TC)
		@apiSuccess (Success 200) {String} 	data.titleSc	SEO Title (SC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
		@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
		@apiSuccess (Success 200) {Object} 	data.banner		Banner Object
		@apiSuccess (Success 200) {String} 	data.banner.videoUrlTc		Banner Video URL (TC)
		@apiSuccess (Success 200) {String} 	data.banner.videoUrlSc		Banner Video URL (SC)
		@apiSuccess (Success 200) {String} 	data.banner.videoUrlEn		Banner Video URL (EN)
		@apiSuccess (Success 200) {String} 	data.banner.videoThumbnailEn 	Banner Video Thumbnail (EN)
		@apiSuccess (Success 200) {String} 	data.banner.videoThumbnailTc	Banner Video Thumbnail (TC)
		@apiSuccess (Success 200) {String} 	data.banner.videoThumbnailSc	Banner Video Thumbnail (SC)
		@apiSuccess (Success 200) {String} 	data.banner.videoAltTextEn		Banner Video Alt Text (EN)
		@apiSuccess (Success 200) {String} 	data.banner.videoAltTextTc		Banner Video Alt Text (TC)
		@apiSuccess (Success 200) {String} 	data.banner.videoAltTextSc		Banner Video Alt Text (SC)
		@apiSuccess (Success 200) {Object[]} 	data.banner.images  Banner Image Object Array
		@apiSuccess (Success 200) {String} 	data.banner.images.imgStandardEn		Banner Image (Desktop) (EN)
		@apiSuccess (Success 200) {String} 	data.banner.images.imgStandardTc		Banner Image (Desktop) (TC)
		@apiSuccess (Success 200) {String} 	data.banner.images.imgStandardSc		Banner Image (Desktop) (SC)
		@apiSuccess (Success 200) {String} 	data.banner.images.imgNarrowEn	Banner Image (Mobile) (EN)
		@apiSuccess (Success 200) {String} 	data.banner.images.imgNarrowTc	Banner Image (Mobile) (TC)
		@apiSuccess (Success 200) {String} 	data.banner.images.imgNarrowSc	Banner Image (Mobile) (SC)
		@apiSuccess (Success 200) {String} 	data.banner.images.altTextEn	Banner Image Alt Text (EN)
		@apiSuccess (Success 200) {String} 	data.banner.images.altTextTc	Banner Image Alt Text (SC)
		@apiSuccess (Success 200) {String} 	data.banner.images.altTextSc	Banner Image Alt Text (SC)
		@apiSuccess (Success 200) {Object} 	data.theNetwork  	The Network Object
		@apiSuccess (Success 200) {Object[]} 	data.theNetwork.parking  		The Network - Parking Object Array
		@apiSuccess (Success 200) {Integer} 	data.theNetwork.parking.facilityKey 	Parking ID
		@apiSuccess (Success 200) {String} 	data.theNetwork.parking.thumbnailPath 	Parking Thumbnail URL
		@apiSuccess (Success 200) {String} 	data.theNetwork.parking.carParkFacilityNameEn		Parking Name (EN)
		@apiSuccess (Success 200) {String} 	data.theNetwork.parking.carParkFacilityNameTc		Parking Name (TC)
		@apiSuccess (Success 200) {String} 	data.theNetwork.parking.carParkFacilityNameSc		Parking Name (SC)
		@apiSuccess (Success 200) {Object[]} 	data.theNetwork.shopping		The Network - Shopping Object Array
		@apiSuccess (Success 200) {String} 	data.theNetwork.shopping.shopCentreId	Shopping Centre ID
		@apiSuccess (Success 200) {String} 	data.theNetwork.shopping.thumbnailPath	Shopping Centre Thumbnail URL
		@apiSuccess (Success 200) {String} 	data.theNetwork.shopping.shopCentreNameEn		Shopping Centre Name (EN)
		@apiSuccess (Success 200) {String} 	data.theNetwork.shopping.shopCentreNameTc		Shopping Centre Name (TC)
		@apiSuccess (Success 200) {String} 	data.theNetwork.shopping.shopCentreNameSc		Shopping Centre Name (SC)
		@apiSuccess (Success 200) {Object[]} 	data.theNetwork.market		The Network - Market Object Array
		@apiSuccess (Success 200) {String} 	data.theNetwork.market.marketCode		Market Code
		@apiSuccess (Success 200) {String} 	data.theNetwork.market.thumbnailPath		Market Thumbnail URL
		@apiSuccess (Success 200) {String} 	data.theNetwork.market.marketNameEn		Market Name (EN)
		@apiSuccess (Success 200) {String} 	data.theNetwork.market.marketNameTc		Market Name (TC)
		@apiSuccess (Success 200) {String} 	data.theNetwork.market.marketNameSc		Market Name (SC)
		@apiSuccess (Success 200) {Object} 	data.happeningHighlight		Happening Highlight Object
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.titleEn		Happening Highlight Title (EN)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.titleTc		Happening Highlight Title (TC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.titleSc		Happening Highlight Title (SC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.subtitleEn		Happening Highlight Subtitle (EN)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.subtitleTc		Happening Highlight Subtitle (TC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.subtitleSc		Happening Highlight Subtitle (SC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.textEn		Happening Highlight Content (EN)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.textTc		Happening Highlight Content (TC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.textSc		Happening Highlight Content (SC)
		@apiSuccess (Success 200) {Object[]} 	data.happeningHighlight.eventList		Happening Highlight Event Object Array
		@apiSuccess (Success 200) {Integer} 	data.happeningHighlight.eventList.id		Happening Highlight Event ID
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightTitleEn	Happening Highlight Event Title (EN)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightTitleTc	Happening Highlight Event Title (TC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightTitleSc	Happening Highlight Event Title (SC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightImageUrlEn	Happening Highlight Event Image URL (EN)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightImageUrlTc	Happening Highlight Event Image URL (TC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightImageUrlSc	Happening Highlight Event Image URL (SC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightLinkEn	Happening Highlight Event Redirect URL (EN)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightLinkTc	Happening Highlight Event Redirect URL (TC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightLinkSc	Happening Highlight Event Redirect URL (SC)
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightStartDatetime 	Happening Highlight Event Start Time
		@apiSuccess (Success 200) {String} 	data.happeningHighlight.eventList.happeningHighlightEndDatetime	Happening Highlight Event End Time
		@apiSuccess (Success 200) {Object} 	data.experienceHighlight	Experience Highlight Object
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.backgroundStandard 	Experience Highlight Background Image (Desktop)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.backgroundNarrow		Experience Highlight Background Image (Mobile)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoThumbnailStandardEn 		Experience Highlight Video Thumbnail (Desktop) (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoThumbnailStandardTc		Experience Highlight Video Thumbnail (Desktop) (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoThumbnailStandardSc		Experience Highlight Video Thumbnail (Desktop) (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoThumbnailNarrowEn		Experience Highlight Video Thumbnail (Mobile) (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoThumbnailNarrowTc		Experience Highlight Video Thumbnail (Mobile) (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoThumbnailNarrowSc		Experience Highlight Video Thumbnail (Mobile) (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoUrlEn		Experience Highlight Video URL (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoUrlTc		Experience Highlight Video URL (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoUrlSc		Experience Highlight Video URL (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoAltTextEn		Experience Highlight Video Alt Text (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoAltTextTc		Experience Highlight Video Alt Text (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.videoAltTextSc		Experience Highlight Video Alt Text (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.headlineEn		Experience Highlight Headline (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.headlineTc		Experience Highlight Headline (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.headlineSc		Experience Highlight Headline (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.subHeadlineEn		Experience Highlight sub-headline (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.subHeadlineTc		Experience Highlight sub-headline (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.subHeadlineSc		Experience Highlight sub-headline (SC)
		@apiSuccess (Success 200) {Object[]} 	data.experienceHighlight.featureBlocks 	Feature Blocks Object Array
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.urlEn		Feature Blocks URL (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.urlTc		Feature Blocks URL (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.urlSc		Feature Blocks URL (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.imgStandard		Feature Blocks Image (Desktop)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.imgNarrow	Feature Blocks Image (Mobile)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.iconEn	Feature Blocks Icon (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.iconTc	Feature Blocks Icon (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.iconSc	Feature Blocks Icon (SC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.altTextEn		Feature Blocks Image Alt Text (EN)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.altTextTc		Feature Blocks Image Alt Text (TC)
		@apiSuccess (Success 200) {String} 	data.experienceHighlight.featureBlocks.altTextSc		Feature Blocks Image Alt Text (SC)
		@apiSuccess (Success 200) {Object[]} 	data.parallax[]		Parallax Object Array
		@apiSuccess (Success 200) {String} 	data.parallax.value		Parallax URL
		@apiSuccess (Success 200) {Object[]} 	data.importantNotices		Important Notices Object Array
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeImageUrlEn		Important Notices Image URL (EN)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeImageUrlTc		Important Notices Image URL (TC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeImageUrlSc		Important Notices Image URL (SC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeLinkEn		Important Notices URL (EN)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeLinkTc		Important Notices URL (TC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeLinkSc		Important Notices URL (SC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeTitleEn		Important Notices Title (EN)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeTitleTc		Important Notices Title (TC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeTitleSc		Important Notices Title (SC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeDescriptionEn		Important Notices Description (EN)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeDescriptionTc		Important Notices Description (TC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeDescriptionSc		Important Notices Description (SC)
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeStartDatetime		Important Notices Start Time
		@apiSuccess (Success 200) {String} 	data.importantNotices.importantNoticeEndDatetime		Important Notices End Time
		@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::HOME_PAGE_API_START::
{
	"error": "0",
	"error_message": "",
	"error_code": "",
	"data": {
		"id": 1,
		"seoTitle": "LINK Customer Site Homepage",
		"seoKeyword": "LINK Customer Site Homepage",
		"seoDescription": "This is LINK Customer Site Homepage.",
		"banner": {
			"videoUrlTc": "../data/sample.mp4",
			"videoUrlSc": "../data/sample.mp4",
			"videoUrlEn": "../data/sample.mp4",
			"videoThumbnailEn": "images/desktop/homepage/video_bg.jpg",
			"videoThumbnailTc": "images/desktop/homepage/video_bg.jpg",
			"videoThumbnailSc": "images/desktop/homepage/video_bg.jpg",
			"videoAltTextEn": "banner video alt text en",
			"videoAltTextTc": "視頻替代文本",
			"videoAltTextSc": "视频替代文本",
			"images": [{
				"imgStandardEn": "images/desktop/homepage/img_key_visual.jpg",
				"imgStandardTc": "images/desktop/homepage/img_key_visual.jpg",
				"imgStandardSc": "images/desktop/homepage/img_key_visual.jpg",
				"imgNarrowEn": "images/mobile/homepage/img_key_visual.jpg",
				"imgNarrowTc": "images/mobile/homepage/img_key_visual.jpg",
				"imgNarrowSc": "images/mobile/homepage/img_key_visual.jpg",
				"altTextEn": "banner image 1 alt text",
				"altTextTc": "橫幅圖片1 alt文字",
				"altTextSc": "横幅图片1 alt文字"
			}]
		},
		"theNetwork": {
			"parking" :[
				{
				"facilityKey" : 1037,
				"thumnailPath" : "",
				"carParkFacilityNameEn" :"",
				"carParkFacilityNameTc" :"",
				"carParkFacilityNameSc" :""
				},
				{
				"facilityKey" : 1038,
				"thumnailPath" : "",
				"carParkFacilityNameEn" :"",
				"carParkFacilityNameTc" :"",
				"carParkFacilityNameSc" :""
				},
				{
				"facilityKey" : 1039,
				"thumnailPath" : "",
				"carParkFacilityNameEn" :"",
				"carParkFacilityNameTc" :"",
				"carParkFacilityNameSc" :""
				}
			],
			"shopping" :[
				{
				"shopCentreId" : 1,
				"thumnailPath" : "",
				"shopCentreNameEn" :"",
				"shopCentreNameTc" :"",
				"shopCentreNameSc" :""
				},
				{
				"shopCentreId" : 1,
				"thumnailPath" : "",
				"shopCentreNameEn" :"",
				"shopCentreNameTc" :"",
				"shopCentreNameSc" :""
				},
				{
				"shopCentreId" : 1,
				"thumnailPath" : "",
				"shopCentreNameEn" :"",
				"shopCentreNameTc" :"",
				"shopCentreNameSc" :""
				}
			],
			"market" :[
				{
				"marketCode" : "",
				"thumnailPath" : "",
				"marketNameEn" :"",
				"marketNameTc" :"",
				"marketNameSc" :""
				},
				{
				"marketCode" : "",
				"thumnailPath" : "",
				"marketNameEn" :"",
				"marketNameTc" :"",
				"marketNameSc" :""
				},
				{
				"marketCode" : "",
				"thumnailPath" : "",
				"marketNameEn" :"",
				"marketNameTc" :"",
				"marketNameSc" :""
				}
			]
		},
		"happeningHighlight": {
			"titleEn": "happenings title",
			"titleTc": "精彩稱號",
			"titleSc": "精彩称号",
			"subtitleEn": "happenings subtitle",
			"subtitleTc": "發生的事情副標題",
			"subtitleSc": "发生的事情副标题",
			"textEn": "happenings text",
			"textTc": "精彩的文字",
			"textSc": "精彩的文字",
			"eventList": [{
				"id": 1,
				"happeningHighlightTitleTc": "精彩亮點1",
				"happeningHighlightTitleSc": "精彩亮点1",
				"happeningHighlightTitleEn": "happening highlight 1",
				"happeningHighlightDescriptionTc": "描述1",
				"happeningHighlightDescriptionSc": "描述1",
				"happeningHighlightDescriptionEn": "description 1",
				"happeningHighlightImageUrlTc": "images/desktop/homepage/img_event_party.jpg",
				"happeningHighlightImageUrlSc": "images/desktop/homepage/img_event_party.jpg",
				"happeningHighlightImageUrlEn": "images/desktop/homepage/img_event_party.jpg",
				"happeningHighlightLinkTc": "/SiteAssets/LINK_30SEC_0826.mp4",
				"happeningHighlightLinkSc": "/SiteAssets/LINK_30SEC_0826.mp4",
				"happeningHighlightLinkEn": "/SiteAssets/LINK_30SEC_0826.mp4",
				"happeningHighlightStartDatetime": "2016-04-15T00:00:00+08:00",
				"happeningHighlightEndDatetime": "2016-05-31T00:00:00+08:00"
			}]
		},
		"experienceHighlight": {
			"backgroundStandard": "images/desktop/homepage/img_bg_experience.jpg",
			"backgroundNarrow": "images/mobile/homepage/img_bg_experience.jpg",
			"videoThumbnailStandardEn": "images/desktop/homepage/img_video_thumbnail.jpg",
			"videoThumbnailStandardTc": "images/desktop/homepage/img_video_thumbnail.jpg",
			"videoThumbnailStandardSc": "images/desktop/homepage/img_video_thumbnail.jpg",
			"videoThumbnailNarrowEn": "images/mobile/homepage/img_video_thumbnail.jpg",
			"videoThumbnailNarrowTc": "images/mobile/homepage/img_video_thumbnail.jpg",
			"videoThumbnailNarrowSc": "images/mobile/homepage/img_video_thumbnail.jpg",
			"videoUrlEn": "../data/LINK_30SEC_0826.mp4",
			"videoUrlTc": "../data/LINK_30SEC_0826.mp4",
			"videoUrlSc": "../data/LINK_30SEC_0826.mp4",
			"videoAltTextEn": "experience highlight video alt text",
			"videoAltTextTc": "體驗精彩場面視頻替代文本",
			"videoAltTextSc": "体验精彩场面视频替代文本",
			"headlineEn": "PARK & DINE WITH EASE",
			"headlineTc": "泊車 搵食，從未如此輕鬆方便",
			"headlineSc": "停车 搜吃，从未如此轻松方便",
			"subHeadlineEn": "Enjoy Your Experience",
			"subHeadlineTc": "Enjoy Your Experience TC",
			"subHeadlineSc": "Enjoy Your Experience SC",
			"featureBlocks": [{
	          "imgStandard": "",
	          "imgNarrow": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconEn": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconTc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconSc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "altTextEn": "Experience Highlight Feature Block 1 Alt Text",
	          "altTextTc": "體驗高亮功能塊1替換文本",
	          "altTextSc": "体验高亮功能块1替换文本",
	          "urlEn": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlTc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlSc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4"
	        },
	        {
	          "imgStandard": "",
	          "imgNarrow": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconEn": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconTc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconSc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "altTextEn": "Experience Highlight Feature Block 2 Alt Text",
	          "altTextTc": "體驗高亮功能塊2替換文本",
	          "altTextSc": "体验高亮功能块2替换文本",
	          "urlEn": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlTc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlSc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4"
	        },
	        {
	          "imgStandard": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/HomePage/deco_balloon.png",
	          "imgNarrow": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconEn": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconTc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconSc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "altTextEn": "Experience Highlight Feature Block 3 Alt Text",
	          "altTextTc": "體驗高亮功能塊3替換文本",
	          "altTextSc": "体验高亮功能块3替换文本",
	          "urlEn": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlTc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlSc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4"
	        },
	        {
	          "imgStandard": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "imgNarrow": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconEn": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconTc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "iconSc": "https://ws-dev.linkreit.com/linkapi/file//SiteAssets/banner img 1 desktop.PNG",
	          "altTextEn": "Experience Highlight Feature Block 4 Alt Text",
	          "altTextTc": "體驗高亮功能塊4替換文本",
	          "altTextSc": "体验高亮功能块4替换文本",
	          "urlEn": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlTc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4",
	          "urlSc": "http://devcms01/SiteAssets/LINK_30SEC_0826.mp4"
        }]
		},
		"parallax": [{
			"value": "/SiteAssets/para img 1.PNG"
		},
		{
			"value": "/SiteAssets/para img 2.PNG"
			
		},
		{
			"value": "/SiteAssets/para img 3.PNG"
			
		},
		{
			"value": "/SiteAssets/para img 4.PNG"
			
		},
		{
			"value": "/SiteAssets/para img 5.PNG"
			
		},
		{
			"value": "/SiteAssets/para img 6.PNG"
			
		}],
		"importantNotices": [{
			"importantNoticeImageUrlEn": "http://devcms01/SiteAssets/para img 1.PNG",
			"importantNoticeImageUrlTc": "http://devcms01/SiteAssets/feature block 2.PNG",
			"importantNoticeImageUrlSc": "http://devcms01/SiteAssets/feature block 3.PNG",
			"importantNoticeLinkEn": "/SiteAssets/LINK_30SEC_0826.mp4",
			"importantNoticeLinkTc": "/SiteAssets/LINK_30SEC_0826.mp4",
			"importantNoticeLinkSc": "/SiteAssets/LINK_30SEC_0826.mp4",
			"importantNoticeTitleEn": "important notice 1",
			"importantNoticeTitleTc": "重要資料",
			"importantNoticeTitleSc": "重要資料",
			"importantNoticeDescriptionEn": "description",
			"importantNoticeDescriptionTc": "描述",
			"importantNoticeDescriptionSc": "描述",
			"importantNoticeStartDatetime": "2016-05-12T00:00:00+08:00",
			"importantNoticeEndDatetime": null
		}]
	}
}::HOME_PAGE_API_END::*/