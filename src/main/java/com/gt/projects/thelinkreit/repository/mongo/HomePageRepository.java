package com.gt.projects.thelinkreit.repository.mongo;

import com.gt.projects.thelinkreit.entity.mongo.HomePage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomePageRepository extends MongoRepository<HomePage, String> {
}
