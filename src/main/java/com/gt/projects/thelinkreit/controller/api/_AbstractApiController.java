package com.gt.projects.thelinkreit.controller.api;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.gt.projects.thelinkreit.entity.response.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gt.projects.thelinkreit.exception.AssetNotFoundException;

public abstract class _AbstractApiController {

	@ExceptionHandler(MissingServletRequestParameterException.class)
	public @ResponseBody
	BaseResponse handleMissingServletRequestParameterException(MissingServletRequestParameterException exception) {

		BaseResponse responseObject = new BaseResponse();

		responseObject.setErrorCode("9999");
		responseObject.setDeveloperMessage(exception.getMessage());

		return responseObject;

	}

	@ExceptionHandler(Exception.class)
	public @ResponseBody
	BaseResponse handleAllException(Exception exception) {
		exception.printStackTrace();

		BaseResponse baseResponse = new BaseResponse();

		baseResponse.setErrorCode("9999");
		baseResponse.setDeveloperMessage(exception.getMessage());

		return baseResponse;

	}

	@ExceptionHandler(AssetNotFoundException.class)
	public ResponseEntity<String> ResponseObjecthandleAssetNotFoundException(AssetNotFoundException exception) {
		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}

	protected String removeAllHtml(String source) {
		String output = source;
		if (output != null) {
			output = output.replaceAll("<br\\s*/>", "\n");
			output = output.replaceAll("\\<.*?>", "");
		}
		return output;
	}

	protected void addGoogleAnalyticsMap(List<Map<String, Object>> gas, Integer index, Object value) {
		if (index != null && value != null) {
			Map<String, Object> ga = new LinkedHashMap<String, Object>();
			ga.put("index", index);
			ga.put("value", value);
			gas.add(ga);
		}
	}
	
	protected HashMap getTempJson(String filePath, String tempJsonKey){
		try{
			String path = this.getClass().getResource(filePath).getPath();
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			String jsonStr = new String(encoded);
			String startKey = "::"+tempJsonKey+"_START::";
			String endKey = "::"+tempJsonKey+"_END::";
			jsonStr = jsonStr.substring(jsonStr.indexOf(startKey) + startKey.length(),jsonStr.indexOf(endKey));
			return new ObjectMapper().readValue(jsonStr, HashMap.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	};

}
