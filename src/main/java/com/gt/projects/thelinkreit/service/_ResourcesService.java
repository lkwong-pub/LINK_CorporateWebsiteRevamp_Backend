//package com.gt.projects.thelinkreit.service;
//
//import java.util.List;
//import java.util.Map;
//
//import com.gt.projects.thelinkreit.entity.resources.Area;
//import com.gt.projects.thelinkreit.entity.resources.CarParkPrivilegeType;
//import com.gt.projects.thelinkreit.entity.resources.District;
//import com.gt.projects.thelinkreit.entity.resources.PaymentOptionType;
//import com.gt.projects.thelinkreit.entity.resources.Shop;
//import com.gt.projects.thelinkreit.entity.resources.ShopCategory;
//import com.gt.projects.thelinkreit.entity.resources.ShopCategoryType;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentre;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentreFeatureType;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentreServiceType;
//import com.gt.projects.thelinkreit.entity.resources.ShopEx;
//import com.gt.projects.thelinkreit.entity.resources.ShopPrivilegeType;
//import com.gt.projects.thelinkreit.entity.resources.ShopType;
//import com.gt.projects.thelinkreit.entity.resources.CarParkFacility;
//
//public interface _ResourcesService {
//
//	public void retrieveAllData();
//
//	public List<Area> getAreas();
//
//	public List<District> getDistricts();
//
//	public List<District> getDistrictsByArea(String area);
//
//	public List<ShopType> getShopTypes();
//
//	public List<ShopCategoryType> getShopCategoryTypesByShopTypeName(String shopTypeName);
//
//	public List<CarParkPrivilegeType> getCarParkPrivilegeTypes();
//
//	public Integer countTbCarParkFacilities();
//
//	public List<CarParkFacility> getNearestTbCarParkFacilities(Double longitude, Double latitude, List<CarParkFacility> exceptTbCarParkFacilities, int offset, int size);
//
//	// public CarPark getCarParkMapByPropCode(String propCode);
//
//	public ShopCentre getShopCentreById(Integer id);
//
//	public CarParkFacility getTbCarParkFacilityById(Integer facilityKey);
//
//	public CarParkFacility getRealTimeTbCarParkFacilityById(Integer facilityKey);
//
//	public Map<Integer, CarParkFacility> getTbCarParkFacilityMapByIdIn(List<Integer> facilityKeys);
//
//	public List<CarParkPrivilegeType> getCarParkPrivilegeTypesByFacilityKey(Integer facilityKey);
//
//	public List<PaymentOptionType> getPaymentOptionTypesByFacilityKey(Integer facilityKey);
//
//	public List<Shop> getRandomShopsByShopCentreIdNShopTypeName(Integer shopCentreId, String shopTypeName, int size);
//
//	public List<ShopCategoryType> getShopCategoryTypesByShopId(Integer shopId);
//
//	public ShopEx getShopExByShopCentreIdNBuildingEnNShopNo(Integer shopCentreId, String buildingEn, String shopNo);
//
//	public District getDistrictById(Integer id);
//
//	public Area getAreaByCode(String code);
//
//	public List<CarParkFacility> getTbCarParkFacilitiesByDistrictId(Integer districtId, int offset, int size);
//
//	public Object[] searchShopByKeyword(String keyword, String language, String shopTypeName, int offset, int size);
//
//	public Object[] searchShopCentreByKeyword(String keyword, String language, int offset, int size);
//
//	public List<ShopCentre> getShopCentres();
//
//	public List<ShopCentre> getShopCentresByDistrictId(Integer districtId);
//
//	public List<ShopCentre> getShopCentresByAreaCode(String code);
//
//	public ShopCentreFeatureType getShopCentreFeatureTypeById(Integer id);
//
//	public List<ShopCentreServiceType> getShopCentreServiceTypesByShopCentreId(Integer shopCentreId);
//
//	public List<CarParkFacility> getTbCarParkFacilitiesByShopCentrePropCode(String propCode);
//
//	// public List<TbCarParkFacility> getTbCarParkFacilitiesByParentPropCode(String propCode);
//
//	// public List<CarPark> getCarParkByShopCentreId(Integer shopCentreId);
//
//	public List<ShopCategoryType> getShopCategoryTypesByShopTypeNameNShopCentreId(String shopTypeName, Integer shopCentreId);
//
//	public Object[] getShopsByShopCentreIdNShopTypeName(Integer shopCentreId, String shopTypeName, int offset, int size);
//
//	public ShopType getShopTypeById(Integer id);
//
//	public Shop getShopById(Integer id);
//
//	public List<ShopPrivilegeType> getShopPrivilegeTypesByShopId(Integer shopId);
//
//	public List<ShopCentre> getShopCentresByShopCategoryTypeId(Integer shopCategoryTypeId);
//
//	public Object[] getShopByAreaNDistrictIdNShopCentreIdNShopTypeIdNShopCategoryTypeId(String language, String area, Integer districtId, Integer shopCentreId, Integer shopTypeId, Integer shopCategoryTypeId, int offset, int size);
//
//	public Object[] getTbCarParkFacilitiesByAreaNDistrictIdNPrivilegeTypeId(String area, Integer districtId, Integer privilegeTypeId, int offset, int size);
//
//	public List<Shop> getShopsWithSameShopName(Shop shop, String areaCode, Integer districtId, Integer shopCentreId, Integer shopCategoryTypeId);
//
//	public List<Shop> getShops();
//
//	public List<Shop> getShopByShopCentreId(Integer shopCentreId);
//
//	public List<Integer> getShopTypeIds(Shop shop);
//
//	public String getShopImage(ShopCentre shopCentre, Shop shop, String imagePath);
//
//	public String getShopImage(ShopCentre shopCentre, Shop shop);
//
//	public List<ShopCentre> getShopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight(Integer shopCentreFeatureTypeId);
//
//	public List<ShopCentreFeatureType> getShopCentreFeatureTypes();
//
//	public List<Shop> getShopsGroupByNameEn();
//
//	public List<ShopCentre> getSortedShopCentres();
//
//	public List<ShopCentre> getShopCentresByPropCode(List<String> propCodes);
//
//	public ShopCentre getShopCentreByPropCode(String propCode);
//
//	public Shop getShopByShopInfo(List<Object> shopKey);
//
//	public List<ShopCategory> getShopCategoryGroupByShopId(Integer shopId);
//
//	public ShopCategoryType getShopCategoryTypeById(Integer shopCategoryTypeId);
//
//	public String getCustomColumnDisplayName(String tableName, String customColumn);
//
//	public String getShopExCustomColumnDisplayName(Integer fieldNo, String language);
//
//	public String getTbCarParkFacilityCustomColumnDisplayName(Integer fieldNo, String language);
//
//	public Long countShopCentre();
//
////	public Integer countPromotion();
//
//}
