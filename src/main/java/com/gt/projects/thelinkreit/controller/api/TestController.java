package com.gt.projects.thelinkreit.controller.api;

import com.gt.projects.thelinkreit.config.Url;
import com.gt.projects.thelinkreit.entity.mongo.HomePage;
import com.gt.projects.thelinkreit.entity.response.BaseResponse;
import com.gt.projects.thelinkreit.service.HomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(Url.API_PREFIX + Url.V1 + "/test")
public class TestController {

    @Autowired
    HomePageService homePageService;

    @RequestMapping(value = "/")
    public BaseResponse<HomePage> test() {
        List<HomePage> test = homePageService.findAll();
        return new BaseResponse(homePageService.findAll());
    }
}
