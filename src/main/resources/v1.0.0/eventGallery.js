/**
	 
@api {get} http://www-dev.linkhk.com/linkweb/api/eventGallery/  Event Gallery Listing
 
@apiName Event Gallery List
@apiGroup Batch3_1-EventGallery
 
@apiVersion 1.0.0
 
@apiDescription 
Retrieve EventGallery Listing Page content<br/> 
Data included 5 components content:<br/>
1. SEO Details<br/> 
2. Banner<br/>
3. Event Gallery Year Bar <br/>
4. Event Gallery List<br/> 
5. Paging Info<br/> 
 
 @apiParam {Integer}	year		Selected Year ( Default = null )
 @apiParam {Integer}	pageNo		Page No ( start = 0 )
 @apiParam {Integer}	pageSize	Number of list record per request
 
@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
@apiSuccess (Success 200) {String} 	data		Data Object.
@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
@apiSuccess (Success 200) {Object} 	data.banner		EventGallery List - Banner Object
@apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
@apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
@apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
@apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
@apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
@apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
@apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
@apiSuccess (Success 200) {Integer} data.filterBarStartYear		Year Filter Bar Start Year		
@apiSuccess (Success 200) {Object[]} 	data.eventGalleryList		EventGallery List - EventGallery List Object Array
@apiSuccess (Success 200) {Integer} 	data.eventGalleryList.id	EventGallery ID
@apiSuccess (Success 200) {String} 	data.eventGalleryList.titleEn		Event Gallery Title (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryList.titleTc		Event Gallery Title (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryList.titleSc		Event Gallery Title (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryList.dateEn		Event Gallery Date (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryList.dateTc		Event Gallery Date (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryList.dateSc		Event Gallery Date (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryList.thumbnail		EventGallery Thumbnail
@apiSuccess (Success 200) {Object} 	data.pageInfo	EventGallery List - Page Info Object
@apiSuccess (Success 200) {Integer} 	data.pageInfo.total		No. of Total Record
@apiSuccess (Success 200) {Integer} 	data.pageInfo.pageCount		No. of Page Count
@apiSuccess (Success 200) {Integer} 	data.pageInfo.pageSize		No. per page
@apiSuccess (Success 200) {Integer} 	data.pageInfo.curPage		Current Page No
		@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::EVENT_GALLERY_LIST_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "seoTitleEn":"Event Gallery",
      "seoTitleTc":"活動相冊",
      "seoTitleSc":"活动相册",
      "seoKeywordEn":"SEO keywords",
      "seoKeywordTc":"搜索引擎優化 關鍵詞",
      "seoKeywordSc":"搜索引擎优化 关键词",
      "seoDescriptionEn":"SEO Description",
      "seoDescriptionTc":"搜索引擎優化 描述",
      "seoDescriptionSc":"搜索引擎优化 描述",
      "banner":{  
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "filterBarStartYear":2000,
      "eventGalleryList":[  
         {  
            "id":1,
            "titleEn":"Promotion 1 ",
            "titleTc":"Promotion 1 TC",
            "titleSc":"Promotion 1 SC",
            "dateEn":"2016 01 01",
            "dateTc":"2016 01 01",
            "dateSc":"2016 01 01",
            "thumbnail":""
         }
      ],
      "pageInfo":{  
         "total":49,
         "pageCount":1,
         "pageSize":49,
         "curPage":1
      }
   }
}
::EVENT_GALLERY_LIST_API_END::
	*/

/**
 @api {get} http://www-dev.linkhk.com/linkweb/api/eventGallery/{id}  Event Gallery Details
 
 @apiName Event Gallery Details
 @apiGroup Batch3_1-EventGallery
 @apiVersion 1.0.0
 
 @apiDescription 
 Retrieve EventGallery Details Page content<br/> 
 Data included 3 components content:<br/>
 1. SEO Details<br/> 
 2. EventGallery Info<br/> 
 3. EventGallery<br/> 
 
 @apiParam {Integer}	id		EventGallery ID
	 
@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
@apiSuccess (Success 200) {String} 	data		Data Object.
@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
@apiSuccess (Success 200) {Object} 	data.eventGalleryInfo		EventGallery - EventGallery Info Object
@apiSuccess (Success 200) {Integer} 	data.eventGalleryInfo.id		Event Gallery ID
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.headlineEn	Event Gallery  Section Headline (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.headlineTc	Event Gallery  Section Headline (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.headlineSc	Event Gallery  Section Headline (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.titleEn		Event Gallery Title (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.titleTc		Event Gallery Title (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.titleSc		Event Gallery Title (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.dateEn		Event Gallery Date (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.dateTc		Event Gallery Date (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.dateSc		Event Gallery Date (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.timeEn		Event Gallery Time (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.timeTc		Event Gallery Time (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.timeSc		Event Gallery Time (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.introductionEn		Event Gallery Introduction (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.introductionTc		Event Gallery Introduction (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.introductionSc		Event Gallery Introduction (SC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.locationEn		Event Gallery Location Name (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.locationTc		Event Gallery Location Name (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.locationSc		Event Gallery Location Name (SC)
@apiSuccess (Success 200) {Boolean} 	data.eventGalleryInfo.isCustomLocation		Custom Location Checking (* Add further click action on Location Text is value = true)
@apiSuccess (Success 200) {Boolean} 	data.eventGalleryInfo.isSingleLocation		Single Location Name Checking (* Redirect to shop centre details page if value = true)
@apiSuccess (Success 200) {Integer} 	data.eventGalleryInfo.singleShopCentreId	Single Location - Shop Centre ID
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.singlePropCode	Single Location - Shop Centre Property Code
@apiSuccess (Success 200) {Object[]} 	data.eventGalleryInfo.shopCentreList	Promotion - Shop Centre Area List Object Array
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.areaCode	Area Code
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.areaNameEn	Area Name (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.areaNameTc	Area Name (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.areaNameSc	Area Name (SC)
@apiSuccess (Success 200) {Object[]} 	data.eventGalleryInfo.shopCentreList.shopCentres	Shop Centre List Object Array
@apiSuccess (Success 200) {Integer} 	data.eventGalleryInfo.shopCentreList.shopCentres.shopCentreId		Shop Centre ID
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.shopCentres.propCode		Shop Centre Property Code
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.shopCentres.shopCentreNameEn		Shop Centre Name (EN)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.shopCentres.shopCentreNameTc		Shop Centre Name (TC)
@apiSuccess (Success 200) {String} 	data.eventGalleryInfo.shopCentreList.shopCentres.shopCentreNameSc		Shop Centre Name (SC)
@apiSuccess (Success 200) {Object} 	data.eventGallery		Event Gallery - Event Gallery Object
@apiSuccess (Success 200) {Object[]} 	data.eventGallery.eventImages	Event Image Object Array
@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.thumbnail		Image URL
@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextEn		Image Alt Text (EN)
@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextTc		Image Alt Text (TC)
@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextSc		Image Alt Text (SC)
@apiSuccess (Success 200) {Object[]} 	data.eventGallery.eventVideos	Event Video Object Array
@apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.videoThumbnail	Event Video Thumbnail
@apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.videoUrl			Event Video URL
@apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.altVideoUrl		Event Alt Video URL
@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::EVENT_GALLERY_DETAILS_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "id":1,
      "seoTitleEn":"LINK Customer Site Event Gallery Details EN",
      "seoTitleTc":"LINK Customer Site Event Gallery Details TC",
      "seoTitleSc":"LINK Customer Site Event Gallery Details SC",
      "seoKeywordEn":"LINK Customer Site Event Gallery Details EN",
      "seoKeywordTc":"LINK Customer Site Event Gallery Details TC",
      "seoKeywordSc":"LINK Customer Site Event Gallery Details SC",
      "seoDescriptionEn":"This is LINK Customer Site Event Gallery Details. EN",
      "seoDescriptionTc":"This is LINK Customer Site Event Gallery Details. TC",
      "seoDescriptionSc":"This is LINK Customer Site Event Gallery Details. SC",
      "eventGalleryInfo":{  
         "id":17402,
         "headlineEn":"Event Gallery",
         "headlineTc":"Event Gallery TC",
         "headlineSc":"Event Gallery SC",
         "titleEn":"Promotion",
         "titleTc":"Promotion TC",
         "titleSc":"Promotion SC",
         "dateEn":"2016 01 01",
         "dateTc":"2016 01 01",
         "dateSC":"2016 01 01",
         "timeEn":"",
         "timeTc":"",
         "timeSc":"",
         "introductionEn":"To celebrate the Grand Opening of brand new Lok Fu Market, The LINK had partnered with Metro Radio programme - \"Happy Family\" and \"La Gourment\", to launch a series of dining activities to allow customers to experience a brand-new shopping environment. Meanwhile, the first ever programme \"Junior Shoppers\" in market and grand reward programme has been launched!",
         "introductionTc":"為慶祝全新的樂福市場隆重開業，鏈接曾與新城電台節目合作 - “快樂家族”和“香格里拉味精”，推出一系列的餐飲活動，讓客戶體驗到一個全新的購物環境。同時，首次節目“少年購物者”，在市場上和大獎勵計劃已經啟動！",
         "introductionSc":"为庆祝全新的乐福市场隆重开业，链接曾与新城电台节目合作 - “快乐家族”和“香格里拉味精”，推出一系列的餐饮活动，让客户体验到一个全新的购物环境。同时，首次节目“少年购物者”，在市场上和大奖励计划已经启动！",
         "locationEn":"8 Shopping Centre",
         "locationTc":"8 Shopping Centre TC",
         "locationSc":"8 Shopping Centre SC",
         "isCustomLocation":true,
         "isSingleLocation" : false,
         "singleShopCentreId" : 1,
         "singlePropCode" : "chfxc2",
         "shopCentreList":[  
            {  
               "areaCode":"NT",
               "areaNameEn":"HKISLAND",
               "areaNameTc":"HKISLAND TC",
               "areaNameSc":"HKISLAND SC",
               "shopCentres":[  
                  {  
                     "shopCentreId":1,
                     "propCode" : "chfxc2",
                     "shopCentreNameEn":"",
                     "shopCentreNameTc":"",
                     "shopCentreNameSc":""
                  }
               ]
            }
         ]
      },
      "eventGallery":{  
         "eventImages":[  
            {  
               "thumbnail":"",
               "altTextEn":"",
               "altTextTc":"",
               "altTextSc":""
            }
         ],
         "eventVideos":[  
            {  
               "videoThumbnail":"",
               "videoUrl":"http://www.youtube.com",
               "altVideoUrl":""
            }
         ]
      }
   }
}
::EVENT_GALLERY_DETAILS_API_END::
	*/