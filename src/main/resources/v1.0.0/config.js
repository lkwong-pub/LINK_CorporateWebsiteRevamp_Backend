/**

@api {post} http://www-dev.linkhk.com/linkweb/api/config/checkImage      Image file format checking
@apiName Image file format checking
@apiGroup Batch1-Other
@apiVersion 1.0.0

@apiDescription 
Image file format checking. JPG/PNG format allow to be uploaded by CMS. 	


@apiParam {File[]}	images		Image file (Support multiple file checking)	

@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
@apiSuccess (Success 200) {Object[]} 	data		Data Object Array.
@apiSuccess (Success 200) {String} 	data.file 	Image file name 
@apiSuccess (Success 200) {boolean} 	data.result 	Image format checking result

@apiSuccessExample {json} Example
HTTP/1.1 200 OK
{
	"error": 0,
	"error_message": null,
	"error_code": null,
	"data": [
		{
			"file" : "abc.jpg"
			"result" : true
		}
	]
}
		
*/