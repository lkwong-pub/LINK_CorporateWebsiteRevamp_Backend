/**
 * 
 * @api {get} http://www-dev.linkhk.com/linkweb/api/searchCriteria/      Search Criteria
 * @apiName Retrieve Search Criteria Content
 * @apiGroup Batch1-Other
 * @apiVersion 1.0.0
 * 
  @apiDescription 
	Retrieve Search Criteria Content<br/>
	Data included 5 components content:<br/>
	1. District Full List<br/>
	2. Shop Centre Full List<br/>
	3. Shop Category Full List<br/>
    4. Car Park Full List<br/>
    3. Car Park Service Full List<br/>
 * 
 * @apiParam {Integer}	sectionType		Section Type (1= Homepage, 2=Shop List Page, 3=Dine List Page, 4=Visit Us Page, 5=Shop Centre List, 6=Car Park List)
 * 
	@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
	@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
	@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
	@apiSuccess (Success 200) {Object} 	data		Data Object.
	@apiSuccess (Success 200) {Object[]} 	data.districtsList[] 	District Object Array
	@apiSuccess (Success 200) {String} 		data.districtsList.areaCode 	Area Code
	@apiSuccess (Success 200) {String} 	data.districtsList.areaNameEn 	Area Name (EN) 
	@apiSuccess (Success 200) {String} 	data.districtsList.areaNameTc		Area Name (TC)
	@apiSuccess (Success 200) {String} 	data.districtsList.areaNameSc		Area Name (SC)
	@apiSuccess (Success 200) {Object[]} 	data.districtsList.districts[]		District Object Array
	@apiSuccess (Success 200) {Integer} 	data.districtsList.districts.districtId		District ID
	@apiSuccess (Success 200) {String} 	data.districtsList.districts.districtNameEn		District Name (EN)
	@apiSuccess (Success 200) {String} 	data.districtsList.districts.districtNameTc		District Name (TC)
	@apiSuccess (Success 200) {String} 	data.districtsList.districts.districtNameSc		District Name (SC)
	@apiSuccess (Success 200) {Object[]} 	data.shopCentreList[]		Shop Centre OBject Array
	@apiSuccess (Success 200) {Integer} 	data.shopCentreList.shopCentreId		Shop Centre ID
	@apiSuccess (Success 200) {Integer} 	data.shopCentreList.districtId		Shop Centre - District ID
	@apiSuccess (Success 200) {String} 	data.shopCentreList.carParkFacilityIds	Shop Centre Included shop category ID
	@apiSuccess (Success 200) {String} 	data.shopCentreList.shopCentreNameEn		Shop Centre Name (EN)
	@apiSuccess (Success 200) {String} 	data.shopCentreList.shopCentreNameTc		Shop Centre Name (TC)
	@apiSuccess (Success 200) {String} 	data.shopCentreList.shopCentreNameSc		Shop Centre Name (SC)
	@apiSuccess (Success 200) {Object[]} 	data.categoryList[]		Shop Category Object Array
	@apiSuccess (Success 200) {Integer} 	data.categoryList.shopCategoryId		Shop Category ID
	@apiSuccess (Success 200) {String} 	data.categoryList.shopCategoryNameEn		Shop Category Name (EN)
	@apiSuccess (Success 200) {String} 	data.categoryList.shopCategoryNameTc		Shop Category Name (TC)
	@apiSuccess (Success 200) {String} 	data.categoryList.shopCategoryNameSc		Shop Category Name (SC)
 	@apiSuccess (Success 200) {Object[]} 	data.carParkList[]		Car Park Object Array
	@apiSuccess (Success 200) {Integer} 	data.carParkList.facilityKey		Car Park ID
	@apiSuccess (Success 200) {String} 	data.carParkList.propCode		Property Code
	@apiSuccess (Success 200) {Integer} 	data.carParkList.districtId		Car Park - District ID
	@apiSuccess (Success 200) {String} 	data.carParkList.type		Type
	@apiSuccess (Success 200) {String} 	data.carParkList.propNameEn		Property Name (EN)
	@apiSuccess (Success 200) {String} 	data.carParkList.propNameTc		Property Name (TC)
	@apiSuccess (Success 200) {String} 	data.carParkList.propNameSc		Property Name (SC)
	@apiSuccess (Success 200) {Object[]} 	data.carParkPrivilegeTypeList[]		Car Park Service Object Array
	@apiSuccess (Success 200) {Integer} 	data.carParkPrivilegeTypeList.carParkPrivilegeTypeId		Car Park Service ID
	@apiSuccess (Success 200) {String} 	data.carParkPrivilegeTypeList.carParkPrivilegeTypeNameEn		Car Park Service Name (EN)
	@apiSuccess (Success 200) {String} 	data.carParkPrivilegeTypeList.carParkPrivilegeTypeNameTc		Car Park Service Name (TC)
	@apiSuccess (Success 200) {String} 	data.carParkPrivilegeTypeList.carParkPrivilegeTypeNameSc		Car Park Service Name (SC)

	@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::SEARCH_API_START::
{
	"error": 0,
	"error_message": null,
	"error_code": null,
	"data": {
		"districtsList": {
			"areaCode": "",
			"areaNameEn": "",
			"areaNameTc": "",
			"areaNameSc": "",
			"districts": [{
				"districtId": 1,
				"districtNameEn": "Tai Po",
				"districtNameTc": "大埔",
				"districtNameSc": "大埔"
			}]
		},
		"shopCentreList": [{
			"shopCentreId": 1,
			"districtId": 1,
			"carParkFacilityIds": "1,2,3,4,5",
			"shopCentreNameEn": "Choi Fai Estate Retail and Car Park",
			"shopCentreNameTc": "彩輝邨商舖及停車場",
			"shopCentreNameSc": "彩辉邨商舖及停车场"
		}],
		"categoryList": [{
			"shopCategoryId": 1,
			"shopCategoryNameEn": "Western Cuisine",
			"shopCategoryNameTc": "西餐",
			"shopCategoryNameSc": "西餐"
		}],
		"carParkList": [{
			"facilityKey": null,
			"propCode": "sswxc2",
			"districtId": null,
			"type": "Label_ShopCentre",
			"propNameEn": "Siu Sai Wan Plaza",
			"propNameTc": "小西灣廣場",
			"propNameSc": "小西湾广场"
		  },
		  {
			"facilityKey": 6403,
			"propCode": "",
			"districtId": null,
			"type": "Label_CarPark",
			"propNameEn": "Ma Hang Car Park A ",
			"propNameTc": "馬坑A停車場",
			"propNameSc": "马坑A停车场"
		  }],
		"carParkPrivilegeTypeList": [{
			"carParkPrivilegeTypeId": 1,
			"carParkPrivilegeTypeNameEn": "12 hour parking",
			"carParkPrivilegeTypeNameTc": "12 hour parking",
			"carParkPrivilegeTypeNameSc": "12 hour parking"
		}]
	}
}
::SEARCH_API_END::
*/