package com.gt.projects.thelinkreit.config.database;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@PropertySources(value = { @PropertySource({ "classpath:config/mongodb.datasource.resources.properties" }) })
@EnableMongoRepositories( basePackages = {"com.gt.projects.thelinkreit.repository.mongo"} )
public class ResourcesMongodbDataSourceConfig extends AbstractMongoConfiguration {

    @Autowired
	private Environment environment;

    protected String getDatabaseName() {
        return "link_corpweb";
    }


//    public Mongo mongo() throws Exception {
//        return new MongoClient(new ServerAddress(environment.getProperty("mongodb.dataSource.resources.address1"), Integer.parseInt(environment.getProperty("mongodb.dataSource.resources.port1"))),
//                Collections.singletonList(MongoCredential.createScramSha1Credential("link_corpweb", "link_corpweb", "link_corpweb".toCharArray()))
//        );
//
//    }

    @Override
    public MongoClient mongo() {
        return new MongoClient(new ServerAddress(environment.getProperty("mongodb.dataSource.resources.address1"), Integer.parseInt(environment.getProperty("mongodb.dataSource.resources.port1"))),
                Collections.singletonList(MongoCredential.createScramSha1Credential("link_corpweb", "link_corpweb", "link_corpweb".toCharArray()))
        );
    }
}
