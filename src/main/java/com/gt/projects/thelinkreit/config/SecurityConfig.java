package com.gt.projects.thelinkreit.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
//@PropertySources(value = { @PropertySource({ "classpath:config/security.properties" }) })
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private Environment environment;


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		super.configure(auth);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		 super.configure(web);
		//web.ignoring().antMatchers(Url.STATIC_CSS_PREFIX + "/**", Url.STATIC_IMAGE_PREFIX + "/**", Url.STATIC_JS_PREFIX + "/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// super.configure(http);
		http.csrf().disable() //
				.exceptionHandling();

        http.headers().frameOptions().disable();
		//if (this.environment.getProperty("api.only") == null || !Boolean.valueOf(this.environment.getProperty("api.only"))) {
			http //
					.authorizeRequests() //
					.antMatchers(Url.API_PREFIX + "/**").permitAll() //
					//.antMatchers(Url.WEB_PREFIX + "/**").permitAll() //
//					.antMatchers(Url.ADMIN_PREFIX + "/**").hasRole(Administrator.Role.ADMIN.getValue()) //
					.anyRequest().authenticated(); //

//			http //
//					.formLogin() //
//					.loginPage(Url.ADMIN_PREFIX + Url.ADMIN_LOGIN_PAGE) //
//					.loginProcessingUrl(Url.ADMIN_PREFIX + Url.ADMIN_LOGIN_PAGE) //
//					// .defaultSuccessUrl(Url.ADMIN_PREFIX + Url.ADMIN_MAIN_PAGE) //
//					.failureUrl(Url.ADMIN_PREFIX + Url.ADMIN_LOGIN_FAILURE_PAGE) //
//					.usernameParameter("username") //
//					.passwordParameter("password") //
//					.successHandler(new AdministratorAuthenticationSuccessHandler(this.administrationService)) //
//					.permitAll(); //

			// http //
			// .requiresChannel() //
			// .anyRequest() //
			// .requiresSecure(); //

//			http //
//					.logout() //
//					.invalidateHttpSession(true) //
//					.logoutUrl(Url.ADMIN_PREFIX + Url.ADMIN_LOGOUT_PAGE) //
//					.logoutSuccessUrl(Url.ADMIN_PREFIX + Url.ADMIN_LOGIN_PAGE) //
//					.permitAll(); //

//			http //
//					.sessionManagement() //
//					.sessionFixation() //
//					.changeSessionId() //
//					.sessionAuthenticationErrorUrl(Url.ADMIN_PREFIX + Url.ADMIN_LOGIN_EXPIRE_PAGE) //
//					.maximumSessions(1) //
//					.expiredUrl(Url.ADMIN_PREFIX + Url.ADMIN_LOGIN_EXPIRE_PAGE); //

//		} else {
//			http //
//					.authorizeRequests() //
//					.antMatchers(Url.API_PREFIX + "/**").permitAll() //
//					.antMatchers(Url.WEB_PREFIX + "/**").permitAll() //
//					.anyRequest().authenticated(); //
//		}
	}

//	@RequiredArgsConstructor
//	private class AdministratorAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
//
//		private final AdministrationService administrationService;
//
//		@Override
//		public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//			Administrator administrator = (Administrator) authentication.getPrincipal();
//			this.administrationService.updateAdministratorLastLoginDatetimeByPk(administrator.getId());
//
//			response.sendRedirect(request.getContextPath() + Url.ADMIN_PREFIX + Url.ADMIN_DASHBOARD_PAGE);
//		}
//	}

	public static void main(String[] args) {
		System.out.println(new BCryptPasswordEncoder().encode("a"));
	}
}
