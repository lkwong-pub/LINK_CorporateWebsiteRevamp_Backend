/**
	 *
	 * @api {get} http://www-dev.linkhk.com/linkweb/api/tenantAcademy/      Tenant Academy Landing
	 *
	 * @apiName Tenant Academy Landing
	 * @apiGroup Batch3_2-TenantAcademy
	 * @apiVersion 1.0.0
	 *
	  @apiDescription
		Retrieve Tenant Academy Page content<br/>
		Data included 5 components content:<br/>
		1. SEO Details<br/>
		2. Banner Details<br/>
		3. Tenant Event Details<br/>
		4. Introduction<br/>
		5. Past Event Details (Tenant Event Gallery)<br/>

        @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
        @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
        @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
        @apiSuccess (Success 200) {String} 	data		Data Object.
        @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
        @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
        @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
        @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
        @apiSuccess (Success 200) {Object} 	data.banner		Tenant Academy - Banner Object
        @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
        @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
        @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
        @apiSuccess (Success 200) {Object}	    data.label      Tenant Academy - Icon Label
        @apiSuccess (Success 200) {String}	    data.label.moreDetailsIconEn       More Details Icon (EN)
        @apiSuccess (Success 200) {String}	    data.label.moreDetailsIconTc       More Details Icon (TC)
        @apiSuccess (Success 200) {String}	    data.label.moreDetailsIconSc       More Details Icon (SC)
        @apiSuccess (Success 200) {String}	    data.label.allPastEventIconEn       All Event Gallery Icon (EN)
        @apiSuccess (Success 200) {String}	    data.label.allPastEventIconTc       All Event Gallery Icon (TC)
        @apiSuccess (Success 200) {String}	    data.label.allPastEventIconSc       All Event Gallery Icon (SC)
        @apiSuccess (Success 200) {String}	    data.label.liveFeedIcon     Live Feed Icon
         @apiSuccess (Success 200) {String}	    data.label.dateLabelEn       Date Label (EN)
         @apiSuccess (Success 200) {String}	    data.label.dateLabelTc       Date Label (TC)
         @apiSuccess (Success 200) {String}	    data.label.dateLabelSc       Date Label (SC)
         @apiSuccess (Success 200) {String}	    data.label.venueLabelEn       Venue Label (EN)
         @apiSuccess (Success 200) {String}	    data.label.venueLabelTc       Venue Label (TC)
         @apiSuccess (Success 200) {String}	    data.label.venueLabelSc       Venue Label (SC)
         @apiSuccess (Success 200) {String}	    data.label.guestLabelEn       Guest Label (EN)
         @apiSuccess (Success 200) {String}	    data.label.guestLabelTc       Guest Label (TC)
         @apiSuccess (Success 200) {String}	    data.label.guestLabelSc       Guest Label (SC)
        @apiSuccess (Success 200) {Object[]}	    data.tenantsEvent	Tenant Academy - Tenant Hero Object Array
        @apiSuccess (Success 200) {Integer}	    data.tenantsEvent.id					Tenants Event ID
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.titleEn	Tenants Event Title (EN)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.titleTc	Tenants Event Title (TC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.titleSc	Tenants Event Title (SC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.dateEn		Tenants Event Date (EN)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.dateTc		Tenants Event Date (TC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.dateSc		Tenants Event Date (SC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.timeEn		Tenants Event Time (EN)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.timeTc		Tenants Event Time (TC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.timeSc		Tenants Event Time (SC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.contact       Telephone Number
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.posterEn		Tenants Event Image URL (EN)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.posterTc		Tenants Event Image URL (TC)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.posterSc		Tenants Event Image URL (SC)
        @apiSuccess (Success 200) {Integer}	    data.tenantsEvent.liveFeedStatus        Live Feed Link Show Checking (1=Before actual start time, 2=start time, 3=finish and no show)
        @apiSuccess (Success 200) {String}	    data.tenantsEvent.liveFeedLink      Live Feed URL
        @apiSuccess (Success 200) {Object}	    data.introduction       Tenant Academy - Introduction Object
        @apiSuccess (Success 200) {String}	    data.introduction.aboutImgStandardEn		About Image (Desktop) (EN)
        @apiSuccess (Success 200) {String}	    data.introduction.aboutImgStandardTc		About HTML (Desktop) (TC)
        @apiSuccess (Success 200) {String}	    data.introduction.aboutImgStandardSc		About HTML (Desktop) (SC)
        @apiSuccess (Success 200) {String}	    data.introduction.aboutImgNarrowEn		About HTML (Mobile) (EN)
        @apiSuccess (Success 200) {String}	    data.introduction.aboutImgNarrowTc		About HTML (Mobile) (TC)
        @apiSuccess (Success 200) {String}	    data.introduction.aboutImgNarrowSc		About HTML (Mobile) (SC)
        @apiSuccess (Success 200) {String}	    data.introduction.themeIntroductionEn		Theme Introduction HTML (EN)
        @apiSuccess (Success 200) {String}	    data.introduction.themeIntroductionTc		Theme Introduction HTML (TC)
        @apiSuccess (Success 200) {String}	    data.introduction.themeIntroductionSc		Theme Introduction HTML (SC)
        @apiSuccess (Success 200) {String}	    data.introduction.introductionEn		 Introduction HTML (EN)
        @apiSuccess (Success 200) {String}	    data.introduction.introductionTc		 Introduction HTML (TC)
        @apiSuccess (Success 200) {String}	    data.introduction.introductionSc		 Introduction HTML (SC)
        @apiSuccess (Success 200) {String}	    data.introduction.backgroundStandard        Section Background Image (Desktop)
        @apiSuccess (Success 200) {String}	    data.introduction.backgroundNarrow        Section Background Image (Mobile)
        @apiSuccess (Success 200) {Object[]}	    data.tenantEventGallery    Tenant Academy - Tenant Event gallery (Past Event) Object Array
        @apiSuccess (Success 200) {Integer}	    data.tenantEventGallery.id    EventGallery ID
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.titleEn    EventGallery Title (EN)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.titleTc    EventGallery Title (TC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.titleSc    EventGallery Title (SC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.dateEn    EventGallery Date (EN)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.dateTc    EventGallery Date (TC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.dateSc    EventGallery Date (SC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.locationEn    EventGallery Location (EN)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.locationTc    EventGallery Location (TC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.locationSc    EventGallery Location (SC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.guestEn    EventGallery Guest (EN)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.guestTc    EventGallery Guest (TC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.guestSc    EventGallery Guest (SC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.descriptionEn    EventGallery Description (EN)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.descriptionTc    EventGallery Description (TC)
        @apiSuccess (Success 200) {String}	    data.tenantEventGallery.descriptionSc    EventGallery Description (SC)
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image1      Event Gallery Image 1
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image1AltTextEn    Event Gallery Image 1 Alt Text (EN)
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image1AltTextTc    Event Gallery Image 1 Alt Text (TC)
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image1AltTextSc    Event Gallery Image 1 Alt Text (SC)
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image2      Event Gallery Image 2
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image2AltTextEn    Event Gallery Image 2 Alt Text (EN)
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image2AltTextTc    Event Gallery Image 2 Alt Text (TC)
         @apiSuccess (Success 200) {String}	    data.tenantEventGallery.image2AltTextSc    Event Gallery Image 2 Alt Text (SC)
        


      	@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::TENANT_ACADEMY_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Tenant Academy Listing",
      "seoTitleTc":"Tenant Academy Listing",
      "seoTitleSc":"Tenant Academy Listing",
      "seoKeywordEn":"Tenant Academy Listing",
      "seoKeywordTc":"Tenant Academy Listing",
      "seoKeywordSc":"Tenant Academy Listing",
      "seoDescriptionEn":"Tenant Academy Listing",
      "seoDescriptionTc":"Tenant Academy Listing",
      "seoDescriptionSc":"Tenant Academy Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "label":{
         "moreDetailsIconEn":"",
         "moreDetailsIconTc":"",
         "moreDetailsIconSc":"",
         "allPastEventIconEn":"",
         "allPastEventIconTc":"",
         "allPastEventIconSc":"",
         "liveFeedIcon":"",
         "dateLabelEn":"",
         "dateLabelTc":"",
         "dateLabelSc":"",
         "venueLabelEn":"",
         "venueLabelTc":"",
         "venueLabelSc":"",
         "guestLabelEn":"",
         "guestLabelTc":"",
         "guestLabelSc":""
      },
      "tenantsEvent": [
          {
             "id":1,
             "titleEn":"Tenants Event",
             "titleTc":"Tenants Event (TC)",
             "titleSc":"Tenants Event (SC)",
             "dateEn":"08 Feb 2012 (Thu)",
             "dateTc":"08 Feb 2012 (Thu)",
             "dateSc":"08 Feb 2012 (Thu)",
             "timeEn":"10:00am-10:00pm",
             "timeTc":"10:00am-10:00pm",
             "timeSc":"10:00am-10:00pm",
             "contact":"21234567",
             "posterEn":"http://yahoo.com/abc.png",
             "posterTc":"http://yahoo.com/abc.png",
             "posterSc":"http://yahoo.com/abc.png",
             "liveFeedStatus":1,
             "liveFeedLink":"http://liveFeed.com/abc/"
          }
      ],
      "introduction":{
         "aboutImgStandardEn":"http://domain.com/about.png",
         "aboutImgStandardTc":"http://domain.com/about.png",
         "aboutImgStandardSc":"http://domain.com/about.png",
         "aboutImgNarrowEn":"http://domain.com/about.png",
         "aboutImgNarrowTc":"http://domain.com/about.png",
         "aboutImgNarrowSc":"http://domain.com/about.png",
         "themeIntroductionEn":"themeIntroductionEn",
         "themeIntroductionTc":"themeIntroductionTc",
         "themeIntroductionSc":"themeIntroductionSc",
         "introductionEn":"introductionEn",
         "introductionTc":"introductionTc",
         "introductionSc":"introductionSc",
         "backgroundStandard":"http://domain.com/sectionImage.png",
         "backgroundNarrow":"http://domain.com/sectionImage.png"
      },
      "tenantEventGallery":[
         {
            "id":1,
            "titleEn":"",
            "titleTc":"",
            "titleSc":"",
            "dateEn":"",
            "dateTc":"",
            "dateSc":"",
            "locationEn":"",
            "locationTc":"",
            "locationSc":"",
            "guestEn":"",
            "guestTc":"",
            "guestSc":"",
            "descriptionEn":"",
            "descriptionTc":"",
            "descriptionSc":"",
            "image1":"",
            "image1AltTextEn":"",
            "image1AltTextTc":"",
            "image1AltTextCc":"",
            "image2":"",
            "image2AltTextEn":"",
            "image2AltTextTc":"",
            "image2AltTextCc":""
         }
      ]
   }
}
::TENANT_ACADEMY_API_END::
*/

/**

 @api {get} http://www-dev.linkhk.com/linkweb/api/tenantEvent/{id}      Tenant Event Details

 @apiName Tenant Event Details
 @apiGroup Batch3_2-TenantAcademy
 @apiVersion 1.0.0

 @apiDescription
 Retrieve Tenant Event Details Page content<br/>
 Data included 2 components content:<br/>
 1. SEO Details<br/>
 2. Event Info<br/>

 @apiParam {Integer}	id		Tenant Event ID

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.eventInfo		Tenant Event Details - Tenant Event Info
 @apiSuccess (Success 200) {String} 	data.eventInfo.headlineEn	Section Headline (EN)
 @apiSuccess (Success 200) {String} 	data.eventInfo.headlineTc	Section Headline (TC)
 @apiSuccess (Success 200) {String} 	data.eventInfo.headlineSc	Section Headline (SC)
 @apiSuccess (Success 200) {String} 	data.eventInfo.titleEn	Event Name (EN)
 @apiSuccess (Success 200) {String} 	data.eventInfo.titleTc	Event Name (TC)
 @apiSuccess (Success 200) {String} 	data.eventInfo.titleSc	Event Name (SC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.dateEn	Event Date (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.dateTc	Event Date (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.dateSc	Event Date (SC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.timeEn	Event Time (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.timeTc	Event Time (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.timeSc	Event Time (SC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.contact  Telephone
 @apiSuccess (Success 200) {String}	    data.eventInfo.locationEn	Event Location (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.locationTc	Event Location (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.locationSc	Event Location (SC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.guestEn	Event Guest (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.guestTc	Event Guest (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.guestSc	Event Guest (SC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.posterEn		Carpark Enquiry Label (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.posterTc		Carpark Enquiry Label (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.posterSc		Carpark Enquiry Label (SC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.liveFeedIconEn   Live Feed Icon (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.liveFeedIconTc   Live Feed Icon (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.liveFeedIconSc   Live Feed Icon (SC)
 @apiSuccess (Success 200) {Integer}	    data.eventInfo.liveFeedStatus		Live Feed Link Show Checking (1=Before actual start time, 2=start time, 3=finish and no show)
 @apiSuccess (Success 200) {String}	    data.eventInfo.liveFeedLink      Live Feed URL
 @apiSuccess (Success 200) {String}	    data.eventInfo.descriptionEn	Description (EN)
 @apiSuccess (Success 200) {String}	    data.eventInfo.descriptionTc	Description (TC)
 @apiSuccess (Success 200) {String}	    data.eventInfo.descriptionSc	Description (SC)



 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::TENANT_EVENT_DETAILS_API_START::
 {
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{
      "id":1,
      "seoTitleEn":"Tenant Academy Details",
      "seoTitleTc":"Tenant Academy Details",
      "seoTitleSc":"Tenant Academy Details",
      "seoKeywordEn":"Tenant Academy Details",
      "seoKeywordTc":"Tenant Academy Details",
      "seoKeywordSc":"Tenant Academy Details",
      "seoDescriptionEn":"Tenant Academy Details",
      "seoDescriptionTc":"Tenant Academy Details",
      "seoDescriptionSc":"Tenant Academy Details",
      "eventInfo":{
         "headlineEn":"Upcoming Event",
         "headlineTc":"Upcoming Event (TC)",
         "headlineSc":"Upcoming Event (SC)",
         "titleEn":"Event",
         "titleTc":"Event TC",
         "titleSc":"Event SC",
         "dateEn":"20 July 2016",
         "dateTc":"20 July 2016",
         "dateSc":"20 July 2016",
         "timeEn":"10:00am - 10:00pm",
         "timeTc":"10:00am - 10:00pm",
         "timeSc":"10:00am - 10:00pm",
         "contact":"61234567",
         "locationEn":"Location",
         "locationTc":"Location TC",
         "locationSc":"Location SC",
         "guestEn":"Guest",
         "guestTc":"Guest TC",
         "guestSc":"Guest SC",
         "posterEn":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
         "posterTc":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
         "posterSc":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
         "liveFeedIconEn":"",
         "liveFeedIconTc":"",
         "liveFeedIconSc":"",
         "liveFeedStatus":3,
         "liveFeedLink":"",
         "descriptionEn":"Description",
         "descriptionTc":"Description",
         "descriptionSc":"Description"
      }
   }
}
 ::TENANT_EVENT_DETAILS_API_END::
 */

/**

 @api {get} http://www-dev.linkhk.com/linkweb/api/tenantEventGallery/  Tenant Event Gallery Listing

 @apiName Tenant Event Gallery Listing
 @apiGroup Batch3_2-TenantAcademy

 @apiVersion 1.0.0

 @apiDescription
 Retrieve Tenant Event Gallery Listing Page content<br/>
 Data included 5 components content:<br/>
 1. SEO Details<br/>
 2. Banner<br/>
 3. Event Gallery Year Bar <br/>
 4. Event Gallery List<br/>
 5. Paging Info<br/>

 @apiParam {Integer}	year		Selected Year ( Default = null )
 @apiParam {Integer}	pageNo		Page No ( start = 0 )
 @apiParam {Integer}	pageSize	Number of list record per request

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.banner		Tenant Event Gallery List - Banner Object
 @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
 @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
 @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
 @apiSuccess (Success 200) {Integer} data.filterBarStartYear		Year Filter Bar Start Year
 @apiSuccess (Success 200) {Object[]} 	data.eventGalleryList		Tenant Event Gallery List - Tenant Event Gallery List Object Array
 @apiSuccess (Success 200) {Integer} 	data.eventGalleryList.id	Tenant Event Gallery ID
 @apiSuccess (Success 200) {String} 	data.eventGalleryList.titleEn		Event Gallery Title (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryList.titleTc		Event Gallery Title (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryList.titleSc		Event Gallery Title (SC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryList.thumbnail		Tenant Event Gallery Thumbnail
 @apiSuccess (Success 200) {Object} 	data.pageInfo	Tenant Event Gallery List - Page Info Object
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.total		No. of Total Record
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageCount		No. of Page Count
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageSize		No. per page
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.curPage		Current Page No
 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::TENANT_EVENT_GALLERY_LIST_API_START::
 {  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "seoTitleEn":"Event Gallery",
      "seoTitleTc":"活動相冊",
      "seoTitleSc":"活动相册",
      "seoKeywordEn":"SEO keywords",
      "seoKeywordTc":"搜索引擎優化 關鍵詞",
      "seoKeywordSc":"搜索引擎优化 关键词",
      "seoDescriptionEn":"SEO Description",
      "seoDescriptionTc":"搜索引擎優化 描述",
      "seoDescriptionSc":"搜索引擎优化 描述",
      "banner":{  
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "filterBarStartYear":2000,
      "eventGalleryList":[  
         {  
            "id":1,
            "titleEn":"Tenant Event 1 ",
            "titleTc":"Tenant Event 1 TC",
            "titleSc":"Tenant Event 1 SC",
            "thumbnail":""
         }
      ],
      "pageInfo":{  
         "total":49,
         "pageCount":1,
         "pageSize":49,
         "curPage":1
      }
   }
}
 ::TENANT_EVENT_GALLERY_LIST_API_END::
 */

/**
 @api {get} http://www-dev.linkhk.com/linkweb/api/tenantEventGallery/{id}  Tenant Event Gallery Details

 @apiName Tenant Event Gallery Details
 @apiGroup Batch3_2-TenantAcademy
 @apiVersion 1.0.0

 @apiDescription
 Retrieve Tenant Event Gallery Details Page content<br/>
 Data included 6 components content:<br/>
 1. SEO Details<br/>
 2. Tenant Event Gallery Info<br/>
 3. Tenant Event Gallery<br/>
 4. You may also like<br/>

 @apiParam {Integer}	id		Tenant Event Gallery ID

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.eventGalleryInfo		Tenant Event Gallery - Tenant Event Gallery Info Object
 @apiSuccess (Success 200) {Integer} 	data.eventGalleryInfo.id		Event Gallery ID
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.headlineEn	Event Gallery  Section Headline (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.headlineTc	Event Gallery  Section Headline (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.headlineSc	Event Gallery  Section Headline (SC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.titleEn		Event Gallery Title (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.titleTc		Event Gallery Title (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.titleSc		Event Gallery Title (SC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.dateEn		Event Gallery Date (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.dateTc		Event Gallery Date (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.dateSc		Event Gallery Date (SC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.guestEn		Event Gallery Guest (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.guestTc		Event Gallery Guest (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.guestSc		Event Gallery Guest (SC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.descriptionEn		Event Gallery Description (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.descriptionTc		Event Gallery Description (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.descriptionSc		Event Gallery Description (SC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.detailsEn		Event Gallery Details HTML (EN)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.detailsTc		Event Gallery Details HTML (TC)
 @apiSuccess (Success 200) {String} 	data.eventGalleryInfo.detailsSc		Event Gallery Details HTML (SC)
 @apiSuccess (Success 200) {Object} 	data.eventGallery		Event Gallery - Event Gallery Object
 @apiSuccess (Success 200) {Object[]} 	data.eventGallery.eventImages	Event Image Object Array
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.thumbnail		Image URL
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextEn		Image Alt Text (EN)
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextTc		Image Alt Text (TC)
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextSc		Image Alt Text (SC)
 @apiSuccess (Success 200) {Object[]} 	data.eventGallery.eventVideos	Event Video Object Array
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.videoThumbnail	Event Video Thumbnail
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.videoUrl			Event Video URL
 @apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.altVideoUrl		Event Alt Video URL
 @apiSuccess (Success 200) {Object}		data.youWillAlsoLike	Event Gallery - You May Also Like Object
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleEn	Title (EN)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleTc	Title (TC)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleSc	Title (SC)
 @apiSuccess (Success 200) {Object[]}		data.youWillAlsoLike.youWillAlsoLikeList	You May Also Like List Object Array
 @apiSuccess (Success 200) {Integer}		data.youWillAlsoLike.youWillAlsoLikeList.id		Event Gallery ID
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.titleEn		Event Gallery Title (EN)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.titleTc		Event Gallery Title (TC)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.titleSc		Event Gallery Title (SC)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.thumbnail	Event Gallery Thumbnail
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllEn		View All Content (EN)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllTc		View All Content (TC)
 @apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllSc		View All Content (SC)
 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::TENANT_EVENT_GALLERY_DETAILS_API_START::
 {
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{
      "id":1,
      "seoTitleEn":"LINK Customer Site Event Gallery Details EN",
      "seoTitleTc":"LINK Customer Site Event Gallery Details TC",
      "seoTitleSc":"LINK Customer Site Event Gallery Details SC",
      "seoKeywordEn":"LINK Customer Site Event Gallery Details EN",
      "seoKeywordTc":"LINK Customer Site Event Gallery Details TC",
      "seoKeywordSc":"LINK Customer Site Event Gallery Details SC",
      "seoDescriptionEn":"This is LINK Customer Site Event Gallery Details. EN",
      "seoDescriptionTc":"This is LINK Customer Site Event Gallery Details. TC",
      "seoDescriptionSc":"This is LINK Customer Site Event Gallery Details. SC",
      "eventGalleryInfo":{
         "id":17402,
         "headlineEn":"Event Gallery",
         "headlineTc":"Event Gallery TC",
         "headlineSc":"Event Gallery SC",
         "titleEn":"Event Gallery",
         "titleTc":"Event Gallery TC",
         "titleSc":"Event Gallery SC",
         "dateEn":"2016 01 01",
         "dateTc":"2016 01 01",
         "dateSC":"2016 01 01",
         "guestEn":"",
         "guestTc":"",
         "guestSc":"",
         "descriptionEn":"To celebrate the Grand Opening of brand new Lok Fu Market, The LINK had partnered with Metro Radio programme - \"Happy Family\" and \"La Gourment\", to launch a series of dining activities to allow customers to experience a brand-new shopping environment. Meanwhile, the first ever programme \"Junior Shoppers\" in market and grand reward programme has been launched!",
         "descriptionTc":"為慶祝全新的樂福市場隆重開業，鏈接曾與新城電台節目合作 - “快樂家族”和“香格里拉味精”，推出一系列的餐飲活動，讓客戶體驗到一個全新的購物環境。同時，首次節目“少年購物者”，在市場上和大獎勵計劃已經啟動！",
         "descriptionSc":"为庆祝全新的乐福市场隆重开业，链接曾与新城电台节目合作 - “快乐家族”和“香格里拉味精”，推出一系列的餐饮活动，让客户体验到一个全新的购物环境。同时，首次节目“少年购物者”，在市场上和大奖励计划已经启动！",
         "detailsEn":"Details",
         "detailsTc":"Details TC",
         "detailsSc":"Details SC"
      },
      "eventGallery":{
         "eventImages":[
            {
               "thumbnail":"",
               "altTextEn":"",
               "altTextTc":"",
               "altTextSc":""
            }
         ],
         "eventVideos":[
            {
               "videoThumbnail":"",
               "videoUrl":"http://www.youtube.com",
               "altVideoUrl":""
            }
         ]
      },
      "youWillAlsoLike":{
         "titleEn":"You may also like En",
         "titleTc":"You may also like Tc",
         "titleSc":"You may also like Sc",
         "youWillAlsoLikeList":[
            {
               "id":17402,
               "titleEn":"",
               "titleTc":"",
               "titleSc":"",
               "thumbnail":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg"
            }
         ],
         "viewAllEn":"View All En",
         "viewAllTc":"View All Tc",
         "viewAllSc":"View All Sc"
      }
   }
}
 ::TENANT_EVENT_GALLERY_DETAILS_API_END::
 */