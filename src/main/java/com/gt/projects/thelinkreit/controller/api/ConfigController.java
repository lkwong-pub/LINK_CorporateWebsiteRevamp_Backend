//package com.gt.projects.thelinkreit.controller.api;
//
//import java.io.FileReader;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.commons.io.IOUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.AntPathMatcher;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.HandlerMapping;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.gt.projects.thelinkreit.config.Url;
//import com.gt.projects.thelinkreit.entity.response.BaseResponse;
//import com.gt.projects.thelinkreit.request.CheckImageReq;
//
//import com.gt.projects.thelinkreit.response.SearchCriteriaRes;
//import com.gt.projects.thelinkreit.service.ApplicationService;
//import com.gt.projects.thelinkreit.service.FileService;
//import com.gt.projects.thelinkreit.service.MongoService;
//import com.gt.projects.thelinkreit.service.ResourcesService;
//import com.gt.projects.thelinkreit.service.ResponseService;
//
//
//
//@Controller
//@RequestMapping(Url.API_PREFIX + Url.API_CONFIG_PREFIX)
//public class ConfigController extends _AbstractApiController {
//
//	@Autowired
//	private FileService fileService;
//
//	@Autowired
//	private ApplicationService applicationService;
//
//	@Autowired
//	private ResourcesService resourcesService;
//
//	@Autowired
//	private MongoService mongoService;
//
//	@RequestMapping(value = Url.API_TEST_PUBLISHNOW_PREFIX)
//	public @ResponseBody BaseResponse publishNow() throws Exception {
//
//		BaseResponse responseObject = new BaseResponse();
//
//		responseObject.setError("0000");
//		responseObject.setErrorMessage("");
//
//		this.applicationService.retrieveAllData();
//		this.resourcesService.retrieveAllData();
//		this.mongoService.retrieveMongoContent();
//
//		return responseObject;
//
//	}
//
//	@RequestMapping(value = Url.API_IMAGECHECK_PREFIX)
//	public @ResponseBody BaseResponse<List<Map<String, Object>>> checkImage( CheckImageReq checkImageReq ) throws Exception {
//
//		BaseResponse<List<Map<String, Object>>> responseObject = new BaseResponse<List<Map<String, Object>>>();
//		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
//		for(MultipartFile image : checkImageReq.getImages()){
//			resultList.add( this.fileService.imageChecking( image ) );
//		}
//		responseObject.setData( resultList );
//		return responseObject;
//
//	}
//
//
//}
