package com.gt.projects.thelinkreit.utils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Clob;
import java.sql.SQLException;

import org.apache.commons.io.IOUtils;

public class StringUtil {

	public static String removeHtmlCode(String source) {

		if (source == null)
			return source;

		String result = source;
		result = result.replaceAll("<br\\s*/>", "\n");
		result = result.replace("\r", "");
		result = result.replaceAll("\\<.*?>", "");

		return result;

	}

	public static String clob2String(Clob clob) {
		try {
			if (clob != null) {
				Reader reader = clob.getCharacterStream();
				StringWriter stringWriter = new StringWriter();
				IOUtils.copy(reader, stringWriter);
				return stringWriter.toString();

			} else {
				return null;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
