package com.gt.projects.thelinkreit.request;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class CheckImageReq extends BaseReq{

	private MultipartFile[] images;
}
