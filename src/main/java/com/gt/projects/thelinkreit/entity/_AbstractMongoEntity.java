package com.gt.projects.thelinkreit.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public abstract class _AbstractMongoEntity {

	@SuppressWarnings("unchecked")
	protected void put2DBObject(DBObject dBObject, String key, Object value) {
		if (value != null) {
			if (value instanceof List<?>) {
				if (!((List<?>) value).isEmpty()) {
					List<Object> result = new ArrayList<Object>();
					for (Object object : (List<?>) value) {
						if (object instanceof Map) {
							DBObject interDBObject = new BasicDBObject();
							for (String tmpKey : ((Map<String, ?>) object).keySet())
								interDBObject.put(tmpKey, ((Map<String, ?>) object).get(tmpKey));
							result.add(interDBObject);
						} else {
							result.add(object);
						}
					}
					dBObject.put(key, result);
				}
			} else {
				dBObject.put(key, value);
			}
		}
	}

	protected String getObjectIdFromDBObject(DBObject dBObject, String key) {
		return ((ObjectId) this.getFromDBObject(dBObject, key)).toString();
	}

	protected String getStringFromDBObject(DBObject dBObject, String key) {
		return (String) this.getFromDBObject(dBObject, key);
	}

	protected Integer getIntegerFromDBObject(DBObject dBObject, String key) {
		return (Integer) this.getFromDBObject(dBObject, key);
	}

	protected Long getLongFromDBObject(DBObject dBObject, String key) {
		return (Long) this.getFromDBObject(dBObject, key);
	}

	protected Boolean getBooleanFromDBObject(DBObject dBObject, String key) {
		return (Boolean) this.getFromDBObject(dBObject, key);
	}

	protected byte[] getBytesFromDBObject(DBObject dBObject, String key) {
		return (byte[]) this.getFromDBObject(dBObject, key);
	}

	protected Date getDateFromDBObject(DBObject dBObject, String key) {
		return (Date) this.getFromDBObject(dBObject, key);
	}
	

	@SuppressWarnings("unchecked")
	protected List<Map<String, Object>> getListOfMapFromDBObject(DBObject dBObject, String key) {
		List<Object> list = this.getListFromDBObject(dBObject, key);
		if (list != null && !list.isEmpty()) {
			List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
			for (Object object : list) {
				if (object instanceof BasicDBObject) {
					Map<String, Object> map = ((BasicDBObject) object).toMap();
					result.add(map);
				}
			}
			return result;
		}
		return null;
	}

	protected Map<String, Integer> getMapOfIntegerFromDBObject(DBObject dBObject, String key) {
		Map<String, Object> map = this.getMapOfObjectFromDBObject(dBObject, key);
		if (map != null && map.size() != 0) {
			Map<String, Integer> result = new HashMap<String, Integer>();
			for (String mapKey : result.keySet())
				result.put(mapKey, (Integer) map.get(key));
		}
		return null;
	}

	protected Map<String, String> getMapOfStringFromDBObject(DBObject dBObject, String key) {
		Map<String, Object> map = this.getMapOfObjectFromDBObject(dBObject, key);
		if (map != null && map.size() != 0) {
			Map<String, String> result = new HashMap<String, String>();
			for (String mapKey : result.keySet())
				result.put(mapKey, (String) map.get(key));
		}
		return null;
	}

	protected Map<String, Long> getMapOfLongFromDBObject(DBObject dBObject, String key) {
		Map<String, Object> map = this.getMapOfObjectFromDBObject(dBObject, key);
		if (map != null && map.size() != 0) {
			Map<String, Long> result = new HashMap<String, Long>();
			for (String mapKey : result.keySet())
				result.put(mapKey, (Long) map.get(key));
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> getMapOfObjectFromDBObject(DBObject dBObject, String key) {
		return (Map<String, Object>) this.getFromDBObject(dBObject, key);
	}

	protected List<String> getStringListFromDBObject(DBObject dBObject, String key) {
		List<Object> list = this.getListFromDBObject(dBObject, key);
		if (list != null && !list.isEmpty()) {
			List<String> result = new ArrayList<String>();
			for (Object object : list)
				result.add((String) object);
			return result;
		}
		return null;
	}

	protected List<Integer> getIntegerListFromDBObject(DBObject dBObject, String key) {
		List<Object> list = this.getListFromDBObject(dBObject, key);
		if (list != null && !list.isEmpty()) {
			List<Integer> result = new ArrayList<Integer>();
			for (Object object : list)
				result.add((Integer) object);
			return result;
		}
		return null;
	}

	protected List<Long> getLongListFromDBObject(DBObject dBObject, String key) {
		List<Object> list = this.getListFromDBObject(dBObject, key);
		if (list != null && !list.isEmpty()) {
			List<Long> result = new ArrayList<Long>();
			for (Object object : list)
				result.add((Long) object);
			return result;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected List<Object> getListFromDBObject(DBObject dBObject, String key) {
		return (List<Object>) this.getFromDBObject(dBObject, key);
	}

	protected Object getFromDBObject(DBObject dBObject, String key) {
		if (dBObject.containsField(key))
			return dBObject.get(key);
		return null;
	}

	@SuppressWarnings("unchecked")
	protected Object getObject(DBObject dBObject, String key, Class clazz){
		try{
			com.mongodb.BasicDBObject obj = (com.mongodb.BasicDBObject)this.getFromDBObject(dBObject, key);
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(obj.toString(), clazz);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	protected List getObjectList(DBObject dBObject, String key, Class clazz){
		try{
			List result = new ArrayList();
			com.mongodb.BasicDBList objs = (com.mongodb.BasicDBList)this.getFromDBObject(dBObject, key);
			ObjectMapper mapper = new ObjectMapper();
			for(Object obj : objs){
				result.add( mapper.readValue(obj.toString(), clazz) );
			}
			return result;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
