package com.gt.projects.thelinkreit.service.impl;
//package com.gt.projects.thelinkreit.service.impl;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.commons.lang3.StringUtils;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.DisposableBean;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.gt.projects.thelinkreit.config.Url;
//import com.gt.projects.thelinkreit.entity.mongo.FsFiles;
//import com.gt.projects.thelinkreit.entity.resources.Area;
//import com.gt.projects.thelinkreit.entity.resources.CarParkFacilityPaymentOption;
//import com.gt.projects.thelinkreit.entity.resources.CarParkFacilityPrivilege;
//import com.gt.projects.thelinkreit.entity.resources.CarParkPrivilegeType;
//import com.gt.projects.thelinkreit.entity.resources.City;
//import com.gt.projects.thelinkreit.entity.resources.CustomColumnDisplayConfig;
//import com.gt.projects.thelinkreit.entity.resources.District;
//import com.gt.projects.thelinkreit.entity.resources.PaymentOptionType;
//import com.gt.projects.thelinkreit.entity.resources.Shop;
//import com.gt.projects.thelinkreit.entity.resources.ShopCategory;
//import com.gt.projects.thelinkreit.entity.resources.ShopCategoryType;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentre;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentreFeatureType;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentreService;
//import com.gt.projects.thelinkreit.entity.resources.ShopCentreServiceType;
//import com.gt.projects.thelinkreit.entity.resources.ShopEx;
//import com.gt.projects.thelinkreit.entity.resources.ShopPrivilege;
//import com.gt.projects.thelinkreit.entity.resources.ShopPrivilegeType;
//import com.gt.projects.thelinkreit.entity.resources.ShopType;
//import com.gt.projects.thelinkreit.entity.resources.CarParkFacility;
//import com.gt.projects.thelinkreit.repository.resources.AreaRepository;
//import com.gt.projects.thelinkreit.repository.resources.CarParkFacilityPaymentOptionRepository;
//import com.gt.projects.thelinkreit.repository.resources.CarParkFacilityPrivilegeRepository;
//// import com.gt.projects.thelinkreit.repository.resources.CarParkPaymentOptionRepository;
//// import com.gt.projects.thelinkreit.repository.resources.CarParkPrivilegeRepository;
//import com.gt.projects.thelinkreit.repository.resources.CarParkPrivilegeTypeRepository;
//// import com.gt.projects.thelinkreit.repository.resources.CarParkRepository;
//import com.gt.projects.thelinkreit.repository.resources.CityRepository;
//import com.gt.projects.thelinkreit.repository.resources.CustomColumnDisplayConfigRepository;
//import com.gt.projects.thelinkreit.repository.resources.DistrictRepository;
//import com.gt.projects.thelinkreit.repository.resources.PaymentOptionTypeRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopCategoryRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopCategoryTypeRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopCentreFeatureTypeRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopCentreRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopCentreServiceRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopCentreServiceTypeRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopExRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopPrivilegeRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopPrivilegeTypeRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopRepository;
//import com.gt.projects.thelinkreit.repository.resources.ShopTypeRepository;
//import com.gt.projects.thelinkreit.repository.resources.CarParkFacilityRepository;
//import com.gt.projects.thelinkreit.service.FileService;
//import com.gt.projects.thelinkreit.service.ResourcesService;
//import com.gt.projects.thelinkreit.utils.LocationUtil;
//
//@Service("resourcesService")
//public class _ResourcesServiceImpl implements ResourcesService, InitializingBean, DisposableBean, Runnable {
//
//	private static Logger LOG = Logger.getLogger(_ResourcesServiceImpl.class);
//
//	
//	@Autowired
//	private CityRepository cityRepository;
//
//	@Autowired
//	private AreaRepository areaRepository;
//
//	@Autowired
//	private DistrictRepository districtRepository;
//	
//	@Autowired
//	private CarParkFacilityPaymentOptionRepository carParkFacilityPaymentOptionRepository;
//
//	@Autowired
//	private CarParkFacilityPrivilegeRepository carParkFacilityPrivilegeRepository;
//
//	@Autowired
//	private CarParkPrivilegeTypeRepository carParkPrivilegeTypeRepository;
//
//	@Autowired
//	private CarParkFacilityRepository carParkFacilityRepository;
//
//	@Autowired
//	private PaymentOptionTypeRepository paymentOptionTypeRepository;
//
//	@Autowired
//	private ShopCentreFeatureTypeRepository shopCentreFeatureTypeRepository;
//
//	@Autowired
//	private ShopCentreServiceRepository shopCentreServiceRepository;
//
//	@Autowired
//	private ShopCentreServiceTypeRepository shopCentreServiceTypeRepository;
//
//	@Autowired
//	private ShopCentreRepository shopCentreRepository;
//
//	@Autowired
//	private ShopCategoryRepository shopCategoryRepository;
//
//	@Autowired
//	private ShopCategoryTypeRepository shopCategoryTypeRepository;
//
//	@Autowired
//	private ShopPrivilegeTypeRepository shopPrivilegeTypeRepository;
//
//	@Autowired
//	private ShopPrivilegeRepository shopPrivilegeRepository;
//
//	@Autowired
//	private ShopTypeRepository shopTypeRepository;
//
//	@Autowired
//	private ShopRepository shopRepository;
//
//	@Autowired
//	private ShopExRepository shopExRepository;
//
//	@Autowired
//	private CustomColumnDisplayConfigRepository customColumnDisplayConfigRepository;
//
//	@Autowired
//	private FileService fileService;
//	
//	/****************** Cached Object ******************/
//
////	private AboutUs aboutUs;
////
////	private List<Promotion> promotions;
////	@SuppressWarnings("unused")
////	private Map<String, Promotion> promotionMapById;
////	private Map<String, Promotion> promotionMapByFilename;
////	private Map<String, Promotion> promotionMapByFilenameMd5;
////	private Map<String, List<Promotion>> promotionMapByPropCode;
//
////	@SuppressWarnings("unused")
////	private List<ParkingPromotion> parkingPromotions;
////	@SuppressWarnings("unused")
////	private Map<String, ParkingPromotion> parkingPromotionMapById;
////	@SuppressWarnings("unused")
////	private Map<String, List<ParkingPromotion>> parkingPromotionMapByPropCode;
//
//	@SuppressWarnings("unused")
//	private List<City> cities;
//	@SuppressWarnings("unused")
//	private Map<Integer, City> cityMapById;
//
//	private List<Area> areas;
//	@SuppressWarnings("unused")
//	private Map<Integer, Area> areaMapById;
//	private Map<String, Area> areaMapByCode;
//
//	private List<District> districts;
//	private Map<Integer, District> districtMapById;
//	@SuppressWarnings("unused")
//	private Map<Integer, List<District>> districtMapGroupByCityId;
//	private Map<String, List<District>> districtMapGroupByArea;
//
//	private List<PaymentOptionType> paymentOptionTypes;
//	@SuppressWarnings("unused")
//	private Map<Integer, PaymentOptionType> paymentOptionTypeMayById;
//
//	@SuppressWarnings("unused")
//	private List<CarParkFacilityPaymentOption> carParkFacilityPaymentOptions;
//	private Map<Integer, List<CarParkFacilityPaymentOption>> carParkFacilityPaymentOptionMapGroupByFacilityKey;
//	@SuppressWarnings("unused")
//	private Map<Integer, List<CarParkFacilityPaymentOption>> carParkFacilityPaymentOptionMapGroupByPaymentOptionTypeId;
//
//	private List<CarParkPrivilegeType> carParkPrivilegeTypes;
//	@SuppressWarnings("unused")
//	private Map<Integer, CarParkPrivilegeType> carParkPrivilegeTypeMapById;
//
//	@SuppressWarnings("unused")
//	private List<CarParkFacilityPrivilege> carParkFacilityPrivileges;
//	private Map<Integer, List<CarParkFacilityPrivilege>> carParkFacilityPrivilegeMapGroupByFacilityKey;
//	private Map<Integer, List<CarParkFacilityPrivilege>> carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId;
//
//	private List<ShopCategoryType> shopCategoryTypes;
//	private Map<Integer, ShopCategoryType> shopCategoryTypeMapById;
//	private Map<String, List<ShopCategoryType>> shopCategoryTypeMapGroupByShopTypeName;
//
//	@SuppressWarnings("unused")
//	private List<ShopCategory> shopCategories;
//	private Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopId;
//	private Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopCategoryTypeId;
//
//	private List<ShopType> shopTypes;
//	private Map<Integer, ShopType> shopTypeMapById;
//	private Map<String, ShopType> shopTypeMapByShopTypeName;
//
//	private List<ShopPrivilegeType> shopPrivilegeTypes;
//	@SuppressWarnings("unused")
//	private Map<Integer, ShopPrivilegeType> shopPrivilegeTypeMapById;
//
//	@SuppressWarnings("unused")
//	private List<ShopPrivilege> shopPrivileges;
//	private Map<Integer, List<ShopPrivilege>> shopPrivilegeMapGroupByShopId;
//	@SuppressWarnings("unused")
//	private Map<Integer, List<ShopPrivilege>> shopPrivilegeMapGroupByShopPrivilegeTypeId;
//
//	private List<Shop> shops;
//	private Map<Integer, Shop> shopMapById;
//	private Map<List<Object>, Shop> shopMapByShopInfo;
//	private Map<Integer, Map<Integer, List<Shop>>> shopMapGroupByShopCentreIdWithShopType;
//	private Map<Integer, List<Shop>> shopMapGroupByShopCentreId;
//	private Map<Integer, List<Shop>> shopMapGroupByShopType;
//	private Map<Integer, List<Shop>> shopMapGroupByShopName;
//
//	@SuppressWarnings("unused")
//	private List<ShopEx> shopExs;
//	private Map<List<Object>, ShopEx> shopExMapByShopInfo;
//
//	private List<ShopCentreFeatureType> shopCentreFeatureTypes;
//	private Map<Integer, ShopCentreFeatureType> shopCentreFeatureTypeMapById;
//	@SuppressWarnings("unused")
//	private Map<String, List<ShopCentreFeatureType>> shopCentreFeatureTypeMapGroupByShopCentreFeatureTypeName;
//
//	private List<ShopCentreServiceType> shopCentreServiceTypes;
//	@SuppressWarnings("unused")
//	private Map<Integer, ShopCentreServiceType> shopCentreServiceTypeMapById;
//
//	@SuppressWarnings("unused")
//	private List<ShopCentreService> shopCentreServices;
//	@SuppressWarnings("unused")
//	private Map<Integer, List<ShopCentreService>> shopCentreServiceMapGroupByShopCentreServiceTypeId;
//	private Map<Integer, List<ShopCentreService>> shopCentreServiceMapGroupByShopCentreId;
//
//	private List<ShopCentre> shopCentres;
//	private Map<Integer, ShopCentre> shopCentreMapById;
//	private Map<String, ShopCentre> shopCentreMapByPropCode;
//	@SuppressWarnings("unused")
//	private Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCentreFeatureTypeId;
//	private Map<Integer, List<ShopCentre>> shopCentreMapGroupByDistrictId;
//	private Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight;
//
//	private List<CarParkFacility> tbCarParkFacilities;
//	private Map<Integer, CarParkFacility> tbCarParkFacilityMapById;
//	private Map<String, List<CarParkFacility>> tbCarParkFacilityMapGroupByShopCentrePropCode;
//	private Map<Integer, List<CarParkFacility>> tbCarParkFacilityMapGroupByDistrictId;
//
//	private Map<String, Map<Integer, List<ShopCategoryType>>> shopCategoryTypeMapGroupByShopTypeNameNShopCentreId;
//	private Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCategoryTypeId;
//
//	private Map<Integer, CarParkFacility> tbCarParkFacilityRealTimeMapById;
//
//	@SuppressWarnings("unused")
//	private List<CustomColumnDisplayConfig> customColumnDisplayConfigs;
//	private Map<String, Map<String, String>> customColumnDisplayConfigMap;
//
//	/****************** InitializingBean && DisposableBean ******************/
//
//	private Thread retrieveAllDataThread;
//	private Boolean running;
//
//	@Override
//	public void afterPropertiesSet() {
//		this.retrieveAllData();
//		this.retrieveCarParkData();
//
//		this.running = Boolean.TRUE;
//		this.retrieveAllDataThread = new Thread(this);
//		this.retrieveAllDataThread.start();
//	}
//
//	@Override
//	public void destroy() throws Exception {
//		this.running = Boolean.FALSE;
//	}
//
//	@Override
//	public void run() {
//		int everySecond = 60 * 15;
//		int timer = everySecond;
//
//		while (true) {
//
//			if (this.running) {
//				if (timer % 15 == 0 || timer == everySecond - 1)
//					try {
//						this.retrieveCarParkData();
//
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				if (timer <= 1)
//					try {
//
//						this.retrieveAllData();
//
//						timer = everySecond;
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				try {
//					Thread.sleep(1000);
//					timer--;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			} else
//				break;
//
//		}
//	}
//
//	/****************** retrieveAllData ******************/
//
//	@Transactional(readOnly = true, value = "resourcesTransactionManager")
//	public void retrieveAllData() {
//
//		long startTime = System.currentTimeMillis();
//
//		/**************/
//
////		String mallLinkedWordingEn = this.databaseMessageResourceService.getMessage("tbCarParkFacility.mallLinkedWording", "en");
////		String mallLinkedWordingTc = this.databaseMessageResourceService.getMessage("tbCarParkFacility.mallLinkedWording", "tc");
////		String mallLinkedWordingSc = this.databaseMessageResourceService.getMessage("tbCarParkFacility.mallLinkedWording", "sc");
////		CarParkFacility.setMailLinkedWording(mallLinkedWordingEn, mallLinkedWordingTc, mallLinkedWordingSc);
//
//		/**************/
//
////		AboutUs aboutUs = this.aboutUsDao.findLatest();
//
//		/**************/
//
////		List<Promotion> promotions = this.promotionDao.findActiveByDeliveryChannelsIn("MobileApp");
////
////		Map<String, Promotion> promotionMapById = new HashMap<String, Promotion>();
////		Map<String, Promotion> promotionMapByFilename = new HashMap<String, Promotion>();
////		Map<String, Promotion> promotionMapByFilenameMd5 = new HashMap<String, Promotion>();
////		Map<String, List<Promotion>> promotionMapByPropCode = new HashMap<String, List<Promotion>>();
////		for (Promotion promotion : promotions) {
////			promotionMapById.put(promotion.getId(), promotion);
////			promotionMapByFilename.put(promotion.getFilename(), promotion);
////			promotionMapByFilenameMd5.put(promotion.getFilenameMd5(), promotion);
////
////			List<String> propCodeList = promotion.getPropCodeList();
////			if (propCodeList != null)
////				for (String propCode : propCodeList) {
////					List<Promotion> promotionsByPropCode = promotionMapByPropCode.get(propCode);
////					if (promotionsByPropCode == null)
////						promotionsByPropCode = new ArrayList<Promotion>();
////					promotionsByPropCode.add(promotion);
////					promotionMapByPropCode.put(propCode, promotionsByPropCode);
////				}
////		}
//
//		/**************/
//
////		List<ParkingPromotion> parkingPromotions = this.parkingPromotionDao.findActiveByDeliveryChannelsIn("MobileApp");
////
////		Map<String, ParkingPromotion> parkingPromotionMapById = new HashMap<String, ParkingPromotion>();
////		Map<String, List<ParkingPromotion>> parkingPromotionMapByPropCode = new HashMap<String, List<ParkingPromotion>>();
////		for (ParkingPromotion parkingPromotion : parkingPromotions) {
////			parkingPromotionMapById.put(parkingPromotion.getId(), parkingPromotion);
////
////			List<String> propCodeList = parkingPromotion.getPropCodeList();
////			if (propCodeList != null)
////				for (String propCode : propCodeList) {
////					List<ParkingPromotion> parkingPromotionsByPropCode = parkingPromotionMapByPropCode.get(propCode);
////					if (parkingPromotionsByPropCode == null)
////						parkingPromotionsByPropCode = new ArrayList<ParkingPromotion>();
////					parkingPromotionsByPropCode.add(parkingPromotion);
////					parkingPromotionMapByPropCode.put(propCode, parkingPromotionsByPropCode);
////				}
////		}
//
//		/**************/
//
//		List<City> cities = this.cityRepository.findByEnableOrderByOrderingAsc(1);
//
//		Map<Integer, City> cityMapById = new HashMap<Integer, City>();
//		for (City city : cities)
//			cityMapById.put(city.getCityId(), city);
//
//		/**************/
//
////		List<String> availableAreas = Arrays.asList(this.systemService.getProperty("availableAreas", "HK,KL,NT").split(","));
//
////		List<Area> areas = this.areaRepository.findByEnableOrderByOrderingAsc(1);
////		for (int i = areas.size() - 1; i >= 0; i--)
////			if (!availableAreas.contains(areas.get(i).getAreaCode()))
////				areas.remove(i);
////
////		Map<Integer, Area> areaMapById = new HashMap<Integer, Area>();
////		Map<String, Area> areaMapByCode = new HashMap<String, Area>();
////		for (Area area : areas) {
////			areaMapById.put(area.getAreaId(), area);
////			areaMapByCode.put(area.getAreaCode(), area);
////		}
//
//		/**************/
//
//		List<District> districts = this.districtRepository.findByEnableOrderByAreaAscOrderingAsc(1);
////		for (int i = districts.size() - 1; i >= 0; i--)
////			if (!availableAreas.contains(districts.get(i).getArea()))
////				districts.remove(i);
//
//		Map<Integer, District> districtMapById = new HashMap<Integer, District>();
//		Map<Integer, List<District>> districtMapGroupByCityId = new HashMap<Integer, List<District>>();
//		Map<String, List<District>> districtMapGroupByArea = new HashMap<String, List<District>>();
//		for (District district : districts) {
////			if (!availableAreas.contains(district.getArea()))
////				continue;
//
//			districtMapById.put(district.getDistrictId(), district);
//
//			List<District> districtsByCityId = districtMapGroupByCityId.get(district.getCityId());
//			if (districtsByCityId == null)
//				districtsByCityId = new ArrayList<District>();
//			districtsByCityId.add(district);
//			districtMapGroupByCityId.put(district.getCityId(), districtsByCityId);
//
//			List<District> districtsByArea = districtMapGroupByArea.get(district.getArea());
//			if (districtsByArea == null)
//				districtsByArea = new ArrayList<District>();
//			districtsByArea.add(district);
//			districtMapGroupByArea.put(district.getArea(), districtsByArea);
//		}
//
//		/**************/
//
//		List<PaymentOptionType> paymentOptionTypes = this.paymentOptionTypeRepository.findByEnableOrderByOrderingAsc(1);
//
//		Map<Integer, PaymentOptionType> paymentOptionTypeMayById = new HashMap<Integer, PaymentOptionType>();
//		for (PaymentOptionType paymentOptionType : paymentOptionTypes)
//			paymentOptionTypeMayById.put(paymentOptionType.getPaymentOptionTypeId(), paymentOptionType);
//
//		/**************/
//
//		// List<CarParkPaymentOption> carParkPaymentOptions = this.carParkPaymentOptionRepository.findAll();
//		//
//		// Map<Integer, List<CarParkPaymentOption>> carParkPaymentOptionMapGroupByCarParkId = new HashMap<Integer, List<CarParkPaymentOption>>();
//		// Map<Integer, List<CarParkPaymentOption>> carParkPaymentOptionMapGroupByPaymentOptionTypeId = new HashMap<Integer, List<CarParkPaymentOption>>();
//		// for (CarParkPaymentOption carParkPaymentOption : carParkPaymentOptions) {
//		// List<CarParkPaymentOption> carParkPaymentOptionsByCarParkId = carParkPaymentOptionMapGroupByCarParkId.get(carParkPaymentOption.getCarParkId());
//		// if (carParkPaymentOptionsByCarParkId == null)
//		// carParkPaymentOptionsByCarParkId = new ArrayList<CarParkPaymentOption>();
//		// carParkPaymentOptionsByCarParkId.add(carParkPaymentOption);
//		// carParkPaymentOptionMapGroupByCarParkId.put(carParkPaymentOption.getCarParkId(), carParkPaymentOptionsByCarParkId);
//		//
//		// List<CarParkPaymentOption> carParkPaymentOptionsByPaymentOptionTypeId = carParkPaymentOptionMapGroupByPaymentOptionTypeId.get(carParkPaymentOption.getPaymentOptionTypeId());
//		// if (carParkPaymentOptionsByPaymentOptionTypeId == null)
//		// carParkPaymentOptionsByPaymentOptionTypeId = new ArrayList<CarParkPaymentOption>();
//		// carParkPaymentOptionsByPaymentOptionTypeId.add(carParkPaymentOption);
//		// carParkPaymentOptionMapGroupByPaymentOptionTypeId.put(carParkPaymentOption.getPaymentOptionTypeId(), carParkPaymentOptionsByPaymentOptionTypeId);
//		// }
//
//		/**************/
//
//		List<CarParkFacilityPaymentOption> carParkFacilityPaymentOptions = this.carParkFacilityPaymentOptionRepository.findAll();
//
//		Map<Integer, List<CarParkFacilityPaymentOption>> carParkFacilityPaymentOptionMapGroupByFacilityKey = new HashMap<Integer, List<CarParkFacilityPaymentOption>>();
//		Map<Integer, List<CarParkFacilityPaymentOption>> carParkFacilityPaymentOptionMapGroupByPaymentOptionTypeId = new HashMap<Integer, List<CarParkFacilityPaymentOption>>();
//		for (CarParkFacilityPaymentOption carParkFacilityPaymentOption : carParkFacilityPaymentOptions) {
//			List<CarParkFacilityPaymentOption> carParkFacilityPaymentOptionsByFacilityKey = carParkFacilityPaymentOptionMapGroupByFacilityKey.get(carParkFacilityPaymentOption.getFacilityKey());
//			if (carParkFacilityPaymentOptionsByFacilityKey == null)
//				carParkFacilityPaymentOptionsByFacilityKey = new ArrayList<CarParkFacilityPaymentOption>();
//			carParkFacilityPaymentOptionsByFacilityKey.add(carParkFacilityPaymentOption);
//			carParkFacilityPaymentOptionMapGroupByFacilityKey.put(carParkFacilityPaymentOption.getFacilityKey(), carParkFacilityPaymentOptionsByFacilityKey);
//
//			List<CarParkFacilityPaymentOption> carParkFacilityPaymentOptionsByPaymentOptionTypeId = carParkFacilityPaymentOptionMapGroupByPaymentOptionTypeId.get(carParkFacilityPaymentOption.getPaymentOptionTypeId());
//			if (carParkFacilityPaymentOptionsByPaymentOptionTypeId == null)
//				carParkFacilityPaymentOptionsByPaymentOptionTypeId = new ArrayList<CarParkFacilityPaymentOption>();
//			carParkFacilityPaymentOptionsByPaymentOptionTypeId.add(carParkFacilityPaymentOption);
//			carParkFacilityPaymentOptionMapGroupByPaymentOptionTypeId.put(carParkFacilityPaymentOption.getPaymentOptionTypeId(), carParkFacilityPaymentOptionsByPaymentOptionTypeId);
//		}
//
//		/**************/
//
//		List<CarParkPrivilegeType> carParkPrivilegeTypes = this.carParkPrivilegeTypeRepository.findByEnableOrderByOrderingAsc(1);
//
//		Map<Integer, CarParkPrivilegeType> carParkPrivilegeTypeMapById = new HashMap<Integer, CarParkPrivilegeType>();
//		for (CarParkPrivilegeType carParkPrivilegeType : carParkPrivilegeTypes)
//			carParkPrivilegeTypeMapById.put(carParkPrivilegeType.getCarParkPrivilegeTypeId(), carParkPrivilegeType);
//
//		/**************/
//
//		// List<CarParkPrivilege> carParkPrivileges = this.carParkPrivilegeRepository.findAll();
//		//
//		// Map<Integer, List<CarParkPrivilege>> carParkPrivilegeMapGroupByCarParkId = new HashMap<Integer, List<CarParkPrivilege>>();
//		// Map<Integer, List<CarParkPrivilege>> carParkPrivilegeMapGroupByCarParkPrivilegeTypeId = new HashMap<Integer, List<CarParkPrivilege>>();
//		// for (CarParkPrivilege carParkPrivilege : carParkPrivileges) {
//		// List<CarParkPrivilege> carParkPrivilegesByCarParkId = carParkPrivilegeMapGroupByCarParkId.get(carParkPrivilege.getCarParkId());
//		// if (carParkPrivilegesByCarParkId == null)
//		// carParkPrivilegesByCarParkId = new ArrayList<CarParkPrivilege>();
//		// carParkPrivilegesByCarParkId.add(carParkPrivilege);
//		// carParkPrivilegeMapGroupByCarParkId.put(carParkPrivilege.getCarParkId(), carParkPrivilegesByCarParkId);
//		//
//		// List<CarParkPrivilege> carParkPrivilegesByCarParkPrivilegeTypeId = carParkPrivilegeMapGroupByCarParkPrivilegeTypeId.get(carParkPrivilege.getCarParkPrivilegeTypeId());
//		// if (carParkPrivilegesByCarParkPrivilegeTypeId == null)
//		// carParkPrivilegesByCarParkPrivilegeTypeId = new ArrayList<CarParkPrivilege>();
//		// carParkPrivilegesByCarParkPrivilegeTypeId.add(carParkPrivilege);
//		// carParkPrivilegeMapGroupByCarParkPrivilegeTypeId.put(carParkPrivilege.getCarParkPrivilegeTypeId(), carParkPrivilegesByCarParkPrivilegeTypeId);
//		// }
//
//		/**************/
//
//		List<CarParkFacilityPrivilege> carParkFacilityPrivileges = this.carParkFacilityPrivilegeRepository.findAll();
//
//		Map<Integer, List<CarParkFacilityPrivilege>> carParkFacilityPrivilegeMapGroupByFacilityKey = new HashMap<Integer, List<CarParkFacilityPrivilege>>();
//		Map<Integer, List<CarParkFacilityPrivilege>> carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId = new HashMap<Integer, List<CarParkFacilityPrivilege>>();
//		for (CarParkFacilityPrivilege carParkFacilityPrivilege : carParkFacilityPrivileges) {
//			List<CarParkFacilityPrivilege> carParkFacilityPrivilegesByFacilityKey = carParkFacilityPrivilegeMapGroupByFacilityKey.get(carParkFacilityPrivilege.getFacilityKey());
//			if (carParkFacilityPrivilegesByFacilityKey == null)
//				carParkFacilityPrivilegesByFacilityKey = new ArrayList<CarParkFacilityPrivilege>();
//			carParkFacilityPrivilegesByFacilityKey.add(carParkFacilityPrivilege);
//			carParkFacilityPrivilegeMapGroupByFacilityKey.put(carParkFacilityPrivilege.getFacilityKey(), carParkFacilityPrivilegesByFacilityKey);
//
//			List<CarParkFacilityPrivilege> carParkFacilityPrivilegesByCarParkPrivilegeTypeId = carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId.get(carParkFacilityPrivilege.getCarParkPrivilegeTypeId());
//			if (carParkFacilityPrivilegesByCarParkPrivilegeTypeId == null)
//				carParkFacilityPrivilegesByCarParkPrivilegeTypeId = new ArrayList<CarParkFacilityPrivilege>();
//			carParkFacilityPrivilegesByCarParkPrivilegeTypeId.add(carParkFacilityPrivilege);
//			carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId.put(carParkFacilityPrivilege.getCarParkPrivilegeTypeId(), carParkFacilityPrivilegesByCarParkPrivilegeTypeId);
//		}
//
//		/**************/
//
//		// List<CarPark> carParks = this.carParkRepository.findByEnableOrderByOrderingAsc(1);
//		//
//		// Map<Integer, CarPark> carParkMapById = new HashMap<Integer, CarPark>();
//		// Map<String, CarPark> carParkMapByPropCode = new HashMap<String, CarPark>();
//		// Map<Integer, List<CarPark>> carParkMapGroupByShopCentreId = new HashMap<Integer, List<CarPark>>();
//		// Map<Integer, List<CarPark>> carParkMapGroupByDistrictId = new HashMap<Integer, List<CarPark>>();
//		// for (CarPark carPark : carParks) {
//		// carParkMapById.put(carPark.getCarParkId(), carPark);
//		// carParkMapByPropCode.put(carPark.getPropCode(), carPark);
//		//
//		// List<CarPark> carParksByShopCentreId = carParkMapGroupByShopCentreId.get(carPark.getShopCentreId());
//		// if (carParksByShopCentreId == null)
//		// carParksByShopCentreId = new ArrayList<CarPark>();
//		// carParksByShopCentreId.add(carPark);
//		// carParkMapGroupByShopCentreId.put(carPark.getShopCentreId(), carParksByShopCentreId);
//		//
//		// List<CarPark> carParksByDistrictId = carParkMapGroupByDistrictId.get(carPark.getDistrictId());
//		// if (carParksByDistrictId == null)
//		// carParksByDistrictId = new ArrayList<CarPark>();
//		// carParksByDistrictId.add(carPark);
//		// carParkMapGroupByDistrictId.put(carPark.getDistrictId(), carParksByDistrictId);
//		// }
//
//		/**************/
//
//		List<ShopCentreFeatureType> shopCentreFeatureTypes = this.shopCentreFeatureTypeRepository.findByEnable(1);
//
//		Map<Integer, ShopCentreFeatureType> shopCentreFeatureTypeMapById = new HashMap<Integer, ShopCentreFeatureType>();
//		Map<String, List<ShopCentreFeatureType>> shopCentreFeatureTypeMapGroupByShopCentreFeatureTypeName = new HashMap<String, List<ShopCentreFeatureType>>();
//		for (ShopCentreFeatureType shopCentreFeatureType : shopCentreFeatureTypes) {
//			shopCentreFeatureTypeMapById.put(shopCentreFeatureType.getShopCentreFeatureTypeId(), shopCentreFeatureType);
//
//			List<ShopCentreFeatureType> shopCentreFeatureTypesByShopCentreFeatureTypeName = shopCentreFeatureTypeMapGroupByShopCentreFeatureTypeName.get(shopCentreFeatureType.getShopCentreFeatureTypeNameEn());
//			if (shopCentreFeatureTypesByShopCentreFeatureTypeName == null)
//				shopCentreFeatureTypesByShopCentreFeatureTypeName = new ArrayList<ShopCentreFeatureType>();
//			shopCentreFeatureTypesByShopCentreFeatureTypeName.add(shopCentreFeatureType);
//			shopCentreFeatureTypeMapGroupByShopCentreFeatureTypeName.put(shopCentreFeatureType.getShopCentreFeatureTypeNameEn(), shopCentreFeatureTypesByShopCentreFeatureTypeName);
//		}
//
//		/**************/
//
//		List<ShopCentreServiceType> shopCentreServiceTypes = this.shopCentreServiceTypeRepository.findByEnableOrderByOrderingAsc(1);
//
//		Map<Integer, ShopCentreServiceType> shopCentreServiceTypeMapById = new HashMap<Integer, ShopCentreServiceType>();
//		for (ShopCentreServiceType shopCentreServiceType : shopCentreServiceTypes)
//			shopCentreServiceTypeMapById.put(shopCentreServiceType.getShopCentreServiceTypeId(), shopCentreServiceType);
//
//		/**************/
//
//		List<ShopCentreService> shopCentreServices = this.shopCentreServiceRepository.findAll();
//
//		Map<Integer, List<ShopCentreService>> shopCentreServiceMapGroupByShopCentreServiceTypeId = new HashMap<Integer, List<ShopCentreService>>();
//		Map<Integer, List<ShopCentreService>> shopCentreServiceMapGroupByShopCentreId = new HashMap<Integer, List<ShopCentreService>>();
//		for (ShopCentreService shopCentreService : shopCentreServices) {
//			List<ShopCentreService> shopCentreServicesByShopCentreServiceTypeId = shopCentreServiceMapGroupByShopCentreServiceTypeId.get(shopCentreService.getShopCentreServiceTypeId());
//			if (shopCentreServicesByShopCentreServiceTypeId == null)
//				shopCentreServicesByShopCentreServiceTypeId = new ArrayList<ShopCentreService>();
//			shopCentreServicesByShopCentreServiceTypeId.add(shopCentreService);
//			shopCentreServiceMapGroupByShopCentreServiceTypeId.put(shopCentreService.getShopCentreServiceTypeId(), shopCentreServicesByShopCentreServiceTypeId);
//
//			List<ShopCentreService> shopCentreServicesByShopCentreId = shopCentreServiceMapGroupByShopCentreId.get(shopCentreService.getShopCentreId());
//			if (shopCentreServicesByShopCentreId == null)
//				shopCentreServicesByShopCentreId = new ArrayList<ShopCentreService>();
//			shopCentreServicesByShopCentreId.add(shopCentreService);
//			shopCentreServiceMapGroupByShopCentreId.put(shopCentreService.getShopCentreId(), shopCentreServicesByShopCentreId);
//		}
//
//		/**************/
//
//		List<ShopCentre> shopCentres = this.shopCentreRepository.findByEnable(1);
//		for (int i = shopCentres.size() - 1; i >= 0; i--)
//			if (!districtMapById.containsKey(shopCentres.get(i).getDistrictId()))
//				shopCentres.remove(i);
//
//		Collections.sort(shopCentres, new Comparator<ShopCentre>() {
//
//			public int compare(ShopCentre o1, ShopCentre o2) {
//				try {
//					return 1;//return new Integer(o1.getField05En()).compareTo(new Integer(o2.getField05En()));
//				} catch (Exception e) {
//					return o1.getShopCentreNameEn().compareTo(o2.getShopCentreNameEn());
//				}
//			}
//		});
//
//		Map<Integer, ShopCentre> shopCentreMapById = new HashMap<Integer, ShopCentre>();
//		Map<String, ShopCentre> shopCentreMapByPropCode = new HashMap<String, ShopCentre>();
//		Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCentreFeatureTypeId = new HashMap<Integer, List<ShopCentre>>();
//		Map<Integer, List<ShopCentre>> shopCentreMapGroupByDistrictId = new HashMap<Integer, List<ShopCentre>>();
//		Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight = new HashMap<Integer, List<ShopCentre>>();
//		for (ShopCentre shopCentre : shopCentres) {
//			if (!districtMapById.containsKey(shopCentre.getDistrictId()))
//				continue;
//
//			shopCentreMapById.put(shopCentre.getShopCentreId(), shopCentre);
//			shopCentreMapByPropCode.put(shopCentre.getPropCode(), shopCentre);
//
//			List<ShopCentre> shopCentresByShopCentreFeatureTypeId = shopCentreMapGroupByShopCentreFeatureTypeId.get(shopCentre.getShopCentreFeatureTypeId());
//			if (shopCentresByShopCentreFeatureTypeId == null)
//				shopCentresByShopCentreFeatureTypeId = new ArrayList<ShopCentre>();
//			shopCentresByShopCentreFeatureTypeId.add(shopCentre);
//			shopCentreMapGroupByShopCentreFeatureTypeId.put(shopCentre.getShopCentreFeatureTypeId(), shopCentresByShopCentreFeatureTypeId);
//
//			List<ShopCentre> shopCentresByDistrictId = shopCentreMapGroupByDistrictId.get(shopCentre.getDistrictId());
//			if (shopCentresByDistrictId == null)
//				shopCentresByDistrictId = new ArrayList<ShopCentre>();
//			shopCentresByDistrictId.add(shopCentre);
//			shopCentreMapGroupByDistrictId.put(shopCentre.getDistrictId(), shopCentresByDistrictId);
//
//			if (shopCentre.getIsShoppingSpotlight() != null && shopCentre.getIsShoppingSpotlight()) {
//				List<ShopCentre> shopCentresByShopCentreFeatureTypeIdWithSpotlight = shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight.get(shopCentre.getShopCentreFeatureTypeId());
//				if (shopCentresByShopCentreFeatureTypeIdWithSpotlight == null)
//					shopCentresByShopCentreFeatureTypeIdWithSpotlight = new ArrayList<ShopCentre>();
//				shopCentresByShopCentreFeatureTypeIdWithSpotlight.add(shopCentre);
//				shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight.put(shopCentre.getShopCentreFeatureTypeId(), shopCentresByShopCentreFeatureTypeIdWithSpotlight);
//			}
//		}
//
//		/**************/
//
//		List<CarParkFacility> tbCarParkFacilities = this.carParkFacilityRepository.findByEnableAndDeliveryChannelLikeOrderByDisplayorderAsc(1, "%MobileApp%");
//
//		Map<Integer, CarParkFacility> tbCarParkFacilityMapById = new HashMap<Integer, CarParkFacility>();
//		// Map<String, List<TbCarParkFacility>> tbCarParkFacilityMapGroupByParentPropCode = new HashMap<String, List<TbCarParkFacility>>();
//		Map<String, List<CarParkFacility>> tbCarParkFacilityMapGroupByShopCentrePropCode = new HashMap<String, List<CarParkFacility>>();
//		Map<Integer, List<CarParkFacility>> tbCarParkFacilityMapGroupByDistrictId = new HashMap<Integer, List<CarParkFacility>>();
//		for (CarParkFacility tbCarParkFacility : tbCarParkFacilities) {
//			tbCarParkFacilityMapById.put(tbCarParkFacility.getFacilityKey(), tbCarParkFacility);
//
//			List<CarParkFacility> tbCarParkFacilitiesByShopCentrePropCode = tbCarParkFacilityMapGroupByShopCentrePropCode.get(tbCarParkFacility.getScPropCode());
//			if (tbCarParkFacilitiesByShopCentrePropCode == null)
//				tbCarParkFacilitiesByShopCentrePropCode = new ArrayList<CarParkFacility>();
//			tbCarParkFacilitiesByShopCentrePropCode.add(tbCarParkFacility);
//			tbCarParkFacilityMapGroupByShopCentrePropCode.put(tbCarParkFacility.getScPropCode(), tbCarParkFacilitiesByShopCentrePropCode);
//
//			List<CarParkFacility> tbCarParkFacilitiesByDistrictId = tbCarParkFacilityMapGroupByDistrictId.get(tbCarParkFacility.getDistrictId());
//			if (tbCarParkFacilitiesByDistrictId == null)
//				tbCarParkFacilitiesByDistrictId = new ArrayList<CarParkFacility>();
//			tbCarParkFacilitiesByDistrictId.add(tbCarParkFacility);
//			tbCarParkFacilityMapGroupByDistrictId.put(tbCarParkFacility.getDistrictId(), tbCarParkFacilitiesByDistrictId);
//
//			// List<TbCarParkFacility> tbCarParkFacilitiesByParentPropCode = tbCarParkFacilityMapGroupByParentPropCode.get(tbCarParkFacility.getParentPropCode());
//			// if (tbCarParkFacilitiesByParentPropCode == null)
//			// tbCarParkFacilitiesByParentPropCode = new ArrayList<TbCarParkFacility>();
//			// tbCarParkFacilitiesByParentPropCode.add(tbCarParkFacility);
//			// tbCarParkFacilityMapGroupByParentPropCode.put(tbCarParkFacility.getParentPropCode(), tbCarParkFacilitiesByParentPropCode);
//		}
//
//		/**************/
//
//		List<ShopCategoryType> shopCategoryTypes = this.shopCategoryTypeRepository.findByEnableOrderByOrderingAsc(1);
//
//		Map<Integer, ShopCategoryType> shopCategoryTypeMapById = new HashMap<Integer, ShopCategoryType>();
//		Map<String, List<ShopCategoryType>> shopCategoryTypeMapGroupByShopTypeName = new HashMap<String, List<ShopCategoryType>>();
//		for (ShopCategoryType shopCategoryType : shopCategoryTypes) {
//			shopCategoryTypeMapById.put(shopCategoryType.getShopCategoryTypeId(), shopCategoryType);
//
//			List<ShopCategoryType> shopCategoryTypesByShopTypeName = shopCategoryTypeMapGroupByShopTypeName.get(shopCategoryType.getShopTypeName());
//			if (shopCategoryTypesByShopTypeName == null)
//				shopCategoryTypesByShopTypeName = new ArrayList<ShopCategoryType>();
//			shopCategoryTypesByShopTypeName.add(shopCategoryType);
//			shopCategoryTypeMapGroupByShopTypeName.put(shopCategoryType.getShopTypeName(), shopCategoryTypesByShopTypeName);
//		}
//		for (String shopTypeName : shopCategoryTypeMapGroupByShopTypeName.keySet())
//			if (shopTypeName.equalsIgnoreCase("both"))
//				for (String shopTypeName2 : shopCategoryTypeMapGroupByShopTypeName.keySet())
//					if (!shopTypeName.equals(shopTypeName2)) {
//						List<ShopCategoryType> tmpShopCategoryTypes = shopCategoryTypeMapGroupByShopTypeName.get(shopTypeName2);
//						tmpShopCategoryTypes.addAll(shopCategoryTypeMapGroupByShopTypeName.get(shopTypeName));
//						shopCategoryTypeMapGroupByShopTypeName.put(shopTypeName2, tmpShopCategoryTypes);
//					}
//		for (String shopTypeName : shopCategoryTypeMapGroupByShopTypeName.keySet()) {
//			List<ShopCategoryType> tmpShopCategoryTypes = shopCategoryTypeMapGroupByShopTypeName.get(shopTypeName);
//			Collections.sort(tmpShopCategoryTypes, new Comparator<ShopCategoryType>() {
//
//				public int compare(ShopCategoryType o1, ShopCategoryType o2) {
//					return o1.getOrdering().compareTo(o2.getOrdering());
//				}
//
//			});
//			shopCategoryTypeMapGroupByShopTypeName.put(shopTypeName, tmpShopCategoryTypes);
//		}
//
//		/**************/
//
//		List<ShopCategory> shopCategories = this.shopCategoryRepository.findAll();
//
//		Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopId = new HashMap<Integer, List<ShopCategory>>();
//		Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopCategoryTypeId = new HashMap<Integer, List<ShopCategory>>();
//		for (ShopCategory shopCategory : shopCategories) {
//			List<ShopCategory> shopCategoriesByShopId = shopCategoryMapGroupByShopId.get(shopCategory.getShopId());
//			if (shopCategoriesByShopId == null)
//				shopCategoriesByShopId = new ArrayList<ShopCategory>();
//			shopCategoriesByShopId.add(shopCategory);
//			shopCategoryMapGroupByShopId.put(shopCategory.getShopId(), shopCategoriesByShopId);
//
//			List<ShopCategory> shopCategoriesByShopCategoryTypeId = shopCategoryMapGroupByShopCategoryTypeId.get(shopCategory.getShopCategoryTypeId());
//			if (shopCategoriesByShopCategoryTypeId == null)
//				shopCategoriesByShopCategoryTypeId = new ArrayList<ShopCategory>();
//			shopCategoriesByShopCategoryTypeId.add(shopCategory);
//			shopCategoryMapGroupByShopCategoryTypeId.put(shopCategory.getShopCategoryTypeId(), shopCategoriesByShopCategoryTypeId);
//		}
//
//		/**************/
//
//		List<ShopType> shopTypes = this.shopTypeRepository.findByEnable(1);
//
//		Map<Integer, ShopType> shopTypeMapById = new HashMap<Integer, ShopType>();
//		Map<String, ShopType> shopTypeMapByShopTypeName = new HashMap<String, ShopType>();
//		for (ShopType shopType : shopTypes) {
//			shopTypeMapById.put(shopType.getShopTypeId(), shopType);
//			shopTypeMapByShopTypeName.put(shopType.getShopTypeName(), shopType);
//		}
//
//		/**************/
//
//		List<ShopPrivilegeType> shopPrivilegeTypes = this.shopPrivilegeTypeRepository.findByEnableOrderByOrderingAsc(1);
//
//		Map<Integer, ShopPrivilegeType> shopPrivilegeTypeMapById = new HashMap<Integer, ShopPrivilegeType>();
//		for (ShopPrivilegeType shopPrivilegeType : shopPrivilegeTypes)
//			shopPrivilegeTypeMapById.put(shopPrivilegeType.getShopPrivilegeTypeId(), shopPrivilegeType);
//
//		/**************/
//
//		List<ShopPrivilege> shopPrivileges = this.shopPrivilegeRepository.findAll();
//
//		Map<Integer, List<ShopPrivilege>> shopPrivilegeMapGroupByShopId = new HashMap<Integer, List<ShopPrivilege>>();
//		Map<Integer, List<ShopPrivilege>> shopPrivilegeMapGroupByShopPrivilegeTypeId = new HashMap<Integer, List<ShopPrivilege>>();
//		for (ShopPrivilege shopPrivilege : shopPrivileges) {
//			List<ShopPrivilege> shopPrivilegesByShopId = shopPrivilegeMapGroupByShopId.get(shopPrivilege.getShopId());
//			if (shopPrivilegesByShopId == null)
//				shopPrivilegesByShopId = new ArrayList<ShopPrivilege>();
//			shopPrivilegesByShopId.add(shopPrivilege);
//			shopPrivilegeMapGroupByShopId.put(shopPrivilege.getShopId(), shopPrivilegesByShopId);
//
//			List<ShopPrivilege> shopPrivilegesByShopPrivilegeTypeId = shopPrivilegeMapGroupByShopPrivilegeTypeId.get(shopPrivilege.getShopPrivilegeTypeId());
//			if (shopPrivilegesByShopPrivilegeTypeId == null)
//				shopPrivilegesByShopPrivilegeTypeId = new ArrayList<ShopPrivilege>();
//			shopPrivilegesByShopPrivilegeTypeId.add(shopPrivilege);
//			shopPrivilegeMapGroupByShopPrivilegeTypeId.put(shopPrivilege.getShopPrivilegeTypeId(), shopPrivilegesByShopPrivilegeTypeId);
//		}
//
//		/**************/
//
//		List<ShopEx> shopExs = this.shopExRepository.findByEnable(1);
//		Map<List<Object>, ShopEx> shopExMapByShopInfo = new HashMap<List<Object>, ShopEx>();
//		for (ShopEx shopEx : shopExs)
//			shopExMapByShopInfo.put(Arrays.asList(shopEx.getShopCentreId(), shopEx.getBuildingEn(), shopEx.getShopNo()), shopEx);
//
//		/**************/
//
//		List<Shop> shops = this.shopRepository.findByEnableOrderByShopNameEnAscOrderingAsc(1);
//		for (int i = shops.size() - 1; i >= 0; i--)
//			if (shopCentreMapById.get(shops.get(i).getShopCentreId()) == null)
//				shops.remove(i);
//
//		Map<Integer, Shop> shopMapById = new HashMap<Integer, Shop>();
//		Map<List<Object>, Shop> shopMapByShopInfo = new HashMap<List<Object>, Shop>();
//		Map<Integer, Map<Integer, List<Shop>>> shopMapGroupByShopCentreIdWithShopType = new HashMap<Integer, Map<Integer, List<Shop>>>();
//		Map<Integer, List<Shop>> shopMapGroupByShopCentreId = new HashMap<Integer, List<Shop>>();
//		Map<Integer, List<Shop>> shopMapGroupByShopType = new HashMap<Integer, List<Shop>>();
//		Map<Integer, List<Shop>> shopMapGroupByShopName = new HashMap<Integer, List<Shop>>();
//		for (Shop shop : shops) {
//			shopMapById.put(shop.getShopId(), shop);
//			shopMapByShopInfo.put(Arrays.asList(shop.getShopCentreId(), shop.getBuildingEn(), shop.getShopNo()), shop);
//
//			{
//				List<Integer> shopTypeIds = this.getShopTypeIds(shop, shopCategoryMapGroupByShopId, shopCategoryTypeMapById, shopTypeMapByShopTypeName, shopTypes);
//				for (Integer shopTypeId : shopTypeIds) {
//					Map<Integer, List<Shop>> shopGroupByShopCentreId = shopMapGroupByShopCentreIdWithShopType.get(shopTypeId);
//					if (shopGroupByShopCentreId == null)
//						shopGroupByShopCentreId = new HashMap<Integer, List<Shop>>();
//					List<Shop> shopsByShopCentreId = shopGroupByShopCentreId.get(shop.getShopCentreId());
//					if (shopsByShopCentreId == null)
//						shopsByShopCentreId = new ArrayList<Shop>();
//					shopsByShopCentreId.add(shop);
//					shopGroupByShopCentreId.put(shop.getShopCentreId(), shopsByShopCentreId);
//					shopMapGroupByShopCentreIdWithShopType.put(shopTypeId, shopGroupByShopCentreId);
//				}
//			}
//
//			List<Shop> shopsByShopCentreId = shopMapGroupByShopCentreId.get(shop.getShopCentreId());
//			if (shopsByShopCentreId == null)
//				shopsByShopCentreId = new ArrayList<Shop>();
//			shopsByShopCentreId.add(shop);
//			shopMapGroupByShopCentreId.put(shop.getShopCentreId(), shopsByShopCentreId);
//
//			{
//				List<Integer> shopTypeIds = this.getShopTypeIds(shop, shopCategoryMapGroupByShopId, shopCategoryTypeMapById, shopTypeMapByShopTypeName, shopTypes);
//				for (Integer shopTypeId : shopTypeIds) {
//					List<Shop> shopsByShopType = shopMapGroupByShopType.get(shopTypeId);
//					if (shopsByShopType == null)
//						shopsByShopType = new ArrayList<Shop>();
//					shopsByShopType.add(shop);
//					shopMapGroupByShopType.put(shopTypeId, shopsByShopType);
//				}
//			}
//
//			List<Shop> shopsWithSameName = new ArrayList<Shop>();
//			for (Shop shop2 : shops) {
//				if(
//						StringUtils.isNotBlank( shop.getShopNameEn() ) &&
//						StringUtils.isNotBlank( shop.getShopNameTc() ) &&
//						StringUtils.isNotBlank( shop.getShopNameSc() ) &&
//						StringUtils.isNotBlank( shop2.getShopNameEn() ) &&
//						StringUtils.isNotBlank( shop2.getShopNameTc() ) &&
//						StringUtils.isNotBlank( shop2.getShopNameSc() ) 
//						){
//					if (shop.getShopNameEn().equals(shop2.getShopNameEn()) && //
//							shop.getShopNameTc().equals(shop2.getShopNameTc()) && //
//							shop.getShopNameSc().equals(shop2.getShopNameSc())) {
//						shopsWithSameName.add(shop2);
//					}
//				}
//				
//			}
//			shopMapGroupByShopName.put(shop.getShopId(), shopsWithSameName);
//		}
//
//		/**************/
//
//		Map<String, Map<Integer, List<ShopCategoryType>>> shopCategoryTypeMapGroupByShopTypeNameNShopCentreId = this.getShopCateogryTypeMapByShopTypeNameNShopCentreId(shopMapGroupByShopCentreId, shopCategoryTypeMapGroupByShopTypeName, shopCategoryMapGroupByShopId, shopCentres);
//
//		/**************/
//
//		Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCategoryTypeId = this.getShopCentreMapGroupByShopCategoryTypeId(shopCentreMapById, shopMapById, shopCategoryMapGroupByShopId);
//
//		/**************/
//
//		List<CustomColumnDisplayConfig> customColumnDisplayConfigs = this.customColumnDisplayConfigRepository.findAll();
//		Map<String, Map<String, String>> customColumnDisplayConfigMap = new HashMap<String, Map<String, String>>();
//		for (CustomColumnDisplayConfig customColumnDisplayConfig : customColumnDisplayConfigs) {
//			if (customColumnDisplayConfig.getEnable() != null && customColumnDisplayConfig.getEnable() != 1)
//				continue;
//
//			String tableName = customColumnDisplayConfig.getTableName();
//			String customColumn = customColumnDisplayConfig.getCustomColumn();
//			String displayName = customColumnDisplayConfig.getDisplayName();
//			Map<String, String> customColumnDisplayConfigInfoMap = customColumnDisplayConfigMap.get(tableName);
//			if (customColumnDisplayConfigInfoMap == null)
//				customColumnDisplayConfigInfoMap = new HashMap<String, String>();
//			customColumnDisplayConfigInfoMap.put(customColumn, displayName);
//			customColumnDisplayConfigMap.put(tableName, customColumnDisplayConfigInfoMap);
//		}
//
//		/**************/
//
////		this.aboutUs = aboutUs;
////
////		this.promotions = promotions;
////		this.promotionMapById = promotionMapById;
////		this.promotionMapByFilename = promotionMapByFilename;
////		this.promotionMapByFilenameMd5 = promotionMapByFilenameMd5;
////		this.promotionMapByPropCode = promotionMapByPropCode;
////
////		this.parkingPromotions = parkingPromotions;
////		this.parkingPromotionMapById = parkingPromotionMapById;
////		this.parkingPromotionMapByPropCode = parkingPromotionMapByPropCode;
//
//		this.cities = cities;
//		this.cityMapById = cityMapById;
//
////		this.areas = areas;
////		this.areaMapById = areaMapById;
////		this.areaMapByCode = areaMapByCode;
//
//		this.districts = districts;
//		this.districtMapById = districtMapById;
//		this.districtMapGroupByCityId = districtMapGroupByCityId;
//		this.districtMapGroupByArea = districtMapGroupByArea;
//
//		this.paymentOptionTypes = paymentOptionTypes;
//		this.paymentOptionTypeMayById = paymentOptionTypeMayById;
//
//		// this.carParkPaymentOptions = carParkPaymentOptions;
//		// this.carParkPaymentOptionMapGroupByCarParkId = carParkPaymentOptionMapGroupByCarParkId;
//		// this.carParkPaymentOptionMapGroupByPaymentOptionTypeId = carParkPaymentOptionMapGroupByPaymentOptionTypeId;
//
//		this.carParkFacilityPaymentOptions = carParkFacilityPaymentOptions;
//		this.carParkFacilityPaymentOptionMapGroupByFacilityKey = carParkFacilityPaymentOptionMapGroupByFacilityKey;
//		this.carParkFacilityPaymentOptionMapGroupByPaymentOptionTypeId = carParkFacilityPaymentOptionMapGroupByPaymentOptionTypeId;
//
//		this.carParkPrivilegeTypes = carParkPrivilegeTypes;
//		this.carParkPrivilegeTypeMapById = carParkPrivilegeTypeMapById;
//
//		// this.carParkPrivileges = carParkPrivileges;
//		// this.carParkPrivilegeMapGroupByCarParkId = carParkPrivilegeMapGroupByCarParkId;
//		// this.carParkPrivilegeMapGroupByCarParkPrivilegeTypeId = carParkPrivilegeMapGroupByCarParkPrivilegeTypeId;
//
//		this.carParkFacilityPrivileges = carParkFacilityPrivileges;
//		this.carParkFacilityPrivilegeMapGroupByFacilityKey = carParkFacilityPrivilegeMapGroupByFacilityKey;
//		this.carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId = carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId;
//
//		// this.carParks = carParks;
//		// this.carParkMapById = carParkMapById;
//		// this.carParkMapByPropCode = carParkMapByPropCode;
//		// this.carParkMapGroupByShopCentreId = carParkMapGroupByShopCentreId;
//		// this.carParkMapGroupByDistrictId = carParkMapGroupByDistrictId;
//
//		this.shopCentreFeatureTypes = shopCentreFeatureTypes;
//		this.shopCentreFeatureTypeMapById = shopCentreFeatureTypeMapById;
//		this.shopCentreFeatureTypeMapGroupByShopCentreFeatureTypeName = shopCentreFeatureTypeMapGroupByShopCentreFeatureTypeName;
//
//		this.shopCentreServiceTypes = shopCentreServiceTypes;
//		this.shopCentreServiceTypeMapById = shopCentreServiceTypeMapById;
//
//		this.shopCentreServices = shopCentreServices;
//		this.shopCentreServiceMapGroupByShopCentreServiceTypeId = shopCentreServiceMapGroupByShopCentreServiceTypeId;
//		this.shopCentreServiceMapGroupByShopCentreId = shopCentreServiceMapGroupByShopCentreId;
//
//		this.shopCentres = shopCentres;
//		this.shopCentreMapById = shopCentreMapById;
//		this.shopCentreMapByPropCode = shopCentreMapByPropCode;
//		this.shopCentreMapGroupByShopCentreFeatureTypeId = shopCentreMapGroupByShopCentreFeatureTypeId;
//		this.shopCentreMapGroupByDistrictId = shopCentreMapGroupByDistrictId;
//		this.shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight = shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight;
//
//		this.tbCarParkFacilities = tbCarParkFacilities;
//		this.tbCarParkFacilityMapById = tbCarParkFacilityMapById;
//		// this.tbCarParkFacilityMapGroupByParentPropCode = tbCarParkFacilityMapGroupByParentPropCode;
//		this.tbCarParkFacilityMapGroupByShopCentrePropCode = tbCarParkFacilityMapGroupByShopCentrePropCode;
//		this.tbCarParkFacilityMapGroupByDistrictId = tbCarParkFacilityMapGroupByDistrictId;
//
//		this.shopCategoryTypes = shopCategoryTypes;
//		this.shopCategoryTypeMapById = shopCategoryTypeMapById;
//		this.shopCategoryTypeMapGroupByShopTypeName = shopCategoryTypeMapGroupByShopTypeName;
//
//		this.shopCategories = shopCategories;
//		this.shopCategoryMapGroupByShopId = shopCategoryMapGroupByShopId;
//		this.shopCategoryMapGroupByShopCategoryTypeId = shopCategoryMapGroupByShopCategoryTypeId;
//
//		this.shopTypes = shopTypes;
//		this.shopTypeMapById = shopTypeMapById;
//		this.shopTypeMapByShopTypeName = shopTypeMapByShopTypeName;
//
//		this.shopPrivilegeTypes = shopPrivilegeTypes;
//		this.shopPrivilegeTypeMapById = shopPrivilegeTypeMapById;
//
//		this.shopPrivileges = shopPrivileges;
//		this.shopPrivilegeMapGroupByShopId = shopPrivilegeMapGroupByShopId;
//		this.shopPrivilegeMapGroupByShopPrivilegeTypeId = shopPrivilegeMapGroupByShopPrivilegeTypeId;
//
//		this.shopExs = shopExs;
//		this.shopExMapByShopInfo = shopExMapByShopInfo;
//
//		this.shops = shops;
//		this.shopMapById = shopMapById;
//		this.shopMapByShopInfo = shopMapByShopInfo;
//		this.shopMapGroupByShopCentreIdWithShopType = shopMapGroupByShopCentreIdWithShopType;
//		this.shopMapGroupByShopCentreId = shopMapGroupByShopCentreId;
//		this.shopMapGroupByShopType = shopMapGroupByShopType;
//		this.shopMapGroupByShopName = shopMapGroupByShopName;
//
//		this.shopCategoryTypeMapGroupByShopTypeNameNShopCentreId = shopCategoryTypeMapGroupByShopTypeNameNShopCentreId;
//
//		this.shopCentreMapGroupByShopCategoryTypeId = shopCentreMapGroupByShopCategoryTypeId;
//
//		this.customColumnDisplayConfigs = customColumnDisplayConfigs;
//		this.customColumnDisplayConfigMap = customColumnDisplayConfigMap;
//
//		long endTime = System.currentTimeMillis();
//		LOG.info("ResourcesService Retrieve All Data Time Used: " + (endTime - startTime) + "ms");
//	}
//
//	/****************** retrieveCarParkData ******************/
//
//	@Transactional(readOnly = true, value = "resourcesTransactionManager")
//	public void retrieveCarParkData() {
//
//		long startTime = System.currentTimeMillis();
//
//		/**************/
//
//		List<CarParkFacility> tbCarParkFacilities = this.carParkFacilityRepository.findByEnableAndDeliveryChannelLikeOrderByDisplayorderAsc(1, "%MobileApp%");
//
//		Map<Integer, CarParkFacility> tbCarParkFacilityMapById = new HashMap<Integer, CarParkFacility>();
//		for (CarParkFacility tbCarParkFacility : tbCarParkFacilities)
//			tbCarParkFacilityMapById.put(tbCarParkFacility.getFacilityKey(), tbCarParkFacility);
//
//		/**************/
//
//		this.tbCarParkFacilityRealTimeMapById = tbCarParkFacilityMapById;
//
//		long endTime = System.currentTimeMillis();
//		LOG.info("ResourcesService Retrieve Car Park Data Time Used: " + (endTime - startTime) + "ms");
//	}
//
//	/****************** others ******************/
//
//	private Map<String, Map<Integer, List<ShopCategoryType>>> getShopCateogryTypeMapByShopTypeNameNShopCentreId(Map<Integer, List<Shop>> shopMapGroupByShopCentreId, Map<String, List<ShopCategoryType>> shopCategoryTypeMapGroupByShopTypeName, Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopId, List<ShopCentre> shopCentres) {
//		Map<String, Map<Integer, List<ShopCategoryType>>> shopCateogryTypeMapByShopTypeNameNShopCentreId = new HashMap<String, Map<Integer, List<ShopCategoryType>>>();
////		shopCateogryTypeMapByShopTypeNameNShopCentreId.put(this.systemService.getProperty("shopTypeNameShop"), this.getShopCateogryTypeMapByShopCentreId(shopMapGroupByShopCentreId, shopCategoryTypeMapGroupByShopTypeName.get(this.systemService.getProperty("shopTypeNameShop")), shopCategoryMapGroupByShopId, shopCentres));
////		shopCateogryTypeMapByShopTypeNameNShopCentreId.put(this.systemService.getProperty("shopTypeNameDining"), this.getShopCateogryTypeMapByShopCentreId(shopMapGroupByShopCentreId, shopCategoryTypeMapGroupByShopTypeName.get(this.systemService.getProperty("shopTypeNameDining")), shopCategoryMapGroupByShopId, shopCentres));
//
//		return shopCateogryTypeMapByShopTypeNameNShopCentreId;
//	}
//
//	private Map<Integer, List<ShopCategoryType>> getShopCateogryTypeMapByShopCentreId(Map<Integer, List<Shop>> shopMapGroupByShopCentreId, List<ShopCategoryType> shopCategoryTypes, Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopId, List<ShopCentre> shopCentres) {
//		Map<Integer, List<ShopCategoryType>> shopCateogryTypeMapByShopCentreId = new HashMap<Integer, List<ShopCategoryType>>();
//
//		for (ShopCentre shopCentre : shopCentres) {
//			List<ShopCategoryType> shopCateogryTypesByShopCentreId = new ArrayList<ShopCategoryType>();
//
//			List<Shop> shops = shopMapGroupByShopCentreId.get(shopCentre.getShopCentreId());
//			if (shops != null) {
//				for (ShopCategoryType shopCategoryType : shopCategoryTypes) {
//					boolean found = false;
//					for (Shop shop : shops) {
//						List<ShopCategory> shopCategories = shopCategoryMapGroupByShopId.get(shop.getShopId());
//						if (shopCategories != null) {
//							for (ShopCategory shopCategory : shopCategories) {
//								if (shopCategory.getShopCategoryTypeId().equals(shopCategoryType.getShopCategoryTypeId())) {
//									found = true;
//									break;
//								}
//							}
//						}
//						if (found)
//							break;
//					}
//					if (found)
//						shopCateogryTypesByShopCentreId.add(shopCategoryType);
//				}
//			}
//
//			shopCateogryTypeMapByShopCentreId.put(shopCentre.getShopCentreId(), shopCateogryTypesByShopCentreId);
//		}
//
//		return shopCateogryTypeMapByShopCentreId;
//	}
//
//	private List<Integer> getShopTypeIds(Shop shop, //
//			Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopId, //
//			Map<Integer, ShopCategoryType> shopCategoryTypeMapById, //
//			Map<String, ShopType> shopTypeMapByShopTypeName, //
//			List<ShopType> shopTypes) {
//		List<Integer> shopTypeIds = new ArrayList<Integer>();
//		List<ShopCategory> shopCategories = shopCategoryMapGroupByShopId.get(shop.getShopId());
//		if (shopCategories != null)
//			for (ShopCategory shopCategory : shopCategories) {
//				Integer shopCategoryTypeId = shopCategory.getShopCategoryTypeId();
//				ShopCategoryType shopCategoryType = shopCategoryTypeMapById.get(shopCategoryTypeId);
//				if (shopCategoryType != null) {
//					String shopTypeName = shopCategoryType.getShopTypeName();
//					if (shopTypeName.equalsIgnoreCase("both")) {
//						for (ShopType shopType : shopTypes)
//							if (!shopType.getShopTypeName().equalsIgnoreCase("both"))
//								if (!shopTypeIds.contains(shopType.getShopTypeId()))
//									shopTypeIds.add(shopType.getShopTypeId());
//					} else {
//						ShopType shopType = shopTypeMapByShopTypeName.get(shopTypeName);
//						if (shopType != null)
//							if (!shopTypeIds.contains(shopType.getShopTypeId()))
//								shopTypeIds.add(shopType.getShopTypeId());
//					}
//				}
//			}
//		return shopTypeIds;
//	}
//
//	public List<Integer> getShopTypeIds(Shop shop) {
//		return this.getShopTypeIds(shop, this.shopCategoryMapGroupByShopId, this.shopCategoryTypeMapById, this.shopTypeMapByShopTypeName, this.shopTypes);
//	}
//
//	public String getShopImage(ShopCentre shopCentre, Shop shop, String imagePath) {
//		Integer type = 0;
//		List<Integer> typeIds = this.getShopTypeIds(shop);
//		if (typeIds != null && typeIds.size() > 0)
//			type = typeIds.get(0);
//
//		if (shop.getShopPhotoPath() == null) {
//			try {
//				return Url.API_ASSET_DEFAULTIMAGE_PREFIX + "/shopType" + type + ".jpg";
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		//FileObject fileObject = this.fileService.findByFilename( imagePath, null, null, false, true);
//		FsFiles files = this.fileService.getFileByImages( "/ShopPhoto/" + shopCentre.getPropCode() + "/" + imagePath );
//		return Url.API_ASSET_SHOPIMAGE_PREFIX + "/" + type + "/" + ( files == null ? "0" : files.getMd5() ) + "/" + shopCentre.getPropCode() + "/" + imagePath;
//	}
//
//	public String getShopImage(ShopCentre shopCentre, Shop shop) {
//		Integer type = 0;
//		List<Integer> typeIds = this.getShopTypeIds(shop);
//		if (typeIds != null && typeIds.size() > 0)
//			type = typeIds.get(0);
//
//		if (shop.getShopPhotoPath() == null) {
//			try {
//				return Url.API_ASSET_DEFAULTIMAGE_PREFIX + "/shopType" + type + ".jpg";
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		//FileObject fileObject = this.fileService.findByFilename( "/ShopPhoto/" + shopCentre.getPropCode() + "/" + shop.getShopPhotoPath(), null, null, false, true);
//		FsFiles files = this.fileService.getFileByImages( "/ShopPhoto/" + shopCentre.getPropCode() + "/" + shop.getShopPhotoPath() );
//		return  Url.API_ASSET_SHOPIMAGE_PREFIX + "/" + type + "/" + ( files == null ? "0" : files.getMd5() ) + "/" + shopCentre.getPropCode() +"/" + shop.getShopPhotoPath() ;
//	}
//
//	public Map<Integer, List<ShopCentre>> getShopCentreMapGroupByShopCategoryTypeId(Map<Integer, ShopCentre> shopCentreMapById, Map<Integer, Shop> shopMapById, Map<Integer, List<ShopCategory>> shopCategoryMapGroupByShopId) {
//
//		Map<Integer, List<ShopCentre>> shopCentreMapGroupByShopCategoryTypeId = new HashMap<Integer, List<ShopCentre>>();
//
//		for (Integer shopId : shopCategoryMapGroupByShopId.keySet()) {
//			Shop shop = shopMapById.get(shopId);
//			if (shop != null) {
//				Integer shopCentreId = shop.getShopCentreId();
//				ShopCentre shopCentre = shopCentreMapById.get(shopCentreId);
//
//				if (shopCentre != null) {
//					List<ShopCategory> shopCategories = shopCategoryMapGroupByShopId.get(shopId);
//					for (ShopCategory shopCategory : shopCategories) {
//						Integer shopCategoryTypeId = shopCategory.getShopCategoryTypeId();
//
//						List<ShopCentre> shopCentres = shopCentreMapGroupByShopCategoryTypeId.get(shopCategoryTypeId);
//						if (shopCentres == null)
//							shopCentres = new ArrayList<ShopCentre>();
//
//						if (!shopCentres.contains(shopCentre))
//							shopCentres.add(shopCentre);
//
//						shopCentreMapGroupByShopCategoryTypeId.put(shopCategoryTypeId, shopCentres);
//					}
//				}
//			}
//		}
//
//		for (Integer shopCategoryTypeId : shopCentreMapGroupByShopCategoryTypeId.keySet()) {
//			List<ShopCentre> shopCentres = shopCentreMapGroupByShopCategoryTypeId.get(shopCategoryTypeId);
//			Collections.sort(shopCentres, new Comparator<ShopCentre>() {
//
//				public int compare(ShopCentre o1, ShopCentre o2) {
//					try {
//						return 1;
//						//return new Integer(o1.getField05En()).compareTo(new Integer(o2.getField05En()));
//					} catch (Exception e) {
//						return o1.getShopCentreNameEn().compareTo(o2.getShopCentreNameEn());
//					}
//				}
//
//			});
//			shopCentreMapGroupByShopCategoryTypeId.put(shopCategoryTypeId, shopCentres);
//		}
//
//		return shopCentreMapGroupByShopCategoryTypeId;
//	}
//
//	/****************** Getter ******************/
//
//	@Override
//	public List<Area> getAreas() {
//		return this.areas;
//	}
//
//	@Override
//	public List<District> getDistricts() {
//		return this.districts;
//	}
//
//	@Override
//	public List<District> getDistrictsByArea(String area) {
//		return this.districtMapGroupByArea.get(area);
//	}
//
//	@Override
//	public List<ShopType> getShopTypes() {
//		return this.shopTypes;
//	}
//
//	@Override
//	public List<ShopCategoryType> getShopCategoryTypesByShopTypeName(String shopTypeName) {
//		return this.shopCategoryTypeMapGroupByShopTypeName.get(shopTypeName);
//	}
//
//	@Override
//	public List<CarParkPrivilegeType> getCarParkPrivilegeTypes() {
//		return this.carParkPrivilegeTypes;
//	}
//
//	@Override
//	public List<CarParkFacility> getTbCarParkFacilitiesByDistrictId(Integer districtId, int offset, int size) {
//		// List<CarPark> carParks = this.carParkMapGroupByDistrictId.get(districtId);
//		//
//		// List<TbCarParkFacility> tbCarParkFacilities = new ArrayList<TbCarParkFacility>();
//		// for (CarPark carPark : carParks)
//		// if (carPark.getPropCode() != null && this.tbCarParkFacilityMapGroupByParentPropCode.get(carPark.getPropCode()) != null)
//		// tbCarParkFacilities.addAll(this.tbCarParkFacilityMapGroupByParentPropCode.get(carPark.getPropCode()));
//
//		List<CarParkFacility> tbCarParkFacilities = new ArrayList<CarParkFacility>();
//		if (this.tbCarParkFacilityMapGroupByDistrictId.get(districtId) != null)
//			tbCarParkFacilities = new ArrayList<CarParkFacility>(this.tbCarParkFacilityMapGroupByDistrictId.get(districtId));
//
//		Collections.sort(tbCarParkFacilities, new Comparator<CarParkFacility>() {
//			public int compare(CarParkFacility o1, CarParkFacility o2) {
//				if (o1.getDisplayorder() != null && o2.getDisplayorder() != null)
//					return o1.getDisplayorder().compareTo(o2.getDisplayorder());
//				else
//					return o1.getCarparkfacilityNameEn().compareTo(o2.getCarparkfacilityNameEn());
//			}
//		});
//
//		List<CarParkFacility> results = new ArrayList<CarParkFacility>();
//		for (int i = offset; i < offset + size && i < tbCarParkFacilities.size(); i++)
//			results.add(tbCarParkFacilities.get(i));
//
//		return results;
//	}
//
//	@Override
//	public Integer countTbCarParkFacilities() {
//		return this.tbCarParkFacilities.size();
//	}
//
//	@Override
//	public List<CarParkFacility> getNearestTbCarParkFacilities(Double longitude, Double latitude, List<CarParkFacility> exceptTbCarParkFacilities, int offset, int size) {
//		List<CarParkFacility> tbCarParkFacilities = this.tbCarParkFacilities;
//
//		List<Object[]> tbCarParkFacilityWithDistanceList = new ArrayList<Object[]>();
//		for (CarParkFacility tbCarParkFacility : tbCarParkFacilities) {
//			if (tbCarParkFacility.getLongitude() != null && tbCarParkFacility.getLatitude() != null) {
//				Double distance = LocationUtil.calculateDistance(longitude, latitude, new Double(tbCarParkFacility.getLongitude()), new Double(tbCarParkFacility.getLatitude()));
//				Object[] tbCarParkFacilityWithDistance = new Object[] { tbCarParkFacility, distance };
//
//				tbCarParkFacilityWithDistanceList.add(tbCarParkFacilityWithDistance);
//			}
//		}
//
//		Collections.sort(tbCarParkFacilityWithDistanceList, new Comparator<Object[]>() {
//			@Override
//			public int compare(Object[] o1, Object[] o2) {
//				return ((Double) o1[1]).compareTo((Double) o2[1]);
//			}
//		});
//
//		List<CarParkFacility> results = new ArrayList<CarParkFacility>();
//		if (exceptTbCarParkFacilities == null) {
//			for (int i = offset; i < offset + size && i < tbCarParkFacilityWithDistanceList.size(); i++)
//				results.add((CarParkFacility) tbCarParkFacilityWithDistanceList.get(i)[0]);
//
//		} else {
//			for (Object[] tbCarParkFacilityWithDistance : tbCarParkFacilityWithDistanceList) {
//				boolean exists = false;
//				CarParkFacility tbCarParkFacility = (CarParkFacility) tbCarParkFacilityWithDistance[0];
//				for (CarParkFacility exceptTbCarParkFacility : exceptTbCarParkFacilities)
//					if (tbCarParkFacility.getFacilityKey().equals(exceptTbCarParkFacility.getFacilityKey())) {
//						exists = true;
//						break;
//					}
//
//				if (!exists) {
//					results.add(tbCarParkFacility);
//
//					if (results.size() >= size)
//						break;
//				}
//			}
//
//		}
//
//		return results;
//	}
//
//	// @Override
//	// public CarPark getCarParkMapByPropCode(String propCode) {
//	// return this.carParkMapByPropCode.get(propCode);
//	// }
//
//	@Override
//	public ShopCentre getShopCentreById(Integer id) {
//		return this.shopCentreMapById.get(id);
//	}
//
//	@Override
//	public CarParkFacility getTbCarParkFacilityById(Integer facilityKey) {
//		// return this.carParkFacilityRepository.findOne(facilityKey);
//		return this.tbCarParkFacilityMapById.get(facilityKey);
//	}
//
//	@Override
//	@Transactional(readOnly = true, value = "resourcesTransactionManager")
//	public Map<Integer, CarParkFacility> getTbCarParkFacilityMapByIdIn(List<Integer> facilityKeys) {
//		List<CarParkFacility> tbCarParkFacilities = this.carParkFacilityRepository.findByFacilityKeyIn(facilityKeys);
//
//		Map<Integer, CarParkFacility> tbCarParkFacilityMapById = new HashMap<Integer, CarParkFacility>();
//		for (CarParkFacility tbCarParkFacility : tbCarParkFacilities)
//			tbCarParkFacilityMapById.put(tbCarParkFacility.getFacilityKey(), tbCarParkFacility);
//
//		return tbCarParkFacilityMapById;
//	}
//
//	@Override
//	public CarParkFacility getRealTimeTbCarParkFacilityById(Integer facilityKey) {
//		return this.tbCarParkFacilityRealTimeMapById.get(facilityKey);
//	}
//
//	@Override
//	public List<CarParkPrivilegeType> getCarParkPrivilegeTypesByFacilityKey(Integer facilityKey) {
//		// List<CarParkPrivilege> carParkPrivileges = this.carParkPrivilegeMapGroupByCarParkId.get(carParkId);
//		List<CarParkFacilityPrivilege> carParkFacilityPrivileges = this.carParkFacilityPrivilegeMapGroupByFacilityKey.get(facilityKey);
//
//		List<CarParkPrivilegeType> carParkPrivilegeTypes = new ArrayList<CarParkPrivilegeType>();
//
//		// if (carParkPrivileges != null)
//		// for (CarParkPrivilegeType carParkPrivilegeType : this.carParkPrivilegeTypes)
//		// for (CarParkPrivilege carParkPrivilege : carParkPrivileges)
//		// if (carParkPrivilege.getCarParkPrivilegeTypeId().equals(carParkPrivilegeType.getCarParkPrivilegeTypeId())) {
//		// carParkPrivilegeTypes.add(carParkPrivilegeType);
//		// break;
//		// }
//
//		if (carParkFacilityPrivileges != null)
//			for (CarParkPrivilegeType carParkPrivilegeType : this.carParkPrivilegeTypes)
//				for (CarParkFacilityPrivilege carParkFacilityPrivilege : carParkFacilityPrivileges)
//					if (carParkFacilityPrivilege.getCarParkPrivilegeTypeId().equals(carParkPrivilegeType.getCarParkPrivilegeTypeId())) {
//						carParkPrivilegeTypes.add(carParkPrivilegeType);
//						break;
//					}
//
//		return carParkPrivilegeTypes;
//	}
//
//	@Override
//	public List<PaymentOptionType> getPaymentOptionTypesByFacilityKey(Integer facilityKey) {
//		// List<CarParkPaymentOption> carParkPaymentOptions = this.carParkPaymentOptionMapGroupByCarParkId.get(carParkId);
//		List<CarParkFacilityPaymentOption> carParkFacilityPaymentOptions = this.carParkFacilityPaymentOptionMapGroupByFacilityKey.get(facilityKey);
//
//		List<PaymentOptionType> paymentOptionTypes = new ArrayList<PaymentOptionType>();
//
//		// if (carParkPaymentOptions != null)
//		// for (PaymentOptionType paymentOptionType : this.paymentOptionTypes)
//		// for (CarParkPaymentOption carParkPaymentOption : carParkPaymentOptions)
//		// if (carParkPaymentOption.getPaymentOptionTypeId().equals(paymentOptionType.getPaymentOptionTypeId())) {
//		// paymentOptionTypes.add(paymentOptionType);
//		// break;
//		// }
//
//		if (carParkFacilityPaymentOptions != null)
//			for (PaymentOptionType paymentOptionType : this.paymentOptionTypes)
//				for (CarParkFacilityPaymentOption carParkFacilityPaymentOption : carParkFacilityPaymentOptions)
//					if (carParkFacilityPaymentOption.getPaymentOptionTypeId().equals(paymentOptionType.getPaymentOptionTypeId())) {
//						paymentOptionTypes.add(paymentOptionType);
//						break;
//					}
//
//		return paymentOptionTypes;
//	}
//
//	@Override
//	public List<Shop> getRandomShopsByShopCentreIdNShopTypeName(Integer shopCentreId, String shopTypeName, int size) {
//		List<Shop> allShops = this.shopMapGroupByShopCentreId.get(shopCentreId);
//
//		List<Shop> selectedShops = new ArrayList<Shop>();
//		if (allShops != null)
//			for (Shop shop : allShops)
//				if (this.getShopTypeIds(shop).contains(this.shopTypeMapByShopTypeName.get(shopTypeName).getShopTypeId()))
//					selectedShops.add(shop);
//
//		Collections.shuffle(selectedShops);
//
//		selectedShops = selectedShops.subList(0, Math.min(size, selectedShops.size()));
//		return selectedShops;
//	}
//
//	@Override
//	public List<ShopCategoryType> getShopCategoryTypesByShopId(Integer shopId) {
//		List<ShopCategory> shopCategories = this.shopCategoryMapGroupByShopId.get(shopId);
//
//		List<ShopCategoryType> shopCategoryTypes = new ArrayList<ShopCategoryType>();
//
//		if (shopCategories != null)
//			for (ShopCategoryType shopCategoryType : this.shopCategoryTypes)
//				for (ShopCategory shopCategory : shopCategories)
//					if (shopCategoryType.getShopCategoryTypeId().equals(shopCategory.getShopCategoryTypeId())) {
//						shopCategoryTypes.add(shopCategoryType);
//						break;
//					}
//
//		return shopCategoryTypes;
//	}
//
//	@Override
//	public District getDistrictById(Integer id) {
//		return this.districtMapById.get(id);
//	}
//
//	@Override
//	public Area getAreaByCode(String code) {
//		return this.areaMapByCode.get(code);
//	}
//
//	@Override
//	public Object[] searchShopCentreByKeyword(String keyword, String language, int offset, int size) {
//		Integer count = 0;
//		List<ShopCentre> shopCentres = new ArrayList<ShopCentre>();
//
//		if (keyword != null && !keyword.trim().equals("")) {
//
//			List<ShopCentre> resultList = new ArrayList<ShopCentre>();
//			for (ShopCentre shopCentre : this.shopCentres)
//				if (shopCentre.getShopCentreNameEn().toLowerCase().indexOf(keyword.toLowerCase()) >= 0 || //
//						shopCentre.getShopCentreNameTc().toLowerCase().indexOf(keyword.toLowerCase()) >= 0 || //
//						shopCentre.getShopCentreNameSc().toLowerCase().indexOf(keyword.toLowerCase()) >= 0)
//					resultList.add(shopCentre);
//
//			count = resultList.size();
//			if (count > 0) {
//				for (int i = offset; i < offset + size && i < resultList.size(); i++)
//					shopCentres.add(resultList.get(i));
//			}
//		}
//
//		return new Object[] { count, shopCentres };
//
//	}
//
//	@Override
//	public Object[] searchShopByKeyword(String keyword, String language, String shopTypeName, int offset, int size) {
//		List<ShopCategoryType> shopCategoryTypes = null;
//		if (shopTypeName != null)
//			shopCategoryTypes = this.shopCategoryTypeMapGroupByShopTypeName.get(shopTypeName);
//
//		Integer count = 0;
//		List<Shop> shops = new ArrayList<Shop>();
//
//		if (keyword != null && !keyword.trim().equals("")) {
//			List<Shop> resultList = new ArrayList<Shop>();
//			for (Shop shop : this.shops)
//				if (shop.getShopNameEn().toLowerCase().indexOf(keyword.trim().toLowerCase()) >= 0 || //
//						shop.getShopNameTc().toLowerCase().indexOf(keyword.trim().toLowerCase()) >= 0 || //
//						shop.getShopNameSc().toLowerCase().indexOf(keyword.trim().toLowerCase()) >= 0)
//					if (shopTypeName == null)
//						resultList.add(shop);
//					else {
//						List<ShopCategory> shopCategories = this.shopCategoryMapGroupByShopId.get(shop.getShopId());
//						if (shopCategories != null) {
//							boolean found = false;
//							for (ShopCategory shopCategory : shopCategories) {
//								for (ShopCategoryType shopCategoryType : shopCategoryTypes)
//									if (shopCategory.getShopCategoryTypeId().equals(shopCategoryType.getShopCategoryTypeId())) {
//										found = true;
//										break;
//									}
//
//								if (found)
//									break;
//							}
//
//							if (found)
//								resultList.add(shop);
//						}
//					}
//
//			count = resultList.size();
//			if (count > 0)
//				for (int i = offset; i < offset + size && i < resultList.size(); i++)
//					shops.add(resultList.get(i));
//
//		}
//
//		return new Object[] { count, shops };
//
//	}
//
//	@Override
//	public List<ShopCentre> getShopCentres() {
//		return this.shopCentres;
//	}
//
//	@Override
//	public List<ShopCentre> getShopCentresByDistrictId(Integer districtId) {
//		List<ShopCentre> districtShopCentres = this.shopCentreMapGroupByDistrictId.get(districtId);
//		if (districtShopCentres != null)
//			return districtShopCentres;
//		else
//			return new ArrayList<ShopCentre>();
//	}
//
//	@Override
//	public List<ShopCentre> getShopCentresByAreaCode(String code) {
//		List<ShopCentre> shopCentres = new ArrayList<ShopCentre>();
//
//		List<District> districts = this.districtMapGroupByArea.get(code);
//		for (District district : districts) {
//			List<ShopCentre> districtShopCentres = this.shopCentreMapGroupByDistrictId.get(district.getDistrictId());
//			if (districtShopCentres != null)
//				shopCentres.addAll(districtShopCentres);
//		}
//
//		Collections.sort(shopCentres, new Comparator<ShopCentre>() {
//
//			public int compare(ShopCentre o1, ShopCentre o2) {
//				try {
//					return 1;
//					//return new Integer(o1.getField05En()).compareTo(new Integer(o2.getField05En()));
//				} catch (Exception e) {
//					return o1.getShopCentreNameEn().compareTo(o2.getShopCentreNameEn());
//				}
//			}
//		});
//
//		return shopCentres;
//	}
//
//	@Override
//	public ShopCentreFeatureType getShopCentreFeatureTypeById(Integer id) {
//		return this.shopCentreFeatureTypeMapById.get(id);
//	}
//
//	@Override
//	public List<ShopCentreServiceType> getShopCentreServiceTypesByShopCentreId(Integer shopCentreId) {
//		List<ShopCentreServiceType> shopCentreServiceTypes = new ArrayList<ShopCentreServiceType>();
//
//		List<ShopCentreService> shopCentreServices = this.shopCentreServiceMapGroupByShopCentreId.get(shopCentreId);
//
//		if (shopCentreServices != null)
//			for (ShopCentreServiceType shopCentreServiceType : this.shopCentreServiceTypes)
//				for (ShopCentreService shopCentreService : shopCentreServices)
//					if (shopCentreService.getShopCentreServiceTypeId().equals(shopCentreServiceType.getShopCentreServiceTypeId())) {
//						shopCentreServiceTypes.add(shopCentreServiceType);
//						break;
//					}
//
//		return shopCentreServiceTypes;
//	}
//
//	// @Override
//	// public List<TbCarParkFacility> getTbCarParkFacilitiesByParentPropCode(String propCode) {
//	// return this.tbCarParkFacilityMapGroupByParentPropCode.get(propCode);
//	// }
//
//	// @Override
//	// public List<CarPark> getCarParkByShopCentreId(Integer shopCentreId) {
//	// return this.carParkMapGroupByShopCentreId.get(shopCentreId);
//	// }
//
//	@Override
//	public List<ShopCategoryType> getShopCategoryTypesByShopTypeNameNShopCentreId(String shopTypeName, Integer shopCentreId) {
//		return this.shopCategoryTypeMapGroupByShopTypeNameNShopCentreId.get(shopTypeName).get(shopCentreId);
//	}
//
//	@Override
//	public Object[] getShopsByShopCentreIdNShopTypeName(Integer shopCentreId, String shopTypeName, int offset, int size) {
//		Integer shopTypeId = null;
//		if (shopTypeName != null)
//			shopTypeId = this.shopTypeMapByShopTypeName.get(shopTypeName).getShopTypeId();
//
//		List<Shop> shops = new ArrayList<Shop>();
//		Integer count = 0;
//
//		List<Shop> shopsInShopCentre = this.shopMapGroupByShopCentreId.get(shopCentreId);
//		if (shopsInShopCentre != null)
//			if (shopTypeId == null) {
//				for (int i = offset; i < offset + size && i < shopsInShopCentre.size(); i++)
//					shops.add(shopsInShopCentre.get(i));
//				count = shopsInShopCentre.size();
//			} else {
//				List<Shop> results = new ArrayList<Shop>();
//
//				for (Shop shop : shopsInShopCentre)
//					if (this.getShopTypeIds(shop).contains(shopTypeId))
//						results.add(shop);
//
//				for (int i = offset; i < offset + size && i < results.size(); i++)
//					shops.add(results.get(i));
//				count = results.size();
//			}
//
//		return new Object[] { count, shops };
//	}
//
//	@Override
//	public ShopType getShopTypeById(Integer id) {
//		return this.shopTypeMapById.get(id);
//	}
//
//	@Override
//	public Shop getShopById(Integer id) {
//		return this.shopMapById.get(id);
//	}
//
//	@Override
//	public List<ShopPrivilegeType> getShopPrivilegeTypesByShopId(Integer shopId) {
//		List<ShopPrivilegeType> shopPrivilegeTypes = new ArrayList<ShopPrivilegeType>();
//		List<ShopPrivilege> shopPrivileges = this.shopPrivilegeMapGroupByShopId.get(shopId);
//
//		if (shopPrivileges != null)
//			for (ShopPrivilegeType shopPrivilegeType : this.shopPrivilegeTypes)
//				for (ShopPrivilege shopPrivilege : shopPrivileges)
//					if (shopPrivilegeType.getShopPrivilegeTypeId().equals(shopPrivilege.getShopPrivilegeTypeId()))
//						shopPrivilegeTypes.add(shopPrivilegeType);
//
//		return shopPrivilegeTypes;
//	}
//
//	@Override
//	public List<ShopCentre> getShopCentresByShopCategoryTypeId(Integer shopCategoryTypeId) {
//		return this.shopCentreMapGroupByShopCategoryTypeId.get(shopCategoryTypeId);
//	}
//
//	@Override
//	public Object[] getTbCarParkFacilitiesByAreaNDistrictIdNPrivilegeTypeId(String area, Integer districtId, Integer privilegeTypeId, int offset, int size) {
//
//		List<Integer> districtIds = new ArrayList<Integer>();
//		if (districtId != null) {
//			districtIds.add(districtId);
//
//		} else if (area != null) {
//			List<District> districts = this.districtMapGroupByArea.get(area);
//			if (districts != null)
//				for (District district : districts)
//					districtIds.add(district.getDistrictId());
//
//		}
//
//		List<CarParkFacility> results = new ArrayList<CarParkFacility>();
//		Integer count = 0;
//
//		if (districtIds.size() == 0 && privilegeTypeId == null) {
//			results = new ArrayList<CarParkFacility>(this.tbCarParkFacilities);
//
//		} else if (districtIds.size() != 0 && privilegeTypeId == null) {
//			// for (Integer districtIdInList : districtIds) {
//			// List<CarPark> carParks = this.carParkMapGroupByDistrictId.get(districtIdInList);
//			// if (carParks != null)
//			// for (CarPark carPark : carParks) {
//			// List<TbCarParkFacility> tbCarParkFacilities = this.tbCarParkFacilityMapGroupByParentPropCode.get(carPark.getPropCode());
//			// if (tbCarParkFacilities != null)
//			// results.addAll(tbCarParkFacilities);
//			// }
//			// }
//
//			for (Integer districtIdInList : districtIds) {
//				List<CarParkFacility> tbCarParkFacilities = this.tbCarParkFacilityMapGroupByDistrictId.get(districtIdInList);
//				if (tbCarParkFacilities != null)
//					results.addAll(tbCarParkFacilities);
//			}
//
//		} else if (districtIds.size() == 0 && privilegeTypeId != null) {
//			// results = new ArrayList<TbCarParkFacility>();
//			//
//			// List<CarPark> carParks = new ArrayList<CarPark>();
//			// List<CarParkPrivilege> carParkPrivileges = this.carParkPrivilegeMapGroupByCarParkPrivilegeTypeId.get(privilegeTypeId);
//			// for (CarParkPrivilege carParkPrivilege : carParkPrivileges) {
//			// CarPark carPark = this.carParkMapById.get(carParkPrivilege.getCarParkId());
//			// if (carPark != null)
//			// carParks.add(carPark);
//			// }
//			//
//			// Collections.sort(carParks, new Comparator<CarPark>() {
//			// public int compare(CarPark o1, CarPark o2) {
//			// if (o1.getOrdering() != null && o2.getOrdering() != null)
//			// return o1.getOrdering().compareTo(o2.getOrdering());
//			// else
//			// return o1.getCarParkId().compareTo(o2.getCarParkId());
//			// }
//			// });
//			//
//			// for (CarPark carPark : carParks) {
//			// List<TbCarParkFacility> tbCarParkFacilities = this.tbCarParkFacilityMapGroupByParentPropCode.get(carPark.getPropCode());
//			// if (tbCarParkFacilities != null)
//			// results.addAll(tbCarParkFacilities);
//			// }
//
//			List<CarParkFacilityPrivilege> carParkFacilityPrivileges = this.carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId.get(privilegeTypeId);
//			if (carParkFacilityPrivileges != null)
//				for (CarParkFacilityPrivilege carParkFacilityPrivilege : carParkFacilityPrivileges) {
//					CarParkFacility tbCarParkFacility = this.tbCarParkFacilityMapById.get(carParkFacilityPrivilege.getFacilityKey());
//					if (tbCarParkFacility != null)
//						results.add(tbCarParkFacility);
//				}
//
//		} else if (districtIds.size() != 0 && privilegeTypeId != null) {
//			// results = new ArrayList<TbCarParkFacility>();
//			//
//			// List<CarPark> carParks = new ArrayList<CarPark>();
//			// List<CarParkPrivilege> carParkPrivileges = this.carParkPrivilegeMapGroupByCarParkPrivilegeTypeId.get(privilegeTypeId);
//			// for (CarParkPrivilege carParkPrivilege : carParkPrivileges) {
//			// CarPark carPark = this.carParkMapById.get(carParkPrivilege.getCarParkId());
//			// if (carPark != null && districtIds.contains(carPark.getDistrictId()))
//			// carParks.add(carPark);
//			// }
//			//
//			// Collections.sort(carParks, new Comparator<CarPark>() {
//			// public int compare(CarPark o1, CarPark o2) {
//			// if (o1.getOrdering() != null && o2.getOrdering() != null)
//			// return o1.getOrdering().compareTo(o2.getOrdering());
//			// else
//			// return o1.getCarParkId().compareTo(o2.getCarParkId());
//			// }
//			// });
//			//
//			// for (CarPark carPark : carParks) {
//			// List<TbCarParkFacility> tbCarParkFacilities = this.tbCarParkFacilityMapGroupByParentPropCode.get(carPark.getPropCode());
//			// if (tbCarParkFacilities != null)
//			// results.addAll(tbCarParkFacilities);
//			// }
//
//			List<CarParkFacilityPrivilege> carParkFacilityPrivileges = this.carParkFacilityPrivilegeMapGroupByCarParkPrivilegeTypeId.get(privilegeTypeId);
//			if (carParkFacilityPrivileges != null)
//				for (CarParkFacilityPrivilege carParkFacilityPrivilege : carParkFacilityPrivileges) {
//					CarParkFacility tbCarParkFacility = this.tbCarParkFacilityMapById.get(carParkFacilityPrivilege.getFacilityKey());
//					if (tbCarParkFacility != null && districtIds.contains(tbCarParkFacility.getDistrictId()))
//						results.add(tbCarParkFacility);
//				}
//
//		}
//
//		Collections.sort(results, new Comparator<CarParkFacility>() {
//			public int compare(CarParkFacility o1, CarParkFacility o2) {
//				if (o1.getDisplayorder() != null && o2.getDisplayorder() != null)
//					return o1.getDisplayorder().compareTo(o2.getDisplayorder());
//				else
//					return o1.getCarparkfacilityNameEn().compareTo(o2.getCarparkfacilityNameEn());
//			}
//		});
//
//		List<CarParkFacility> tbCarParkFacilities = new ArrayList<CarParkFacility>();
//
//		for (int i = offset; i < offset + size && i < results.size(); i++)
//			tbCarParkFacilities.add(results.get(i));
//		count = results.size();
//
//		return new Object[] { count, tbCarParkFacilities };
//
//	}
//
//	@Override
//	public Object[] getShopByAreaNDistrictIdNShopCentreIdNShopTypeIdNShopCategoryTypeId(String language, String area, Integer districtId, Integer shopCentreId, Integer shopTypeId, Integer shopCategoryTypeId, int offset, int size) {
//
//		List<Integer> shopCentreIds = new ArrayList<Integer>();
//		if (shopCentreId != null) {
//			shopCentreIds.add(shopCentreId);
//
//		} else if (districtId != null) {
//			List<ShopCentre> shopCentres = this.shopCentreMapGroupByDistrictId.get(districtId);
//			if (shopCentres != null)
//				for (ShopCentre shopCentre : shopCentres)
//					shopCentreIds.add(shopCentre.getShopCentreId());
//
//		} else if (area != null) {
//			List<District> districts = this.districtMapGroupByArea.get(area);
//			if (districts != null)
//				for (District district : districts) {
//					List<ShopCentre> shopCentres = this.shopCentreMapGroupByDistrictId.get(district.getDistrictId());
//					if (shopCentres != null)
//						for (ShopCentre shopCentre : shopCentres)
//							shopCentreIds.add(shopCentre.getShopCentreId());
//				}
//
//		}
//
//		List<Shop> results = new ArrayList<Shop>();
//		Integer count = 0;
//
//		if (shopCentreIds.size() == 0 && shopCategoryTypeId == null) {
//			if (shopTypeId == null)
//				results = this.shops;
//			else
//				results = this.shopMapGroupByShopType.get(shopTypeId);
//
//		} else if (shopCentreIds.size() != 0 && shopCategoryTypeId == null) {
//			for (Integer shopCentreIdInList : shopCentreIds) {
//				List<Shop> shops = null;
//				if (shopTypeId == null)
//					shops = this.shopMapGroupByShopCentreId.get(shopCentreIdInList);
//				else
//					shops = this.shopMapGroupByShopCentreIdWithShopType.get(shopTypeId).get(shopCentreIdInList);
//				if (shops != null)
//					results.addAll(shops);
//			}
//
//		} else if (shopCentreIds.size() == 0 && shopCategoryTypeId != null) {
//			results = new ArrayList<Shop>();
//			List<ShopCategory> shopCategories = this.shopCategoryMapGroupByShopCategoryTypeId.get(shopCategoryTypeId);
//			if (shopCategories != null)
//				for (ShopCategory shopCategory : shopCategories) {
//					Shop shop = this.shopMapById.get(shopCategory.getShopId());
//					if (shop != null)
//						if (shopTypeId == null || this.getShopTypeIds(shop).contains(shopTypeId))
//							results.add(shop);
//				}
//
//		} else if (shopCentreIds.size() != 0 && shopCategoryTypeId != null) {
//			List<ShopCategory> shopCategories = this.shopCategoryMapGroupByShopCategoryTypeId.get(shopCategoryTypeId);
//			if (shopCategories != null)
//				for (ShopCategory shopCategory : shopCategories) {
//					Shop shop = this.shopMapById.get(shopCategory.getShopId());
//					if (shop != null && shopCentreIds.contains(shop.getShopCentreId()))
//						if (shopTypeId == null || this.getShopTypeIds(shop).contains(shopTypeId))
//							results.add(shop);
//				}
//
//		}
//
//		Collections.sort(results, new Comparator<Shop>() {
//			public int compare(Shop o1, Shop o2) {
//				return o1.getShopNameEn().compareTo(o2.getShopNameEn());
//			}
//		});
//
//		Set<String> shopNames = new HashSet<String>();
//		List<Shop> results2 = new ArrayList<Shop>();
//		for (Shop shop : results)
//			if (!shopNames.contains(shop.getShopName(language))) {
//				shopNames.add(shop.getShopName(language));
//				results2.add(shop);
//			}
//
//		List<Shop> shops = new ArrayList<Shop>();
//
//		for (int i = offset; i < offset + size && i < results2.size(); i++)
//			shops.add(results2.get(i));
//		count = results2.size();
//
//		return new Object[] { count, shops };
//
//	}
//
//	@Override
//	public List<Shop> getShopsWithSameShopName(Shop shop, String areaCode, Integer districtId, Integer shopCentreId, Integer shopCategoryTypeId) {
//		List<Shop> shops = this.shopMapGroupByShopName.get(shop.getShopId());
//
//		List<Shop> results = new ArrayList<Shop>();
//		for (Shop shopObj : shops) {
//			if (areaCode != null) {
//				ShopCentre shopCentre = this.shopCentreMapById.get(shopObj.getShopCentreId());
//				if (shopCentre == null)
//					continue;
//				District district = this.districtMapById.get(shopCentre.getDistrictId());
//				if (district == null)
//					continue;
//				Area area = this.areaMapByCode.get(district.getArea());
//				if (area == null)
//					continue;
//				if (!area.getAreaCode().equals(areaCode))
//					continue;
//			}
//
//			if (districtId != null) {
//				ShopCentre shopCentre = this.shopCentreMapById.get(shopObj.getShopCentreId());
//				if (shopCentre == null)
//					continue;
//				District district = this.districtMapById.get(shopCentre.getDistrictId());
//				if (district == null)
//					continue;
//				if (!district.getDistrictId().equals(districtId))
//					continue;
//			}
//
//			if (shopCentreId != null)
//				if (!shopObj.getShopCentreId().equals(shopCentreId))
//					continue;
//
//			if (shopCategoryTypeId != null) {
//				List<ShopCategory> shopCategories = this.shopCategoryMapGroupByShopId.get(shopObj.getShopId());
//				boolean found = false;
//				for (ShopCategory shopCategory : shopCategories)
//					if (shopCategory.getShopCategoryTypeId().equals(shopCategoryTypeId)) {
//						found = true;
//						break;
//					}
//				if (!found)
//					continue;
//			}
//
//			results.add(shopObj);
//		}
//
//		return results;
//	}
//
//	@Override
//	public List<Shop> getShops() {
//		return this.shops;
//	}
//
////	@Override
////	public List<Promotion> getPromotions() {
////		return this.promotions;
////	}
////
////	@Override
////	public List<Promotion> getPromotionsByPropCode(String propCode) {
////		return this.promotionMapByPropCode.get(propCode);
////	}
//
//	// @Override
//	// public List<ParkingPromotion> getParkingPromotionsByPropCode(String propCode) {
//	// return this.parkingPromotionMapByPropCode.get(propCode);
//	// }
//
////	@Override
////	public List<Promotion> getPromotionByAreaCodeNDistrictIdNShopCentreId(String areaCode, Integer districtId, Integer shopCentreId) {
////		if (areaCode == null && districtId == null && shopCentreId == null)
////			return this.promotions;
////
////		List<Integer> shopCentreIds = new ArrayList<Integer>();
////		if (areaCode != null) {
////			List<District> districts = this.districtMapGroupByArea.get(areaCode);
////			if (districts != null)
////				for (District district : districts) {
////					List<ShopCentre> shopCentres = this.shopCentreMapGroupByDistrictId.get(district.getDistrictId());
////					if (shopCentres != null)
////						for (ShopCentre shopCentre : shopCentres)
////							if (!shopCentreIds.contains(shopCentre.getShopCentreId()))
////								shopCentreIds.add(shopCentre.getShopCentreId());
////				}
////		}
////		if (districtId != null) {
////			District district = this.districtMapById.get(districtId);
////			if (district != null) {
////				List<ShopCentre> shopCentres = this.shopCentreMapGroupByDistrictId.get(districtId);
////				if (shopCentres != null)
////					for (ShopCentre shopCentre : shopCentres)
////						if (!shopCentreIds.contains(shopCentre.getShopCentreId()))
////							shopCentreIds.add(shopCentre.getShopCentreId());
////			}
////		}
////		if (shopCentreId != null)
////			if (!shopCentreIds.contains(shopCentreId))
////				shopCentreIds.add(shopCentreId);
////
////		List<Promotion> promotions = new ArrayList<Promotion>();
////		for (Integer shopCentreIdInList : shopCentreIds) {
////			ShopCentre shopCentre = this.shopCentreMapById.get(shopCentreIdInList);
////			if (shopCentre != null) {
////				List<Promotion> promotionsWithinShopCentre = this.promotionMapByPropCode.get(shopCentre.getPropCode());
////				if (promotionsWithinShopCentre != null)
////					for (Promotion promotion : promotionsWithinShopCentre) {
////						if (!promotions.contains(promotion))
////							promotions.add(promotion);
////					}
////			}
////		}
////
////		return promotions;
////	}
//
//	// @Override
//	// public List<ParkingPromotion> getParkingPromotionByAreaCodeNDistrictIdNShopCentreId(String areaCode, Integer districtId, Integer shopCentreId) {
//	// List<Integer> shopCentreIds = new ArrayList<Integer>();
//	// if (areaCode != null) {
//	// List<District> districts = this.districtMapGroupByArea.get(areaCode);
//	// if (districts != null)
//	// for (District district : districts) {
//	// List<ShopCentre> shopCentres = this.shopCentreMapGroupByDistrictId.get(district.getDistrictId());
//	// if (shopCentres != null)
//	// for (ShopCentre shopCentre : shopCentres)
//	// if (!shopCentreIds.contains(shopCentre.getShopCentreId()))
//	// shopCentreIds.add(shopCentre.getShopCentreId());
//	// }
//	// }
//	// if (districtId != null) {
//	// District district = this.districtMapById.get(districtId);
//	// if (district != null) {
//	// List<ShopCentre> shopCentres = this.shopCentreMapGroupByDistrictId.get(districtId);
//	// if (shopCentres != null)
//	// for (ShopCentre shopCentre : shopCentres)
//	// if (!shopCentreIds.contains(shopCentre.getShopCentreId()))
//	// shopCentreIds.add(shopCentre.getShopCentreId());
//	// }
//	// }
//	// if (shopCentreId != null)
//	// if (!shopCentreIds.contains(shopCentreId))
//	// shopCentreIds.add(shopCentreId);
//	//
//	// List<ParkingPromotion> parkingPromotions = new ArrayList<ParkingPromotion>();
//	// for (Integer shopCentreIdInList : shopCentreIds) {
//	// List<CarPark> carParks = this.carParkMapGroupByShopCentreId.get(shopCentreIdInList);
//	// if (carParks != null)
//	// for (CarPark carPark : carParks) {
//	// List<ParkingPromotion> parkingPromotionsWithinCarPark = this.parkingPromotionMapByPropCode.get(carPark.getPropCode());
//	// if (parkingPromotionsWithinCarPark != null)
//	// for (ParkingPromotion parkingPromotion : parkingPromotionsWithinCarPark) {
//	// if (!parkingPromotions.contains(parkingPromotion))
//	// parkingPromotions.add(parkingPromotion);
//	// }
//	// }
//	//
//	// }
//	//
//	// return parkingPromotions;
//	// }
//
////	public AboutUs getAboutUs() {
////		return this.aboutUs;
////	}
//
//	public List<Shop> getShopByShopCentreId(Integer shopCentreId) {
//		return this.shopMapGroupByShopCentreId.get(shopCentreId);
//	}
//
////	public Promotion getPromotionByFilenameMd5(String filenameMd5) {
////		return this.promotionMapByFilenameMd5.get(filenameMd5);
////	}
////
////	public Promotion getPromotionByFilename(String filename) {
////		return this.promotionMapByFilename.get(filename);
////	}
//
//	public List<ShopCentre> getShopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight(Integer shopCentreFeatureTypeId) {
//		return this.shopCentreMapGroupByShopCentreFeatureTypeIdWithSpotlight.get(shopCentreFeatureTypeId);
//	}
//
//	public List<ShopCentreFeatureType> getShopCentreFeatureTypes() {
//		return this.shopCentreFeatureTypes;
//	}
//
//	public List<Shop> getShopsGroupByNameEn() {
//		List<Shop> shops = new ArrayList<Shop>();
//
//		for (Shop shop : this.shops) {
//			boolean exist = false;
//			for (Shop shop2 : shops) {
//				if (shop2.getShopNameEn().equals(shop.getShopNameEn())) {
//					exist = true;
//					break;
//				}
//			}
//
//			if (!exist)
//				shops.add(shop);
//		}
//
//		Collections.sort(shops, new Comparator<Shop>() {
//
//			public int compare(Shop o1, Shop o2) {
//				return o1.getShopNameEn().compareTo(o2.getShopNameEn());
//			}
//
//		});
//
//		return shops;
//	}
//
//	@Override
//	public List<ShopCentre> getSortedShopCentres() {
//		List<ShopCentre> shopCentres = new ArrayList<ShopCentre>();
//		shopCentres.addAll(this.shopCentres);
//
//		Collections.sort(shopCentres, new Comparator<ShopCentre>() {
//
//			public int compare(ShopCentre o1, ShopCentre o2) {
//				return o1.getShopCentreNameEn().compareTo(o2.getShopCentreNameEn());
//			}
//
//		});
//
//		return shopCentres;
//	}
//
//	public List<ShopCentre> getShopCentresByPropCode(List<String> propCodes) {
//		List<ShopCentre> shopCentres = new ArrayList<ShopCentre>();
//
//		if (propCodes != null)
//			for (String propCode : propCodes) {
//				ShopCentre shopCentre = this.shopCentreMapByPropCode.get(propCode);
//				if (shopCentre != null)
//					shopCentres.add(shopCentre);
//			}
//
//		return shopCentres;
//	}
//
//	public ShopCentre getShopCentreByPropCode(String propCode) {
//		return this.shopCentreMapByPropCode.get(propCode);
//	}
//
//	public Shop getShopByShopInfo(List<Object> shopKey) {
//		return this.shopMapByShopInfo.get(shopKey);
//	}
//
//	public List<ShopCategory> getShopCategoryGroupByShopId(Integer shopId) {
//		return this.shopCategoryMapGroupByShopId.get(shopId);
//	}
//
//	public ShopCategoryType getShopCategoryTypeById(Integer shopCategoryTypeId) {
//		return this.shopCategoryTypeMapById.get(shopCategoryTypeId);
//	}
//
//	public ShopEx getShopExByShopCentreIdNBuildingEnNShopNo(Integer shopCentreId, String buildingEn, String shopNo) {
//		return this.shopExMapByShopInfo.get(Arrays.asList(shopCentreId, buildingEn, shopNo));
//	}
//
//	public String getCustomColumnDisplayName(String tableName, String customColumn) {
//		if (this.customColumnDisplayConfigMap.get(tableName) != null)
//			return this.customColumnDisplayConfigMap.get(tableName).get(customColumn);
//		return null;
//	}
//
//	public String getShopExCustomColumnDisplayName(Integer fieldNo, String language) {
//		String fieldName = fieldNo.toString();
//		if (fieldName.length() == 1)
//			fieldName = "0" + fieldName;
//		return this.getCustomColumnDisplayName("ShopExTable", "field" + fieldName + "_" + language);
//	}
//
//	public String getTbCarParkFacilityCustomColumnDisplayName(Integer fieldNo, String language) {
//		String fieldName = fieldNo.toString();
//		if (fieldName.length() == 1)
//			fieldName = "0" + fieldName;
//		return this.getCustomColumnDisplayName("tb_CarParkFacility", "field" + fieldName + "_" + language);
//	}
//
//	public List<CarParkFacility> getTbCarParkFacilitiesByShopCentrePropCode(String propCode) {
//		return this.tbCarParkFacilityMapGroupByShopCentrePropCode.get(propCode);
//	}
//
//	public Long countShopCentre() {
//		return this.shopCentreRepository.count();
//	}
//
////	public Integer countPromotion() {
////		List<Promotion> promotions = this.promotionDao.findActiveByDeliveryChannelsIn("MobileApp");
////		return promotions.size();
////	}
//
//}
