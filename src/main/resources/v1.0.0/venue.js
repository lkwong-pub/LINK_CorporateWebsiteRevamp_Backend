/**

 @api {get} http://www-dev.linkhk.com/linkweb/api/salesVenue/      Sales Venues Landing

 @apiName Sales Venues Landing
 @apiGroup Batch3_2-Venue
 @apiVersion 1.0.0

 @apiDescription
 Retrieve Sales Venue Landing Page content<br/>
 Data included 3 components content:<br/>
 1. SEO Details<br/>
 2. Banner Details<br/>
 3. Sales Venues Information<br/>

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.banner		Venues - Banner Object
 @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
 @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
 @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
 @apiSuccess (Success 200) {Object}	    data.introduction 	Venues - Description
 @apiSuccess (Success 200) {String}	    data.introduction.introductionTitleEn		Introduction Tag Title (EN)
 @apiSuccess (Success 200) {String}	    data.introduction.introductionTitleTc		Introduction Tag Title (TC)
 @apiSuccess (Success 200) {String}	    data.introduction.introductionTitleSc		Introduction Tag Title (SC)
 @apiSuccess (Success 200) {String}	    data.introduction.registrationAsClientsTitleEn		Registration Tag Title (EN)
 @apiSuccess (Success 200) {String}	    data.introduction.registrationAsClientsTitleTc		Registration Tag Title (TC)
 @apiSuccess (Success 200) {String}	    data.introduction.registrationAsClientsTitleSc		Registration Tag Title (SC)
 @apiSuccess (Success 200) {String}	    data.introduction.applicationProcedureTitleEn		Application Procedure Tag Title (EN)
 @apiSuccess (Success 200) {String}	    data.introduction.applicationProcedureTitleTc		Application Procedure Tag Title (TC)
 @apiSuccess (Success 200) {String}	    data.introduction.applicationProcedureTitleSc		Application Procedure Tag Title (SC)
 @apiSuccess (Success 200) {String}	    data.introduction.introductionEn		Introduction Tag HTML (EN)
 @apiSuccess (Success 200) {String}	    data.introduction.introductionTc		Introduction Tag HTML (TC)
 @apiSuccess (Success 200) {String}	    data.introduction.introductionSc		Introduction Tag HTML (SC)
 @apiSuccess (Success 200) {String}	    data.introduction.registrationAsClientsEn		Registration Tag HTML (EN)
 @apiSuccess (Success 200) {String}	    data.introduction.registrationAsClientsTc		Registration Tag HTML (TC)
 @apiSuccess (Success 200) {String}	    data.introduction.registrationAsClientsSc		Registration Tag HTML (SC)
 @apiSuccess (Success 200) {String}	    data.introduction.applicationProcedureEn		Application Procedure Tag HTML (EN)
 @apiSuccess (Success 200) {String}	    data.introduction.applicationProcedureTc		Application Procedure Tag HTML (TC)
 @apiSuccess (Success 200) {String}	    data.introduction.applicationProcedureSc		Application Procedure Tag HTML (SC)


 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::VENUE_LANDING_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Sale Venues Listing",
      "seoTitleTc":"Sale Venues Listing",
      "seoTitleSc":"Sale Venues Listing",
      "seoKeywordEn":"Sale Venues Listing",
      "seoKeywordTc":"Sale Venues Listing",
      "seoKeywordSc":"Sale Venues Listing",
      "seoDescriptionEn":"Sale Venues Listing",
      "seoDescriptionTc":"Sale Venues Listing",
      "seoDescriptionSc":"Sale Venues Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "introduction":{
         "introductionTitleEn":"",
         "introductionTitleTc":"",
         "introductionTitleSc":"",
         "registrationAsClientsTitleEn":"",
         "registrationAsClientsTitleTc":"",
         "registrationAsClientsTitleSc":"",
         "applicationProcedureTitleEn":"",
         "applicationProcedureTitleTc":"",
         "applicationProcedureTitleSc":"",
         "introductionEn":"",
         "introductionTc":"",
         "introductionSc":"",
         "registrationAsClientsEn":"",
         "registrationAsClientsTc":"",
         "registrationAsClientsSc":"",
         "applicationProcedureEn":"",
         "applicationProcedureTc":"",
         "applicationProcedureSc":""
      }
   }
}
 ::VENUE_LANDING_API_END::
 */

/**

     @api {get} http://www-dev.linkhk.com/linkweb/api/venue/      Venue Listing

     @apiName Venue Listing
     @apiGroup Batch3_2-Venue
     @apiVersion 1.0.0

	  @apiDescription
		Retrieve Venue List Page content<br/>
		Data included 5 components content:<br/>
		1. SEO Details<br/>
		2. Banner Details<br/>
		3. Area Details<br/>
        4. District Details<br/>
        5. Venue Details<br/>

        @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
        @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
        @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
        @apiSuccess (Success 200) {String} 	data		Data Object.
        @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
        @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
        @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
        @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
        @apiSuccess (Success 200) {Object} 	data.banner		Venues - Banner Object
        @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
        @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
        @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
        @apiSuccess (Success 200) {Object[]}	    data.areaInfo		Venues - Area Info
        @apiSuccess (Success 200) {String}	    data.areaInfo.areaCode      Area Code
        @apiSuccess (Success 200) {String}	    data.areaInfo.areaNameEn    Area Name (EN)
        @apiSuccess (Success 200) {String}	    data.areaInfo.areaNameTc    Area Name (TC)
        @apiSuccess (Success 200) {String}	    data.areaInfo.areaNameSc    Area Name (SC)
        @apiSuccess (Success 200) {Object[]}	    data.districtInfo		Venues - District Info
        @apiSuccess (Success 200) {Integer} 	data.districtInfo.districtId		District ID
        @apiSuccess (Success 200) {String} 	data.districtInfo.areaCode		District Area Code
        @apiSuccess (Success 200) {String} 	data.districtInfo.districtNameEn		District Name (EN)
        @apiSuccess (Success 200) {String} 	data.districtInfo.districtNameTc		District Name (TC)
        @apiSuccess (Success 200) {String} 	data.districtInfo.districtNameSc		District Name (SC)
        @apiSuccess (Success 200) {Object[]}	    data.list    Venues - Venues Info Object Array
        @apiSuccess (Success 200) {Integer}	    data.list.id    Venues ID
        @apiSuccess (Success 200) {String}	    data.list.areaCode   Venues AreaCode
        @apiSuccess (Success 200) {Integer}	    data.list.districtId Venues District ID
        @apiSuccess (Success 200) {Integer}	    data.list.shopCentreId Venues Shop Centre ID
        @apiSuccess (Success 200) {Integer}	    data.list.propCode Venues Shop Centre Prop Code
        @apiSuccess (Success 200) {String}	    data.list.letter    Letter of the alphabet
        @apiSuccess (Success 200) {String}	    data.list.shopCentreNameEn    Shop Centre Name (EN)
        @apiSuccess (Success 200) {String}	    data.list.shopCentreNameTc    Shop Centre Name (TC)
        @apiSuccess (Success 200) {String}	    data.list.shopCentreNameSc    Shop Centre Name (SC)
        @apiSuccess (Success 200) {String}	    data.remarkEn  Venue Remarks (EN)
        @apiSuccess (Success 200) {String}	    data.remarkTc  Venue Remarks (TC)
        @apiSuccess (Success 200) {String}	    data.remarkSc  Venue Remarks (SC)
 
      	@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::VENUE_LIST_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Venues Listing",
      "seoTitleTc":"Venues Listing",
      "seoTitleSc":"Venues Listing",
      "seoKeywordEn":"Venues Listing",
      "seoKeywordTc":"Venues Listing",
      "seoKeywordSc":"Venues Listing",
      "seoDescriptionEn":"Venues Listing",
      "seoDescriptionTc":"Venues Listing",
      "seoDescriptionSc":"Venues Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "areaInfo":[
         {
            "areaCode":"",
            "areaNameEn":"",
            "areaNameTc":"",
            "areaNameSc":""
         }
      ],
      "districtInfo":[
         {
            "districtId":1,
            "areaCode":"NT",
            "districtNameEn":"Tai Po",
            "districtNameTc":"大埔",
            "districtNameSc":"大埔"
         }
      ],
      "list":[
         {
            "id":1,
            "areaCode":"NT",
            "districtId":1,
            "shopCentreId": 1,
            "propCode" : "wttxc2",
            "letter": "a",
            "shopCentreNameEn":"",
            "shopCentreNameTc":"",
            "shopCentreNameSc":""
         }
      ],
      "remarkEn":"",
      "remarkTc":"",
      "remarkSc":""
   }
}
::VENUE_LIST_API_END::
*/

/**

 @api {get} http://www-dev.linkhk.com/linkweb/api/venue/{propCode}      Shop Centre Details

 @apiName Shop Centre Details
 @apiGroup Batch3_2-Venue
 @apiVersion 1.0.0

 @apiDescription
 Retrieve Promotion Details Page content<br/>
 Data included 4 components content:<br/>
 1. SEO Details<br/>
 2. Shop Centre Info<br/>
 3. Venues Label<br/>
 4. Venues Details<br/>

 @apiParam {String}	propCode		Shop Centre Prop Code

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.shopCentreInfo		Venues - Shop Centre Info
 @apiSuccess (Success 200) {Integer} 	data.shopCentreInfo.shopCentreId    Shop Centre ID
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.headlineEn	Section Headline (EN)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.headlineTc	Section Headline (TC)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.headlineSc	Section Headline (SC)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.shopCentreNameEn	Shop Centre Namme (EN)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.shopCentreNameTc	Shop Centre Namme (TC)
 @apiSuccess (Success 200) {String} 	data.shopCentreInfo.shopCentreNameSc	Shop Centre Namme (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressLabelEn	Address Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressLabelTc	Address Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressLabelSc	Address Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressEn		Address (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressTc		Address (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.addressSc		Address (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLineLabelEn	Customer Hotline Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLineLabelTc	Customer Hotline Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLineLabelSc	Customer Hotline Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.customerHotLine		Customer Hotline
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiryLabelEn		Carpark Enquiry Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiryLabelTc		Carpark Enquiry Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiryLabelSc		Carpark Enquiry Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.carParkEnquiry		Carpark Enquiry
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiryLabelEn		Leasing Enquiry Label (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiryLabelTc		Leasing Enquiry Label (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiryLabelSc		Leasing Enquiry Label (SC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.leasingEnquiry	Leasing Enquiry
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.viewLocationThumbnailUrlEn View Location Icon (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.viewLocationThumbnailUrlTc View Location Icon (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.viewLocationThumbnailUrlSc View Location Icon (SC)
 @apiSuccess (Success 200) {Object[]}	data.shopCentreInfo.mallThumbnails	Shop Centre Thumbnail Object Array
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnail	Shop Centre Thumbnail URL
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnailAltTextEn		Shop Centre Thumbnail Alt Text (EN)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnailAltTextTc		Shop Centre Thumbnail Alt Text (TC)
 @apiSuccess (Success 200) {String}	    data.shopCentreInfo.mallThumbnails.thumbnailAltTextSc		Shop Centre Thumbnail Alt Text (SC)
 @apiSuccess (Success 200) {Object}	    data.label       Venues - Details Label
 @apiSuccess (Success 200) {String}	    data.label.locationLabelEn     Location Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.locationLabelTc     Location Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.locationLabelSc     Location Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.sizeLabelEn     Size Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.sizeLabelTc     Size Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.sizeLabelSc     Size Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.dailyChargeLabelEn     Daily Charge Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.dailyChargeLabelTc     Daily Charge Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.dailyChargeLabelSc     Daily Charge Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.remarksLabelEn     Remarks Label (EN)
 @apiSuccess (Success 200) {String}	    data.label.remarksLabelTc     Remarks Label (TC)
 @apiSuccess (Success 200) {String}	    data.label.remarksLabelSc     Remarks Label (SC)
 @apiSuccess (Success 200) {String}	    data.label.floorPlanIconEn     Floor Plan Icon URL (EN)
 @apiSuccess (Success 200) {String}	    data.label.floorPlanIconTc     Floor Plan Icon URL (TC)
 @apiSuccess (Success 200) {String}	    data.label.floorPlanIconSc     Floor Plan Icon URL (SC)
 @apiSuccess (Success 200) {Object[]}	data.list       Venues - Details List
 @apiSuccess (Success 200) {String}	    data.list.photo1     Photo 1
 @apiSuccess (Success 200) {String}	    data.list.photo1AltTextEn     Photo 1 Alt Text (EN)
 @apiSuccess (Success 200) {String}	    data.list.photo1AltTextTc     Photo 1 Alt Text (TC)
 @apiSuccess (Success 200) {String}	    data.list.photo1AltTextSc     Photo 1 Alt Text (SC)
 @apiSuccess (Success 200) {String}	    data.list.photo2     Photo 2 (EN)
 @apiSuccess (Success 200) {String}	    data.list.photo2AltTextEn     Photo 2 Alt Text (EN)
 @apiSuccess (Success 200) {String}	    data.list.photo2AltTextTc     Photo 2 Alt Text (TC)
 @apiSuccess (Success 200) {String}	    data.list.photo2AltTextSc     Photo 2 Alt Text (SC)
 @apiSuccess (Success 200) {String}	    data.list.locationEn     Location (EN)
 @apiSuccess (Success 200) {String}	    data.list.locationTc     Location (TC)
 @apiSuccess (Success 200) {String}	    data.list.locationSc     Location (SC)
 @apiSuccess (Success 200) {String}	    data.list.sizeEn     Size (EN)
 @apiSuccess (Success 200) {String}	    data.list.sizeTc     Size (TC)
 @apiSuccess (Success 200) {String}	    data.list.sizeSc     Size (SC)
 @apiSuccess (Success 200) {String}	    data.list.dailyChargeEn     Daily Charge (EN)
 @apiSuccess (Success 200) {String}	    data.list.dailyChargeTc     Daily Charge (TC)
 @apiSuccess (Success 200) {String}	    data.list.dailyChargeSc     Daily Charge (SC)
 @apiSuccess (Success 200) {String}	    data.list.remarksEn     Remarks (EN)
 @apiSuccess (Success 200) {String}	    data.list.remarksTc     Remarks (TC)
 @apiSuccess (Success 200) {String}	    data.list.remarksSc     Remarks (SC)
 @apiSuccess (Success 200) {String}	    data.list.floorPlanPath     Floor Plan Image URL


 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::VENUE_DETAILS_API_START::
 {
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{
      "id":1,
      "seoTitleEn":"Venues Details",
      "seoTitleTc":"Venues Details",
      "seoTitleSc":"Venues Details",
      "seoKeywordEn":"Venues Details",
      "seoKeywordTc":"Venues Details",
      "seoKeywordSc":"Venues Details",
      "seoDescriptionEn":"Venues Details",
      "seoDescriptionTc":"Venues Details",
      "seoDescriptionSc":"Venues Details",
      "shopCentreInfo":{
         "shopCentreId":7,
         "headlineEn":"Leasing",
         "headlineTc":"Leasing (TC)",
         "headlineSc":"Leasing (SC)",
         "addressLabelEn":"Address",
         "addressLabelTc":"地址",
         "addressLabelSc":"地址",
         "addressEn":"Lok Fu Place 198 Junction Road Wang Tau Hom,  Kowloon ",
         "addressTc":"九龍橫頭磡聯合道198號樂富廣場",
         "addressSc":"九龙横头磡联合道198号乐富广场",
         "customerHotLineLabelEn":"Customer Hotline",
         "customerHotLineLabelTc":"客戶服務熱線",
         "customerHotLineLabelSc":"客户服务热线",
         "customerHotLine":"2338 7781",
         "carParkEnquiryLabelEn":"Car Park Enquiry",
         "carParkEnquiryLabelTc":"停車場查詢",
         "carParkEnquiryLabelSc":"停车场查询",
         "carParkEnquiry":"2626 7777",
         "leasingEnquiryLabelEn":"Leasing Enquiry",
         "leasingEnquiryLabelTc":"租務查詢",
         "leasingEnquiryLabelSc":"租务查询",
         "leasingEnquiry":"2175 1747 ",
         "viewLocationThumbnailUrlEn":"/shopDetailViewLocationEn.png",
         "viewLocationThumbnailUrlTc":"/shopDetailViewLocationTc.png",
         "viewLocationThumbnailUrlSc":"/shopDetailViewLocationSc.png",
         "mallThumbnails":[
            {
               "thumbnail":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
               "thumbnailAltTextEn":"Outlook",
               "thumbnailAltTextTc":"外觀",
               "thumbnailAltTextSc":"外观"
            },
            {
               "thumbnail":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/LF_after_5night.jpg",
               "thumbnailAltTextEn":"Podium Food Square",
               "thumbnailAltTextTc":"美食坊",
               "thumbnailAltTextSc":"美食坊"
            },
            {
               "thumbnail":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/LF_after_6.jpg",
               "thumbnailAltTextEn":"Atrium",
               "thumbnailAltTextTc":"中庭",
               "thumbnailAltTextSc":"中庭"
            }
         ]
      },
      "label":{
         "locationLabelEn":"Location",
         "locationLabelTc":"Location",
         "locationLabelSc":"Location",
         "sizeLabelEn":"Size",
         "sizeLabelTc":"Size",
         "sizeLabelSc":"Size",
         "dailyChargeLabelEn":"Trade Nature",
         "dailyChargeLabelTc":"Trade Nature",
         "dailyChargeLabelSc":"Trade Nature",
         "remarksLabelEn":"Remarks",
         "remarksLabelTc":"Remarks",
         "remarksLabelSc":"Remarks",
         "floorPlanIconEn":"http://domain.com/floorPlanIcon.png",
         "floorPlanIconTc":"http://domain.com/floorPlanIcon.png",
         "floorPlanIconSc":"http://domain.com/floorPlanIcon.png"
      },
      "list":[
         {
            "photo1" : "",
            "photo1AltTextEn" : "",
            "photo1AltTextTc" : "",
            "photo1AltTextSc" : "",
            "photo2" : "",
            "photo2AltTextEn" : "",
            "photo2AltTextTc" : "",
            "photo2AltTextSc" : "",
            "locationEn":"",
            "locationTc":"",
            "locationSc":"",
            "sizeEn":"",
            "sizeTc":"",
            "sizeSc":"",
            "dailyChargeEn":"",
            "dailyChargeTc":"",
            "dailyChargeSc":"",
            "remarksEn":"",
            "remarksTc":"",
            "remarksSc":"",
            "floorPlanPath":""
         }
      ]
   }
}
 ::VENUE_DETAILS_API_END::
 */
