package com.gt.projects.thelinkreit.config;

public final class Url {

	public static final String ROOT_PAGE = "/";

	public static final String V1 = "/v1";

	/******************** API ********************/

	public static final String API_PREFIX = "/api";

	public static final String API_TEST_PREFIX = "/test";
	public static final String API_TEST_PUBLISHNOW_PREFIX = "/publishNow";
	public static final String API_TEST_HEALTHCHECK_PREFIX = "/healthCheck";
	public static final String API_TEST_DEPLOYTEST_PREFIX = "/deployTest";
    public static final String API_FILE_PREFIX = "/file";
    public static final String API_IMAGECHECK_PREFIX = "/checkImage";

	public static final String API_HOMEPAGE_PREFIX = "/homepage";
	public static final String API_SEARCHCRITERIA_PREFIX = "/searchCriteria";
	public static final String API_SHOP_PREFIX = "/shop";
	public static final String API_VISITUS_PREFIX = "/visitUs";
	public static final String API_SHOPCENTRE_PREFIX = "/shopCentre";
	public static final String API_CONFIG_PREFIX = "/config/";
	public static final String API_PROMOTION_PREFIX = "/promotion";
	public static final String API_EVENTGALLERY_PREFIX = "/eventGallery";
	public static final String API_READINGCORNER_PREFIX = "/readingCorner";
    public static final String API_PARKING_PREFIX = "/parking";
    public static final String API_PARKINGPERKS_PREFIX = "/parkingPerks";
    public static final String API_LEASINGINFO_PREFIX = "/leasingInfo";
    public static final String API_KIOSK_PREFIX = "/kiosk";
    public static final String API_SALESKIOSK_PREFIX = "/salesKiosk";
    public static final String API_VENUE_PREFIX = "/venue";
    public static final String API_SALESVENUE_PREFIX = "/salesVenue";
    public static final String API_TENANTEVENT_PREFIX = "/tenantEvent";
    public static final String API_TENANTACADEMY_PREFIX = "/tenantAcademy";
    public static final String API_TENANTEVENTGALLERY_PREFIX = "/tenantEventGallery";
    public static final String API_HAPPYVISACARD_PREFIX = "/happyVisaCard";


	public static final String API_ASSET_PREFIX = "/asset";
	public static final String API_ASSET_IMAGE_PAGE = "/image/{id}";
	public static final String API_ASSET_DEFAULTIMAGE_PAGE = "/image/defaultImage/**";
	public static final String API_ASSET_PROMOTIONIMAGE_PAGE = "/image/promotion/**";
	public static final String API_ASSET_SHOPIMAGE_PAGE = "/image/shopPhoto/{type}/{md5}/**";
	public static final String API_ASSET_FLOORPLANIMAGE_PAGE = "/image/floorPlan/**";
	public static final String API_ASSET_SHOPCENTREIMAGE_PAGE = "/image/shopCentrePhoto/**";
	public static final String API_ASSET_SHOPCENTRECARPARKIMAGE_PAGE = "/image/carPark/**";
	public static final String API_ASSET_SHOPCENTRESERVICEIMAGE_PAGE = "/image/shopCentreService/**";
	public static final String API_ASSET_SHOPCATEGORYIMAGE_PAGE = "/image/shopCategory/**";
	public static final String API_ASSET_CARPARKSERVICEIMAGE_PAGE = "/image/carParkService/_layouts/LinkCustomerSite/neu/images/**";
	public static final String API_ASSET_SHOPPROMTIONIMAGE_PAGE = "/image/shopPromotion/{id}";
	public static final String API_ASSET_QUEUINGPROMTIONIMAGE_PAGE = "/image/queuingPromotion/{id}";
	public static final String API_ASSET_POPUPPROMTIONIMAGE_PAGE = "/image/popupPromotion/{id}";
	public static final String API_ASSET_FLOATINGPROMTIONIMAGE_PAGE = "/image/floatingPromotion/{id}";
	public static final String API_ASSET_QUEUINGMERCHANTIMAGE_PAGE = "/image/queuingMerchant/**";
	public static final String API_ASSET_SHOPPARTNERIMAGE_PAGE = "/image/shopPartner/{partner}/**";
	public static final String API_ASSET_QRCODEIMAGE_PAGE = "/image/qrCode";

	/******************** API ASSET ********************/

	public static final String API_ASSET_DEFAULTIMAGE_PREFIX = "/defaultImage";
	public static final String API_ASSET_PROMOTIONIMAGE_PREFIX = "/promotion";
	public static final String API_ASSET_SHOPIMAGE_PREFIX = "/shopPhoto";
	public static final String API_ASSET_FLOORPLANIMAGE_PREFIX = "/floorPlan";
	public static final String API_ASSET_SHOPCENTREIMAGE_PREFIX = "/shopCentrePhoto";
	public static final String API_ASSET_SHOPCENTRECARPARKIMAGE_PREFIX = "/carPark";
	public static final String API_ASSET_SHOPCENTRESERVICEIMAGE_PREFIX = "/shopCentreService";
	public static final String API_ASSET_SHOPCATEGORYIMAGE_PREFIX = "/shopCategory";
	public static final String API_ASSET_CARPARKSERVICEIMAGE_PREFIX = "/carParkService";
	public static final String API_ASSET_SHOPPROMTIONIMAGE_PREFIX = "/shopPromotion";
	public static final String API_ASSET_QUEUINGPROMTIONIMAGE_PREFIX = "/queuingPromotion";
	public static final String API_ASSET_POPUPPROMTIONIMAGE_PREFIX = "/popupPromotion";
	public static final String API_ASSET_FLOATINGPROMTIONIMAGE_PREFIX = "/floatingPromotion";
	public static final String API_ASSET_QUEUINGMERCHANTIMAGE_PREFIX = "/queuingMerchant";
	public static final String API_ASSET_QRCODEIMAGE_PREFIX = "/qrCode";
	

	
	
}