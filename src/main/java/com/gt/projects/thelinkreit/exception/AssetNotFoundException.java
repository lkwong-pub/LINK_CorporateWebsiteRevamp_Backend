package com.gt.projects.thelinkreit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public final class AssetNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8765751217513881496L;

}
