//package com.gt.projects.thelinkreit.config.interceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import com.gt.projects.thelinkreit.service.SystemService;
//
//@Configuration
//public class PresetAttributeInterceptor extends HandlerInterceptorAdapter {
//
//	@Autowired
//	private SystemService systemService;
//
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//
//		request.setAttribute("_appPath", this.systemService.getProperty("appPath"));
//		request.setAttribute("_resPath", this.systemService.getProperty("resPath"));
//
//		return true;
//	}
//}
