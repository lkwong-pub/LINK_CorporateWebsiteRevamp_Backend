/**
	 *
	 * @api {get} http://www-dev.linkhk.com/linkweb/api/happyVisaCard/      Happy Visa Card
	 *
	 * @apiName Happy Visa Card
	 * @apiGroup Batch3.2-HappyVisaCard
	 * @apiVersion 1.0.0
	 *
	  @apiDescription
		Retrieve Happy Visa Card Page content<br/>
		Data included 3 components content:<br/>
		1. SEO Details<br/>
		2. Banner Details<br/>
		3. Happy Visa Card Details<br/>

        @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
        @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
        @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
        @apiSuccess (Success 200) {String} 	data		Data Object.
        @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
        @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
        @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
        @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
        @apiSuccess (Success 200) {Object} 	data.banner		Happy Visa Card - Banner Object
        @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
        @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
        @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
        @apiSuccess (Success 200) {Object}	data.description	Happy Visa Card - Description Object
        @apiSuccess (Success 200) {String}	data.description.introduction1En	Happy Visa Card Introduction 1 (EN)
        @apiSuccess (Success 200) {String}	data.description.introduction1Tc	Happy Visa Card Introduction 1 (TC)
        @apiSuccess (Success 200) {String}	data.description.introduction1Sc	Happy Visa Card Introduction 1 (SC)
        @apiSuccess (Success 200) {String}	data.description.introduction2En	Happy Visa Card Introduction 2 (EN)
        @apiSuccess (Success 200) {String}	data.description.introduction2Tc	Happy Visa Card Introduction 2 (TC)
        @apiSuccess (Success 200) {String}	data.description.introduction2Sc	Happy Visa Card Introduction 2 (SC)
        @apiSuccess (Success 200) {String}	data.description.description1En	Happy Visa Card Description 1 (EN)
        @apiSuccess (Success 200) {String}	data.description.description1Tc	Happy Visa Card Description 1 (TC)
        @apiSuccess (Success 200) {String}	data.description.description1Sc	Happy Visa Card Description 1 (SC)
        @apiSuccess (Success 200) {String}	data.description.description2En	Happy Visa Card Description 2 (EN)
        @apiSuccess (Success 200) {String}	data.description.description2Tc	Happy Visa Card Description 2 (TC)
        @apiSuccess (Success 200) {String}	data.description.description2Sc	Happy Visa Card Description 2 (SC)
        @apiSuccess (Success 200) {String}	data.description.description3En	Happy Visa Card Description 3 (EN)
        @apiSuccess (Success 200) {String}	data.description.description3Tc	Happy Visa Card Description 3 (TC)
        @apiSuccess (Success 200) {String}	data.description.description3Sc	Happy Visa Card Description 3 (SC)


      	@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::HAPPY_VISA_CARD_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Happy Visa Card Listing",
      "seoTitleTc":"Happy Visa Card Listing",
      "seoTitleSc":"Happy Visa Card Listing",
      "seoKeywordEn":"Happy Visa Card Listing",
      "seoKeywordTc":"Happy Visa Card Listing",
      "seoKeywordSc":"Happy Visa Card Listing",
      "seoDescriptionEn":"Happy Visa Card Listing",
      "seoDescriptionTc":"Happy Visa Card Listing",
      "seoDescriptionSc":"Happy Visa Card Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "description":{
         "introduction1En":"introduction 1 En",
         "introduction1Tc":"introduction 1 Tc",
         "introduction1Sc":"introduction 1 Sc",
         "introduction2En":"introduction 2 En",
         "introduction2Tc":"introduction 2 Tc",
         "introduction2Sc":"introduction 2 Sc",
         "description1En":"description1En",
         "description1Tc":"description1Tc",
         "description1Sc":"description1Sc",
         "description2En":"description2En",
         "description2Tc":"description2Tc",
         "description2Sc":"description2Sc",
         "description3En":"description3En",
         "description3Tc":"description3Tc",
         "description3Sc":"description3Sc"
      }
   }
}
::HAPPY_VISA_CARD_API_END::
*/


