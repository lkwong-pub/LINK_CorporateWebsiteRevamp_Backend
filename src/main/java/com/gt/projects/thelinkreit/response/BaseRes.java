package com.gt.projects.thelinkreit.response;

import lombok.Data;

@Data
public class BaseRes<T> {
	private Integer error;
	private String error_message;
	private String error_code;
	private T data;
}
