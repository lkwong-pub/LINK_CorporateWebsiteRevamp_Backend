/**
	 
	 @api {get} http://www-dev.linkhk.com/linkweb/api/readingCorner/      Reading Corner Listing
	 
	 @apiName Reading Corner List
	 @apiGroup Batch3_1-ReadingCorner
	 @apiVersion 1.0.0
	 
	 @apiDescription 
	 Retrieve Reading Corner Listing Page content<br/> 
	 Data included 4 components content:<br/> 
	 1. SEO Details<br/> 
	 2. Banner<br/>
	 3. ReadingCorner List<br/> 
	 4. Paging Info<br/>
	 @apiParam {Integer}	pageNo		Page No ( start = 0 )
	 @apiParam {Integer}	pageSize	Number of list record per request
	 
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
		@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
		@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
		@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
		@apiSuccess (Success 200) {Object} 	data.banner		ReadingCorner List - Banner Object
		@apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
		@apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
		@apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
		@apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
		@apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
		@apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
		@apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
		@apiSuccess (Success 200) {Object[]} 	data.readingCornerList		ReadingCorner List - ReadingCorner List Object Array
		@apiSuccess (Success 200) {Integer} 	data.readingCornerList.readingCornerId	ReadingCorner ID
        @apiSuccess (Success 200) {String} 	data.readingCornerList.titleEn		ReadingCorner Name (EN)
        @apiSuccess (Success 200) {String} 	data.readingCornerList.titleTc		ReadingCorner Name (TC)
        @apiSuccess (Success 200) {String} 	data.readingCornerList.titleSc		ReadingCorner Name (SC)
        @apiSuccess (Success 200) {String} 	data.readingCornerList.thumbnailEn	ReadingCorner Thumbnail (EN)
        @apiSuccess (Success 200) {String} 	data.readingCornerList.thumbnailTc	ReadingCorner Thumbnail (TC)
        @apiSuccess (Success 200) {String} 	data.readingCornerList.thumbnailSc	ReadingCorner Thumbnail (SC)
        @apiSuccess (Success 200) {String} 	data.readingCornerList.downloadIconUrl	Download Icon URL
        @apiSuccess (Success 200) {String} 	data.readingCornerList.downloadUrl		Download Icon Redirect URL
        @apiSuccess (Success 200) {Object} 	data.pageInfo	ReadingCorner List - Page Info Object
		@apiSuccess (Success 200) {Integer} 	data.pageInfo.total		No. of Total Record
	    @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageCount		No. of Page Count
	    @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageSize		No. per page
	    @apiSuccess (Success 200) {Integer} 	data.pageInfo.curPage		Current Page No
		@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::READING_CORNER_LIST_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "seoTitleEn":"Reading Corner",
      "seoTitleTc":"閱讀角",
      "seoTitleSc":"阅读角",
      "seoKeywordEn":"SEO keywords",
      "seoKeywordTc":"搜索引擎優化 關鍵詞",
      "seoKeywordSc":"搜索引擎优化 关键词",
      "seoDescriptionEn":"SEO Description",
      "seoDescriptionTc":"搜索引擎優化 描述",
      "seoDescriptionSc":"搜索引擎优化 描述",
      "banner":{  
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "readingCornerList":[  
         {  
            "id":1,
            "titleEn":"Promotion 1 ",
            "titleTc":"Promotion 1 TC",
            "titleSc":"Promotion 1 SC",
            "thumbnailEn":"",
            "thumbnailTc":"",
            "thumbnailSc":"",
            "downloadIconUrl":"download.svg",
            "downloadUrl":"http://google.com"
         }
      ],
      "pageInfo":{  
         "total":49,
         "pageCount":1,
         "pageSize":49,
         "curPage":1
      }
   }
}
::READING_CORNER_LIST_API_END::
	*/