package com.gt.projects.thelinkreit.entity.mongo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "HomePage")
public class HomePage {

    private String test;

}
