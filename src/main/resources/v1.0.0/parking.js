/**
	 *
	 * @api {get} http://www-dev.linkhk.com/linkweb/api/parkingPerks/      Parking Perks
	 *
	 * @apiName Parking Perks
	 * @apiGroup Batch3_2-Parking
	 * @apiVersion 1.0.0
	 *
	  @apiDescription
		Retrieve Parking Perks Landing Page content<br/>
		Data included 4 components content:<br/>
		1. SEO Details<br/>
		2. Banner Details<br/>
		3. Parking Promotion Details<br/>
        4. Parking Details Details<br/>

        @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
        @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
        @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
        @apiSuccess (Success 200) {String} 	data		Data Object.
        @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
        @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
        @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
        @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
        @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
        @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
        @apiSuccess (Success 200) {Object} 	data.banner		Parking Details - Banner Object
        @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
        @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
        @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
        @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
        @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
        @apiSuccess (Success 200) {Object[]}	    data.promotion	Parking Details - Tenant Hero Object Array
        @apiSuccess (Success 200) {Integer}	    data.promotion.id		Promotion ID
        @apiSuccess (Success 200) {String}	    data.promotion.titleEn	Promotion Title (EN)
        @apiSuccess (Success 200) {String}	    data.promotion.titleTc	Promotion Title (TC)
        @apiSuccess (Success 200) {String}	    data.promotion.titleSc	Promotion Title (SC)
        @apiSuccess (Success 200) {String}	    data.promotion.dateEn		Promotion Date (EN)
        @apiSuccess (Success 200) {String}	    data.promotion.dateTc		Promotion Date (TC)
        @apiSuccess (Success 200) {String}	    data.promotion.dateSc		Promotion Date (SC)
        @apiSuccess (Success 200) {String}	    data.promotion.timeEn		Promotion Time (EN)
        @apiSuccess (Success 200) {String}	    data.promotion.timeTc		Promotion Time (TC)
        @apiSuccess (Success 200) {String}	    data.promotion.timeSc		Promotion Time (SC)
        @apiSuccess (Success 200) {String}	    data.promotion.locationEn		Promotion Location (EN)
        @apiSuccess (Success 200) {String}	    data.promotion.locationTc		Promotion Location (TC)
        @apiSuccess (Success 200) {String}	    data.promotion.locationSc		Promotion Location (SC)
        @apiSuccess (Success 200) {String}	    data.promotion.posterEn		Promotion Image URL (EN)
        @apiSuccess (Success 200) {String}	    data.promotion.posterTc		Promotion Image URL (TC)
        @apiSuccess (Success 200) {String}	    data.promotion.posterSc		Promotion Image URL (SC)
        @apiSuccess (Success 200) {Object}	    data.description 	Parking Details - Description
        @apiSuccess (Success 200) {String}	    data.description.introductionTitleEn		Section Tagline (EN)
        @apiSuccess (Success 200) {String}	    data.description.introductionTitleTc		Section Tagline (TC)
        @apiSuccess (Success 200) {String}	    data.description.introductionTitleSc		Section Tagline (SC)
        @apiSuccess (Success 200) {String}	    data.description.introductionContentEn		Section Description (EN)
        @apiSuccess (Success 200) {String}	    data.description.introductionContentTc		Section Description (TC)
        @apiSuccess (Success 200) {String}	    data.description.introductionContentSc		Section Description (SC)
        @apiSuccess (Success 200) {String}	    data.description.privilegesImgStandardEn		Privileges Image (Desktop) (EN)
        @apiSuccess (Success 200) {String}	    data.description.privilegesImgStandardTc		Privileges Image (Desktop) (TC)
        @apiSuccess (Success 200) {String}	    data.description.privilegesImgStandardSc		Privileges Image (Desktop) (SC)
        @apiSuccess (Success 200) {String}	    data.description.privilegesImgNarrowEn		Privileges Image (Mobile) (EN)
        @apiSuccess (Success 200) {String}	    data.description.privilegesImgNarrowTc		Privileges Image (Mobile) (TC)
        @apiSuccess (Success 200) {String}	    data.description.privilegesImgNarrowSc		Privileges Image (Mobile) (SC)
        @apiSuccess (Success 200) {String}	    data.description.perksContentEn  Privileges Description (EN)
        @apiSuccess (Success 200) {String}	    data.description.perksContentTc  Privileges Description (TC)
        @apiSuccess (Success 200) {String}	    data.description.perksContentSc  Privileges Description (SC)
	 	@apiSuccess (Success 200) {String}	    data.description.privilegesContentEn  Privileges Content (EN)
	 	@apiSuccess (Success 200) {String}	    data.description.privilegesContentTc  Privileges Content (TC)
	 	@apiSuccess (Success 200) {String}	    data.description.privilegesContentSc  Privileges Content (SC)
        @apiSuccess (Success 200) {String}	    data.description.remarkEn		Remarks (EN)
        @apiSuccess (Success 200) {String}	    data.description.remarkTc		Remarks (TC)
        @apiSuccess (Success 200) {String}	    data.description.remarkSc		Remarks (SC)


      	@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::PARKING_PERKS_LANDING_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Parking Details Listing",
      "seoTitleTc":"Parking Details Listing",
      "seoTitleSc":"Parking Details Listing",
      "seoKeywordEn":"Parking Details Listing",
      "seoKeywordTc":"Parking Details Listing",
      "seoKeywordSc":"Parking Details Listing",
      "seoDescriptionEn":"Parking Details Listing",
      "seoDescriptionTc":"Parking Details Listing",
      "seoDescriptionSc":"Parking Details Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "promotion":[
            {
               "id":17402,
               "titleEn":"Current Promotion",
               "titleTc":"Current Promotion TC",
               "titleSc":"Current Promotion SC",
               "dateEn":"NOW TILL FURTHER NOTICE",
               "dateTc":"NOW TILL FURTHER NOTICE",
               "dateSc":"NOW TILL FURTHER NOTICE",
               "timeEn":"10:00am. - 8:00p.m.",
               "timeTc":"10:00am. - 8:00p.m.",
               "timeSc":"10:00am. - 8:00p.m.",
               "locationEn":"8 Shopping Centre",
		       "locationTc":"8 Shopping Centre TC",
		       "locationSc":"8 Shopping Centre SC",
               "posterEn":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterTc":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterSc":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg"
            }
      ],
      "description":{
         "introductionTitleEn":"introductionTitleEn",
         "introductionTitleTc":"introductionTitleTc",
         "introductionTitleSc":"introductionTitleSc",
         "introductionContentEn":"introductionContentEn",
         "introductionContentTc":"introductionContentTc",
         "introductionContentSc":"introductionContentSc",
         "privilegesImgStandardEn" : null,
         "privilegesImgStandardTc" : null,
         "privilegesImgStandardSc" : null,
         "privilegesImgNarrowEn" : null,
         "privilegesImgNarrowTc" : null,
         "privilegesImgNarrowSc" : null,
         "perksContentEn":"perksContentEn",
         "perksContentTc":"perksContentTc",
         "perksContentSc":"perksContentSc",
         "privilegesContentEn":"privilegesContentEn",
         "privilegesContentTc":"privilegesContentTc",
         "privilegesContentSc":"privilegesContentSc",
         "remarkEn":"remarkEn",
         "remarkTc":"remarkTc",
         "remarkSc":"remarkSc",
      }
   }
::PARKING_PERKS_LANDING_API_END::
*/

/**
 *
 * @api {get} http://www-dev.linkhk.com/linkweb/api/parking/      Parking Listing
 *
 * @apiName Parking Listing
 * @apiGroup Batch3_2-Parking
 * @apiVersion 1.0.0
 *
 @apiDescription
 Retrieve Parking Listing Page content<br/>
 Data included 4 components content:<br/>
 1. SEO Details<br/>
 2. Banner Details<br/>
 3. Parking List<br/>
 4. Paging Info<br/>


 @apiParam {Integer}	districtId		District ID
 @apiParam {Integer}	facilityKey		Car Park ID
 @apiParam {String}		propCode		Property Code
 @apiParam {Integer}	carParkPrivilegeTypeId		Parking Privilege Type ID
 @apiParam {Integer}	pageNo		Page No ( start = 0 )
 @apiParam {Integer}	pageSize	Number of list record per request


 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.banner		Parking Listing - Banner Object
 @apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
 @apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
 @apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
 @apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
 @apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
 @apiSuccess (Success 200) {Object[]}	data.parkingList 	Parking Listing - Parking Object Array
 @apiSuccess (Success 200) {Integer}	data.parkingList.facilityKey 	Parking ID
 @apiSuccess (Success 200) {String}	    data.parkingList.carParkFacilityNameEn		Parking Name (EN)
 @apiSuccess (Success 200) {String}	    data.parkingList.carParkFacilityNameTc		Parking Name (TC)
 @apiSuccess (Success 200) {String}	    data.parkingList.carParkFacilityNameSc		Parking Name (SC)
 @apiSuccess (Success 200) {String}	    data.parkingList.shopCentreNameEn		Shop Centre Name (EN)
 @apiSuccess (Success 200) {String}	    data.parkingList.shopCentreNameTc		Shop Centre Name (TC)
 @apiSuccess (Success 200) {String}	    data.parkingList.shopCentreNameSc		Shop Centre Name (SC)
 @apiSuccess (Success 200) {Integer}	data.parkingList.availableCarParkSpace		Available Car Park Space
 @apiSuccess (Success 200) {String}	    data.parkingList.carParkFacilityPhoto		Car Park Photo
 @apiSuccess (Success 200) {Object} 	data.pageInfo	Parking Listing - Page Info Object
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.total		No. of Total Record
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageCount		No. of Page Count
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageSize		No. per page
 @apiSuccess (Success 200) {Integer} 	data.pageInfo.curPage		Current Page No



 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::PARKING_LISTING_API_START::
 {
   "error":"0000",
   "errorMessage":"",
   "errorCode":"",
   "data":{
      "seoTitleEn":"Parking Details Listing",
      "seoTitleTc":"Parking Details Listing",
      "seoTitleSc":"Parking Details Listing",
      "seoKeywordEn":"Parking Details Listing",
      "seoKeywordTc":"Parking Details Listing",
      "seoKeywordSc":"Parking Details Listing",
      "seoDescriptionEn":"Parking Details Listing",
      "seoDescriptionTc":"Parking Details Listing",
      "seoDescriptionSc":"Parking Details Listing",
      "banner":{
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "parkingList":[
         {
            "facilityKey":17402,
            "carParkFacilityNameEn":"Car Park",
            "carParkFacilityNameTc":"Car Park TC",
            "carParkFacilityNameSc":"Car Park SC",
            "shopCentreNameEn":"Lok Fu Plaza",
            "shopCentreNameTc":"Lok Fu Plaza TC",
            "shopCentreNameSc":"Lok Fu Plaza SC",
            "availableCarParkSpace":10,
            "carParkFacilityPhoto":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg"
         }
      ],
      "pageInfo":{
         "total":49,
         "pageCount":1,
         "pageSize":49,
         "curPage":1
      }
   }
}
::PARKING_LISTING_API_END::
*/

/**

 @api {get} http://www-dev.linkhk.com/linkweb/api/parking/{id}      Parking Details

 @apiName Parking Details
 @apiGroup Batch3_2-Parking
 @apiVersion 1.0.0

 @apiDescription
 Retrieve Promotion Details Page content<br/>
 Data included 5 components content:<br/>
 1. SEO Details<br/>
 2. Parking Details<br/>
 3. Parking Promotion<br/>
 4. The Nearest Car Park<br/>

 @apiParam {Integer}	facilityKey		Parking ID

 @apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
 @apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
 @apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
 @apiSuccess (Success 200) {String} 	data		Data Object.
 @apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
 @apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
 @apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
 @apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
 @apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
 @apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
 @apiSuccess (Success 200) {Object} 	data.parkingInfo		Parking Details - Parking Info
 @apiSuccess (Success 200) {Integer} 	data.parkingInfo.facilityKey    Parking ID
 @apiSuccess (Success 200) {Integer} 	data.parkingInfo.districtId    District ID
 @apiSuccess (Success 200) {String} 	data.parkingInfo.headlineEn	Section Headline (EN)
 @apiSuccess (Success 200) {String} 	data.parkingInfo.headlineTc	Section Headline (TC)
 @apiSuccess (Success 200) {String} 	data.parkingInfo.headlineSc	Section Headline (SC)
 @apiSuccess (Success 200) {String} 	data.parkingInfo.carParkFacilityNameEn	Parking Name (EN)
 @apiSuccess (Success 200) {String} 	data.parkingInfo.carParkFacilityNameTc	Parking Name (TC)
 @apiSuccess (Success 200) {String} 	data.parkingInfo.carParkFacilityNameSc	Parking Name (SC)
 @apiSuccess (Success 200) {String} 	data.parkingInfo.longitude  Longitude
 @apiSuccess (Success 200) {String} 	data.parkingInfo.latitude   Latitude
 @apiSuccess (Success 200) {String}	    data.parkingInfo.allRateHtmlEn	Car Park Duration & Rate HTML (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.allRateHtmlTc	Car Park Duration & Rate HTML (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.allRateHtmlSc	Car Park Duration & Rate HTML (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.addressLabelEn		Address Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.addressLabelTc		Address Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.addressLabelSc		Address Label (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.addressEn		Address (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.addressTc		Address (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.addressSc		Address (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.telephoneLabelEn	Telephone Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.telephoneLabelTc	Telephone Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.telephoneLabelSc	Telephone Label (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.telephone		Telephone
 @apiSuccess (Success 200) {String}	    data.parkingInfo.remarkLabelEn		Remark Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.remarkLabelTc		Remark Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.remarkLabelSc		Remark Label (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.remarkEn		Remark (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.remarkTc		Remark (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.remarkSc		Remark (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.availableCarParkSpace  Available Car Park Space
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeLabelEn   Privilege Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeLabelTc   Privilege Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeLabelSc   Privilege Label (SC)
 @apiSuccess (Success 200) {Object[]}	data.parkingInfo.privilegeList  Privilege Indicator Object Array
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeList.carParkPrivilegeThumbnailPath    Privilege Icon
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeList.carParkPrivilegeTypeNameEn   Alt Text Name (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeList.carParkPrivilegeTypeNameTc   Alt Text Name (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.privilegeList.carParkPrivilegeTypeNameSc   Alt Text Name (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.totalCarParkSpaceLabelEn   Total Space Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.totalCarParkSpaceLabelTc   Total Space Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.totalCarParkSpaceLabelSc   Total Space Label (SC)
 @apiSuccess (Success 200) {Integer}	data.parkingInfo.totalCarParkSpace      Total Space
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionLabelEn   Payment Option Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionLabelTc   Payment Option Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionLabelSc   Payment Option Label (SC)
 @apiSuccess (Success 200) {Object[]}	data.parkingInfo.paymentOptionList      Payment Option Indicator Object Array
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionList.paymentOptionThumbnailPath   Payment Option Icon
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionList.paymentOptionTypeNameEn  Payment Option Name (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionList.paymentOptionTypeNameTc  Payment Option Name (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.paymentOptionList.paymentOptionTypeNameSc  Payment Option Name (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.otherAccessLabelEn   Other Access Label (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.otherAccessLabelTc   Other Access Label (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.otherAccessLabelSc   Other Access Label (SC)
 @apiSuccess (Success 200) {Object}	    data.parkingInfo.otherAccessList       Other Access Car Park Object Array
 @apiSuccess (Success 200) {Integer}	data.parkingInfo.otherAccessList.facilityKey    Car Park ID
 @apiSuccess (Success 200) {String}	    data.parkingInfo.otherAccessList.carParkFacilityNameEn	Parking Name (EN)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.otherAccessList.carParkFacilityNameTc	Parking Name (TC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.otherAccessList.carParkFacilityNameSc	Parking Name (SC)
 @apiSuccess (Success 200) {String}	    data.parkingInfo.carParkFacilityPhoto	Car Park Facility Thumbnail URL
 @apiSuccess (Success 200) {String}	    data.parkingInfo.serviceTncEn	Car Park Service Tnc En
 @apiSuccess (Success 200) {String}	    data.parkingInfo.serviceTncTc	Car Park Service Tnc Tc
 @apiSuccess (Success 200) {String}	    data.parkingInfo.serviceTncSc	Car Park Service Tnc Sc
 @apiSuccess (Success 200) {Object}	    data.promotion       Parking Details - Promotion Details Object
 @apiSuccess (Success 200) {String}	    data.promotion.privilegeDescriptionEn     Promotion Details HTML (EN)
 @apiSuccess (Success 200) {String}	    data.promotion.privilegeDescriptionTc     Promotion Details HTML (TC)
 @apiSuccess (Success 200) {String}	    data.promotion.privilegeDescriptionSc     Promotion Details HTML (SC)
 @apiSuccess (Success 200) {String}	    data.promotion.disclaimerEn     Promotion Terms and Condition HTML (EN)
 @apiSuccess (Success 200) {String}	    data.promotion.disclaimerTc     Promotion Terms and Condition HTML (TC)
 @apiSuccess (Success 200) {String}	    data.promotion.disclaimerSc     Promotion Terms and Condition HTML (SC)
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark  Parking Details - The Nearest CarPark Object
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark.titleEn  Title (EN)
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark.titleTc  Title (TC)
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark.titleSc  Title (SC)
 @apiSuccess (Success 200) {Object[]}	data.theNearestCarPark.carParkList  Car Park Object Array
 @apiSuccess (Success 200) {Integer}	data.theNearestCarPark.carParkList.facilityKey  Car Park Key
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.carParkFacilityNameEn Parking Name (EN)
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.carParkFacilityNameTc Parking Name (TC)
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.carParkFacilityNameSc Parking Name (SC)
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.shopCentreNameEn   Shop Centre Name (EN)
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.shopCentreNameTc   Shop Centre Name (TC)
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.shopCentreNameSc   Shop Centre Name (SC)
 @apiSuccess (Success 200) {String}	    data.theNearestCarPark.carParkList.carParkFacilityPhoto     Car Park Facility Thumbnail URL
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark.viewAllEn  View All Content (EN)
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark.viewAllTc  View All Content (TC)
 @apiSuccess (Success 200) {Object}	    data.theNearestCarPark.viewAllSc  View All Content (SC)


 @apiSuccessExample {json} Example
 HTTP/1.1 200 OK
 ::PARKING_DETAILS_API_START::
 {
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{
      "id":1,
      "seoTitleEn":"Parking Details Details",
      "seoTitleTc":"Parking Details Details",
      "seoTitleSc":"Parking Details Details",
      "seoKeywordEn":"Parking Details Details",
      "seoKeywordTc":"Parking Details Details",
      "seoKeywordSc":"Parking Details Details",
      "seoDescriptionEn":"Parking Details Details",
      "seoDescriptionTc":"Parking Details Details",
      "seoDescriptionSc":"Parking Details Details",
      "parkingInfo":{
         "facilityKey":7,
         "districtId":7,
         "headlineEn":"Leasing",
         "headlineTc":"Leasing (TC)",
         "headlineSc":"Leasing (SC)",
         "carParkingFacilityNameEn":"Lok Fu CarPark",
         "carParkingFacilityNameTc":"Lok Fu CarPark (TC)",
         "carParkingFacilityNameSc":"Lok Fu CarPark (SC)",
         "longitude":"22.11278121",
         "latitude":"100.11278121",
         "allRateHtmlEn":"",
         "allRateHtmlTc":"",
         "allRateHtmlSc":"",
         "addressLabelEn":"Address",
         "addressLabelTc":"地址",
         "addressLabelSc":"地址",
         "addressEn":"Lok Fu Place 198 Junction Road Wang Tau Hom,  Kowloon ",
         "addressTc":"九龍橫頭磡聯合道198號樂富廣場",
         "addressSc":"九龙横头磡联合道198号乐富广场",
         "telephoneLabelEn":"Telephone",
         "telephoneLabelTc":"客戶服務熱線",
         "telephoneLabelSc":"客户服务热线",
         "telephone":"2338 7781",
         "remarkLabelEn":"Remark",
         "remarkLabelTc":"備注",
         "remarkLabelSc":"備注",
         "remarkEn":"Leasing Enquiry",
         "remarkTc":"租務查詢",
         "remarkSc":"租务查询",
         "availableCarParkSpace":10,
         "privilegeLabelEn":"Privilege",
         "privilegeLabelTc":"Privilege TC",
         "privilegeLabelSc":"Privilege SC",
         "privilegeList":[
            {
               "carParkPrivilegeThumbnailPath":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
               "carParkPrivilegeTypeNameEn":"24",
               "carParkPrivilegeTypeNameTc":"24",
               "carParkPrivilegeTypeNameSc":"24"
            }
         ],
         "totalCarParkSpaceLabelEn":"Total Space",
         "totalCarParkSpaceLabelTc":"Total Space TC",
         "totalCarParkSpaceLabelSc":"Total Space SC",
         "totalCarParkSpace":456,
         "paymentOptionLabelEn":"Payment Option",
         "paymentOptionLabelTc":"Payment Option TC",
         "paymentOptionLabelSc":"Payment Option SC",
         "paymentOptionList":[
            {
               "paymentOptionThumbnailPath":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg",
               "paymentOptionTypeNameEn":"eps",
               "paymentOptionTypeNameTc":"eps",
               "paymentOptionTypeNameSc":"eps"
            }
         ],
         "otherAccessLabelEn":"Other Access",
         "otherAccessLabelTc":"Other Access TC",
         "otherAccessLabelSc":"Other Access SC",
         "otherAccessList":[
            {
               "facilityKey":6965,
               "carParkingFacilityNameEn":"Test Car Park",
               "carParkingFacilityNameTc":"Test Car Park TC",
               "carParkingFacilityNameSc":"Test Car Park SC"
            }
         ],
         "carParkFacilityPhoto":"http://www-dev.linkhk.com/linkapi/file/ShopCentrePhoto/lfxxc2/IMG_6965.jpg"
      },
      "promotion":{
         "privilegeDescriptionEn":"Details",
         "privilegeDescriptionTc":"Details",
         "privilegeDescriptionSc":"Details",
         "disclaimerEn":"TNC",
         "disclaimerTc":"TNC",
         "disclaimerSc":"TNC"
      },
      "theNearestCarPark":{
         "titleEn":"You may also like En",
         "titleTc":"You may also like Tc",
         "titleSc":"You may also like Sc",
         "carParkList":[
            {
               "facilityKey":17402,
               "carParkingFacilityNameEn":"Car Park",
               "carParkingFacilityNameTc":"Car Park TC",
               "carParkingFacilityNameSc":"Car Park SC",
               "shopCentreNameEn":"Lok Fu Plaza",
               "shopCentreNameTc":"Lok Fu Plaza TC",
               "shopCentreNameSc":"Lok Fu Plaza SC",
               "carParkFacilityPhoto":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg"
            }
         ],
         "viewAllEn":"View All En",
         "viewAllTc":"View All Tc",
         "viewAllSc":"View All Sc"
      }
   }
}
 ::PARKING_DETAILS_API_END::
 */
