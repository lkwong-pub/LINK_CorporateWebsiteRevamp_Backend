/**
	 
	 @api {get} http://www-dev.linkhk.com/linkweb/api/promotion/      Promotion Listing
	 
	 @apiName Promotion List
	 @apiGroup Batch3_1-Promotion
	 @apiVersion 1.0.0
	 
	 @apiDescription 
	 Retrieve Promotion Listing Page content<br/> 
	 Data included 6 components content:<br/> 
	 1. SEO Details<br/> 
	 2. Banner<br/> 
	 3. Promotion Type Filter<br/>
	 4. Mainland China Shop Centre List<br/> 
	 5. Parallax 
	 6. Promotion List<br/> 
	 7. Paging Info<br/> 
	 
	 @apiParam {Integer}	promotionTypeId		Promotion Type ID
	 @apiParam {Integer}	promotionPeriodType		Promotion Period Type (1 = All, 2 = Pass)
	 @apiParam {Integer}	pageNo		Page No ( start = 0 )
	 @apiParam {Integer}	pageSize	Number of list record per request
	 
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
		@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
		@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
		@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
		@apiSuccess (Success 200) {Object} 	data.banner		Promotion List - Banner Object
		@apiSuccess (Success 200) {String} 	data.banner.iconStandard	Section Icon URL (Desktop)
		@apiSuccess (Success 200) {String} 	data.banner.iconNarrow		Section Icon URL (Mobile)
		@apiSuccess (Success 200) {String} 	data.banner.initialImgStandard	Banner URL (Desktop)
		@apiSuccess (Success 200) {String} 	data.banner.initialImgNarrow	Banner URL (Mobile)
		@apiSuccess (Success 200) {String} 	data.banner.titleEn		Banner Title (EN)
		@apiSuccess (Success 200) {String} 	data.banner.titleTc		Banner Title (TC)
		@apiSuccess (Success 200) {String} 	data.banner.titleSc		Banner Title (SC)
		@apiSuccess (Success 200) {Object[]} 	data.promotionTypes	Promotion List - Promotion Type Object Array
		@apiSuccess (Success 200) {String} 	data.promotionTypes.promotionTypeCode	Promotion Type Code
		@apiSuccess (Success 200) {String} 	data.promotionTypes.promotionTypeNameEn		Promotion Type Name (EN)
		@apiSuccess (Success 200) {String} 	data.promotionTypes.promotionTypeNameTc		Promotion Type Name (TC)
		@apiSuccess (Success 200) {String} 	data.promotionTypes.promotionTypeNameSc		Promotion Type Name (SC)
		@apiSuccess (Success 200) {Object} 	data.mainlandChina		Promotion List - Mainland China Shop Centre Object
		@apiSuccess (Success 200) {String} 	data.mainlandChina.titleEn		Shop Centre Pulldown Title (EN)
		@apiSuccess (Success 200) {String} 	data.mainlandChina.titleTc		Shop Centre Pulldown Title (TC)
		@apiSuccess (Success 200) {String} 	data.mainlandChina.titleSc		Shop Centre Pulldown Title (SC)
		@apiSuccess (Success 200) {Object[]} 	data.mainlandChina.shopCentre	Shop Centre List Object Array
		@apiSuccess (Success 200) {Integer} 	data.mainlandChina.shopCentre.shopCentreId		Shop Centre ID
 		@apiSuccess (Success 200) {String} 	data.mainlandChina.shopCentre.propCode		Shop Centre Property Code
		@apiSuccess (Success 200) {String} 	data.mainlandChina.shopCentre.shopCentreNameEn		Shop Centre Name (EN)
		@apiSuccess (Success 200) {String} 	data.mainlandChina.shopCentre.shopCentreNameTc		Shop Centre Name (TC)
		@apiSuccess (Success 200) {String} 	data.mainlandChina.shopCentre.shopCentreNameSc		Shop Centre Name (SC)
		@apiSuccess (Success 200) {Object[]} 	data.parallax 			Promotion List - Parallax Object Array
		@apiSuccess (Success 200) {String} 	data.parallax.value		Parallax URL
		@apiSuccess (Success 200) {Object[]} 	data.promotionList		Promotion List - Promotion List Object Array
		@apiSuccess (Success 200) {Integer} 	data.promotionList.promotionId	Promotion ID
        @apiSuccess (Success 200) {String} 	data.promotionList.titleEn		Promotion Name (EN)
        @apiSuccess (Success 200) {String} 	data.promotionList.titleTc		Promotion Name (TC)
        @apiSuccess (Success 200) {String} 	data.promotionList.titleSc		Promotion Name (SC)
        @apiSuccess (Success 200) {String} 	data.promotionList.posterEn		Promotion Poster URL (EN)
        @apiSuccess (Success 200) {String} 	data.promotionList.posterTc		Promotion Poster URL (TC)
        @apiSuccess (Success 200) {String} 	data.promotionList.posterSc		Promotion Poster URL (SC)
        @apiSuccess (Success 200) {Object[]} 	data.promotionList.indications		Prmotion List - Indication Object Array
        @apiSuccess (Success 200) {String} 	data.promotionList.indications.urlEn	Indication URL (EN)
        @apiSuccess (Success 200) {String} 	data.promotionList.indications.urlTc	Indication URL (TC)
        @apiSuccess (Success 200) {String} 	data.promotionList.indications.urlSc	Indication URL (SC)
		@apiSuccess (Success 200) {Object} 	data.pageInfo	Promotion List - Page Info Object
		@apiSuccess (Success 200) {Integer} 	data.pageInfo.total		No. of Total Record
	    @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageCount		No. of Page Count
	    @apiSuccess (Success 200) {Integer} 	data.pageInfo.pageSize		No. per page
	    @apiSuccess (Success 200) {Integer} 	data.pageInfo.curPage		Current Page No
		@apiSuccessExample {json} Example
HTTP/1.1 200 OK
::PROMOTION_LIST_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "seoTitleEn":"Promotion",
      "seoTitleTc":"推廣",
      "seoTitleSc":"推广",
      "seoKeywordEn":"",
      "seoKeywordTc":"",
      "seoKeywordSc":"",
      "seoDescriptionEn":"",
      "seoDescriptionTc":"",
      "seoDescriptionSc":"",
      "banner":{  
         "iconStandard":"",
         "iconNarrow":"",
         "initialImgStandard":"",
         "initialImgNarrow":"",
         "titleEn":"",
         "titleTc":"",
         "titleSc":""
      },
      "promotionTypes":[  
         {  
            "id":1,
            "titleEn":"Promotion",
            "titleTc":"Promotion Tc",
            "titleSc":"Promotion Sc"
         }
      ],
      "mainlandChina":{  
         "titleEn":"Go Shoppin In Mainland China",
         "titleTc":"Go Shoppin In Mainland China TC",
         "titleSc":"Go Shoppin In Mainland China SC",
         "shopCentre":[  
            {  
               "shopCentreId":1,
               "propCode" : "chfxc2",
               "shopCentreNameEn":"Lok Fu Plaza",
               "shopCentreNameTc":"Lok Fu Plaza",
               "shopCentreNameSc":"Lok Fu Plaza"
            }
         ]
      },
      "parallax": [
      	{
			"value": "/SiteAssets/para img 1.PNG"
		},
		{
			"value": "/SiteAssets/para img 2.PNG"	
		}
	  ],
      "promotionList":[  
         {  
            "id":1,
            "titleEn":"Promotion 1 ",
            "titleTc":"Promotion 1 TC",
            "titleSc":"Promotion 1 SC",
            "posterEn":"/poster.png",
            "posterTc":"/poster.png",
            "posterSc":"/poster.png",
            "indications":[
            	{
            		"urlEn": "newest.png",
            		"urlTc": "newest.png",
            		"urlSc": "newest.png"
            	}
            ]
         }
      ],
      "pageInfo":{  
         "total":49,
         "pageCount":1,
         "pageSize":49,
         "curPage":1
      }
   }
}
::PROMOTION_LIST_API_END::
	*/

/**
	 
	 @api {get} http://www-dev.linkhk.com/linkweb/api/promotion/{id}      Promotion Details
	 
	 @apiName Promotion Details
	 @apiGroup Batch3_1-Promotion
	 @apiVersion 1.0.0
	 
	 @apiDescription 
	 Retrieve Promotion Details Page content<br/> 
	 Data included 5 components content:<br/> 
	 1. SEO Details<br/> 
	 2. Promotion Info<br/> 
	 3. Event Gallery<br/> 
	 4. Promotion Details<br/> 
	 5. You may also like<br/>
	 
	 @apiParam {Integer}	id		Promotion ID
	 
		@apiSuccess (Success 200) {String} 	error	API Call Status. "0000" for success.
		@apiSuccess (Success 200) {String} 	error_message	Error Message When API Call Fails ( status is not "0000" ) .
		@apiSuccess (Success 200) {String} 	error_code		Error Code (Develop mode)
		@apiSuccess (Success 200) {String} 	data		Data Object.
		@apiSuccess (Success 200) {String} 	data.seoTitleEn	SEO Title (EN)
		@apiSuccess (Success 200) {String} 	data.seoTitleTc	SEO Title (TC)
		@apiSuccess (Success 200) {String} 	data.seoTitleSc	SEO Title (SC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordEn		SEO Keyword (EN)
		@apiSuccess (Success 200) {String} 	data.seoKeywordTc		SEO Keyword (TC)
		@apiSuccess (Success 200) {String} 	data.seoKeywordSc		SEO Keyword (SC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionEn		SEO Description (EN)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionTc		SEO Description (TC)
		@apiSuccess (Success 200) {String} 	data.seoDescriptionSc		SEO Description (SC)
		@apiSuccess (Success 200) {Object} 	data.promotionInfo		Promotion - Promotion Info Object
		@apiSuccess (Success 200) {Integer} 	data.promotionInfo.promotionId		Promotion ID
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionType		Promotion Type ( 'magazine' = Magazine View, 'promotion' = Default Promotion View )
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionTypeNameEn	Promotion Type Name (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionTypeNameTc	Promotion Type Name (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionTypeNameSc	Promotion Type Name (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.titleEn		Promotion Name (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.titleTc		Promotion Name (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.titleSc		Promotion Name (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.dateEn		Promotion Date (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.dateTc		Promotion Date (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.dateSc		Promotion Date (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.timeEn		Promotion Time (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.timeTc		Promotion Time (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.timeSc		Promotion Time (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.introductionEn		Promotion Introduction (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.introductionTc		Promotion Introduction (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.introductionSc		Promotion Introduction (SC)
        @apiSuccess (Success 200) {String} 	data.promotionInfo.contact		Promotion Contact Number
 		@apiSuccess (Success 200) {String} 	data.promotionInfo.viewLocationThumbnailUrlEn	View Location Thumbnail URL (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.viewLocationThumbnailUrlTc	View Location Thumbnail URL (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.viewLocationThumbnailUrlSc	View Location Thumbnail URL (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.downloadIconUrlEn	Download Icon URL (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.downloadIconUrlTc	Download Icon URL (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.downloadIconUrlSc	Download Icon URL (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.downloadUrl	Download URL
		@apiSuccess (Success 200) {String} 	data.promotionInfo.locationEn	Location Name (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.locationTc 	Location Name (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.locationSC	Location Name (SC)
		@apiSuccess (Success 200) {Boolean} 	data.promotionInfo.isCustomLocation		Custom Location Name Checking (* Add further click action on Location Text if value = true)
		@apiSuccess (Success 200) {Boolean} 	data.promotionInfo.isSingleLocation		Single Location Name Checking (* Redirect to shop centre details page if value = true)
		@apiSuccess (Success 200) {Integer} 	data.promotionInfo.singleShopCentreId	Single Location - Shop Centre ID
 		@apiSuccess (Success 200) {String} 	data.promotionInfo.singlePropCode	Single Location - Shop Centre PropCode
		@apiSuccess (Success 200) {Object[]} 	data.promotionInfo.shopCentreList	Promotion - Shop Centre Area List Object Array
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.areaCode	Area Code
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.areaNameEn	Area Name (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.areaNameTc	Area Name (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.areaNameSc	Area Name (SC)
		@apiSuccess (Success 200) {Object[]} 	data.promotionInfo.shopCentreList.shopCentres	Shop Centre List Object Array
		@apiSuccess (Success 200) {Integer} 	data.promotionInfo.shopCentreList.shopCentres.shopCentreId		Shop Centre ID
 		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.shopCentres.propCode		Shop Centre Property Code
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.shopCentres.shopCentreNameEn		Shop Centre Name (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.shopCentres.shopCentreNameTc		Shop Centre Name (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.shopCentreList.shopCentres.shopCentreNameSc		Shop Centre Name (SC)
		@apiSuccess (Success 200) {Object[]} 	data.promotionInfo.promotionPhotos		Poster Image Object Array
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionPhotos.posterEn		Poster URL (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionPhotos.posterTc		Poster URL (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionPhotos.posterSc		Poster URL (SC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionPhotos.posterAltTextEn	Poster Alt Text (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionPhotos.posterAltTextTc	Poster Alt Text (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.promotionPhotos.posterAltTextSc	Poster Alt Text (SC)
		@apiSuccess (Success 200) {Integer} 	data.promotionInfo.lastYearPromotionId		Last Promotion ID
		@apiSuccess (Success 200) {String} 	data.promotionInfo.lastYearPromotionIconUrlEn	Last Promotion Icon (EN)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.lastYearPromotionIconUrlTc	Last Promotion Icon (TC)
		@apiSuccess (Success 200) {String} 	data.promotionInfo.lastYearPromotionIconUrlSc	Last Promotion Icon (SC)
		@apiSuccess (Success 200) {Object} 	data.eventGallery		Promotion - Event Gallery Object
		@apiSuccess (Success 200) {Object[]} 	data.eventGallery.eventImages	Event Image Object Array
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.thumbnail		Image URL
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextEn		Image Alt Text (EN)
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextTc		Image Alt Text (TC)
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventImages.altTextSc		Image Alt Text (SC)
		@apiSuccess (Success 200) {Object[]} 	data.eventGallery.eventVideos	Event Video Object Array
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.videoThumbnail	Event Video Thumbnail
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.videoUrl			Event Video URL
		@apiSuccess (Success 200) {String} 	data.eventGallery.eventVideos.altVideoUrl		Event Alt Video URL
		@apiSuccess (Success 200) {Object} 	data.promotionDetails		Promotion - Prmotion Details Object
		@apiSuccess (Success 200) {String} 	data.promotionDetails.detailEn		Promotion Details (EN) (* HTML data )
		@apiSuccess (Success 200) {String} 	data.promotionDetails.detailTc		Promotion Details (TC) (* HTML data )
		@apiSuccess (Success 200) {String} 	data.promotionDetails.detailSc		Promotion Details (SC) (* HTML data )
		@apiSuccess (Success 200) {String} 	data.promotionDetails.additionalTitleEn		TNC Title (EN) 
		@apiSuccess (Success 200) {String} 	data.promotionDetails.additionalTitleTc		TNC Title (TC)
		@apiSuccess (Success 200) {String} 	data.promotionDetails.additionalTitleSc		TNC Title (SC)
		@apiSuccess (Success 200) {String} 	data.promotionDetails.additionalContentEn	TNC Details (EN)
		@apiSuccess (Success 200) {String} 	data.promotionDetails.additionalContentTc	TNC Details (TC)
		@apiSuccess (Success 200) {String} 	data.promotionDetails.additionalContentSc	TNC Details (SC)
        @apiSuccess (Success 200) {Object}		data.youWillAlsoLike	Promotion - You May Also Like Object 
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleEn	Title (EN)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleTc	Title (TC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.titleSc	Title (SC) 
        @apiSuccess (Success 200) {Object[]}		data.youWillAlsoLike.youWillAlsoLikeList	You May Also Like List Object Array
        @apiSuccess (Success 200) {Integer}		data.youWillAlsoLike.youWillAlsoLikeList.id		Promotion ID
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.locationEn		Location Name (EN)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.locationTc		Location Name (TC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.locationSc		Location Name (SC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.titleEn		Promotion Name (EN)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.titleTc		Promotion Name (TC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.titleSc		Promotion Name (SC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.posterEn		Promotion Poster Name (EN)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.posterTc		Promotion Poster Name (TC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.posterSc		Promotion Poster Name (SC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.posterAltTextEn		Promotion Poster Alt Text (EN)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.posterAltTextTc		Promotion Poster Alt Text (TC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.youWillAlsoLikeList.posterAltTextSc		Promotion Poster Alt Text (SC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllEn		View All Content (EN)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllTc		View All Content (TC)
        @apiSuccess (Success 200) {String}		data.youWillAlsoLike.viewAllSc		View All Content (SC)
        @apiSuccessExample {json} Example
HTTP/1.1 200 OK
::PROMOTION_DETAILS_API_START::
{  
   "error":0,
   "error_message":"",
   "error_code":"",
   "data":{  
      "id":1,
      "seoTitleEn":"LINK Customer Site Promotion Details EN",
      "seoTitleTc":"LINK Customer Site Promotion Details TC",
      "seoTitleSc":"LINK Customer Site Promotion Details SC",
      "seoKeywordEn":"LINK Customer Site Promotion Details EN",
      "seoKeywordTc":"LINK Customer Site Promotion Details TC",
      "seoKeywordSc":"LINK Customer Site Promotion Details SC",
      "seoDescriptionEn":"This is LINK Customer Site Promotion Details. EN",
      "seoDescriptionTc":"This is LINK Customer Site Promotion Details. TC",
      "seoDescriptionSc":"This is LINK Customer Site Promotion Details. SC",
      "promotionInfo":{  
         "id":17402,
         "promotionType":"promotion",
         "promotionTypeNameEn":"Current Promotion",
         "promotionTypeNameTc":"Current Promotion TC",
         "promotionTypeNameSc":"Current Promotion SC",
         "titleEn":"Promotion",
         "titleTc":"Promotion TC",
         "titleSc":"Promotion SC",
         "dateEn":"6/14/2016 ",
         "dateTc":"6/14/2016 ",
         "dateSc":"6/14/2016 ",
         "timeEn":"12AM",
         "timeTc":"12AM",
         "timeSc":"12AM",
         "introductionEn":"LINK is pleased to present the well-received \"Stanley World of Food and Music\" for the fifth consecutive year with fun and innovative twists this year!",
         "introductionTc":"LINK是高興現在廣受歡迎的“赤柱廣場美食和音樂世界”今年樂趣和創新的曲折連續第五年！",
         "introductionSc":"LINK是高兴现在广受欢迎的“赤柱广场美食和音乐世界”今年乐趣和创新的曲折连续第五年！",
         "contact" : "21234567",
         "viewLocationThumbnailUrlEn":"/shopDetailViewLocationEn.png",
         "viewLocationThumbnailUrlTc":"/shopDetailViewLocationTc.png",
         "viewLocationThumbnailUrlSc":"/shopDetailViewLocationSc.png",
         "downloadIconUrlEn":"download.svg",
         "downloadIconUrlTc":"download.svg",
         "downloadIconUrlSc":"download.svg",
         "downloadUrl" : "http://yahoo.com",
         "locationEn":"8 Shopping Centre",
         "locationTc":"8 Shopping Centre TC",
         "locationSc":"8 Shopping Centre SC",
         "isCustomLocation" : true,
         "isSingleLocation" : false,
         "singleShopCentreId" : 1,
         "singlePropCode" : "chfxc2",
         "shopCentreList":[  
            {  
               "areaCode":"NT",
               "areaNameEn":"HKISLAND",
               "areaNameTc":"HKISLAND TC",
               "areaNameSc":"HKISLAND SC",
               "shopCentres":[  
                  {  
                     "shopCentreId":1,
                     "propCode" : "chfxc2",
                     "shopCentreNameEn":"",
                     "shopCentreNameTc":"",
                     "shopCentreNameSc":""
                  }
               ]
            }
         ],
         "promotionPhotos":[  
            {  
               "posterEn":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterTc":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterSc":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterAltTextEn":"Promotion1",
               "posterAltTextTc":"Promotion1 TC",
               "posterAltTextSc":"Promotion1 SC"
            }
         ],
         "lastYearPromotionId":1,
         "lastYearPromotionIconUrlEn" : "lastYear.svg",
         "lastYearPromotionIconUrlTc" : "lastYear.svg",
         "lastYearPromotionIconUrlSc" : "lastYear.svg"
      },
      "eventGallery":{  
         "eventImages":[  
            {  
               "thumbnail":"",
               "altTextEn":"",
               "altTextTc":"",
               "altTextSc":""
            }
         ],
         "eventVideos":[  
            {  
               "videoThumbnail":"",
               "videoUrl":"http://www.youtube.com",
               "altVideoUrl":""
            }
         ]
      },
      "promotionDetails":{  
         "detailEn":"<div>Details</div>",
         "detailTc":"<div>Details TC</div>",
         "detailSc":"<div>Details SC</div>",
         "addtionalTitleEn":"Terms and Condition",
         "addtionalTitleTc":"附帶條約",
         "addtionalTitleSc":"附带条约",
         "addtionalContentEn":"TNC",
         "addtionalContentTc":"TNC",
         "addtionalContentSc":"TNC"
      },
      "youWillAlsoLike":{  
         "titleEn":"You may also like En",
         "titleTc":"You may also like Tc",
         "titleSc":"You may also like Sc",
         "youWillAlsoLikeList":[  
            {  
               "id":17402,
               "titleEn":"Current Promotion",
               "titleTc":"Current Promotion TC",
               "titleSc":"Current Promotion SC",
               "locationEn":"8 Shopping Centre",
		       "locationTc":"8 Shopping Centre TC",
		       "locationSc":"8 Shopping Centre SC",
               "posterEn":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterTc":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterSc":"images/desktop/shop-and-dine/img_shop_small_thumbnail_1.jpg",
               "posterAltTextEn":"Promotion1",
               "posterAltTextTc":"Promotion1 TC",
               "posterAltTextSc":"Promotion1 SC"
            }
         ],
         "viewAllEn":"View All En",
         "viewAllTc":"View All Tc",
         "viewAllSc":"View All Sc"
      }
   }
}
::PROMOTION_DETAILS_API_END::
	*/