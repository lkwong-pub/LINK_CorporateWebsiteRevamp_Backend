#Link Corp Website API Server
##

## Introduction
The application using for retrieve the Link database content from MSSQL and MongoDB and return to frontend by customized API. 


## Information
GT Server Details
    - TBC

							
LINK Server details
	-TBC
	
## Definition of build configurations
- Using Maven Build > install
- Using maven Build > tomcat:run for local testing

## Git branches definitions
- master: Release branches
- develop: development branches
- release/uat: LINK UAT deployment branches