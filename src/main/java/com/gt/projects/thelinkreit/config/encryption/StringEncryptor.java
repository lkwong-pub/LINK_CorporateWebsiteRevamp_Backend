package com.gt.projects.thelinkreit.config.encryption;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

@Configuration("stringEncryptor")
@PropertySources(value = { @PropertySource({ "classpath:config/encryptorKey.properties" }) })
public class StringEncryptor implements InitializingBean {

	private static String prefix = "$AES$@#@";
	private static String algorithm = "AES";

	private byte[] keyValue = "Gt4T5h#e@L4i%n@k".getBytes();

	@Autowired
	private Environment environment;

	public void afterPropertiesSet() throws Exception {
		this.setKeyValue(this.environment.getProperty("stringEncryptorKeyValue"));
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue.getBytes();
	}

	public String encrypt(String plainText) throws Exception {
		return this.encrypt(plainText, null);
	}

	// Performs Encryption
	public String encrypt(String plainText, String keyValue) throws Exception {
		if (plainText == null || plainText.startsWith(StringEncryptor.prefix))
			return plainText;

		Key key = null;
		if (keyValue == null)
			key = this.generateKey(this.keyValue);
		else
			key = this.generateKey(keyValue.getBytes());
		Cipher chiper = Cipher.getInstance(StringEncryptor.algorithm);
		chiper.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = chiper.doFinal(plainText.getBytes());
		String encryptedValue = Hex.encodeHexString(encVal);
		return StringEncryptor.prefix + encryptedValue;
	}

	public String decrypt(String encryptedText) throws Exception {
		return this.decrypt(encryptedText, null);
	}

	// Performs decryption
	public String decrypt(String encryptedText, String keyValue) throws Exception {
		if (encryptedText == null || !encryptedText.startsWith(StringEncryptor.prefix))
			return encryptedText;

		String encryptedTextWithoutPrefix = encryptedText.substring(StringEncryptor.prefix.length());

		// generate key
		Key key = null;
		if (keyValue == null)
			key = this.generateKey(this.keyValue);
		else
			key = this.generateKey(keyValue.getBytes());
		Cipher chiper = Cipher.getInstance(StringEncryptor.algorithm);
		chiper.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = Hex.decodeHex(encryptedTextWithoutPrefix.toCharArray());
		byte[] decValue = chiper.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	// generateKey() is used to generate a secret key for AES StringEncryptor.algorithm
	private Key generateKey(byte[] keyValue) throws Exception {
		Key key = new SecretKeySpec(keyValue, StringEncryptor.algorithm);
		return key;
	}

	public static void main(String[] args) {
		try {
			StringEncryptor stringEncryptor = new StringEncryptor();
			stringEncryptor.setKeyValue("Gt4T5h#e@L4i%n@k");
			System.out.println(stringEncryptor.decrypt(URLDecoder.decode("%24AES%24%40%23%403a785ecd64a740942490e66f1484c99047140eb73b90d894c225d9c76715a03b", "UTF-8")));
			System.out.println(URLEncoder.encode(stringEncryptor.encrypt("type=popupPromotion&id=55"), "UTF-8"));

			System.out.println(stringEncryptor.encrypt("vicky@vickypang.com"));
			System.out.println(stringEncryptor.encrypt("linktest820@yahoo.com.hk"));
			System.out.println(stringEncryptor.encrypt("linktest820@yahoo.com"));
			System.out.println(stringEncryptor.encrypt("andy.cheung@gtomato.com"));
			System.out.println(stringEncryptor.encrypt("andycheuggt@gmail.com"));
			System.out.println(stringEncryptor.encrypt("andy.cheung@gtomato.com"));
			System.out.println(stringEncryptor.encrypt("kenho42491@gmail.com"));
			System.out.println(stringEncryptor.encrypt("kenho042491@gmail.com"));
			System.out.println(stringEncryptor.encrypt("kyle.ly.yeung@linkreit.com"));
			System.out.println(stringEncryptor.encrypt("ida.mak@gtomato.com"));
			System.out.println(stringEncryptor.encrypt("ida0820@gmail.com"));

			System.out.println(stringEncryptor.decrypt(URLDecoder.decode("%24AES%24%40%23%405090b729f8dc3edafd336da6fe89d52b", "UTF-8"), "RzZ@1$K@o/5T#YQT"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
