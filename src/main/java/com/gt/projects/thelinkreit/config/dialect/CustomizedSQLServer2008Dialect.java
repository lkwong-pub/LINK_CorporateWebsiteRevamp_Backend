package com.gt.projects.thelinkreit.config.dialect;

import java.sql.Types;

import org.hibernate.dialect.SQLServer2008Dialect;

public class CustomizedSQLServer2008Dialect extends SQLServer2008Dialect {

	public CustomizedSQLServer2008Dialect() {
		super();
		// Use Unicode Characters
		registerColumnType(Types.VARCHAR, 255, "nvarchar($l)");
		registerColumnType(Types.CHAR, "nchar(1)");
		registerColumnType(Types.CLOB, "nvarchar(max)");
		registerColumnType(Types.CLOB, "ntext");

		registerColumnType(Types.BOOLEAN, "bit");
	}

}